object dm: Tdm
  OldCreateOrder = False
  Height = 1019
  Width = 1076
  object tblAdvokati: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_ADVOKATI'
      'SET '
      '    EMBG = :EMBG,'
      '    LICENCA = :LICENCA,'
      '    LICENCA_DATUM = :LICENCA_DATUM,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    ADVOKAT_NAZIV = :ADVOKAT_NAZIV,'
      '    STATUS = :STATUS,'
      '    AKTIVEN = :AKTIVEN,'
      '    ADVOKATSKO_DRUSTVO = :ADVOKATSKO_DRUSTVO,'
      '    ADVOKATSKA_KANCELARIJA = :ADVOKATSKA_KANCELARIJA,'
      '    MESTO = :MESTO,'
      '    ADVOKATSKA_ZAEDNICA = :ADVOKATSKA_ZAEDNICA,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    EMAIL = :EMAIL,'
      '    WEB_STRANA = :WEB_STRANA,'
      '    RESENIE_ID = :RESENIE_ID,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    EMBS = :EMBS,'
      '    IMPORT_ID = :IMPORT_ID,'
      '    FLAG_TOCEN = :FLAG_TOCEN,'
      '    MESTO_LK = :MESTO_LK,'
      '    WEB_STRANA_LK = :WEB_STRANA_LK,'
      '    EMAIL_LK = :EMAIL_LK,'
      '    TELEFON_LK = :TELEFON_LK,'
      '    MOBILEN_TELEFON_LK = :MOBILEN_TELEFON_LK'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_ADVOKATI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_ADVOKATI('
      '    ID,'
      '    EMBG,'
      '    LICENCA,'
      '    LICENCA_DATUM,'
      '    IME,'
      '    PREZIME,'
      '    ADVOKAT_NAZIV,'
      '    STATUS,'
      '    AKTIVEN,'
      '    ADVOKATSKO_DRUSTVO,'
      '    ADVOKATSKA_KANCELARIJA,'
      '    MESTO,'
      '    ADVOKATSKA_ZAEDNICA,'
      '    ADRESA,'
      '    TELEFON,'
      '    MOBILEN_TELEFON,'
      '    EMAIL,'
      '    WEB_STRANA,'
      '    RESENIE_ID,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    EMBS,'
      '    IMPORT_ID,'
      '    FLAG_TOCEN,'
      '    MESTO_LK,'
      '    WEB_STRANA_LK,'
      '    EMAIL_LK,'
      '    TELEFON_LK,'
      '    MOBILEN_TELEFON_LK'
      ')'
      'VALUES('
      '    :ID,'
      '    :EMBG,'
      '    :LICENCA,'
      '    :LICENCA_DATUM,'
      '    :IME,'
      '    :PREZIME,'
      '    :ADVOKAT_NAZIV,'
      '    :STATUS,'
      '    :AKTIVEN,'
      '    :ADVOKATSKO_DRUSTVO,'
      '    :ADVOKATSKA_KANCELARIJA,'
      '    :MESTO,'
      '    :ADVOKATSKA_ZAEDNICA,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :MOBILEN_TELEFON,'
      '    :EMAIL,'
      '    :WEB_STRANA,'
      '    :RESENIE_ID,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :EMBS,'
      '    :IMPORT_ID,'
      '    :FLAG_TOCEN,'
      '    :MESTO_LK,'
      '    :WEB_STRANA_LK,'
      '    :EMAIL_LK,'
      '    :TELEFON_LK,'
      '    :MOBILEN_TELEFON_LK'
      ')')
    RefreshSQL.Strings = (
      'select a.id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       z.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       a.resenie_id,'
      '       --coalesce(rp.tip_resenie_id,100)as tip_resenie_id,'
      '       a.custom1,'
      '       a.custom2,'
      '       a.custom3,'
      '       a.ts_ins,'
      '       a.ts_upd,'
      '       a.usr_ins,'
      '       a.usr_upd,'
      '       a.embs, a.import_id, a.flag_tocen,'
      '       a.mesto_lk,'
      '       mlk.naziv as mesto_naziv_lk,'
      
        '       a.web_strana_lk, a.email_lk, a.telefon_lk, a.mobilen_tele' +
        'fon_lk,'
      '       case when(select count(dm.id)'
      '                from adv_disciplinski_merki dm'
      '                where dm.advokat_id = a.id)>0 then 1'
      '            else 0'
      '       end br_disciplinski,'
      '       coalesce((select first 1 tp.status'
      '                from adv_resenija_advokati rp'
      
        '                inner join adv_tip_resenie tp on tp.id = rp.rese' +
        'nie_tip'
      
        '                where rp.advokat_id = :OLD_ID and (current_date ' +
        'between rp.datum_od and coalesce(rp.datum_do, current_date))orde' +
        'r by rp.datum_od desc ),100)tip_resenie_id'
      'from adv_advokati a'
      'left outer join mat_mesto m on m.id = a.mesto'
      'left outer join mat_mesto mlk on mlk.id = a.mesto_lk'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici z on z.id = a.advokatska_zaednica'
      ''
      ' WHERE '
      '        A.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select a.id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       z.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       a.resenie_id,'
      '       --coalesce(rp.tip_resenie_id,100)as tip_resenie_id,'
      '       a.custom1,'
      '       a.custom2,'
      '       a.custom3,'
      '       a.ts_ins,'
      '       a.ts_upd,'
      '       a.usr_ins,'
      '       a.usr_upd,'
      '       a.embs, a.import_id, a.flag_tocen,'
      '       a.mesto_lk,'
      '       mlk.naziv as mesto_naziv_lk,'
      
        '       a.web_strana_lk, a.email_lk, a.telefon_lk, a.mobilen_tele' +
        'fon_lk,'
      '       case when(select count(dm.id)'
      '                from adv_disciplinski_merki dm'
      '                where dm.advokat_id = a.id)>0 then 1'
      '            else 0'
      '       end br_disciplinski,'
      '       coalesce((select first 1 tp.status'
      '                from adv_resenija_advokati rp'
      
        '                inner join adv_tip_resenie tp on tp.id = rp.rese' +
        'nie_tip and tp.status >0'
      
        '                where rp.advokat_id = a.id and (current_date bet' +
        'ween rp.datum_od and coalesce(rp.datum_do, current_date))order b' +
        'y rp.datum_od desc ),100)tip_resenie_id'
      'from adv_advokati a'
      'left outer join mat_mesto m on m.id = a.mesto'
      'left outer join mat_mesto mlk on mlk.id = a.mesto_lk'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici z on z.id = a.advokatska_zaednica'
      '')
    AutoUpdateOptions.UpdateTableName = 'ADV_ADVOKATI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_ADVOKATI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 24
    object tblAdvokatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ID'
    end
    object tblAdvokatiLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblAdvokatiIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblAdvokatiMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblAdvokatiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblAdvokatiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiEMBG: TFIBStringField
      DisplayLabel = #1045#1052#1041#1043
      FieldName = 'EMBG'
      Size = 16
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatistatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblAdvokatiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblAdvokatiWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblAdvokatiADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblAdvokatiADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiRESENIE_ID: TFIBIntegerField
      DisplayLabel = #1056#1077#1096#1077#1085#1080#1077' '#1096#1080#1092#1088#1072
      FieldName = 'RESENIE_ID'
    end
    object tblAdvokatiADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblAdvokatiADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiTIP_RESENIE_ID: TFIBIntegerField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'TIP_RESENIE_ID'
    end
    object tblAdvokatiEMBS: TFIBIntegerField
      FieldName = 'EMBS'
    end
    object tblAdvokatiIMPORT_ID: TFIBIntegerField
      FieldName = 'IMPORT_ID'
    end
    object tblAdvokatiFLAG_TOCEN: TFIBSmallIntField
      DisplayLabel = #1042#1072#1083#1080#1076#1077#1085
      FieldName = 'FLAG_TOCEN'
    end
    object tblAdvokatiBR_DISCIPLINSKI: TFIBIntegerField
      DisplayLabel = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1080' '#1084#1077#1088#1082#1080
      FieldName = 'BR_DISCIPLINSKI'
    end
    object tblAdvokatiMESTO_LK: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO_LK'
    end
    object tblAdvokatiMESTO_NAZIV_LK: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTO_NAZIV_LK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiWEB_STRANA_LK: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072' ('#1083#1080#1095#1085#1072')'
      FieldName = 'WEB_STRANA_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiEMAIL_LK: TFIBStringField
      DisplayLabel = 'EMAIL ('#1083#1080#1095#1077#1085')'
      FieldName = 'EMAIL_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiTELEFON_LK: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' ('#1083#1080#1095#1077#1085')'
      FieldName = 'TELEFON_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatiMOBILEN_TELEFON_LK: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' ('#1083#1080#1095#1077#1085')'
      FieldName = 'MOBILEN_TELEFON_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsAdvokati: TDataSource
    DataSet = tblAdvokati
    Left = 168
    Top = 24
  end
  object tblStrucniSorabotnici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_STRUCNI_SORABOTNICI'
      'SET '
      '    DATUM = :DATUM,'
      '    BROJ = :BROJ,'
      '    EMBG = :EMBG,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    NAZIV = :NAZIV,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    EMAIL = :EMAIL,'
      '    STATUS = :STATUS,'
      '    ADVOKAT = :ADVOKAT,'
      '    ADVOKATSKO_DRUSTVO = :ADVOKATSKO_DRUSTVO,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_STRUCNI_SORABOTNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_STRUCNI_SORABOTNICI('
      '    ID,'
      '    DATUM,'
      '    BROJ,'
      '    EMBG,'
      '    IME,'
      '    PREZIME,'
      '    NAZIV,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    MOBILEN_TELEFON,'
      '    EMAIL,'
      '    STATUS,'
      '    ADVOKAT,'
      '    ADVOKATSKO_DRUSTVO,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATUM,'
      '    :BROJ,'
      '    :EMBG,'
      '    :IME,'
      '    :PREZIME,'
      '    :NAZIV,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :MOBILEN_TELEFON,'
      '    :EMAIL,'
      '    :STATUS,'
      '    :ADVOKAT,'
      '    :ADVOKATSKO_DRUSTVO,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.datum,'
      '       p.broj,'
      '       p.embg,'
      '       p.ime,'
      '       p.prezime,'
      '       p.naziv,'
      '       p.mesto,'
      '       m.naziv as mesto_naziv,'
      '       p.adresa,'
      '       p.telefon,'
      '       p.mobilen_telefon,'
      '       p.email,'
      '       p.status,'
      '       p.advokat,'
      '       a.advokat_naziv,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_advokat_naziv",'
      '       ak.naziv as adv_kancelarija_naziv,'
      '       mak.naziv as advokatska_kancelarija_mesto,'
      '       a.licenca,'
      '       p.advokatsko_drustvo,'
      '       ad.naziv as adv_drustvo_naziv,'
      '       mad.naziv  as advokatsko_drustvo_mesto,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from adv_strucni_sorabotnici p'
      'left outer join mat_mesto m on m.id = p.mesto'
      'left outer join adv_advokati a on a.id = p.advokat'
      
        'left outer join adv_advokatski_drustva ad on ad.id = p.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join mat_mesto mak on mak.id = ak.mesto'
      'left outer join mat_mesto mad on mad.id = ad.mesto'
      
        'where(  ((:status = 2) or (p.status = :status)) and  ((:advokat ' +
        '= -1)or(:advokat = p.advokat))'
      '     ) and (     P.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.datum,'
      '       p.broj,'
      '       p.embg,'
      '       p.ime,'
      '       p.prezime,'
      '       p.naziv,'
      '       p.mesto,'
      '       m.naziv as mesto_naziv,'
      '       p.adresa,'
      '       p.telefon,'
      '       p.mobilen_telefon,'
      '       p.email,'
      '       p.status,'
      '       p.advokat,'
      '       a.advokat_naziv,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_advokat_naziv",'
      '       ak.naziv as adv_kancelarija_naziv,'
      '       mak.naziv as advokatska_kancelarija_mesto,'
      '       a.licenca,'
      '       p.advokatsko_drustvo,'
      '       ad.naziv as adv_drustvo_naziv,'
      '       mad.naziv  as advokatsko_drustvo_mesto,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from adv_strucni_sorabotnici p'
      'left outer join mat_mesto m on m.id = p.mesto'
      'left outer join adv_advokati a on a.id = p.advokat'
      
        'left outer join adv_advokatski_drustva ad on ad.id = p.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join mat_mesto mak on mak.id = ak.mesto'
      'left outer join mat_mesto mad on mad.id = ad.mesto'
      
        'where ((:status = 2) or (p.status = :status)) and  ((:advokat = ' +
        '-1)or(:advokat = p.advokat))'
      '')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 88
    object tblStrucniSorabotniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblStrucniSorabotniciIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblStrucniSorabotniciMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'ADRESA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'. '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'MOBILEN_TELEFON'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblStrucniSorabotniciCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblStrucniSorabotniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblStrucniSorabotniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciNAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblStrucniSorabotniciADVOKAT: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKAT'
    end
    object tblStrucniSorabotniciADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotnicistatus_advokat_naziv: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090' - '#1089#1090#1072#1090#1091#1089' '#1074#1088#1072#1073#1086#1090#1077#1085
      FieldName = 'status_advokat_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciADV_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADV_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblStrucniSorabotniciADV_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADV_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciADVOKATSKA_KANCELARIJA_MESTO: TFIBStringField
      FieldName = 'ADVOKATSKA_KANCELARIJA_MESTO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblStrucniSorabotniciADVOKATSKO_DRUSTVO_MESTO: TFIBStringField
      FieldName = 'ADVOKATSKO_DRUSTVO_MESTO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsStrucniSorabotnici: TDataSource
    DataSet = tblStrucniSorabotnici
    Left = 168
    Top = 88
  end
  object dsPripravnici: TDataSource
    DataSet = tblPripravnici
    Left = 168
    Top = 152
  end
  object tblPripravnici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_PRIPRAVNICI'
      'SET '
      '    DATUM = :DATUM,'
      '    BROJ = :BROJ,'
      '    EMBG = :EMBG,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    NAZIV = :NAZIV,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    EMAIL = :EMAIL,'
      '    STATUS = :STATUS,'
      '    ADVOKAT = :ADVOKAT,'
      '    ADVOKATSKO_DRUSTVO = :ADVOKATSKO_DRUSTVO,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_PRIPRAVNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_PRIPRAVNICI('
      '    ID,'
      '    DATUM,'
      '    BROJ,'
      '    EMBG,'
      '    IME,'
      '    PREZIME,'
      '    NAZIV,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    MOBILEN_TELEFON,'
      '    EMAIL,'
      '    STATUS,'
      '    ADVOKAT,'
      '    ADVOKATSKO_DRUSTVO,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :DATUM,'
      '    :BROJ,'
      '    :EMBG,'
      '    :IME,'
      '    :PREZIME,'
      '    :NAZIV,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :MOBILEN_TELEFON,'
      '    :EMAIL,'
      '    :STATUS,'
      '    :ADVOKAT,'
      '    :ADVOKATSKO_DRUSTVO,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.datum,'
      '       p.broj,'
      '       p.embg,'
      '       p.ime,'
      '       p.prezime,'
      '       p.naziv,'
      '       p.mesto,'
      '       m.naziv as mesto_naziv,'
      '       p.adresa,'
      '       p.telefon,'
      '       p.mobilen_telefon,'
      '       p.email,'
      '       p.status,'
      '       p.advokat,'
      '       a.advokat_naziv,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_advokat_naziv",'
      '       ak.naziv as adv_kancelarija_naziv,'
      '       mak.naziv as advokatska_kancelarija_mesto,'
      '       a.licenca,'
      '       p.advokatsko_drustvo,'
      '       ad.naziv as adv_drustvo_naziv,'
      '       mad.naziv  as advokatsko_drustvo_mesto,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from adv_pripravnici p'
      'left outer join mat_mesto m on m.id = p.mesto'
      'left outer join adv_advokati a on a.id = p.advokat'
      
        'left outer join adv_advokatski_drustva ad on ad.id = p.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join mat_mesto mak on mak.id = ak.mesto'
      'left outer join mat_mesto mad on mad.id = ad.mesto'
      
        'where(  ((:status = 2) or (p.status = :status)) and  ((:advokat ' +
        '= -1)or(:advokat = p.advokat))'
      '     ) and (     P.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.datum,'
      '       p.broj,'
      '       p.embg,'
      '       p.ime,'
      '       p.prezime,'
      '       p.naziv,'
      '       p.mesto,'
      '       m.naziv as mesto_naziv,'
      '       p.adresa,'
      '       p.telefon,'
      '       p.mobilen_telefon,'
      '       p.email,'
      '       p.status,'
      '       p.advokat,'
      '       a.advokat_naziv,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_advokat_naziv",'
      '       ak.naziv as adv_kancelarija_naziv,'
      '       mak.naziv as advokatska_kancelarija_mesto,'
      '       a.licenca,'
      '       p.advokatsko_drustvo,'
      '       ad.naziv as adv_drustvo_naziv,'
      '       mad.naziv  as advokatsko_drustvo_mesto,'
      '       p.custom1,'
      '       p.custom2,'
      '       p.custom3,'
      '       p.ts_ins,'
      '       p.ts_upd,'
      '       p.usr_ins,'
      '       p.usr_upd'
      'from adv_pripravnici p'
      'left outer join mat_mesto m on m.id = p.mesto'
      'left outer join adv_advokati a on a.id = p.advokat'
      
        'left outer join adv_advokatski_drustva ad on ad.id = p.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join mat_mesto mak on mak.id = ak.mesto'
      'left outer join mat_mesto mad on mad.id = ad.mesto'
      
        'where ((:status = 2) or (p.status = :status)) and  ((:advokat = ' +
        '-1)or(:advokat = p.advokat))'
      '')
    AutoUpdateOptions.UpdateTableName = 'ADV_PRIPRAVNICI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_PRIPRAVNICI_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 152
    object tblPripravniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPripravniciDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblPripravniciIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciNAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblPripravniciMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'ADRESA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'. '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'MOBILEN_TELEFON'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblPripravniciADVOKAT: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKAT'
    end
    object tblPripravniciADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPripravniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblPripravniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravnicistatus_advokat_naziv: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090' - '#1089#1090#1072#1090#1091#1089' '#1074#1088#1072#1073#1086#1090#1077#1085
      FieldName = 'status_advokat_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciADV_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADV_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciADV_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADV_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciADVOKATSKO_DRUSTVO: TFIBSmallIntField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblPripravniciADVOKATSKA_KANCELARIJA_MESTO: TFIBStringField
      FieldName = 'ADVOKATSKA_KANCELARIJA_MESTO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPripravniciADVOKATSKO_DRUSTVO_MESTO: TFIBStringField
      FieldName = 'ADVOKATSKO_DRUSTVO_MESTO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblAdvokatskiDrustva: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_ADVOKATSKI_DRUSTVA'
      'SET '
      '    BROJ = :BROJ,'
      '    NAZIV = :NAZIV,'
      '    DATUM = :DATUM,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    EMAIL = :EMAIL,'
      '    WEB_STRANA = :WEB_STRANA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_ADVOKATSKI_DRUSTVA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_ADVOKATSKI_DRUSTVA('
      '    ID,'
      '    BROJ,'
      '    NAZIV,'
      '    DATUM,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    MOBILEN_TELEFON,'
      '    EMAIL,'
      '    WEB_STRANA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :NAZIV,'
      '    :DATUM,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :MOBILEN_TELEFON,'
      '    :EMAIL,'
      '    :WEB_STRANA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    AD.ID,'
      '    AD.BROJ,'
      '    AD.NAZIV,'
      '    AD.DATUM,'
      '    AD.MESTO,'
      '    M.NAZIV as MESTONAZIV,'
      '    AD.ADRESA,'
      '    AD.TELEFON,'
      '    AD.MOBILEN_TELEFON,'
      '    AD.EMAIL,'
      '    AD.WEB_STRANA,'
      '    AD.CUSTOM1,'
      '    AD.CUSTOM2,'
      '    AD.CUSTOM3,'
      '    AD.TS_INS,'
      '    AD.TS_UPD,'
      '    AD.USR_INS,'
      '    AD.USR_UPD'
      'from ADV_ADVOKATSKI_DRUSTVA AD'
      'left outer join MAT_MESTO M on M.ID = AD.MESTO'
      ' WHERE '
      '        AD.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    AD.ID,'
      '    AD.BROJ,'
      '    AD.NAZIV,'
      '    AD.DATUM,'
      '    AD.MESTO,'
      '    M.NAZIV as MESTONAZIV,'
      '    AD.ADRESA,'
      '    AD.TELEFON,'
      '    AD.MOBILEN_TELEFON,'
      '    AD.EMAIL,'
      '    AD.WEB_STRANA,'
      '    AD.CUSTOM1,'
      '    AD.CUSTOM2,'
      '    AD.CUSTOM3,'
      '    AD.TS_INS,'
      '    AD.TS_UPD,'
      '    AD.USR_INS,'
      '    AD.USR_UPD'
      'from ADV_ADVOKATSKI_DRUSTVA AD'
      'left outer join MAT_MESTO M on M.ID = AD.MESTO')
    AutoUpdateOptions.UpdateTableName = 'ADV_ADVOKATSKI_DRUSTVA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_ADVOKATSKI_DRUSTVA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 216
    object tblAdvokatskiDrustvaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblAdvokatskiDrustvaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblAdvokatskiDrustvaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblAdvokatskiDrustvaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblAdvokatskiDrustvaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblAdvokatskiDrustvaMESTONAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaEMAIL: TFIBStringField
      DisplayLabel = 'eMail'
      FieldName = 'EMAIL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073'. '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiDrustvaWEB_STRANA: TFIBStringField
      DisplayLabel = #1042#1077#1073' '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsAdvokatskiDrustva: TDataSource
    DataSet = tblAdvokatskiDrustva
    Left = 168
    Top = 216
  end
  object qSysSetup: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'Select s.v1, s.v2, s.vrednost'
      'from sys_setup s'
      'where s.p1 = '#39'ADV'#39' and s.p2 = :p2')
    Left = 536
    Top = 24
  end
  object tblAdvokatskiKancelarii: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_ADVOKATSKI_KANCELARII'
      'SET '
      '    BROJ = :BROJ,'
      '    NAZIV = :NAZIV,'
      '    DATUM = :DATUM,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    WEB_STRANA = :WEB_STRANA,'
      '    EMAIL = :EMAIL,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    DATUM_DO = :DATUM_DO,'
      '    DATUM_OD = :DATUM_OD,'
      '    ADV_RESENIE_ID = :ADV_RESENIE_ID,'
      '    ADVOKATSKA_ZAEDNICA = :ADVOKATSKA_ZAEDNICA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_ADVOKATSKI_KANCELARII'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_ADVOKATSKI_KANCELARII('
      '    ID,'
      '    BROJ,'
      '    NAZIV,'
      '    DATUM,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    MOBILEN_TELEFON,'
      '    WEB_STRANA,'
      '    EMAIL,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    DATUM_DO,'
      '    DATUM_OD,'
      '    ADV_RESENIE_ID,'
      '    ADVOKATSKA_ZAEDNICA'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :NAZIV,'
      '    :DATUM,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :MOBILEN_TELEFON,'
      '    :WEB_STRANA,'
      '    :EMAIL,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :DATUM_DO,'
      '    :DATUM_OD,'
      '    :ADV_RESENIE_ID,'
      '    :ADVOKATSKA_ZAEDNICA'
      ')')
    RefreshSQL.Strings = (
      'select  ak.id,'
      '        ak.broj,'
      '        ak.naziv,'
      '        ak.datum,'
      '        ak.mesto,'
      '        m.naziv as mestoNaziv,'
      '        ak.adresa,'
      '        ak.telefon,'
      '        ak.mobilen_telefon,'
      '        ak.web_strana,'
      '        ak.email,'
      '        ak.custom1,'
      '        ak.custom2,'
      '        ak.custom3,'
      '        ak.ts_ins,'
      '        ak.ts_upd,'
      '        ak.usr_ins,'
      '        ak.usr_upd,'
      '        ak.DATUM_DO,'
      '        ak.DATUM_OD,'
      '        ak.ADV_RESENIE_ID ,'
      '        ak.advokatska_zaednica'
      'from adv_advokatski_kancelarii ak'
      'left outer join mat_mesto m on m.id=ak.mesto'
      ''
      ' WHERE '
      '        AK.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select  ak.id,'
      '        ak.broj,'
      '        ak.naziv,'
      '        ak.datum,'
      '        ak.mesto,'
      '        m.naziv as mestoNaziv,'
      '        ak.adresa,'
      '        ak.telefon,'
      '        ak.mobilen_telefon,'
      '        ak.web_strana,'
      '        ak.email,'
      '        ak.custom1,'
      '        ak.custom2,'
      '        ak.custom3,'
      '        ak.ts_ins,'
      '        ak.ts_upd,'
      '        ak.usr_ins,'
      '        ak.usr_upd,'
      '        ak.DATUM_DO,'
      '        ak.DATUM_OD,'
      '        ak.ADV_RESENIE_ID ,'
      '        ak.advokatska_zaednica'
      'from adv_advokatski_kancelarii ak'
      'left outer join mat_mesto m on m.id=ak.mesto')
    AutoUpdateOptions.UpdateTableName = 'ADV_ADVOKATSKI_KANCELARII'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_KANCELARI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 288
    object tblAdvokatskiKancelariiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblAdvokatskiKancelariiBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblAdvokatskiKancelariiMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblAdvokatskiKancelariiMESTONAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTONAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiEMAIL: TFIBStringField
      DisplayLabel = 'eMail'
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblAdvokatskiKancelariiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblAdvokatskiKancelariiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1054#1076
      FieldName = 'DATUM_DO'
    end
    object tblAdvokatskiKancelariiDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1044#1086
      FieldName = 'DATUM_OD'
    end
    object tblAdvokatskiKancelariiADV_RESENIE_ID: TFIBIntegerField
      DisplayLabel = #1056#1077#1096#1077#1085#1080#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      FieldName = 'ADV_RESENIE_ID'
    end
    object tblAdvokatskiKancelariiMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085
      FieldName = 'MOBILEN_TELEFON'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiWEB_STRANA: TFIBStringField
      DisplayLabel = #1042#1077#1073' '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokatskiKancelariiADVOKATSKA_ZAEDNICA: TFIBIntegerField
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
  end
  object dsAdvokatskiKancelarii: TDataSource
    DataSet = tblAdvokatskiKancelarii
    Left = 168
    Top = 288
  end
  object tblAdvokati_Drustva: TpFIBDataSet
    SelectSQL.Strings = (
      'Select  a.embg,'
      '        a.aktiven,'
      '        a.licenca,'
      '        a.licenca_datum,'
      '        a.ime,'
      '        a.prezime,'
      '        a.advokat_naziv, '
      '        case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '        end "status_naziv",'
      '        a.advokatska_zaednica,'
      '        z.naziv as advokatska_zaednica_naziv,'
      '        a.mesto,'
      '        m.naziv as mesto_naziv,'
      '        a.adresa,'
      '        a.telefon,'
      '        a.mobilen_telefon,'
      '        a.web_strana,'
      '        a.email,'
      '        a.resenie_id'
      'from adv_advokati a'
      'left outer join adv_zaednici z on z.id = a.advokatska_zaednica'
      'left outer join mat_mesto m on m.id = a.mesto'
      'where a.advokatsko_drustvo = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsAdvokatskiDrustva
    Left = 48
    Top = 352
    object tblAdvokati_DrustvaEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblAdvokati_DrustvaLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblAdvokati_DrustvaADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblAdvokati_DrustvaADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblAdvokati_DrustvaMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_DrustvaEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_Drustvastatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsAdvokati_Drustva: TDataSource
    DataSet = tblAdvokati_Drustva
    Left = 168
    Top = 352
  end
  object tblAdvokati_Kancelarii: TpFIBDataSet
    SelectSQL.Strings = (
      'Select  a.embg,'
      '        a.aktiven,'
      '        a.licenca,'
      '        a.licenca_datum,'
      '        a.ime,'
      '        a.prezime,'
      '        a.advokat_naziv'
      'from adv_advokati a'
      'where a.advokatska_kancelarija = :mas_id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsAdvokatskiKancelarii
    Left = 48
    Top = 416
    object tblAdvokati_KancelariiEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_KancelariiAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblAdvokati_KancelariiLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAdvokati_KancelariiLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblAdvokati_KancelariiADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsAdvokati_Kancelarii: TDataSource
    DataSet = tblAdvokati_Kancelarii
    Left = 168
    Top = 416
  end
  object tblTipResenija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_TIP_RESENIE'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    PROMENA_PODATOCI = :PROMENA_PODATOCI,'
      '    STATUS = :STATUS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_TIP_RESENIE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_TIP_RESENIE('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    PROMENA_PODATOCI,'
      '    STATUS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :PROMENA_PODATOCI,'
      '    :STATUS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select  ar.id,'
      '        ar.naziv,'
      '        ar.opis,'
      '        ar.promena_podatoci,'
      '        ar.status,'
      '        ar.custom1,'
      '        ar.custom2,'
      '        ar.custom3,'
      '        ar.ts_ins,'
      '        ar.ts_upd,'
      '        ar.usr_ins,'
      '        ar.usr_upd'
      'from adv_tip_resenie ar'
      ''
      ' WHERE '
      '        AR.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select  ar.id,'
      '        ar.naziv,'
      '        ar.opis,'
      '        ar.promena_podatoci,'
      '        ar.status,'
      '        ar.custom1,'
      '        ar.custom2,'
      '        ar.custom3,'
      '        ar.ts_ins,'
      '        ar.ts_upd,'
      '        ar.usr_ins,'
      '        ar.usr_upd'
      'from adv_tip_resenie ar')
    AutoUpdateOptions.UpdateTableName = 'ADV_TIP_RESENIE'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_TIP_RESENIE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 312
    Top = 24
    object tblTipResenijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTipResenijaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaPROMENA_PODATOCI: TFIBSmallIntField
      DisplayLabel = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1087#1086#1076#1072#1090#1086#1094#1080
      FieldName = 'PROMENA_PODATOCI'
    end
    object tblTipResenijaSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATUS'
    end
    object tblTipResenijaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTipResenijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblTipResenijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTipResenija: TDataSource
    DataSet = tblTipResenija
    Left = 424
    Top = 24
  end
  object tblResenijaAdvokati: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_RESENIJA_ADVOKATI'
      'SET '
      '    BROJ_RESENIE = :BROJ_RESENIE,'
      '    RESENIE_TIP = :RESENIE_TIP,'
      '    ADVOKAT_ID = :ADVOKAT_ID,'
      '    EMBG = :EMBG,'
      '    IME = :IME,'
      '    PREZIME = :PREZIME,'
      '    NAZIV = :NAZIV,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    LICENCA_BROJ = :LICENCA_BROJ,'
      '    LICENCA_DATUM = :LICENCA_DATUM,'
      '    DATUM_SEDNICA = :DATUM_SEDNICA,'
      '    KOMISIJA_PRETSEDATEL = :KOMISIJA_PRETSEDATEL,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    EMAIL = :EMAIL,'
      '    WEB_STRANA = :WEB_STRANA,'
      '    MOBILEN = :MOBILEN,'
      '    TELEFON = :TELEFON,'
      '    STATUS = :STATUS,'
      '    ADV_DRUSTVO = :ADV_DRUSTVO,'
      '    ADV_KANCELARIJA = :ADV_KANCELARIJA,'
      '    OPIS = :OPIS,'
      '    MESTO_LK = :MESTO_LK,'
      '    WEB_STRANA_LK = :WEB_STRANA_LK,'
      '    EMAIL_LK = :EMAIL_LK,'
      '    TELEFON_LK = :TELEFON_LK,'
      '    MOBILEN_TELEFON_LK = :MOBILEN_TELEFON_LK,'
      '    KANCELARIJA_NAZIV = :KANCELARIJA_NAZIV,'
      '    ADV_ZAEDNICA = :ADV_ZAEDNICA,'
      '    BARATEL_NAZIV = :BARATEL_NAZIV,'
      '    BARATEL_MESTO = :BARATEL_MESTO,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_RESENIJA_ADVOKATI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_RESENIJA_ADVOKATI('
      '    ID,'
      '    BROJ_RESENIE,'
      '    RESENIE_TIP,'
      '    ADVOKAT_ID,'
      '    EMBG,'
      '    IME,'
      '    PREZIME,'
      '    NAZIV,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    LICENCA_BROJ,'
      '    LICENCA_DATUM,'
      '    DATUM_SEDNICA,'
      '    KOMISIJA_PRETSEDATEL,'
      '    MESTO,'
      '    ADRESA,'
      '    EMAIL,'
      '    WEB_STRANA,'
      '    MOBILEN,'
      '    TELEFON,'
      '    STATUS,'
      '    ADV_DRUSTVO,'
      '    ADV_KANCELARIJA,'
      '    OPIS,'
      '    MESTO_LK,'
      '    WEB_STRANA_LK,'
      '    EMAIL_LK,'
      '    TELEFON_LK,'
      '    MOBILEN_TELEFON_LK,'
      '    KANCELARIJA_NAZIV,'
      '    ADV_ZAEDNICA,'
      '    BARATEL_NAZIV,'
      '    BARATEL_MESTO,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ_RESENIE,'
      '    :RESENIE_TIP,'
      '    :ADVOKAT_ID,'
      '    :EMBG,'
      '    :IME,'
      '    :PREZIME,'
      '    :NAZIV,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :LICENCA_BROJ,'
      '    :LICENCA_DATUM,'
      '    :DATUM_SEDNICA,'
      '    :KOMISIJA_PRETSEDATEL,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :EMAIL,'
      '    :WEB_STRANA,'
      '    :MOBILEN,'
      '    :TELEFON,'
      '    :STATUS,'
      '    :ADV_DRUSTVO,'
      '    :ADV_KANCELARIJA,'
      '    :OPIS,'
      '    :MESTO_LK,'
      '    :WEB_STRANA_LK,'
      '    :EMAIL_LK,'
      '    :TELEFON_LK,'
      '    :MOBILEN_TELEFON_LK,'
      '    :KANCELARIJA_NAZIV,'
      '    :ADV_ZAEDNICA,'
      '    :BARATEL_NAZIV,'
      '    :BARATEL_MESTO,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select ar.id,'
      '       ar.broj_resenie,'
      '       ar.resenie_tip,'
      '       tr.naziv as tip_resenie_naziv,'
      '       tr.opis as tip_resenie_opis,'
      '       ar.advokat_id,'
      '       ar.embg,'
      '       ar.ime,'
      '       ar.prezime,'
      '       ar.naziv,'
      '       ar.datum_od,'
      '       ar.datum_do,'
      '       ar.licenca_broj,'
      '       ar.licenca_datum,'
      '       ar.datum_sednica,'
      '       ar.komisija_pretsedatel,'
      '       ar.mesto,'
      '       m.naziv as mesto_naziv,'
      '       ar.adresa,'
      '       ar.email,'
      '       ar.web_strana,'
      '       ar.mobilen,'
      '       ar.telefon,'
      '       ar.status,'
      '       case when ar.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when ar.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when ar.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096 +
        #1090#1074#1086#39
      
        '            when ar.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094 +
        #1077#1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       ar.adv_drustvo,'
      '       ak.naziv as adv_kancelarija_naziv,'
      '       ad.naziv as adv_drustvo_naziv,'
      '       ar.adv_kancelarija,'
      '       ar.opis,'
      '       case when(select count(dm.id)'
      '                 from adv_disciplinski_merki dm'
      '                 where dm.advokat_id = ar.advokat_id)>0 then 1'
      '            else 0'
      '       end br_disciplinski,'
      '       ar.mesto_lk,'
      '       mlk.naziv as mesto_naziv_lk,'
      '       ar.web_strana_lk,'
      '       ar.email_lk,'
      '       ar.telefon_lk,'
      '       ar.mobilen_telefon_lk,'
      '       ar.kancelarija_naziv,'
      '       ar.adv_zaednica,'
      '       az.naziv as adv_zaednica_naziv,'
      '       ar.baratel_naziv,'
      '       ar.baratel_mesto,'
      '       ar.ts_ins,'
      '       ar.ts_upd,'
      '       ar.usr_ins,'
      '       ar.usr_upd,'
      '       ar.custom1,'
      '       ar.custom2,'
      '       ar.custom3'
      'from adv_resenija_advokati ar'
      'inner join adv_tip_resenie tr on tr.id = ar.resenie_tip'
      'left outer join mat_mesto m on m.id = ar.mesto'
      'left outer join mat_mesto mlk on mlk.id = ar.mesto_lk'
      
        'left outer join adv_advokatski_drustva ad on ad.id = ar.adv_drus' +
        'tvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = ar.adv_k' +
        'ancelarija'
      'left outer join adv_zaednici az on az.id = ar.adv_zaednica'
      'where(  ((ar.advokat_id = :advokat_id) or (-1 = :advokat_id))'
      '     ) and (     AR.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select ar.id,'
      '       ar.broj_resenie,'
      '       ar.resenie_tip,'
      '       tr.naziv as tip_resenie_naziv,'
      '       tr.opis as tip_resenie_opis,'
      '       ar.advokat_id,'
      '       ar.embg,'
      '       ar.ime,'
      '       ar.prezime,'
      '       ar.naziv,'
      '       ar.datum_od,'
      '       ar.datum_do,'
      '       ar.licenca_broj,'
      '       ar.licenca_datum,'
      '       ar.datum_sednica,'
      '       ar.komisija_pretsedatel,'
      '       ar.mesto,'
      '       m.naziv as mesto_naziv,'
      '       ar.adresa,'
      '       ar.email,'
      '       ar.web_strana,'
      '       ar.mobilen,'
      '       ar.telefon,'
      '       ar.status,'
      '       case when ar.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when ar.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when ar.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096 +
        #1090#1074#1086#39
      
        '            when ar.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094 +
        #1077#1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       ar.adv_drustvo,'
      '       ak.naziv as adv_kancelarija_naziv,'
      '       ad.naziv as adv_drustvo_naziv,'
      '       ar.adv_kancelarija,'
      '       ar.opis,'
      '       case when(select count(dm.id)'
      '                 from adv_disciplinski_merki dm'
      '                 where dm.advokat_id = ar.advokat_id)>0 then 1'
      '            else 0'
      '       end br_disciplinski,'
      '       ar.mesto_lk,'
      '       mlk.naziv as mesto_naziv_lk,'
      '       ar.web_strana_lk,'
      '       ar.email_lk,'
      '       ar.telefon_lk,'
      '       ar.mobilen_telefon_lk,'
      '       ar.kancelarija_naziv,'
      '       ar.adv_zaednica,'
      '       az.naziv as adv_zaednica_naziv,'
      '       ar.baratel_naziv,'
      '       ar.baratel_mesto,'
      '       ar.ts_ins,'
      '       ar.ts_upd,'
      '       ar.usr_ins,'
      '       ar.usr_upd,'
      '       ar.custom1,'
      '       ar.custom2,'
      '       ar.custom3'
      'from adv_resenija_advokati ar'
      'inner join adv_tip_resenie tr on tr.id = ar.resenie_tip'
      'left outer join mat_mesto m on m.id = ar.mesto'
      'left outer join mat_mesto mlk on mlk.id = ar.mesto_lk'
      
        'left outer join adv_advokatski_drustva ad on ad.id = ar.adv_drus' +
        'tvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = ar.adv_k' +
        'ancelarija'
      'left outer join adv_zaednici az on az.id = ar.adv_zaednica'
      'where ((ar.advokat_id = :advokat_id) or (-1 = :advokat_id))'
      'order by ar.datum_od')
    AutoUpdateOptions.UpdateTableName = 'ADV_RESENIJA_ADVOKATI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_RESENIJA_ADVOKATI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 312
    Top = 80
    object tblResenijaAdvokatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblResenijaAdvokatiBROJ_RESENIE: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      FieldName = 'BROJ_RESENIE'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiRESENIE_TIP: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'RESENIE_TIP'
    end
    object tblResenijaAdvokatiTIP_RESENIE_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
      FieldName = 'TIP_RESENIE_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiTIP_RESENIE_OPIS: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' - '#1086#1087#1080#1089
      FieldName = 'TIP_RESENIE_OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKAT_ID'
    end
    object tblResenijaAdvokatiEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 17
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiNAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'NAZIV'
      Size = 120
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1086#1076' -'
      FieldName = 'DATUM_OD'
    end
    object tblResenijaAdvokatiDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' - '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblResenijaAdvokatiLICENCA_BROJ: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblResenijaAdvokatiDATUM_SEDNICA: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1077#1076#1085#1080#1094#1072
      FieldName = 'DATUM_SEDNICA'
    end
    object tblResenijaAdvokatiKOMISIJA_PRETSEDATEL: TFIBStringField
      DisplayLabel = #1055#1088#1077#1090#1089#1077#1076#1072#1090#1077#1083' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072
      FieldName = 'KOMISIJA_PRETSEDATEL'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblResenijaAdvokatiMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiMOBILEN: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'. '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'MOBILEN'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblResenijaAdvokatiADV_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADV_DRUSTVO'
    end
    object tblResenijaAdvokatiADV_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADV_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiADV_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADV_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiADV_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADV_KANCELARIJA'
    end
    object tblResenijaAdvokatiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblResenijaAdvokatiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblResenijaAdvokatiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatistatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 8
    end
    object tblResenijaAdvokatiBR_DISCIPLINSKI: TFIBIntegerField
      DisplayLabel = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1080' '#1084#1077#1088#1082#1080
      FieldName = 'BR_DISCIPLINSKI'
    end
    object tblResenijaAdvokatiMESTO_LK: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO_LK'
    end
    object tblResenijaAdvokatiMESTO_NAZIV_LK: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
      FieldName = 'MESTO_NAZIV_LK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiWEB_STRANA_LK: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072' ('#1083#1080#1095#1085#1072')'
      FieldName = 'WEB_STRANA_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiEMAIL_LK: TFIBStringField
      DisplayLabel = 'EMAIL ('#1083#1080#1095#1077#1085')'
      FieldName = 'EMAIL_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiTELEFON_LK: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' ('#1083#1080#1095#1077#1085')'
      FieldName = 'TELEFON_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiMOBILEN_TELEFON_LK: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' ('#1083#1080#1095#1077#1085')'
      FieldName = 'MOBILEN_TELEFON_LK'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiKANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1076#1074' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'KANCELARIJA_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiADV_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADV_ZAEDNICA'
    end
    object tblResenijaAdvokatiADV_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADV_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiBARATEL_NAZIV: TFIBStringField
      FieldName = 'BARATEL_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaAdvokatiBARATEL_MESTO: TFIBStringField
      FieldName = 'BARATEL_MESTO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsResenijaAdvokati: TDataSource
    DataSet = tblResenijaAdvokati
    Left = 424
    Top = 80
  end
  object qTipResenie: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  tr.promena_podatoci, tr.status'
      'from adv_tip_resenie  tr'
      'where tr.id = :tip_resenie_id')
    Left = 536
    Top = 80
  end
  object qResenieDatum: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 coalesce(ra.datum_od, current_date) datum_od'
      'from adv_resenija_advokati ra'
      'inner join adv_tip_resenie tr on tr.id = ra.resenie_tip'
      'where ra.advokat_id = :advokat_id and tr.promena_podatoci = 1'
      'order by 1 desc')
    Left = 536
    Top = 152
  end
  object tblZaednici: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_ZAEDNICI'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_ZAEDNICI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_ZAEDNICI('
      '    ID,'
      '    NAZIV,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select z.id,'
      '       z.naziv,'
      '       z.custom1,'
      '       z.custom2,'
      '       z.custom3,'
      '       z.ts_ins,'
      '       z.ts_upd,'
      '       z.usr_ins,'
      '       z.usr_upd'
      'from adv_zaednici z'
      ''
      ' WHERE '
      '        Z.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select z.id,'
      '       z.naziv,'
      '       z.custom1,'
      '       z.custom2,'
      '       z.custom3,'
      '       z.ts_ins,'
      '       z.ts_upd,'
      '       z.usr_ins,'
      '       z.usr_upd'
      'from adv_zaednici z')
    AutoUpdateOptions.UpdateTableName = 'ADV_ZAEDNICI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_ZAEDNICI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 488
    object tblZaedniciID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblZaedniciNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZaedniciCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZaedniciCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZaedniciCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZaedniciTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblZaedniciTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblZaedniciUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZaedniciUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsZaednici: TDataSource
    DataSet = tblZaednici
    Left = 168
    Top = 488
  end
  object qADpodatoci: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  ad.mesto, ad.adresa, ad.telefon, ad.email'
      'from adv_advokatski_drustva ad'
      'where ad.id = :id')
    Left = 536
    Top = 216
  end
  object tblClenarinaIznos: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_CLENARINA_IZNOS'
      'SET '
      '    ID = :ID,'
      '    GODINA = :GODINA,'
      '    IZNOS = :IZNOS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_CLENARINA_IZNOS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_CLENARINA_IZNOS('
      '    ID,'
      '    GODINA,'
      '    IZNOS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :IZNOS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select c.id,'
      '       c.godina,'
      '       c.iznos,'
      '       c.custom1,'
      '       c.custom2,'
      '       c.custom3,'
      '       c.ts_ins,'
      '       c.ts_upd,'
      '       c.usr_ins,'
      '       c.usr_upd'
      'from adv_clenarina_iznos c'
      ' WHERE '
      '        C.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select c.id,'
      '       c.godina,'
      '       c.iznos,'
      '       c.custom1,'
      '       c.custom2,'
      '       c.custom3,'
      '       c.ts_ins,'
      '       c.ts_upd,'
      '       c.usr_ins,'
      '       c.usr_upd'
      'from adv_clenarina_iznos c')
    AutoUpdateOptions.UpdateTableName = 'ADV_CLENARINA_IZNOS'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_CLENARINA_IZNOS_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 560
    object tblClenarinaIznosID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblClenarinaIznosGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblClenarinaIznosCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblClenarinaIznosCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblClenarinaIznosCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblClenarinaIznosTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblClenarinaIznosTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblClenarinaIznosUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblClenarinaIznosUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblClenarinaIznosIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
  end
  object dsClenarinaIznos: TDataSource
    DataSet = tblClenarinaIznos
    Left = 168
    Top = 560
  end
  object tblNezadolzeniAdvokati: TpFIBDataSet
    SelectSQL.Strings = (
      'select a.id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       z.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       a.resenie_id,'
      '       a.mesto_lk,'
      '       mlk.naziv as mesto_naziv_lk,'
      '       coalesce((select first 1 tp.status'
      '                from adv_resenija_advokati rp'
      
        '                inner join adv_tip_resenie tp on tp.id = rp.rese' +
        'nie_tip and tp.status >0'
      
        '                where rp.advokat_id = a.id and (current_date bet' +
        'ween rp.datum_od and coalesce(rp.datum_do, current_date))order b' +
        'y rp.datum_od desc ),100)tip_resenie_id'
      'from adv_advokati a'
      
        '--left outer join adv_resenija_period rp on rp.advokat_id = a.id' +
        ' and (current_date between rp.datum_od and coalesce(rp.datum_do,' +
        ' current_date))'
      'left outer join mat_mesto m on m.id = a.mesto'
      'left outer join mat_mesto mlk on mlk.id = a.mesto_lk'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici z on z.id = a.advokatska_zaednica'
      
        'left outer join adv_zadolzuvanje zd on zd.advokat_id = a.id and ' +
        'zd.godina = :godina'
      'where  zd.advokat_id is null'
      
        'group by  1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,' +
        '22,23,24,25,26--,27,28,29,30,31,32,33,34'
      'having  coalesce((select first 1 tp.status'
      '                from adv_resenija_advokati rp'
      
        '                inner join adv_tip_resenie tp on tp.id = rp.rese' +
        'nie_tip and tp.status >0'
      
        '                where rp.advokat_id = a.id and (current_date bet' +
        'ween rp.datum_od and coalesce(rp.datum_do, current_date))order b' +
        'y rp.datum_od desc ),100) <> 100')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 312
    Top = 152
    object tblNezadolzeniAdvokatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblNezadolzeniAdvokatiEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 16
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblNezadolzeniAdvokatiIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblNezadolzeniAdvokatiAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblNezadolzeniAdvokatistatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblNezadolzeniAdvokatiADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblNezadolzeniAdvokatiADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblNezadolzeniAdvokatiMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblNezadolzeniAdvokatiADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNezadolzeniAdvokatiTIP_RESENIE_ID: TFIBIntegerField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'TIP_RESENIE_ID'
    end
    object tblNezadolzeniAdvokatiRESENIE_ID: TFIBIntegerField
      DisplayLabel = #1056#1077#1096#1077#1085#1080#1077' - '#1096#1080#1092#1088#1072
      FieldName = 'RESENIE_ID'
    end
    object tblNezadolzeniAdvokatiMESTO_LK: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' ('#1086#1076' '#1051#1050')'
      FieldName = 'MESTO_LK'
    end
    object tblNezadolzeniAdvokatiMESTO_NAZIV_LK: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' ('#1086#1076' '#1051#1050')'
      FieldName = 'MESTO_NAZIV_LK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNezadolzeniAdvokati: TDataSource
    DataSet = tblNezadolzeniAdvokati
    Left = 424
    Top = 152
  end
  object tblZadolzeniAdvokati: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_ZADOLZUVANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select z.id,'
      '       z.advokat_id,'
      '       z.advokat_embg,'
      '       z.godina,'
      '       z.datum,'
      '       z.iznos as zadolzen_iznos,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '        coalesce(rp.tip_resenie_id,100)as tip_resenie_id,'
      '       ((select coalesce( sum(zz.iznos),0)'
      
        '         from adv_zadolzuvanje zz where zz.godina = :godina and ' +
        'zz.advokat_id = a.id)-'
      '        (select coalesce( sum(u.iznos),0)'
      
        '         from adv_uplati u where u.godina = :godina and u.advoka' +
        't_id = a.id)) as dolg'
      'from adv_zadolzuvanje z'
      'inner join adv_advokati a on a.id = z.advokat_id'
      
        'left outer join adv_resenija_period rp on rp.advokat_id = a.id a' +
        'nd (current_date between rp.datum_od and coalesce(rp.datum_do, c' +
        'urrent_date))'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where(  z.godina = :godina'
      '     ) and (     Z.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select z.id,'
      '       z.advokat_id,'
      '       z.advokat_embg,'
      '       z.godina,'
      '       z.datum,'
      '       z.iznos as zadolzen_iznos,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       -- coalesce(rp.tip_resenie_id,100)as tip_resenie_id,'
      '       ((select coalesce( sum(zz.iznos),0)'
      
        '         from adv_zadolzuvanje zz where zz.godina = :godina and ' +
        'zz.advokat_id = a.id)-'
      '        (select coalesce( sum(u.iznos),0)'
      
        '         from adv_uplati u where u.godina = :godina and u.advoka' +
        't_id = a.id)) as dolg,'
      '        coalesce((select first 1 tp.status'
      '                from adv_resenija_advokati rp'
      
        '                inner join adv_tip_resenie tp on tp.id = rp.rese' +
        'nie_tip and tp.status >0'
      
        '                where rp.advokat_id = a.id and (current_date bet' +
        'ween rp.datum_od and coalesce(rp.datum_do, current_date))order b' +
        'y rp.datum_od desc ),100)tip_resenie_id'
      'from adv_zadolzuvanje z'
      'inner join adv_advokati a on a.id = z.advokat_id'
      
        '--left outer join adv_resenija_period rp on rp.advokat_id = a.id' +
        ' and (current_date between rp.datum_od and coalesce(rp.datum_do,' +
        ' current_date))'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where z.godina = :godina')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 312
    Top = 216
    object tblZadolzeniAdvokatiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblZadolzeniAdvokatiADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ADVOKAT_ID'
    end
    object tblZadolzeniAdvokatiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblZadolzeniAdvokatiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblZadolzeniAdvokatiEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblZadolzeniAdvokatiADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblZadolzeniAdvokatiAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblZadolzeniAdvokatistatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblZadolzeniAdvokatiADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblZadolzeniAdvokatiADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblZadolzeniAdvokatiMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblZadolzeniAdvokatiADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblZadolzeniAdvokatiZADOLZEN_IZNOS: TFIBBCDField
      DisplayLabel = #1047#1072#1076#1086#1083#1078#1077#1085' '#1080#1079#1085#1086#1089
      FieldName = 'ZADOLZEN_IZNOS'
      Size = 2
    end
    object tblZadolzeniAdvokatiDOLG: TFIBBCDField
      DisplayLabel = #1044#1086#1083#1075
      FieldName = 'DOLG'
      Size = 2
    end
    object tblZadolzeniAdvokatiTIP_RESENIE_ID: TFIBIntegerField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'TIP_RESENIE_ID'
    end
  end
  object dsZadolzeniAdvokati: TDataSource
    DataSet = tblZadolzeniAdvokati
    Left = 424
    Top = 216
  end
  object qUpdateAdvokatFLAG: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update adv_advokati a'
      'set a.flag = :flag'
      'where a.id LIKE :id')
    Left = 728
    Top = 88
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object TransakcijaP: TpFIBTransaction
    DefaultDatabase = dmKon.fibBaza
    Left = 728
    Top = 24
  end
  object pZadolzuvanje: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_ADV_ZADOLZUVANJE_MINUS (?GODINA, ?DATUM, ' +
        '?ADVOKATI_ID_IN)')
    StoredProcName = 'PROC_ADV_ZADOLZUVANJE_MINUS'
    Left = 728
    Top = 160
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblUplata: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_UPLATI'
      'SET '
      '    GODINA = :GODINA,'
      '    DATUM = :DATUM,'
      '    IZNOS = :IZNOS,'
      '    ADVOKAT_ID = :ADVOKAT_ID,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_UPLATI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_UPLATI('
      '    ID,'
      '    GODINA,'
      '    DATUM,'
      '    IZNOS,'
      '    ADVOKAT_ID,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :DATUM,'
      '    :IZNOS,'
      '    :ADVOKAT_ID,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select u.id,'
      '       -1  as zadolzuvanje_id,'
      '       a.embg as advokat_embg,'
      '       u.godina,'
      '       u.datum,'
      '       u.iznos,'
      '       u.advokat_id,'
      '       u.custom1,'
      '       u.custom2,'
      '       u.custom3,'
      '       u.ts_ins,'
      '       u.ts_upd,'
      '       u.usr_ins,'
      '       u.usr_upd,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '      -- coalesce(rp.tip_resenie_id,100)as tip_resenie_id,'
      '       coalesce((select first 1 rp.tip_resenie_id'
      '                from adv_resenija_period rp'
      
        '                where rp.advokat_id = a.id and (current_date bet' +
        'ween rp.datum_od and coalesce(rp.datum_do, current_date))order b' +
        'y rp.id desc ),100)tip_resenie_id'
      'from adv_uplati u'
      '--inner join adv_zadolzuvanje z on z.id=u.zadolzuvanje_id'
      'inner join adv_advokati a on a.id = u.advokat_id'
      
        '--left outer join adv_resenija_period rp on rp.advokat_id = a.id' +
        ' and (current_date between rp.datum_od and coalesce(rp.datum_do,' +
        ' current_date))'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      
        'where(  ((u.godina = :godina) or(:godina = - 100)) and ((u.advok' +
        'at_id = :advokat_id)or(-100 = :advokat_id))'
      '     ) and (     U.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select u.id,'
      '       -1  as zadolzuvanje_id,'
      '       a.embg as advokat_embg,'
      '       u.godina,'
      '       u.datum,'
      '       u.iznos,'
      '       u.advokat_id,'
      '       u.custom1,'
      '       u.custom2,'
      '       u.custom3,'
      '       u.ts_ins,'
      '       u.ts_upd,'
      '       u.usr_ins,'
      '       u.usr_upd,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '      -- coalesce(rp.tip_resenie_id,100)as tip_resenie_id,'
      '       coalesce((select first 1 rp.tip_resenie_id'
      '                from adv_resenija_period rp'
      
        '                where rp.advokat_id = a.id and (current_date bet' +
        'ween rp.datum_od and coalesce(rp.datum_do, current_date))order b' +
        'y rp.id desc ),100)tip_resenie_id'
      'from adv_uplati u'
      '--inner join adv_zadolzuvanje z on z.id=u.zadolzuvanje_id'
      'inner join adv_advokati a on a.id = u.advokat_id'
      
        '--left outer join adv_resenija_period rp on rp.advokat_id = a.id' +
        ' and (current_date between rp.datum_od and coalesce(rp.datum_do,' +
        ' current_date))'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      
        'where ((u.godina = :godina) or(:godina = - 100)) and ((u.advokat' +
        '_id = :advokat_id)or(-100 = :advokat_id))')
    AutoUpdateOptions.UpdateTableName = 'ADV_UPLATI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_UPLATI_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 624
    object tblUplataID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblUplataZADOLZUVANJE_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
      FieldName = 'ZADOLZUVANJE_ID'
    end
    object tblUplataADVOKAT_EMBG: TFIBStringField
      DisplayLabel = 'EMBG'
      FieldName = 'ADVOKAT_EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ADVOKAT_ID'
    end
    object tblUplataGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblUplataDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblUplataIZNOS: TFIBBCDField
      DisplayLabel = #1059#1087#1083#1072#1090#1077#1085' '#1080#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblUplataCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblUplataTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblUplataUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblUplataIME: TFIBStringField
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataPREZIME: TFIBStringField
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblUplataAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblUplatastatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblUplataADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblUplataADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblUplataMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblUplataADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplataTIP_RESENIE_ID: TFIBIntegerField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'TIP_RESENIE_ID'
    end
  end
  object dsUplata: TDataSource
    DataSet = tblUplata
    Left = 168
    Top = 624
  end
  object qCountUplatiPoZadolzuvanje: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(u.id) br'
      'from adv_uplati u'
      'where u.zadolzuvanje_id = :zadolzuvanje_id')
    Left = 536
    Top = 288
  end
  object pPROC_ADV_INSERTUPLATA_FROMEXCEL: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_ADV_INSERTUPLATA_FROMEXCEL (?IZNOS, ?GODI' +
        'NA, ?LICENCA, ?DATUM)')
    StoredProcName = 'PROC_ADV_INSERTUPLATA_FROMEXCEL'
    Left = 728
    Top = 288
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblUplatiOdExcel: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_UPLATI_EXCEL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select au.id,'
      '       au.godina,'
      '       au.datum,'
      '       a.licenca,'
      '       au.iznos,'
      '       a.id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       z.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       a.resenie_id,'
      '       au.ts_ins,'
      '       au.ts_upd,'
      '       au.usr_ins,'
      '       au.usr_upd'
      'from adv_uplati au'
      'left outer join adv_advokati a on a.id = au.advokat_id'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici z on z.id = a.advokatska_zaednica')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 288
    object tblUplatiOdExcelID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblUplatiOdExcelGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblUplatiOdExcelDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblUplatiOdExcelLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblUplatiOdExcelIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblUplatiOdExcelAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblUplatiOdExcelstatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblUplatiOdExcelADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblUplatiOdExcelADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblUplatiOdExcelMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblUplatiOdExcelADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelRESENIE_ID: TFIBIntegerField
      DisplayLabel = #1056#1077#1096#1077#1085#1080#1077' '#1096#1080#1092#1088#1072
      FieldName = 'RESENIE_ID'
    end
    object tblUplatiOdExcelTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblUplatiOdExcelTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblUplatiOdExcelUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUplatiOdExcelIZNOS: TFIBFloatField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
    end
  end
  object dsUplatiOdExcel: TDataSource
    DataSet = tblUplatiOdExcel
    Left = 424
    Top = 288
  end
  object pPROC_ADV_AKTIVNO_RESENIE: TpFIBStoredProc
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_ADV_AKTIVNO_RESENIE (?ADVOKAT_ID, ?DATUM_' +
        'OD_IN, ?DATUM_DO_IN, ?TIP_RESENIE_IN, ?RESENIE_ID_IN)')
    StoredProcName = 'PROC_ADV_AKTIVNO_RESENIE'
    Left = 536
    Top = 352
  end
  object pPROC_ADV_GRUPNO_BRISENJE: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_ADV_GRUPNO_BRISENJE (?SIFRA_OD, ?SIFRA_DO' +
        ', ?UPLATA_ZADOLZ, ?DATUM, ?TIP_BRISENJE)')
    StoredProcName = 'PROC_ADV_GRUPNO_BRISENJE'
    Left = 728
    Top = 352
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pPROC_ADV_INSERT_UPLATA: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_ADV_INSERTUPLATA (?LICENCA, ?IZNOS_ZA_UPL' +
        'ATA, ?DATUM, ?GODINA)')
    StoredProcName = 'PROC_ADV_INSERTUPLATA'
    Left = 728
    Top = 232
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblTipDisciplinskiMerki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_TIP_DISCIPLINSKI_MERKI'
      'SET '
      '    NAZIV = :NAZIV,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_TIP_DISCIPLINSKI_MERKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_TIP_DISCIPLINSKI_MERKI('
      '    ID,'
      '    NAZIV,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select a.id,'
      '       a.naziv,'
      '       a.custom1,'
      '       a.custom2,'
      '       a.custom3,'
      '       a.ts_ins,'
      '       a.ts_upd,'
      '       a.usr_ins,'
      '       a.usr_upd'
      'from adv_tip_disciplinski_merki a'
      ''
      ' WHERE '
      '        A.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select a.id,'
      '       a.naziv,'
      '       a.custom1,'
      '       a.custom2,'
      '       a.custom3,'
      '       a.ts_ins,'
      '       a.ts_upd,'
      '       a.usr_ins,'
      '       a.usr_upd'
      'from adv_tip_disciplinski_merki a')
    AutoUpdateOptions.UpdateTableName = 'ADV_TIP_DISCIPLINSKI_MERKI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_TIP_DM_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 352
    object tblTipDisciplinskiMerkiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblTipDisciplinskiMerkiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipDisciplinskiMerkiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipDisciplinskiMerkiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipDisciplinskiMerkiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipDisciplinskiMerkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblTipDisciplinskiMerkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblTipDisciplinskiMerkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipDisciplinskiMerkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTipDisciplinskiMerki: TDataSource
    DataSet = tblTipDisciplinskiMerki
    Left = 424
    Top = 352
  end
  object tblDisciplinskiMerki: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_DISCIPLINSKI_MERKI'
      'SET '
      '    TIP = :TIP,'
      '    OPIS = :OPIS,'
      '    DATUM = :DATUM,'
      '    DATUM_DO = :DATUM_DO,'
      '    ADVOKAT_ID = :ADVOKAT_ID,'
      '    IZNOS = :IZNOS,'
      '    GODINA = :GODINA,'
      '    DO_BR = :DO_BR,'
      '    DS_BR = :DS_BR,'
      '    EV_BR = :EV_BR,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_DISCIPLINSKI_MERKI'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_DISCIPLINSKI_MERKI('
      '    ID,'
      '    TIP,'
      '    OPIS,'
      '    DATUM,'
      '    DATUM_DO,'
      '    ADVOKAT_ID,'
      '    IZNOS,'
      '    GODINA,'
      '    DO_BR,'
      '    DS_BR,'
      '    EV_BR,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :TIP,'
      '    :OPIS,'
      '    :DATUM,'
      '    :DATUM_DO,'
      '    :ADVOKAT_ID,'
      '    :IZNOS,'
      '    :GODINA,'
      '    :DO_BR,'
      '    :DS_BR,'
      '    :EV_BR,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select dm.id,'
      '       dm.tip,'
      '       tdm.naziv as tip_naziv,'
      '       dm.opis,'
      '       dm.datum,'
      '       dm.datum_do,'
      '       dm.advokat_id,'
      '       dm.iznos,'
      '       dm.godina,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.DO_BR,'
      '       dm.DS_BR,'
      '       dm.EV_BR,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_disciplinski_merki dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      'inner join adv_tip_disciplinski_merki tdm on tdm.id = dm.tip'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where(  ((dm.advokat_id = :advokat_id) or (:advokat_id = -100))'
      '     ) and (     DM.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select dm.id,'
      '       dm.tip,'
      '       tdm.naziv as tip_naziv,'
      '       dm.opis,'
      '       dm.datum,'
      '       dm.datum_do,'
      '       dm.advokat_id,'
      '       dm.iznos,'
      '       dm.godina,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.DO_BR,'
      '       dm.DS_BR,'
      '       dm.EV_BR,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_disciplinski_merki dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      'inner join adv_tip_disciplinski_merki tdm on tdm.id = dm.tip'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where ((dm.advokat_id = :advokat_id) or (:advokat_id = -100))')
    AutoUpdateOptions.UpdateTableName = 'ADV_DISCIPLINSKI_MERKI'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_DISCIPLINSKI_M_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 416
    object tblDisciplinskiMerkiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblDisciplinskiMerkiTIP: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1072' '#1084#1077#1088#1082#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'TIP'
    end
    object tblDisciplinskiMerkiTIP_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072' '#1076#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1072' '#1084#1077#1088#1082#1072
      FieldName = 'TIP_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiOPIS: TFIBBlobField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 8
    end
    object tblDisciplinskiMerkiDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1086#1076
      FieldName = 'DATUM'
    end
    object tblDisciplinskiMerkiGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblDisciplinskiMerkiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblDisciplinskiMerkiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblDisciplinskiMerkiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ADVOKAT_ID'
    end
    object tblDisciplinskiMerkiIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblDisciplinskiMerkiEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 16
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblDisciplinskiMerkiIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblDisciplinskiMerkiAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblDisciplinskiMerkistatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblDisciplinskiMerkiADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblDisciplinskiMerkiADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblDisciplinskiMerkiMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblDisciplinskiMerkiADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblDisciplinskiMerkiWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiDO_BR: TFIBStringField
      DisplayLabel = #1044#1086'.'
      FieldName = 'DO_BR'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiDS_BR: TFIBStringField
      DisplayLabel = #1044#1057'. '#1073#1088'.'
      FieldName = 'DS_BR'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDisciplinskiMerkiEV_BR: TFIBStringField
      DisplayLabel = #1045#1042'. '#1073#1088','
      FieldName = 'EV_BR'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDisciplinskiMerki: TDataSource
    DataSet = tblDisciplinskiMerki
    Left = 424
    Top = 416
  end
  object tblSetUp: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SYS_SETUP'
      'SET '
      '    PARAMETAR = :PARAMETAR,'
      '    PARAM_VREDNOST = :PARAM_VREDNOST,'
      '    VREDNOST = :VREDNOST,'
      '    P1 = :P1,'
      '    P2 = :P2,'
      '    V1 = :V1,'
      '    V2 = :V2'
      'WHERE P1 = '#39'ADV'#39' and P2=:p2 and VREDNOST = 1'
      '    ')
    SelectSQL.Strings = (
      'select s.parametar,'
      '       s.param_vrednost,'
      '       s.vrednost,'
      '       s.p1,'
      '       s.p2,'
      '       s.v1,'
      '       s.v2'
      'from sys_setup s'
      'where s.p1 = '#39'ADV'#39' and s.vrednost = 1')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 472
    object tblSetUpPARAMETAR: TFIBStringField
      FieldName = 'PARAMETAR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpPARAM_VREDNOST: TFIBStringField
      FieldName = 'PARAM_VREDNOST'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpVREDNOST: TFIBIntegerField
      FieldName = 'VREDNOST'
    end
    object tblSetUpP1: TFIBStringField
      FieldName = 'P1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpP2: TFIBStringField
      FieldName = 'P2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpV1: TFIBStringField
      DisplayLabel = #1055#1072#1088#1072#1084#1077#1090#1072#1088
      FieldName = 'V1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSetUpV2: TFIBStringField
      DisplayLabel = #1042#1088#1077#1076#1085#1086#1089#1090
      FieldName = 'V2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSetUp: TDataSource
    DataSet = tblSetUp
    Left = 416
    Top = 472
  end
  object tblPocetnaSostojba: TpFIBDataSet
    SelectSQL.Strings = (
      'select dm.id,'
      '       dm.opis,'
      '       dm.advokat_id,'
      '       dm.iznos,'
      '       dm.godina,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_pocetna_sostojba dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      '')
    AutoUpdateOptions.UpdateTableName = 'ADV_POCETNA_SOSTOJBA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_POCETNA_SOSTOJBA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 528
    object tblPocetnaSostojbaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPocetnaSostojbaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ADVOKAT_ID'
    end
    object tblPocetnaSostojbaIZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 2
    end
    object tblPocetnaSostojbaGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPocetnaSostojbaEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 13
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblPocetnaSostojbaIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblPocetnaSostojbaAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblPocetnaSostojbastatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblPocetnaSostojbaADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblPocetnaSostojbaADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblPocetnaSostojbaMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblPocetnaSostojbaADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblPocetnaSostojbaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblPocetnaSostojbaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPocetnaSostojbaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPocetnaSostojba: TDataSource
    DataSet = tblPocetnaSostojba
    Left = 416
    Top = 528
  end
  object tblNeuspesniUplati: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_UPLATI_NEUSPESNI'
      ''
      '        ')
    SelectSQL.Strings = (
      'select s.godina, s.datum, s.licenca, s.iznos'
      'from adv_uplati_neuspesni s')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 40
    Top = 688
    object tblNeuspesniUplatiGODINA: TFIBStringField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNeuspesniUplatiDATUM: TFIBStringField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNeuspesniUplatiLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNeuspesniUplatiIZNOS: TFIBStringField
      DisplayLabel = #1048#1079#1085#1086#1089
      FieldName = 'IZNOS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNeuspesniUplati: TDataSource
    DataSet = tblNeuspesniUplati
    Left = 168
    Top = 688
  end
  object qUpdateAdvokatAK: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE ADV_ADVOKATI'
      'SET '
      '   '
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    EMAIL = :EMAIL'
      'WHERE'
      '    ADVOKATSKA_KANCELARIJA = :ADVOKATSKA_KANCELARIJA'
      '    ')
    Left = 728
    Top = 416
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateAdvokatAD: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE ADV_ADVOKATI'
      'SET '
      '   '
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    EMAIL = :EMAIL'
      'WHERE'
      '    ADVOKATSKO_DRUSTVO= :ADVOKATSKO_DRUSTVO'
      '    ')
    Left = 728
    Top = 472
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qAKpodatoci: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select  ad.mesto, ad.adresa, ad.telefon, ad.email, ad.advokatska' +
        '_zaednica, ad.mobilen_telefon, ad.web_strana'
      'from adv_advokatski_kancelarii ad'
      'where ad.id = :id')
    Left = 608
    Top = 216
  end
  object tblDokumentiPolinja: TpFIBDataSet
    SelectSQL.Strings = (
      'select adp.id,'
      '       adp.pole_id,'
      '       adp.pole_naziv,'
      '       adp.pole_opis,'
      '       adp.report_br'
      'from adv_dokumenti_polinja adp'
      'where adp.report_br = :report_br')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 296
    Top = 624
    object tblDokumentiPolinjaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblDokumentiPolinjaPOLE_ID: TFIBStringField
      FieldName = 'POLE_ID'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaPOLE_NAZIV: TFIBStringField
      FieldName = 'POLE_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaPOLE_OPIS: TFIBStringField
      FieldName = 'POLE_OPIS'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaREPORT_BR: TFIBIntegerField
      FieldName = 'REPORT_BR'
    end
  end
  object dsDokumentiPolinja: TDataSource
    DataSet = tblDokumentiPolinja
    Left = 448
    Top = 624
  end
  object tblDokumentiPolinjaVrednost: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_DOK_POLINJA_VREDNOST'
      'SET '
      '    ADVOKAT_ID = :ADVOKAT_ID,'
      '    POLE_ID = :POLE_ID,'
      '    VREDNOST = :VREDNOST,'
      '    RESENIJA_ID = :RESENIJA_ID,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_DOK_POLINJA_VREDNOST'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_DOK_POLINJA_VREDNOST('
      '    ID,'
      '    ADVOKAT_ID,'
      '    POLE_ID,'
      '    VREDNOST,'
      '    RESENIJA_ID,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :ADVOKAT_ID,'
      '    :POLE_ID,'
      '    :VREDNOST,'
      '    :RESENIJA_ID,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select dpv.id,'
      '       dpv.advokat_id,'
      '       dpv.pole_id,'
      '       dpv.vrednost,'
      '       dpv.resenija_id,'
      '       dpv.ts_ins,'
      '       dpv.ts_upd,'
      '       dpv.usr_ins,'
      '       dpv.usr_upd,'
      '       dpv.custom1,'
      '       dpv.custom2,'
      '       dpv.custom3'
      'from adv_dok_polinja_vrednost dpv'
      
        'where(  dpv.advokat_id = :advokat_id and dpv.pole_id = :pole_id ' +
        'and dpv.resenija_id = :resenie_id'
      '     ) and (     DPV.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select dpv.id,'
      '       dpv.advokat_id,'
      '       dpv.pole_id,'
      '       dpv.vrednost,'
      '       dpv.resenija_id,'
      '       dpv.ts_ins,'
      '       dpv.ts_upd,'
      '       dpv.usr_ins,'
      '       dpv.usr_upd,'
      '       dpv.custom1,'
      '       dpv.custom2,'
      '       dpv.custom3'
      'from adv_dok_polinja_vrednost dpv'
      
        'where dpv.advokat_id = :advokat_id and dpv.pole_id = :pole_id an' +
        'd dpv.resenija_id = :resenie_id')
    AutoUpdateOptions.UpdateTableName = 'ADV_DOK_POLINJA_VREDNOST'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_DOK_POLINJA_VREDNOST_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 296
    Top = 680
    object tblDokumentiPolinjaVrednostID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblDokumentiPolinjaVrednostADVOKAT_ID: TFIBIntegerField
      FieldName = 'ADVOKAT_ID'
    end
    object tblDokumentiPolinjaVrednostVREDNOST: TFIBStringField
      FieldName = 'VREDNOST'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaVrednostTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblDokumentiPolinjaVrednostTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblDokumentiPolinjaVrednostUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaVrednostUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaVrednostCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaVrednostCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaVrednostCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentiPolinjaVrednostRESENIJA_ID: TFIBIntegerField
      FieldName = 'RESENIJA_ID'
    end
    object tblDokumentiPolinjaVrednostPOLE_ID: TFIBStringField
      FieldName = 'POLE_ID'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsDokumentiPolinjaVrednost: TDataSource
    DataSet = tblDokumentiPolinjaVrednost
    Left = 448
    Top = 680
  end
  object tblPretsedatelKomisija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_PRETSEDATEL_KOMISIJA'
      'SET '
      '    ID = :ID,'
      '    NAZIV = :NAZIV,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_PRETSEDATEL_KOMISIJA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_PRETSEDATEL_KOMISIJA('
      '    ID,'
      '    NAZIV,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select p.id,'
      '       p.naziv,'
      '       p.datum_od,'
      '       p.datum_do,'
      
        '       p.custom1,p.custom2, p.custom3, p.ts_ins, p.ts_upd, p.usr' +
        '_ins, p.usr_upd'
      'from adv_pretsedatel_komisija p'
      ''
      ' WHERE '
      '        P.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select p.id,'
      '       p.naziv,'
      '       p.datum_od,'
      '       p.datum_do,'
      
        '       p.custom1,p.custom2, p.custom3, p.ts_ins, p.ts_upd, p.usr' +
        '_ins, p.usr_upd'
      'from adv_pretsedatel_komisija p')
    AutoUpdateOptions.UpdateTableName = 'ADV_PRETSEDATEL_KOMISIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_PRETSEDATEL_KOMISIJA_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 32
    Top = 760
    object tblPretsedatelKomisijaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPretsedatelKomisijaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPretsedatelKomisijaDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1086#1076
      FieldName = 'DATUM_OD'
    end
    object tblPretsedatelKomisijaDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblPretsedatelKomisijaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPretsedatelKomisijaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPretsedatelKomisijaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPretsedatelKomisijaTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPretsedatelKomisijaTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblPretsedatelKomisijaUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPretsedatelKomisijaUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPretsedatelKomisija: TDataSource
    DataSet = tblPretsedatelKomisija
    Left = 160
    Top = 760
  end
  object qPretsedatel: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      '  select pk.naziv'
      '  from adv_pretsedatel_komisija pk'
      
        '  where :datum_sednica between pk.datum_od and coalesce(pk.datum' +
        '_do, current_date)')
    Left = 736
    Top = 544
  end
  object qProveriLicenca: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 a.advokat_naziv,'
      
        '       (select count(a.id) from adv_advokati a where a.licenca =' +
        ' :licenca) as br,'
      
        '       (select count(a.id) from adv_advokati a where a.licenca =' +
        ' :licenca and a.id <> :adv_id) as br_edit'
      'from adv_advokati a'
      'where a.licenca = :licenca')
    Left = 736
    Top = 616
  end
  object qProveriLicencaAK: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select first 1 a.naziv,'
      
        '       (select count(a.id) from adv_advokatski_kancelarii a wher' +
        'e a.broj = :licenca) as br'
      'from adv_advokatski_kancelarii a'
      'where a.broj = :licenca')
    Left = 736
    Top = 672
  end
  object qNajdiAK: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select first 1 a.id, a.mesto, a.adresa, a.telefon, a.email, a.ad' +
        'vokatska_zaednica, a.mobilen_telefon, a.web_strana,'
      
        '       (select count(a.id) from adv_advokatski_kancelarii a wher' +
        'e/*$$IBEC$$  a.naziv = :ime_prezime and $$IBEC$$*/ a.broj = :lic' +
        'enca) as br'
      'from adv_advokatski_kancelarii a'
      'where a.naziv = :ime_prezime and a.broj = :licenca')
    Left = 736
    Top = 744
  end
  object qInsertAK: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO ADV_ADVOKATSKI_KANCELARII('
      '    BROJ,'
      '    NAZIV,'
      '    DATUM,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    MESTO,'
      '    ADRESA,  '
      '    TELEFON,'
      '    mobilen_telefon,'
      '    EMAIL,'
      '    WEB_STRANA)'
      'VALUES('
      '    :BROJ,'
      '    :NAZIV,'
      '    :DATUM,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :mobilen_telefon,'
      '    :EMAIL,'
      '    :WEB_STRANA'
      ')')
    Left = 728
    Top = 808
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qNajdiAK_poID: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'select first 1 a.id, a.mesto, a.adresa, a.telefon, a.email, a.ad' +
        'vokatska_zaednica, a.mobilen_telefon, a.web_strana,'
      
        '       (select count(a.id) from adv_advokatski_kancelarii a wher' +
        'e a.id = :id) as br'
      'from adv_advokatski_kancelarii a'
      'where a.id = :id')
    Left = 816
    Top = 744
  end
  object qEditAK: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE ADV_ADVOKATSKI_KANCELARII'
      'SET '
      '    NAZIV = :NAZIV,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    EMAIL = :EMAIL,'
      '    ADVOKATSKA_ZAEDNICA = :ADVOKATSKA_ZAEDNICA,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    WEB_STRANA = :WEB_STRANA,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    ADV_RESENIE_ID = :ADV_RESENIE_ID'
      'WHERE'
      '    ID = :ID'
      '    ')
    Left = 816
    Top = 808
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qDeleteAK_PoID: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    ADV_ADVOKATSKI_KANCELARII'
      'WHERE'
      '        ID = :AK_ID')
    Left = 896
    Top = 808
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblResenijaAD: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_RESENIJA_AD'
      'SET '
      '    RESENIE_TIP = :RESENIE_TIP,'
      '    AD_ID = :AD_ID,'
      '    BROJ_RESENIE = :BROJ_RESENIE,'
      '    DATUM_SEDNICA = :DATUM_SEDNICA,'
      '    KOMISIJA_PRETSEDATEL = :KOMISIJA_PRETSEDATEL,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    NAZIV = :NAZIV,'
      '    DATUM = :DATUM,'
      '    MESTO = :MESTO,'
      '    ADRESA = :ADRESA,'
      '    TELEFON = :TELEFON,'
      '    MOBILEN_TELEFON = :MOBILEN_TELEFON,'
      '    EMAIL = :EMAIL,'
      '    WEB_STRANA = :WEB_STRANA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_RESENIJA_AD'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_RESENIJA_AD('
      '    ID,'
      '    RESENIE_TIP,'
      '    AD_ID,'
      '    BROJ_RESENIE,'
      '    DATUM_SEDNICA,'
      '    KOMISIJA_PRETSEDATEL,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    NAZIV,'
      '    DATUM,'
      '    MESTO,'
      '    ADRESA,'
      '    TELEFON,'
      '    MOBILEN_TELEFON,'
      '    EMAIL,'
      '    WEB_STRANA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :RESENIE_TIP,'
      '    :AD_ID,'
      '    :BROJ_RESENIE,'
      '    :DATUM_SEDNICA,'
      '    :KOMISIJA_PRETSEDATEL,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :NAZIV,'
      '    :DATUM,'
      '    :MESTO,'
      '    :ADRESA,'
      '    :TELEFON,'
      '    :MOBILEN_TELEFON,'
      '    :EMAIL,'
      '    :WEB_STRANA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    D.ID,'
      '    D.RESENIE_TIP,'
      '    R.NAZIV RESENIE_TIP_NAZIV,'
      '    D.AD_ID,'
      '    D.BROJ_RESENIE,'
      '    D.DATUM_SEDNICA,'
      '    D.KOMISIJA_PRETSEDATEL,'
      '    D.DATUM_OD,'
      '    D.DATUM_DO,'
      '    D.NAZIV,'
      '    AD.BROJ REG_BROJ,'
      '    D.DATUM,'
      '    D.MESTO,'
      '    D.ADRESA,'
      '    D.TELEFON,'
      '    D.MOBILEN_TELEFON,'
      '    D.EMAIL,'
      '    D.WEB_STRANA,'
      '    D.CUSTOM1,'
      '    D.CUSTOM2,'
      '    D.CUSTOM3,'
      '    D.TS_INS,'
      '    D.TS_UPD,'
      '    D.USR_INS,'
      '    D.USR_UPD'
      'from ADV_RESENIJA_AD D'
      'join ADV_ADVOKATSKI_DRUSTVA AD on D.AD_ID = AD.ID'
      'join ADV_TIP_RESENIE_AD R on D.RESENIE_TIP = R.ID'
      'join MAT_MESTO M on D.MESTO = M.ID'
      'where(  ((D.AD_ID = :AD_ID) or (-1 = :AD_ID))'
      '     ) and (     D.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select'
      '    D.ID,'
      '    D.RESENIE_TIP,'
      '    R.NAZIV RESENIE_TIP_NAZIV,'
      '    D.AD_ID,'
      '    D.BROJ_RESENIE,'
      '    D.DATUM_SEDNICA,'
      '    D.KOMISIJA_PRETSEDATEL,'
      '    D.DATUM_OD,'
      '    D.DATUM_DO,'
      '    D.NAZIV,'
      '    AD.BROJ REG_BROJ,'
      '    D.DATUM,'
      '    D.MESTO,'
      '    D.ADRESA,'
      '    D.TELEFON,'
      '    D.MOBILEN_TELEFON,'
      '    D.EMAIL,'
      '    D.WEB_STRANA,'
      '    D.CUSTOM1,'
      '    D.CUSTOM2,'
      '    D.CUSTOM3,'
      '    D.TS_INS,'
      '    D.TS_UPD,'
      '    D.USR_INS,'
      '    D.USR_UPD'
      'from ADV_RESENIJA_AD D'
      'join ADV_ADVOKATSKI_DRUSTVA AD on D.AD_ID = AD.ID'
      'join ADV_TIP_RESENIE_AD R on D.RESENIE_TIP = R.ID'
      'join MAT_MESTO M on D.MESTO = M.ID'
      'where ((D.AD_ID = :AD_ID) or (-1 = :AD_ID))   ')
    AutoUpdateOptions.UpdateTableName = 'ADV_RESENIJA_AD'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_RESENIJA_AD_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 872
    Top = 24
    object tblResenijaADID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblResenijaADRESENIE_TIP: TFIBSmallIntField
      FieldName = 'RESENIE_TIP'
    end
    object tblResenijaADRESENIE_TIP_NAZIV: TFIBStringField
      FieldName = 'RESENIE_TIP_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADAD_ID: TFIBIntegerField
      FieldName = 'AD_ID'
    end
    object tblResenijaADBROJ_RESENIE: TFIBStringField
      FieldName = 'BROJ_RESENIE'
      Size = 25
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADDATUM_SEDNICA: TFIBDateField
      FieldName = 'DATUM_SEDNICA'
    end
    object tblResenijaADKOMISIJA_PRETSEDATEL: TFIBStringField
      FieldName = 'KOMISIJA_PRETSEDATEL'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADDATUM_OD: TFIBDateField
      FieldName = 'DATUM_OD'
    end
    object tblResenijaADDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblResenijaADNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADREG_BROJ: TFIBStringField
      FieldName = 'REG_BROJ'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object tblResenijaADMESTO: TFIBIntegerField
      FieldName = 'MESTO'
    end
    object tblResenijaADADRESA: TFIBStringField
      FieldName = 'ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADTELEFON: TFIBStringField
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADMOBILEN_TELEFON: TFIBStringField
      FieldName = 'MOBILEN_TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADEMAIL: TFIBStringField
      FieldName = 'EMAIL'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADWEB_STRANA: TFIBStringField
      FieldName = 'WEB_STRANA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblResenijaADTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblResenijaADUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblResenijaADUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsResenijaAD: TDataSource
    DataSet = tblResenijaAD
    Left = 984
    Top = 24
  end
  object tblTipResenijaAD: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_TIP_RESENIE_AD'
      'SET '
      '    NAZIV = :NAZIV,'
      '    OPIS = :OPIS,'
      '    PROMENA_PODATOCI = :PROMENA_PODATOCI,'
      '    STATUS = :STATUS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_TIP_RESENIE_AD'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_TIP_RESENIE_AD('
      '    ID,'
      '    NAZIV,'
      '    OPIS,'
      '    PROMENA_PODATOCI,'
      '    STATUS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :OPIS,'
      '    :PROMENA_PODATOCI,'
      '    :STATUS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    TR.ID,'
      '    TR.NAZIV,'
      '    TR.OPIS,'
      '    TR.PROMENA_PODATOCI,'
      '    TR.STATUS,'
      '    TR.CUSTOM1,'
      '    TR.CUSTOM2,'
      '    TR.CUSTOM3,'
      '    TR.TS_INS,'
      '    TR.TS_UPD,'
      '    TR.USR_INS,'
      '    TR.USR_UPD'
      'from ADV_TIP_RESENIE_AD TR  '
      ' WHERE '
      '        TR.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    TR.ID,'
      '    TR.NAZIV,'
      '    TR.OPIS,'
      '    TR.PROMENA_PODATOCI,'
      '    TR.STATUS,'
      '    TR.CUSTOM1,'
      '    TR.CUSTOM2,'
      '    TR.CUSTOM3,'
      '    TR.TS_INS,'
      '    TR.TS_UPD,'
      '    TR.USR_INS,'
      '    TR.USR_UPD'
      'from ADV_TIP_RESENIE_AD TR  ')
    AutoUpdateOptions.UpdateTableName = 'ADV_TIP_RESENIE_AD'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_TIP_RESENIE_AD_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 872
    Top = 88
    object tblTipResenijaADID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblTipResenijaADNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaADOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaADPROMENA_PODATOCI: TFIBSmallIntField
      FieldName = 'PROMENA_PODATOCI'
    end
    object tblTipResenijaADSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
    object tblTipResenijaADCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaADCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaADCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaADTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblTipResenijaADTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblTipResenijaADUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTipResenijaADUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTipResenijaAD: TDataSource
    DataSet = tblTipResenijaAD
    Left = 984
    Top = 88
  end
  object tblZaednicaLista: TpFIBDataSet
    SelectSQL.Strings = (
      ' select z.id,'
      '       z.naziv,'
      '       z.custom1,'
      '       z.custom2,'
      '       z.custom3,'
      '       z.ts_ins,'
      '       z.ts_upd,'
      '       z.usr_ins,'
      '       z.usr_upd'
      'from adv_zaednici z')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 40
    Top = 832
    object tblZaednicaListaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblZaednicaListaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsZaednicaLista: TDataSource
    DataSet = tblZaednicaLista
    Left = 152
    Top = 832
  end
  object tblPorodilno: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_PORODILNO'
      'SET '
      '    OPIS = :OPIS,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    ADVOKAT_ID = :ADVOKAT_ID,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_PORODILNO'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_PORODILNO('
      '    ID,'
      '    OPIS,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    ADVOKAT_ID,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OPIS,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :ADVOKAT_ID,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select dm.id,'
      '       dm.opis,'
      '       dm.datum_od,'
      '       dm.datum_do,'
      '       dm.advokat_id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_porodilno dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where(  ((dm.advokat_id = :advokat_id) or (:advokat_id = -100))'
      '     ) and (     DM.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select dm.id,'
      '       dm.opis,'
      '       dm.datum_od,'
      '       dm.datum_do,'
      '       dm.advokat_id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_porodilno dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where ((dm.advokat_id = :advokat_id) or (:advokat_id = -100))')
    AutoUpdateOptions.UpdateTableName = 'ADV_PORODILNO'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_PORODILNO_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 760
    object tblPorodilnoID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblPorodilnoOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1054#1076
      FieldName = 'DATUM_OD'
    end
    object tblPorodilnoDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1044#1086
      FieldName = 'DATUM_DO'
    end
    object tblPorodilnoADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ADVOKAT_ID'
    end
    object tblPorodilnoEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 16
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblPorodilnoIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoPREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblPorodilnoAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblPorodilnostatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblPorodilnoADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblPorodilnoADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblPorodilnoMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblPorodilnoADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblPorodilnoTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblPorodilnoUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorodilnoUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPorodilno: TDataSource
    DataSet = tblPorodilno
    Left = 432
    Top = 760
  end
  object tblOsiguritelniKompanii: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_OSIGURITELNI_KOMP'
      'SET '
      '    NAZIV = :NAZIV,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_OSIGURITELNI_KOMP'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_OSIGURITELNI_KOMP('
      '    ID,'
      '    NAZIV,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      '   select o.id,'
      '       o.naziv,'
      '       o.ts_ins,'
      '       o.ts_upd,'
      '       o.usr_ins,'
      '       o.usr_upd,'
      '       o.custom1,'
      '       o.custom2,'
      '       o.custom3'
      'from adv_osiguritelni_komp o'
      ''
      ' WHERE '
      '        O.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      '   select o.id,'
      '       o.naziv,'
      '       o.ts_ins,'
      '       o.ts_upd,'
      '       o.usr_ins,'
      '       o.usr_upd,'
      '       o.custom1,'
      '       o.custom2,'
      '       o.custom3'
      'from adv_osiguritelni_komp o')
    AutoUpdateOptions.UpdateTableName = 'ADV_OSIGURITELNI_KOMP'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoReWriteSqls = True
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_OSIGURITELNI_KOMP_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 832
    object tblOsiguritelniKompaniiID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOsiguritelniKompaniiTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblOsiguritelniKompaniiTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblOsiguritelniKompaniiUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguritelniKompaniiUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguritelniKompaniiCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguritelniKompaniiCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguritelniKompaniiCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguritelniKompaniiNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsOsiguritelniKompanii: TDataSource
    DataSet = tblOsiguritelniKompanii
    Left = 432
    Top = 832
  end
  object PROC_ADV_OSLOBODUVANJE: TpFIBStoredProc
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_ADV_PORODILNO (?DATUM_OD, ?DATUM_DO, ?ADV' +
        'OKAT_ID, ?DATUM)')
    StoredProcName = 'PROC_ADV_PORODILNO'
    Left = 728
    Top = 872
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblOsiguruvanje: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ADV_OSIGURUVANJE'
      'SET '
      '    OSIGURITELNA_KOMP_ID = :OSIGURITELNA_KOMP_ID,'
      '    POLISA = :POLISA,'
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    ADVOKAT_ID = :ADVOKAT_ID,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ADV_OSIGURUVANJE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ADV_OSIGURUVANJE('
      '    ID,'
      '    OSIGURITELNA_KOMP_ID,'
      '    POLISA,'
      '    DATUM_OD,'
      '    DATUM_DO,'
      '    ADVOKAT_ID,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OSIGURITELNA_KOMP_ID,'
      '    :POLISA,'
      '    :DATUM_OD,'
      '    :DATUM_DO,'
      '    :ADVOKAT_ID,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select dm.id,'
      '       dm.osiguritelna_komp_id,'
      '       ok.naziv as  osiguritelna_komp_naziv,'
      '       dm.polisa,'
      '       dm.datum_od,'
      '       dm.datum_do,'
      '       dm.advokat_id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_osiguruvanje dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      
        'inner join adv_osiguritelni_komp ok on ok.id = dm.osiguritelna_k' +
        'omp_id'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where(  ((dm.advokat_id = :advokat_id) or (:advokat_id = -100))'
      '     ) and (     DM.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select dm.id,'
      '       dm.osiguritelna_komp_id,'
      '       ok.naziv as  osiguritelna_komp_naziv,'
      '       dm.polisa,'
      '       dm.datum_od,'
      '       dm.datum_do,'
      '       dm.advokat_id,'
      '       a.embg,'
      '       a.licenca,'
      '       a.licenca_datum,'
      '       a.ime,'
      '       a.prezime,'
      '       a.advokat_naziv,'
      '       a.status,'
      '       a.aktiven,'
      '       case when a.status = 1 then '#39#1057#1072#1084#1086#1089#1090#1086#1077#1085#39
      
        '            when a.status = 2 then '#39#1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074 +
        #1086#39
      
        '            when a.status = 3 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090 +
        #1074#1086#39
      
        '            when a.status = 4 then '#39#1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072#39
      '       end "status_naziv",'
      '       a.advokatsko_drustvo,'
      '       ad.naziv as advokatsko_drustvo_naziv,'
      '       a.advokatska_kancelarija,'
      '       ak.naziv as advokatska_kancelarija_naziv,'
      '       a.mesto,'
      '       m.naziv as mesto_naziv,'
      '       a.advokatska_zaednica,'
      '       za.naziv as advokatska_zaednica_naziv,'
      '       a.adresa,'
      '       a.telefon,'
      '       a.mobilen_telefon,'
      '       a.email,'
      '       a.web_strana,'
      '       dm.custom1,'
      '       dm.custom2,'
      '       dm.custom3,'
      '       dm.ts_ins,'
      '       dm.ts_upd,'
      '       dm.usr_ins,'
      '       dm.usr_upd'
      'from adv_osiguruvanje dm'
      'inner join adv_advokati a on a.id = dm.advokat_id'
      
        'inner join adv_osiguritelni_komp ok on ok.id = dm.osiguritelna_k' +
        'omp_id'
      'left outer join mat_mesto m on m.id = a.mesto'
      
        'left outer join adv_advokatski_drustva ad on ad.id = a.advokatsk' +
        'o_drustvo'
      
        'left outer join adv_advokatski_kancelarii ak on ak.id = a.advoka' +
        'tska_kancelarija'
      'left outer join adv_zaednici za on za.id = a.advokatska_zaednica'
      'where ((dm.advokat_id = :advokat_id) or (:advokat_id = -100))')
    AutoUpdateOptions.UpdateTableName = 'ADV_OSIGURUVANJE'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_ADV_OSIGURUVANJE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 304
    Top = 904
    object tblOsiguruvanjeID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblOsiguruvanjeOSIGURITELNA_KOMP_ID: TFIBIntegerField
      DisplayLabel = #1054#1089#1080#1075#1091#1088#1080#1090#1077#1083#1085#1072' '#1082#1086#1084#1087#1072#1085#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'OSIGURITELNA_KOMP_ID'
    end
    object tblOsiguruvanjeOSIGURITELNA_KOMP_NAZIV: TFIBStringField
      DisplayLabel = #1054#1089#1080#1075#1091#1088#1080#1090#1077#1083#1085#1072' '#1082#1086#1084#1087#1072#1085#1080#1112#1072
      FieldName = 'OSIGURITELNA_KOMP_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjePOLISA: TFIBStringField
      DisplayLabel = #1055#1086#1083#1080#1089#1072
      FieldName = 'POLISA'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeDATUM_OD: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1054#1076
      FieldName = 'DATUM_OD'
    end
    object tblOsiguruvanjeDATUM_DO: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1076#1086
      FieldName = 'DATUM_DO'
    end
    object tblOsiguruvanjeADVOKAT_ID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088
      FieldName = 'ADVOKAT_ID'
    end
    object tblOsiguruvanjeEMBG: TFIBStringField
      FieldName = 'EMBG'
      Size = 16
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeLICENCA: TFIBStringField
      DisplayLabel = #1051#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeLICENCA_DATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
      FieldName = 'LICENCA_DATUM'
    end
    object tblOsiguruvanjeIME: TFIBStringField
      DisplayLabel = #1048#1084#1077
      FieldName = 'IME'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjePREZIME: TFIBStringField
      DisplayLabel = #1055#1088#1077#1079#1080#1084#1077
      FieldName = 'PREZIME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeADVOKAT_NAZIV: TFIBStringField
      DisplayLabel = #1048#1084#1077' '#1080' '#1087#1088#1077#1079#1080#1084#1077
      FieldName = 'ADVOKAT_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeSTATUS: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089' '#1085#1072' '#1074#1088#1072#1073#1086#1090#1077#1085#1086#1089#1090
      FieldName = 'STATUS'
    end
    object tblOsiguruvanjeAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblOsiguruvanjestatus_naziv: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'status_naziv'
      Size = 34
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeADVOKATSKO_DRUSTVO: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKO_DRUSTVO'
    end
    object tblOsiguruvanjeADVOKATSKO_DRUSTVO_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeADVOKATSKA_KANCELARIJA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA'
    end
    object tblOsiguruvanjeADVOKATSKA_KANCELARIJA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
      FieldName = 'MESTO'
    end
    object tblOsiguruvanjeMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeADVOKATSKA_ZAEDNICA: TFIBIntegerField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA'
    end
    object tblOsiguruvanjeADVOKATSKA_ZAEDNICA_NAZIV: TFIBStringField
      DisplayLabel = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
      FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
      Size = 1000
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeTELEFON: TFIBStringField
      DisplayLabel = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
      FieldName = 'TELEFON'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeMOBILEN_TELEFON: TFIBStringField
      DisplayLabel = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083'.'
      FieldName = 'MOBILEN_TELEFON'
      Size = 150
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeEMAIL: TFIBStringField
      DisplayLabel = 'EMAIL '#1072#1076#1088#1077#1089#1072
      FieldName = 'EMAIL'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeWEB_STRANA: TFIBStringField
      DisplayLabel = 'WEB '#1089#1090#1088#1072#1085#1072
      FieldName = 'WEB_STRANA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeTS_INS: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1079#1072#1087#1080#1089
      FieldName = 'TS_INS'
    end
    object tblOsiguruvanjeTS_UPD: TFIBDateTimeField
      DisplayLabel = #1042#1088#1077#1084#1077' '#1085#1072' '#1087#1086#1089#1083#1077#1076#1085#1086' '#1072#1078#1091#1088#1080#1088#1072#1114#1077
      FieldName = 'TS_UPD'
    end
    object tblOsiguruvanjeUSR_INS: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1075#1086' '#1077#1074#1080#1076#1077#1085#1090#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOsiguruvanjeUSR_UPD: TFIBStringField
      DisplayLabel = #1050#1086#1088#1080#1089#1085#1080#1082' '#1082#1086#1112' '#1087#1086#1089#1083#1077#1076#1077#1085' '#1075#1086' '#1072#1078#1091#1088#1080#1088#1072#1083' '#1079#1072#1087#1080#1089#1086#1090
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsOsiguruvanje: TDataSource
    DataSet = tblOsiguruvanje
    Left = 432
    Top = 904
  end
  object qEditAK_Period: TpFIBQuery
    Transaction = TransakcijaP
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE ADV_ADVOKATSKI_KANCELARII'
      'SET '
      ''
      '    DATUM_OD = :DATUM_OD,'
      '    DATUM_DO = :DATUM_DO,'
      '    ADV_RESENIE_ID = :ADV_RESENIE_ID'
      'WHERE'
      '    ID = :ID'
      '    ')
    Left = 976
    Top = 808
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qCountResenija: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(a.id) br'
      'from adv_resenija_advokati a'
      'where a.advokat_id = :advokat_id')
    Left = 728
    Top = 944
  end
end
