object frmRegStrucniSorabotnici: TfrmRegStrucniSorabotnici
  Left = 128
  Top = 212
  Hint = 'frmRegStrucniSorabotnici'
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1089#1090#1088#1091#1095#1085#1080' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1094#1080
  ClientHeight = 722
  ClientWidth = 944
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel
    Left = 39
    Top = 29
    Width = 46
    Height = 13
    Caption = #1064#1080#1092#1088#1072' :'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 944
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'radioStatus'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 699
    Width = 944
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 659
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 944
    Height = 235
    Align = alTop
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGroupBoxPodatociKontakt: TcxGroupBox
      Left = 504
      Top = 16
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
      TabOrder = 2
      Height = 174
      Width = 393
      object Label6: TLabel
        Left = 86
        Top = 84
        Width = 57
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 24
        Top = 30
        Width = 119
        Height = 13
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 17
        Top = 57
        Width = 126
        Height = 13
        Caption = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 32
        Top = 111
        Width = 111
        Height = 13
        Caption = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 61
        Top = 138
        Width = 82
        Height = 13
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object TELEFON: TcxDBTextEdit
        Left = 149
        Top = 81
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'TELEFON'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object MESTO: TcxDBTextEdit
        Left = 149
        Top = 27
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1086#1090' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object ADRESA: TcxDBTextEdit
        Left = 149
        Top = 54
        Hint = #1040#1076#1088#1077#1089#1072' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object MESTO_NAZIV: TcxDBLookupComboBox
        Left = 192
        Top = 27
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 185
      end
      object MOBILEN_TELEFON: TcxDBTextEdit
        Left = 149
        Top = 108
        Hint = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN_TELEFON'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object EMAIL: TcxDBTextEdit
        Left = 149
        Top = 135
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
    end
    object cxGroupBoxOsnovniPodatoci: TcxGroupBox
      Left = 16
      Top = 16
      Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
      TabOrder = 0
      Height = 90
      Width = 482
      object Label5: TLabel
        Left = 252
        Top = 57
        Width = 55
        Height = 13
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 49
        Top = 57
        Width = 29
        Height = 13
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 47
        Top = 30
        Width = 31
        Height = 13
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 272
        Top = 30
        Width = 35
        Height = 13
        Caption = #1045#1052#1041#1043' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 84
        Top = 54
        Hint = #1048#1084#1077' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 153
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 313
        Top = 54
        Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 153
      end
      object BROJ: TcxDBTextEdit
        Tag = 1
        Left = 84
        Top = 27
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 153
      end
      object EMBG: TcxDBTextEdit
        Left = 313
        Top = 27
        Hint = #1045#1076#1080#1085#1089#1090#1074#1077#1085' '#1084#1090#1080#1095#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        BeepOnEnter = False
        DataBinding.DataField = 'EMBG'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 153
      end
    end
    object ZapisiButton: TcxButton
      Left = 741
      Top = 204
      Width = 75
      Height = 25
      Action = aZapisi
      TabOrder = 3
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButtonOtkazi: TcxButton
      Left = 822
      Top = 204
      Width = 75
      Height = 25
      Action = aOtkazi
      TabOrder = 4
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxGroupBoxAdvokat: TcxGroupBox
      Left = 16
      Top = 105
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086
      TabOrder = 1
      Height = 117
      Width = 482
      object Label3: TLabel
        Left = 33
        Top = 84
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label1: TLabel
        Left = 20
        Top = 30
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = #1040#1076#1074#1086#1082#1072#1090' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label13: TLabel
        Left = 6
        Top = 51
        Width = 72
        Height = 26
        Alignment = taRightJustify
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM: TcxDBDateEdit
        Tag = 1
        Left = 84
        Top = 81
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1087#1086#1095#1085#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1086#1088#1072#1073#1086#1090#1082#1072#1090#1072
        DataBinding.DataField = 'DATUM'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 90
      end
      object ADVOKATSKO_DRUSTVO: TcxDBLookupComboBox
        Left = 84
        Top = 54
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' '#1089#1086' '#1082#1086#1077' '#1089#1086#1088#1072#1073#1086#1090#1091#1074#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        DataBinding.DataField = 'ADVOKATSKO_DRUSTVO'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsAdvokatskiDrustva
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 382
      end
      object ADVOKAT: TcxDBLookupComboBox
        Left = 84
        Top = 27
        Hint = #1040#1076#1074#1086#1082#1072#1090' '#1089#1086' '#1082#1086#1112' '#1089#1086#1088#1072#1073#1086#1090#1091#1074#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082
        DataBinding.DataField = 'ADVOKAT'
        DataBinding.DataSource = dm.dsStrucniSorabotnici
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ADVOKAT_NAZIV'
          end>
        Properties.ListSource = dm.dsAdvokati
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 382
      end
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 361
    Width = 944
    Height = 338
    Align = alClient
    Caption = 'lPanel'
    TabOrder = 3
    ExplicitHeight = 298
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 942
      Height = 336
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitHeight = 296
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsStrucniSorabotnici
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 69
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxSmallImages
          Properties.Items = <
            item
              Description = #1040#1082#1090#1080#1074#1077#1085
              ImageIndex = 79
              Value = 1
            end
            item
              Description = #1053#1077#1072#1082#1090#1080#1074#1077#1085
              ImageIndex = 77
              Value = 0
            end>
          Width = 86
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 129
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 120
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1ADVOKAT: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT'
          Visible = False
          Width = 98
        end
        object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_NAZIV'
          Width = 198
        end
        object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA'
          Width = 110
        end
        object cxGrid1DBTableView1status_advokat_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_advokat_naziv'
        end
        object cxGrid1DBTableView1ADV_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADV_KANCELARIJA_NAZIV'
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
          Visible = False
          Width = 168
        end
        object cxGrid1DBTableView1ADV_DRUSTVO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADV_DRUSTVO_NAZIV'
          Width = 174
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 78
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 205
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 200
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Width = 200
        end
        object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN_TELEFON'
          Width = 200
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 200
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 848
    Top = 528
  end
  object PopupMenu1: TPopupMenu
    Left = 824
    Top = 648
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 744
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 191
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 555
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 800
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object radioStatus: TdxBar
      Caption = #1055#1077#1073#1072#1088#1072#1112
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 800
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'radioGroupStatus'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      CaptionButtons = <>
      DockedLeft = 374
      DockedTop = 0
      FloatLeft = 970
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object radioGroupStatus: TcxBarEditItem
      Caption = #1055#1088#1080#1087#1088#1072#1074#1085#1080#1094#1080
      Category = 0
      Hint = #1055#1088#1080#1087#1088#1072#1074#1085#1080#1094#1080
      Visible = ivAlways
      OnChange = radioGroupStatusChange
      ImageIndex = 32
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080
          Value = 1
        end
        item
          Caption = #1053#1077#1072#1082#1090#1080#1074#1085#1080
          Value = 0
        end
        item
          Caption = #1057#1080#1090#1077
          Value = 2
        end>
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Items = <>
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPResZapisStrucenSorabotnik
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 632
    Top = 560
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPResZapisStrucenSorabotnik: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1091#1087#1080#1089' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082' '#1074#1086' '#1080#1084#1077#1085#1080#1082
      ImageIndex = 19
      OnExecute = aPResZapisStrucenSorabotnikExecute
    end
    object aDResZapisStrucenSorabotnik: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1091#1087#1080#1089' '#1085#1072' '#1089#1090#1088#1091#1095#1077#1085' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1082' '#1074#1086' '#1080#1084#1077#1085#1080#1082
      ImageIndex = 19
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDResZapisStrucenSorabotnikExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 208
    Top = 432
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 336
    Top = 472
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 536
    Top = 454
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmMat.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 44
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 49
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        SortIndex = 0
        SortOrder = soAscending
        Width = 345
      end
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 456
    Top = 358
  end
end
