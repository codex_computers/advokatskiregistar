unit RegAdvokatskiDrustva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions, cxNavigator, cxPCdxBarPopupMenu,
  cxImageComboBox, dxBarBuiltInMenu, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmAdvokatskiDrustva = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcelAD: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    Panel1: TPanel;
    PanelDrustva: TPanel;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxButton1: TcxButton;
    aDodadiDogovorenOrgan: TAction;
    aIzvadiDogovorenOrgan: TAction;
    cxPageControl1: TcxPageControl;
    cxTabSheetAdvokati: TcxTabSheet;
    cxButton8: TcxButton;
    cxButton9: TcxButton;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    aDodadiPaket: TAction;
    aIzvadiPAket: TAction;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    Label1: TLabel;
    aDodadiMedUslugi: TAction;
    aIzvadiMedUslugi: TAction;
    aAzurirajDog: TAction;
    aZapisisDog: TAction;
    aOtkaziDog: TAction;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    DATUM: TcxDBDateEdit;
    Label11: TLabel;
    NAZIV: TcxDBTextEdit;
    Label2: TLabel;
    BROJ: TcxDBTextEdit;
    EMAIL: TcxDBTextEdit;
    Label8: TLabel;
    TELEFON: TcxDBTextEdit;
    Label6: TLabel;
    Label10: TLabel;
    MESTO_NAZIV: TcxDBLookupComboBox;
    MESTO: TcxDBTextEdit;
    Label3: TLabel;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTONAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid2DBTableView1EMBG: TcxGridDBColumn;
    cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid2DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid2DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    ADRESA: TcxDBTextEdit;
    cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MESTO: TcxGridDBColumn;
    cxGrid2DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid2DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid2DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid2DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid2DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid2DBTableView1status_naziv: TcxGridDBColumn;
    dxBarLargeButton18: TdxBarLargeButton;
    aZacuvajExcelA: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    aPPotvrdaUpisAdvokatskoDrustvoBezKazniPovredi: TAction;
    aDPotvrdaUpisAdvokatskoDrustvoBezKazniPovredi: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aResenijaAD: TAction;
    cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    Label4: TLabel;
    MOBILEN_TELEFON: TcxDBTextEdit;
    Label5: TLabel;
    WEB_STRANA: TcxDBTextEdit;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelADExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aDodadiDogovorenOrganExecute(Sender: TObject);
    procedure aIzvadiDogovorenOrganExecute(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aDodadiPaketExecute(Sender: TObject);
    procedure aIzvadiPAketExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aDodadiMedUslugiExecute(Sender: TObject);
    procedure aZacuvajExcelAExecute(Sender: TObject);
    procedure aPPotvrdaUpisAdvokatskoDrustvoBezKazniPovrediExecute(Sender: TObject);
    procedure aDPotvrdaUpisAdvokatskoDrustvoBezKazniPovrediExecute(Sender: TObject);
    procedure aResenijaADExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmAdvokatskiDrustva: TfrmAdvokatskiDrustva;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, dmUnit, dmMaticni, RegAdvokati, ResenijaAD;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmAdvokatskiDrustva.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmAdvokatskiDrustva.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    PanelDrustva.Enabled:=True;
    BROJ.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmAdvokatskiDrustva.aAzurirajExecute(Sender: TObject);
begin

  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    PanelDrustva.Enabled:=True;
    NAZIV.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmAdvokatskiDrustva.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmAdvokatskiDrustva.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmAdvokatskiDrustva.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmAdvokatskiDrustva.aResenijaADExecute(Sender: TObject);
begin
  frmResenijaAD := TfrmResenijaAD.Create(self,false);
  frmResenijaAD.Tag:=1;
  frmResenijaAD.ShowModal;
  frmResenijaAD.Free;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmAdvokatskiDrustva.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmAdvokatskiDrustva.aIzvadiDogovorenOrganExecute(Sender: TObject);
begin
if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid2DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmAdvokatskiDrustva.aIzvadiPAketExecute(Sender: TObject);
begin

end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmAdvokatskiDrustva.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmAdvokatskiDrustva.aZacuvajExcelADExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmAdvokatskiDrustva.aZacuvajExcelAExecute(Sender: TObject);
begin
       zacuvajVoExcel(cxGrid2, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmAdvokatskiDrustva.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
      {  VK_INSERT:
        begin
          //  �� ����� ��������
          if (kom = PARTNERNAZIV)  then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := kom.Name;
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
              PARTNERNAZIV.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
            if PARTNERNAZIV.Text <> '' then
               begin
                 dm.tblDogovoriTIP_PARTNER.Value:=PARTNERNAZIV.EditValue[0];
                 dm.tblDogovoriPARTNER.Value:=PARTNERNAZIV.EditValue[1];
               end
            else
               begin
                 PARTNER.Clear;
                 TIP_PARTNER.Clear;
               end;
            frmNurkoRepository.Free;
       	 end;
	      end;     }
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmAdvokatskiDrustva.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
      if(Sender = EMAIL)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmAdvokatskiDrustva.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if(Sender = EMAIL)then
       ActivateKeyboardLayout($042F042F, KLF_REORDER);
end;

procedure TfrmAdvokatskiDrustva.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin

end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmAdvokatskiDrustva.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmAdvokatskiDrustva.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmAdvokatskiDrustva.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmAdvokatskiDrustva.prefrli;
begin
end;

procedure TfrmAdvokatskiDrustva.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(PanelDrustva) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
//  dm.tblDogovori.Close;
//  dm.tblDogovorniOrgani.Close;
//  dm.tblDogovoriSanPaketi.Close;
  dmRes.FreeRepository(rData);
end;
procedure TfrmAdvokatskiDrustva.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmAdvokatskiDrustva.FormShow(Sender: TObject);
begin

    SpremiForma(self);

    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    if (Tag = 3)  then
       begin
          dm.tblAdvokatskiDrustva.Locate('ID',dm.tblAdvokatiADVOKATSKO_DRUSTVO.Value, []);
       end;

 end;
//------------------------------------------------------------------------------

procedure TfrmAdvokatskiDrustva.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmAdvokatskiDrustva.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmAdvokatskiDrustva.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmAdvokatskiDrustva.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin

end;

//  ����� �� �����
procedure TfrmAdvokatskiDrustva.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(PanelDrustva) = false) then
    begin
      cxGrid1DBTableView1.DataController.DataSet.Post;
      cxGrid1DBTableView1.DataController.Refresh;
      PanelDrustva.Enabled:=false;
      cxGrid1.SetFocus;

      if Tag = 3 then
         begin
           dm.qUpdateAdvokatAD.Close;
           dm.qUpdateAdvokatAD.ParamByName('MESTO').Value:=dm.tblAdvokatskiDrustvaMESTO.Value;
           dm.qUpdateAdvokatAD.ParamByName('ADRESA').Value:=dm.tblAdvokatskiDrustvaADRESA.Value;
           dm.qUpdateAdvokatAD.ParamByName('TELEFON').Value:=dm.tblAdvokatskiDrustvaTELEFON.Value;
           dm.qUpdateAdvokatAD.ParamByName('EMAIL').Value:=dm.tblAdvokatskiDrustvaEMAIL.Value;
           dm.qUpdateAdvokatAD.ParamByName('ADVOKATSKO_DRUSTVO').Value:=dm.tblAdvokatskiDrustvaID.Value;
           dm.qUpdateAdvokatAD.ExecQuery;
           dm.tblAdvokati.Locate('ADVOKATSKO_DRUSTVO',dm.tblAdvokatskiDrustvaID.Value, []);
           dm.tblAdvokati.Refresh;
         end;
    end;
  end;
end;



//	����� �� ���������� �� �������
procedure TfrmAdvokatskiDrustva.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(PanelDrustva);
      PanelDrustva.Enabled := false;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmAdvokatskiDrustva.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmAdvokatskiDrustva.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmAdvokatskiDrustva.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmAdvokatskiDrustva.aPPotvrdaUpisAdvokatskoDrustvoBezKazniPovrediExecute(
  Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',4);
        dmKon.tblSqlReport.ParamByName('ad_id').Value:=dm.tblAdvokatskiDrustvaID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmAdvokatskiDrustva.aSnimiPecatenjeExecute(Sender: TObject);
begin
 zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmAdvokatskiDrustva.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmAdvokatskiDrustva.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
 brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmAdvokatskiDrustva.aDodadiDogovorenOrganExecute(Sender: TObject);
begin
    frmRegAdvokati:=TfrmRegAdvokati.Create(self,false);
    frmRegAdvokati.ShowModal;
    //if (frmRegAdvokati.ModalResult = mrOK) then
//    begin
//
//        dm.qAktivenDogovorDo.Close;
//        dm.qAktivenDogovorDo.ParamByName('datum_od').Value:=dm.tblDogovoriDATUM_OD.Value;
//        dm.qAktivenDogovorDo.ParamByName('datum_do').Value:=dm.tblDogovoriDATUM_DO.Value;
//        dm.qAktivenDogovorDo.ParamByName('tip_partner').Value:=StrToInt(frmPartner.GetSifra(0));
//        dm.qAktivenDogovorDo.ParamByName('partner').Value:=StrToInt(frmPartner.GetSifra(1));
//        dm.qAktivenDogovorDo.ExecQuery;
//        if (dm.qAktivenDogovorDo.FldByName['br'].Value = 1)then
//           begin
//             ShowMessage('��� ������������ �� ������ �� ������� �� ��������� �������!!!');
//           end
//        else
//           begin
//            dm.tblDogovorniOrgani.Insert;
//            dm.tblDogovorniOrganiTIP_PARTNER.Value:= StrToInt(frmPartner.GetSifra(0));
//            dm.tblDogovorniOrganiPARTNER.Value:= StrToInt(frmPartner.GetSifra(1));
//            dm.tblDogovorniOrganiDOGOVOR_ID.Value:=dm.tblDogovoriID.Value;
//            dm.tblDogovorniOrgani.Post;
//            dm.tblDogovorniOrgani.FullRefresh;
//           end;
//    end;
  frmRegAdvokati.Free;

end;

procedure TfrmAdvokatskiDrustva.aDodadiMedUslugiExecute(Sender: TObject);
begin
//  frmMedUslugii:=TfrmMedUslugii.Create(self,false);
//  frmMedUslugii.ShowModal;
//  if (frmMedUslugii.ModalResult = mrOK) then
//    begin
//      dm.tblDogovoriMedUslugi.FullRefresh;
//    end;
//  frmMedUslugii.Free;
end;

procedure TfrmAdvokatskiDrustva.aDodadiPaketExecute(Sender: TObject);
begin
//  frmSanPaketi:=TfrmSanPaketi.Create(self,false);
//  frmSanPaketi.ShowModal;
//  if (frmSanPaketi.ModalResult = mrOK) then
//    begin
//     // dm.tblDogovoriSanPaketi.FullRefresh;
//    end;
//  frmSanPaketi.Free;
end;

procedure TfrmAdvokatskiDrustva.aDPotvrdaUpisAdvokatskoDrustvoBezKazniPovrediExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',4);
      dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmAdvokatskiDrustva.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmAdvokatskiDrustva.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmAdvokatskiDrustva.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmAdvokatskiDrustva.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;



end.
