unit AdvokatiOsiguruvanje;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbonCustomizationForm,
  dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore, dxPScxCommon,
  System.Actions, Vcl.ActnList, cxBarEditItem, cxClasses, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, dxRibbon, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxCalendar, cxGroupBox, cxMemo, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox;

type
  TfrmAdvokatiOsiguruvanje = class(TfrmMaster)
    lbl1: TLabel;
    ADVOKAT_ID: TcxDBTextEdit;
    ADVOKAT_NAZIV: TcxDBLookupComboBox;
    cxGroupBoxPeriod: TcxGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    DATUM_DO: TcxDBDateEdit;
    DATUM_OD: TcxDBDateEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGroupBoxOsiguritelnaKompanija: TcxGroupBox;
    lbl5: TLabel;
    OSIGURITELNA_KOMP_ID: TcxDBTextEdit;
    OSIGURITELNA_KOMP_NAZIV: TcxDBLookupComboBox;
    POLISA: TcxDBTextEdit;
    lbl6: TLabel;
    cxGrid1DBTableView1OSIGURITELNA_KOMP_ID: TcxGridDBColumn;
    cxGrid1DBTableView1OSIGURITELNA_KOMP_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1POLISA: TcxGridDBColumn;
    dxbrlrgbtn1: TdxBarLargeButton;
    actPregledNaOsiguruvanje: TAction;
    cxGodina: TcxBarEditItem;
    actDPregledNaOsiguruvanje: TAction;
    procedure FormShow(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure actPregledNaOsiguruvanjeExecute(Sender: TObject);
    procedure actDPregledNaOsiguruvanjeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdvokatiOsiguruvanje: TfrmAdvokatiOsiguruvanje;

implementation

uses
  dmUnit, dmResources, dmKonekcija;

{$R *.dfm}

procedure TfrmAdvokatiOsiguruvanje.actDPregledNaOsiguruvanjeExecute(
  Sender: TObject);
begin
  inherited;
      dmRes.Spremi('ADV',24);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmAdvokatiOsiguruvanje.actPregledNaOsiguruvanjeExecute(
  Sender: TObject);
begin
  inherited;
      try
        dmRes.Spremi('ADV',24);
        if tag = 1 then
           dmKon.tblSqlReport.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value
        else
           dmKon.tblSqlReport.ParamByName('advokat_id').Value:=-100;
        if cxGodina.EditValue = null then
          begin
            ShowMessage('�������� ������ !!!');
            Abort   ;
          end
        else
           dmKon.tblSqlReport.ParamByName('godina').Value:=cxGodina.EditValue;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmAdvokatiOsiguruvanje.aNovExecute(Sender: TObject);
begin
  inherited;

  if tag = 1 then
     dm.tblOsiguruvanjeADVOKAT_ID.Value:=dm.tblAdvokatiID.Value

end;

procedure TfrmAdvokatiOsiguruvanje.aZapisiExecute(Sender: TObject);
var iznos_zadolzuvanje_od,iznos_zadolzuvanje_do:Integer ;
begin
  inherited;

end;

procedure TfrmAdvokatiOsiguruvanje.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblOsiguruvanje.Close;
  if tag = 1 then
       dm.tblOsiguruvanje.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value
  else
       dm.tblOsiguruvanje.ParamByName('advokat_id').Value:=-100;
  dm.tblOsiguruvanje.Open;
end;

end.
