object frmResenijaAdvokati: TfrmResenijaAdvokati
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1077#1096#1077#1085#1080#1112#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
  ClientHeight = 741
  ClientWidth = 1213
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel
    Left = 39
    Top = 29
    Width = 46
    Height = 13
    Caption = #1064#1080#1092#1088#1072' :'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1213
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 718
    Width = 1213
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 1213
    Height = 379
    Align = alTop
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGroupBoxPodatociKontakt: TcxGroupBox
      Left = 207
      Top = 129
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      TabOrder = 4
      DesignSize = (
        315
        219)
      Height = 219
      Width = 315
      object Label6: TLabel
        Left = 46
        Top = 135
        Width = 57
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 26
        Top = 21
        Width = 77
        Height = 26
        Alignment = taRightJustify
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label10: TLabel
        Left = 53
        Top = 81
        Width = 50
        Height = 13
        Caption = #1040#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 16
        Top = 108
        Width = 87
        Height = 13
        Caption = #1052#1086#1073'. '#1090#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 21
        Top = 162
        Width = 82
        Height = 13
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 28
        Top = 189
        Width = 75
        Height = 13
        Caption = 'Web '#1089#1090#1088#1072#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 28
        Top = 47
        Width = 75
        Height = 26
        Alignment = taRightJustify
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object TELEFON: TcxDBTextEdit
        Left = 109
        Top = 132
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'TELEFON'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
      object MESTO: TcxDBTextEdit
        Tag = 1
        Left = 109
        Top = 24
        Hint = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object ADRESA: TcxDBTextEdit
        Left = 109
        Top = 78
        Hint = #1040#1076#1088#1077#1089#1072
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
      object MESTO_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 152
        Top = 24
        Hint = #1052#1077#1089#1090#1086
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'OpstinaNaziv'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 153
      end
      object MOBILEN: TcxDBTextEdit
        Left = 109
        Top = 105
        Hint = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
      object EMAIL: TcxDBTextEdit
        Left = 109
        Top = 159
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
      object WEB_STRANA: TcxDBTextEdit
        Left = 109
        Top = 186
        Hint = #1042#1077#1073' '#1089#1090#1088#1072#1085#1072' '
        BeepOnEnter = False
        DataBinding.DataField = 'WEB_STRANA'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.OnChange = WEB_STRANAPropertiesChange
        Style.Shadow = False
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
      object ADVOKATSKA_ZAEDNICA_NAZIV: TcxDBLookupComboBox
        Left = 152
        Top = 51
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ADV_ZAEDNICA'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsZaednici
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 153
      end
      object ADVOKATSKA_ZAEDNICA: TcxDBTextEdit
        Left = 109
        Top = 51
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'ADV_ZAEDNICA'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
    end
    object cxGroupBoxOsnovniPodatoci: TcxGroupBox
      Left = 0
      Top = 226
      Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      TabOrder = 3
      Height = 121
      Width = 208
      object Label5: TLabel
        Left = 11
        Top = 84
        Width = 55
        Height = 13
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 37
        Top = 57
        Width = 29
        Height = 13
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 31
        Top = 30
        Width = 35
        Height = 13
        Caption = #1045#1052#1041#1043' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 72
        Top = 54
        Hint = #1048#1084#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 127
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 72
        Top = 81
        Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 127
      end
      object EMBG: TcxDBTextEdit
        Left = 72
        Top = 27
        Hint = #1045#1052#1041#1043' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'EMBG'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 127
      end
    end
    object cxGroupBoxLicenca: TcxGroupBox
      Left = 0
      Top = 127
      Caption = #1051#1080#1094#1077#1085#1094#1072
      TabOrder = 2
      Height = 97
      Width = 210
      object Label3: TLabel
        Left = 21
        Top = 51
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label2: TLabel
        Left = 35
        Top = 24
        Width = 31
        Height = 13
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LICENCA_DATUM: TcxDBDateEdit
        Tag = 1
        Left = 72
        Top = 48
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1079#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
        DataBinding.DataField = 'LICENCA_DATUM'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Enabled = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 129
      end
      object LICENCA_BROJ: TcxDBTextEdit
        Tag = 1
        Left = 72
        Top = 21
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
        BeepOnEnter = False
        DataBinding.DataField = 'LICENCA_BROJ'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Enabled = False
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 129
      end
    end
    object ZapisiButton: TcxButton
      Left = 1020
      Top = 348
      Width = 75
      Height = 25
      Action = aZapisi
      TabOrder = 8
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButtonOtkazi: TcxButton
      Left = 1101
      Top = 348
      Width = 75
      Height = 25
      Action = aOtkazi
      TabOrder = 10
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object VrabotenVo: TcxGroupBox
      Left = 523
      Top = 213
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086
      TabOrder = 6
      Height = 134
      Width = 359
      object Label14: TLabel
        Left = 3
        Top = 59
        Width = 80
        Height = 31
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label13: TLabel
        Left = 4
        Top = 27
        Width = 80
        Height = 31
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object ADVOKATSKO_DRUSTVO_NAZIV: TcxDBLookupComboBox
        Left = 89
        Top = 63
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1085#1072#1079#1080#1074
        DataBinding.DataField = 'ADV_DRUSTVO'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsAdvokatskiDrustva
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object cxButton1: TcxButton
        Left = 324
        Top = 60
        Width = 25
        Height = 25
        Action = aCopyAD
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 3
        TabStop = False
      end
      object ADVOKATSKA_KANCELARIJA_NAZIV: TcxDBLookupComboBox
        Left = 90
        Top = 32
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1085#1072#1079#1080#1074
        DataBinding.DataField = 'ADV_KANCELARIJA'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsAdvokatskiKancelarii
        Properties.OnEditValueChanged = ADVOKATSKA_KANCELARIJA_NAZIVPropertiesEditValueChanged
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object cxButton5: TcxButton
        Left = 327
        Top = 90
        Width = 25
        Height = 25
        Action = aCopyAK
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 4
        TabStop = False
        Visible = False
      end
      object KANCELARIJA_NAZIV: TcxDBTextEdit
        Left = 93
        Top = 90
        DataBinding.DataField = 'KANCELARIJA_NAZIV'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Enabled = False
        StyleDisabled.TextColor = clBackground
        TabOrder = 5
        Visible = False
        Width = 228
      end
      object btnaKreirajAK: TcxButton
        Left = 324
        Top = 28
        Width = 25
        Height = 25
        Action = actKreirajAKancelarija
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 1
        TabStop = False
      end
    end
    object cxGroupBoxPodatociResenie: TcxGroupBox
      Left = 0
      Top = 0
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1088#1077#1096#1077#1085#1080#1077
      TabOrder = 0
      Height = 121
      Width = 879
      object Label15: TLabel
        Left = 40
        Top = 29
        Width = 26
        Height = 13
        Caption = #1058#1080#1087' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 35
        Top = 56
        Width = 31
        Height = 13
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 180
        Top = 56
        Width = 48
        Height = 13
        Caption = #1044#1072#1090#1091#1084'  :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 341
        Top = 50
        Width = 99
        Height = 26
        Hint = #1055#1088#1077#1090#1089#1077#1076#1072#1090#1077#1083' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072
        Caption = #1055#1088#1077#1090#1089#1077#1076#1072#1090#1077#1083' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label21: TLabel
        Left = 33
        Top = 81
        Width = 33
        Height = 13
        Caption = #1054#1087#1080#1089' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object RESENIE_TIP: TcxDBTextEdit
        Tag = 1
        Left = 72
        Top = 26
        Hint = #1058#1080#1087' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'RESENIE_TIP'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.OnEditValueChanged = RESENIE_TIPPropertiesEditValueChanged
        Style.Shadow = False
        StyleDisabled.TextColor = clBtnText
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object RESENIE_TIP_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 112
        Top = 26
        Hint = #1058#1080#1087' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077' - '#1085#1072#1079#1080#1074
        DataBinding.DataField = 'RESENIE_TIP'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsTipResenija
        StyleDisabled.TextColor = clBtnText
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 759
      end
      object BROJ_RESENIE: TcxDBTextEdit
        Left = 72
        Top = 53
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
        BeepOnEnter = False
        DataBinding.DataField = 'BROJ_RESENIE'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 97
      end
      object KOMISIJA_PRETSEDATEL: TcxDBTextEdit
        Left = 440
        Top = 53
        Hint = #1055#1088#1077#1090#1089#1077#1076#1072#1090#1077#1083' '#1085#1072' '#1082#1086#1084#1080#1089#1080#1112#1072
        BeepOnEnter = False
        DataBinding.DataField = 'KOMISIJA_PRETSEDATEL'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 431
      end
      object DATUM_SEDNICA: TcxDBDateEdit
        Left = 234
        Top = 53
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1089#1077#1076#1085#1080#1094#1072
        DataBinding.DataField = 'DATUM_SEDNICA'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.OnChange = DATUM_SEDNICAPropertiesChange
        Properties.OnCloseUp = DATUM_SEDNICAPropertiesCloseUp
        Properties.OnEditValueChanged = DATUM_SEDNICAPropertiesEditValueChanged
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 95
      end
      object OPIS: TcxDBMemo
        Left = 72
        Top = 80
        DataBinding.DataField = 'OPIS'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 30
        Width = 799
      end
    end
    object cxGroupBoxPeriodVaznost: TcxGroupBox
      Left = 878
      Top = 0
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077#1090#1086
      TabOrder = 1
      Height = 74
      Width = 309
      object Label19: TLabel
        Left = 39
        Top = 21
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1086#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label20: TLabel
        Left = 39
        Top = 48
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 109
        Top = 18
        Hint = #1044#1072#1090#1091#1084' '#1086#1076
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object DATUM_DO: TcxDBDateEdit
        Left = 109
        Top = 45
        Hint = #1044#1072#1090#1091#1084' '#1076#1086
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
    end
    object cxButton2: TcxButton
      Left = 906
      Top = 348
      Width = 25
      Height = 25
      Action = aKreirajAK
      OptionsImage.Images = dmRes.cxSmallImages
      TabOrder = 9
      TabStop = False
      Visible = False
    end
    object cxDBRadioGroupStatus: TcxDBRadioGroup
      Left = 523
      Top = 129
      TabStop = False
      Caption = #1057#1090#1072#1090#1091#1089
      DataBinding.DataField = 'STATUS'
      DataBinding.DataSource = dm.dsResenijaAdvokati
      ParentColor = False
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1072#1084#1086#1089#1090#1086#1077#1085' '#1072#1076#1074#1086#1082#1072#1090
          Value = 1
        end
        item
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1040'. '#1050#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
          Value = 4
        end
        item
          Caption = #1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1040'. '#1044#1088#1091#1096#1090#1074#1086
          Value = 2
        end
        item
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1040'. '#1044#1088#1091#1096#1090#1074#1086
          Value = 3
        end>
      Style.Color = clBtnFace
      Style.LookAndFeel.Kind = lfFlat
      Style.LookAndFeel.NativeStyle = True
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.SkinName = ''
      TabOrder = 5
      Transparent = True
      Height = 78
      Width = 361
    end
    object cxGroupBox2: TcxGroupBox
      Left = 878
      Top = 80
      Caption = #1051#1080#1095#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      TabOrder = 7
      DesignSize = (
        311
        187)
      Height = 187
      Width = 311
      object Label23: TLabel
        Left = 44
        Top = 14
        Width = 59
        Height = 26
        Alignment = taRightJustify
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label24: TLabel
        Left = 16
        Top = 49
        Width = 87
        Height = 13
        Caption = #1052#1086#1073'. '#1090#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label25: TLabel
        Left = 46
        Top = 76
        Width = 57
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 21
        Top = 103
        Width = 82
        Height = 13
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 28
        Top = 130
        Width = 75
        Height = 13
        Caption = 'Web '#1089#1090#1088#1072#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MESTO_LK_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 152
        Top = 18
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'MESTO_LK'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'OpstinaNaziv'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 149
      end
      object MESTO_LK: TcxDBTextEdit
        Tag = 1
        Left = 109
        Top = 18
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO_LK'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object MOBILEN_TELEFON_LK: TcxDBTextEdit
        Left = 109
        Top = 46
        Hint = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN_TELEFON_LK'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 192
      end
      object TELEFON_LK: TcxDBTextEdit
        Left = 109
        Top = 73
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'TELEFON_LK'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 192
      end
      object EMAIL_LK: TcxDBTextEdit
        Left = 109
        Top = 100
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL_LK'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 192
      end
      object WEB_STRANA_LK: TcxDBTextEdit
        Left = 109
        Top = 127
        Hint = #1042#1077#1073' '#1089#1090#1088#1072#1085#1072' '
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'WEB_STRANA_LK'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.OnChange = WEB_STRANAPropertiesChange
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 192
      end
      object cxButton6: TcxButton
        Left = 119
        Top = 154
        Width = 162
        Height = 25
        Action = aCopyAKVOLK
        Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 6
        TabStop = False
      end
    end
    object cxGroupBoxBaratel: TcxGroupBox
      Left = 878
      Top = 272
      Caption = #1043#1077#1085#1077#1088#1072#1083#1080#1080' '#1085#1072' '#1073#1072#1088#1072#1090#1077#1083#1086#1090' ('#1074#1086' '#1089#1083#1091#1095#1072#1112' '#1073#1088#1080#1096#1077#1114#1077' '#1087#1086#1088#1072#1076#1080' '#1089#1084#1088#1090')'
      TabOrder = 11
      Visible = False
      Height = 74
      Width = 309
      object Label28: TLabel
        Left = 43
        Top = 19
        Width = 60
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1041#1072#1088#1072#1090#1077#1083' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label29: TLabel
        Left = 43
        Top = 43
        Width = 60
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object cxDBTextEdit1: TcxDBTextEdit
        Left = 109
        Top = 16
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1073#1072#1088#1072#1090#1077#1083#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'BARATEL_NAZIV'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
      object cxDBTextEdit2: TcxDBTextEdit
        Left = 109
        Top = 38
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1073#1072#1088#1072#1090#1077#1083#1086#1090
        BeepOnEnter = False
        DataBinding.DataField = 'BARATEL_MESTO'
        DataBinding.DataSource = dm.dsResenijaAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 196
      end
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 505
    Width = 1213
    Height = 213
    Align = alClient
    Caption = 'lPanel'
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1211
      Height = 211
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        DataController.DataSource = dm.dsResenijaAdvokati
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 49
        end
        object cxGrid1DBTableView1BROJ_RESENIE: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_RESENIE'
          Width = 92
        end
        object cxGrid1DBTableView1RESENIE_TIP: TcxGridDBColumn
          DataBinding.FieldName = 'RESENIE_TIP'
          Visible = False
          Width = 130
        end
        object cxGrid1DBTableView1TIP_RESENIE_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_RESENIE_NAZIV'
          Width = 162
        end
        object cxGrid1DBTableView1TIP_RESENIE_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_RESENIE_OPIS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 91
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 94
        end
        object cxGrid1DBTableView1DATUM_SEDNICA: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_SEDNICA'
          Width = 150
        end
        object cxGrid1DBTableView1KOMISIJA_PRETSEDATEL: TcxGridDBColumn
          DataBinding.FieldName = 'KOMISIJA_PRETSEDATEL'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_ID'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
          Width = 98
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1LICENCA_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_BROJ'
          Width = 78
        end
        object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_DATUM'
          Width = 99
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
          Width = 138
        end
        object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_naziv'
        end
        object cxGrid1DBTableView1ADV_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADV_DRUSTVO'
          Visible = False
          Width = 164
        end
        object cxGrid1DBTableView1ADV_DRUSTVO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADV_DRUSTVO_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADV_KANCELARIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ADV_KANCELARIJA'
          Visible = False
          Width = 187
        end
        object cxGrid1DBTableView1ADV_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADV_KANCELARIJA_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Width = 168
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 150
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 150
        end
        object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
          DataBinding.FieldName = 'WEB_STRANA'
          Width = 150
        end
        object cxGrid1DBTableView1MOBILEN: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN'
          Width = 150
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Width = 150
        end
        object cxGrid1DBTableView1BR_DISCIPLINSKI: TcxGridDBColumn
          DataBinding.FieldName = 'BR_DISCIPLINSKI'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1053#1077#1084#1072
              ImageIndex = 15
              Value = 0
            end
            item
              Description = #1048#1084#1072
              ImageIndex = 7
              Value = 1
            end>
          Width = 127
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 150
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object PanelREport: TPanel
    Left = 366
    Top = 164
    Width = 346
    Height = 259
    Color = clSilver
    ParentBackground = False
    TabOrder = 4
    Visible = False
    DesignSize = (
      346
      259)
    object cxGroupBox1: TcxGroupBox
      Left = 7
      Top = 5
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      Height = 249
      Width = 336
      object LabelReport: TLabel
        Left = 27
        Top = 19
        Width = 31
        Height = 13
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MemoReport: TcxDBMemo
        Left = 24
        Top = 38
        DataBinding.DataField = 'VREDNOST'
        DataBinding.DataSource = dm.dsDokumentiPolinjaVrednost
        TabOrder = 0
        Height = 137
        Width = 289
      end
      object cxButton3: TcxButton
        Left = 150
        Top = 192
        Width = 75
        Height = 25
        Action = aZpisiReport
        TabOrder = 1
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxButton4: TcxButton
        Left = 231
        Top = 192
        Width = 75
        Height = 25
        Action = aOtkaziReport
        TabOrder = 2
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 848
    Top = 528
  end
  object PopupMenu1: TPopupMenu
    Left = 792
    Top = 520
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 584
    Top = 536
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 678
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 923
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 1100
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = '    '
      CaptionButtons = <>
      DockedLeft = 480
      DockedTop = 0
      FloatLeft = 1100
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = '     '
      CaptionButtons = <>
      DockedLeft = 308
      DockedTop = 0
      FloatLeft = 1168
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Items = <>
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPripravnici
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aStrucniSorabotnici
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080' '#1088#1077#1096#1077#1085#1080#1112#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aPotvrdaZaZapisVoRegistar
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aPSvecenaZakletvaAdvokat
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPotvrdaZaZapisVoRegistar
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aPSvecenaZakletvaAdvokat
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPResZapisVoRegistar
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076#1072#1112'/'#1055#1077#1095#1072#1090#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
      ItemLinks = <
        item
          Visible = True
          ItemName = 'bbPResZapisVoRegistar'
        end
        item
          Visible = True
          ItemName = 'bbPSvecenaZakletvaAdvokat'
        end
        item
          Visible = True
          ItemName = 'bbPotvrdaZaZapisVoRegistar'
        end
        item
          Visible = True
          ItemName = 'bbPEObrazec'
        end
        item
          Visible = True
          ItemName = 'bbPPropratnoPismoCRM'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbPResenieMiruvanje_Bolest'
        end
        item
          Visible = True
          ItemName = 'bbPResenieMiruvanje_RabOdnosNeOpredeleno'
        end
        item
          Visible = True
          ItemName = 'bbPResenieMiruvanje_RabOdnosOpredeleno'
        end
        item
          Visible = True
          ItemName = 'bbPResenieMiruvanje_RabOdnosProbnaRabota'
        end
        item
          Visible = True
          ItemName = 'bbPResenieMiruvanje_StrucnoUsovrsuvanje'
        end
        item
          Visible = True
          ItemName = 'bbPResenieMiruvanje_IzborFunkcija'
        end
        item
          Visible = True
          ItemName = 'bbPResenieProdolzuvanjeMiruvanje'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbPResBrisiAdvokatImenikOtkazuvanje'
        end
        item
          Visible = True
          ItemName = 'bbPResBrisiAdvokatImenikSmrt'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'bbPResPromenaPrezime'
        end
        item
          Visible = True
          ItemName = 'bbPResOdSamostoenVoVrabotenVoAK'
        end
        item
          Visible = True
          ItemName = 'bbPOdVrabotenKajAdvokatVoSamostoen'
        end
        item
          Visible = True
          ItemName = 'bbPResMiruvanje'
        end
        item
          Visible = True
          ItemName = 'bbPResMiruvanjeProdolzeno'
        end
        item
          Visible = True
          ItemName = 'bbPResPromenaSedisteAK'
        end>
      OnPopup = dxBarSubItem3Popup
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object bbPResZapisVoRegistar: TdxBarButton
      Action = aPResZapisVoRegistar
      Category = 0
    end
    object bbPotvrdaZaZapisVoRegistar: TdxBarButton
      Action = aPotvrdaZaZapisVoRegistar
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1087#1077#1095#1072#1090
      Category = 0
    end
    object bbPResOdSamostoenVoVrabotenVoAK: TdxBarButton
      Action = aPResOdSamostoenVoVrabotenVoAK
      Category = 0
      Visible = ivInCustomizing
    end
    object bbPOdVrabotenKajAdvokatVoSamostoen: TdxBarButton
      Action = aPOdVrabotenKajAdvokatVoSamostoen
      Category = 0
      Visible = ivInCustomizing
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object bbPSvecenaZakletvaAdvokat: TdxBarButton
      Action = aPSvecenaZakletvaAdvokat
      Category = 0
    end
    object dxBarSubItem6: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object bbPResBrisiAdvokatImenikOtkazuvanje: TdxBarButton
      Action = aPResBrisiAdvokatImenikOtkazuvanje
      Category = 0
      Visible = ivNever
    end
    object bbPResMiruvanje: TdxBarButton
      Action = aPResMiruvanje
      Category = 0
      Visible = ivNever
    end
    object bbPResMiruvanjeProdolzeno: TdxBarButton
      Action = aPResMiruvanjeProdolzeno
      Category = 0
      Visible = ivInCustomizing
    end
    object bbPResPromenaPrezime: TdxBarButton
      Action = aPResPromenaPrezime
      Category = 0
      Visible = ivInCustomizing
    end
    object bbPResPromenaSedisteAK: TdxBarButton
      Action = aPResPromenaSedisteAK
      Category = 0
      Visible = ivInCustomizing
    end
    object bbPEObrazec: TdxBarButton
      Action = aPEObrazec
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aDisciplinskiMerki
      Category = 0
    end
    object bbPPropratnoPismoCRM: TdxBarLargeButton
      Action = aPPropratnoPismoCRM
      Category = 0
    end
    object dxBarSubItem7: TdxBarSubItem
      Action = aDResenieMiruvanje_Bolest
      Category = 0
      ItemLinks = <>
    end
    object dxBarSubItem8: TdxBarSubItem
      Action = aPResenieMiruvanje_RabOdnosNeOpredeleno
      Category = 0
      ItemLinks = <>
    end
    object dxBarSubItem9: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem10: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem11: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarSubItem12: TdxBarSubItem
      Action = aDResenieMiruvanje_RabOdnosNeOpredeleno
      Category = 0
      ItemLinks = <>
    end
    object dxBarSubItem13: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object bbPResenieMiruvanje_Bolest: TdxBarButton
      Action = aPResenieMiruvanje_Bolest
      Category = 0
    end
    object bbPResenieMiruvanje_RabOdnosNeOpredeleno: TdxBarButton
      Action = aPResenieMiruvanje_RabOdnosNeOpredeleno
      Category = 0
    end
    object bbPResenieMiruvanje_RabOdnosOpredeleno: TdxBarButton
      Action = aPResenieMiruvanje_RabOdnosOpredeleno
      Category = 0
    end
    object bbPResenieMiruvanje_StrucnoUsovrsuvanje: TdxBarButton
      Action = aPResenieMiruvanje_StrucnoUsovrsuvanje
      Category = 0
    end
    object bbPResenieMiruvanje_RabOdnosProbnaRabota: TdxBarButton
      Action = aPResenieMiruvanje_RabOdnosProbnaRabota
      Category = 0
    end
    object bbPResenieMiruvanje_IzborFunkcija: TdxBarButton
      Action = aPResenieMiruvanje_IzborFunkcija
      Category = 0
    end
    object bbPResenieProdolzuvanjeMiruvanje: TdxBarButton
      Action = aPResenieProdolzuvanjeMiruvanje
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aAdvokatskaKancelarija
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aAdvokatskoDruzstvo
      Category = 0
    end
    object bbPResBrisiAdvokatImenikSmrt: TdxBarButton
      Action = aPResBrisiAdvokatImenikSmrt
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = actPorodino
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 792
    Top = 456
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPripravnici: TAction
      Caption = #1055#1088#1080#1087#1088#1072#1074#1085#1080#1094#1080
      Hint = #1055#1088#1080#1087#1088#1072#1074#1085#1080#1094#1080
      ImageIndex = 44
      OnExecute = aPripravniciExecute
    end
    object aStrucniSorabotnici: TAction
      Caption = #1057#1090#1088#1091#1095#1085#1080' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1094#1080
      Hint = #1057#1090#1088#1091#1095#1085#1080' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1094#1080
      ImageIndex = 44
      OnExecute = aStrucniSorabotniciExecute
    end
    object aCopyAD: TAction
      Hint = 
        #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' ('#1052#1077#1089#1090#1086', '#1040#1076#1088#1077#1089#1072', '#1058#1077#1083#1077#1092#1086#1085', ' +
        'Email)'
      ImageIndex = 8
      OnExecute = aCopyADExecute
    end
    object aPotvrdaZaZapisVoRegistar: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1079#1072#1087#1080#1089' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
      ImageIndex = 19
      OnExecute = aPotvrdaZaZapisVoRegistarExecute
    end
    object aDPotvrdaZaZapisVoRegistar: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1087#1077#1095#1072#1090
      OnExecute = aDPotvrdaZaZapisVoRegistarExecute
    end
    object aDizajnReport: TAction
      Caption = 'aDizajnReport'
      ShortCut = 24697
      OnExecute = aDizajnReportExecute
    end
    object aPSvecenaZakletvaAdvokat: TAction
      Caption = #1057#1074#1077#1095#1077#1085#1072' '#1079#1072#1082#1083#1077#1090#1074#1072
      ImageIndex = 19
      OnExecute = aPSvecenaZakletvaAdvokatExecute
    end
    object aDSvecenaZakletvaAdvokat: TAction
      Caption = #1057#1074#1077#1095#1077#1085#1072' '#1079#1072#1082#1083#1077#1090#1074#1072
      OnExecute = aDSvecenaZakletvaAdvokatExecute
    end
    object aPResZapisVoRegistar: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1079#1072#1087#1080#1089' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
      ImageIndex = 19
      OnExecute = aPResZapisVoRegistarExecute
    end
    object aDResZapisVoRegistar: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1079#1072#1087#1080#1089' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
      OnExecute = aDResZapisVoRegistarExecute
    end
    object aPResOdSamostoenVoVrabotenVoAK: TAction
      Caption = 
        #1056#1077#1096#1077#1085#1080#1077' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1085#1072' '#1089#1072#1084#1086#1089#1090#1086#1077#1085' '#1072#1076#1074#1086#1082#1072#1090' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072' '#1082#1072#1082#1086' '#1074#1088#1072#1073#1086#1090#1077#1085' ' +
        #1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      ImageIndex = 19
      OnExecute = aPResOdSamostoenVoVrabotenVoAKExecute
    end
    object aDResOdSamostoenVoVrabotenVoAK: TAction
      Caption = 
        #1056#1077#1096#1077#1085#1080#1077' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1085#1072' '#1089#1072#1084#1086#1089#1090#1086#1077#1085' '#1072#1076#1074#1086#1082#1072#1090' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072' '#1082#1072#1082#1086' '#1074#1088#1072#1073#1086#1090#1077#1085' ' +
        #1074#1086' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      OnExecute = aDResOdSamostoenVoVrabotenVoAKExecute
    end
    object aPOdVrabotenKajAdvokatVoSamostoen: TAction
      Caption = 
        #1056#1077#1096#1077#1085#1080#1077' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1082#1072#1082#1086' '#1074#1088#1072#1073#1086#1090#1077#1085' '#1082#1072#1112' '#1072#1076#1074#1086#1082#1072#1090' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072' '#1082#1072#1082#1086' '#1089#1072#1084#1086 +
        #1089#1090#1086#1077#1085' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 19
      OnExecute = aPOdVrabotenKajAdvokatVoSamostoenExecute
    end
    object aDOdVrabotenKajAdvokatVoSamostoen: TAction
      Caption = 
        #1056#1077#1096#1077#1085#1080#1077' '#1087#1088#1077#1089#1090#1072#1085#1086#1082' '#1082#1072#1082#1086' '#1074#1088#1072#1073#1086#1090#1077#1085' '#1082#1072#1112' '#1072#1076#1074#1086#1082#1072#1090' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072' '#1082#1072#1082#1086' '#1089#1072#1084#1086 +
        #1089#1090#1086#1077#1085' '#1072#1076#1074#1086#1082#1072#1090
      OnExecute = aDOdVrabotenKajAdvokatVoSamostoenExecute
    end
    object aPResBrisiAdvokatImenikSmrt: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1073#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1086#1076' '#1080#1084#1077#1085#1080#1082' '#1087#1086#1088#1072#1076#1080' '#1089#1084#1088#1090
      ImageIndex = 19
      OnExecute = aPResBrisiAdvokatImenikSmrtExecute
    end
    object aPResBrisiAdvokatImenikOtkazuvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1073#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1086#1076' '#1080#1084#1077#1085#1080#1082' '#1087#1086#1088#1072#1076#1080' '#1086#1090#1082#1072#1078#1091#1074#1072#1114#1077
      ImageIndex = 19
      OnExecute = aPResBrisiAdvokatImenikOtkazuvanjeExecute
    end
    object aDResBrisiAdvokatImenikSmrt: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1073#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1086#1076' '#1080#1084#1077#1085#1080#1082' '#1087#1086#1088#1072#1076#1080' '#1089#1084#1088#1090
      OnExecute = aDResBrisiAdvokatImenikSmrtExecute
    end
    object aDResBrisiAdvokatImenikOtkazuvanje: TAction
      Caption = ' '#1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1073#1088#1080#1096#1077#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1086#1076' '#1080#1084#1077#1085#1080#1082' '#1087#1086#1088#1072#1076#1080' '#1086#1090#1082#1072#1078#1091#1074#1072#1114#1077
      OnExecute = aDResBrisiAdvokatImenikOtkazuvanjeExecute
    end
    object aPResMiruvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 19
      OnExecute = aPResMiruvanjeExecute
    end
    object aDResMiruvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      OnExecute = aDResMiruvanjeExecute
    end
    object aPResMiruvanjeProdolzeno: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 19
      OnExecute = aPResMiruvanjeProdolzenoExecute
    end
    object aDResMiruvanjeProdolzeno: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      OnExecute = aDResMiruvanjeProdolzenoExecute
    end
    object aPResPromenaPrezime: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1087#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 19
      OnExecute = aPResPromenaPrezimeExecute
    end
    object aDResPromenaPrezime: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1087#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      OnExecute = aDResPromenaPrezimeExecute
    end
    object aPResPromenaSedisteAK: TAction
      Caption = 
        #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1089#1077#1076#1080#1096#1090#1077' '#1086#1076' '#1075#1088#1072#1076' '#1074#1086' '#1075#1088#1072#1076' '#1085#1072' '#1072#1076#1074#1086#1082#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 19
      OnExecute = aPResPromenaSedisteAKExecute
    end
    object aDResPromenaSedisteAK: TAction
      Caption = 
        #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1089#1077#1076#1080#1096#1090#1077' '#1086#1076' '#1075#1088#1072#1076' '#1074#1086' '#1075#1088#1072#1076' '#1085#1072' '#1072#1076#1074#1086#1082#1090#1089#1082#1072' '#1082#1072#1085#1094#1077 +
        #1083#1072#1088#1080#1112#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
      OnExecute = aDResPromenaSedisteAKExecute
    end
    object aKreirajAK: TAction
      ImageIndex = 76
      OnExecute = aKreirajAKExecute
    end
    object aUpdateResenieZapis: TAction
      Caption = 'aUpdateResenieZapis'
    end
    object aPEObrazec: TAction
      Caption = #1055#1045' '#1054#1073#1088#1072#1079#1077#1094
      ImageIndex = 19
      OnExecute = aPEObrazecExecute
    end
    object aDPEObrazec: TAction
      Caption = #1055#1045' '#1054#1073#1088#1072#1079#1077#1094
      OnExecute = aDPEObrazecExecute
    end
    object aDisciplinskiMerki: TAction
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1080' '#1084#1077#1088#1082#1080
      ImageIndex = 77
      OnExecute = aDisciplinskiMerkiExecute
    end
    object aPPropratnoPismoCRM: TAction
      Caption = #1055#1088#1086#1087#1088#1072#1090#1085#1086' '#1087#1080#1089#1084#1086' '#1076#1086' '#1062#1056#1052
      ImageIndex = 19
      OnExecute = aPPropratnoPismoCRMExecute
    end
    object aDPropratnoPismoCRM: TAction
      Caption = #1055#1088#1086#1087#1088#1072#1090#1085#1086' '#1087#1080#1089#1084#1086' '#1076#1086' '#1062#1056#1052
      OnExecute = aDPropratnoPismoCRMExecute
    end
    object aPResenieMiruvanje_Bolest: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1073#1086#1083#1077#1089#1090
      ImageIndex = 19
      OnExecute = aPResenieMiruvanje_BolestExecute
    end
    object aDResenieMiruvanje_Bolest: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1073#1086#1083#1077#1089#1090
      OnExecute = aDResenieMiruvanje_BolestExecute
    end
    object aPResenieMiruvanje_RabOdnosOpredeleno: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1088#1072#1073'. '#1086#1076#1085#1086#1089' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086
      ImageIndex = 19
      OnExecute = aPResenieMiruvanje_RabOdnosOpredelenoExecute
    end
    object aDResenieMiruvanje_RabOdnosOpredeleno: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1088#1072#1073'. '#1086#1076#1085#1086#1089' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086
      OnExecute = aDResenieMiruvanje_RabOdnosOpredelenoExecute
    end
    object aPResenieMiruvanje_RabOdnosNeOpredeleno: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1088#1072#1073'. '#1086#1076#1085#1086#1089' '#1085#1077#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086
      ImageIndex = 19
      OnExecute = aPResenieMiruvanje_RabOdnosNeOpredelenoExecute
    end
    object aDResenieMiruvanje_RabOdnosNeOpredeleno: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1088#1072#1073'. '#1086#1076#1085#1086#1089' '#1085#1077#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086
      OnExecute = aDResenieMiruvanje_RabOdnosNeOpredelenoExecute
    end
    object aPResenieMiruvanje_RabOdnosProbnaRabota: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1088#1072#1073'. '#1086#1076#1085#1086#1089' '#1087#1088#1086#1073#1085#1072' '#1088#1072#1073#1086#1090#1072
      ImageIndex = 19
      OnExecute = aPResenieMiruvanje_RabOdnosProbnaRabotaExecute
    end
    object aDResenieMiruvanje_RabOdnosProbnaRabota: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1088#1072#1073'. '#1086#1076#1085#1086#1089' '#1087#1088#1086#1073#1085#1072' '#1088#1072#1073#1086#1090#1072
      OnExecute = aDResenieMiruvanje_RabOdnosProbnaRabotaExecute
    end
    object aPResenieMiruvanje_StrucnoUsovrsuvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1089#1090#1088#1091#1095#1085#1086' '#1091#1089#1086#1074#1088#1096#1091#1074#1072#1114#1077
      ImageIndex = 19
      OnExecute = aPResenieMiruvanje_StrucnoUsovrsuvanjeExecute
    end
    object aDResenieMiruvanje_StrucnoUsovrsuvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1089#1090#1088#1091#1095#1085#1086' '#1091#1089#1086#1074#1088#1096#1091#1074#1072#1114#1077
      OnExecute = aDResenieMiruvanje_StrucnoUsovrsuvanjeExecute
    end
    object aPResenieMiruvanje_IzborFunkcija: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1092#1091#1085#1082#1094#1080#1112#1072
      ImageIndex = 19
      OnExecute = aPResenieMiruvanje_IzborFunkcijaExecute
    end
    object aDResenieMiruvanje_IzborFunkcija: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1084#1080#1088#1091#1074#1072#1114#1077' - '#1079#1072' '#1080#1079#1073#1086#1088' '#1085#1072' '#1092#1091#1085#1082#1094#1080#1112#1072
      OnExecute = aDResenieMiruvanje_IzborFunkcijaExecute
    end
    object aPResenieProdolzuvanjeMiruvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1084#1080#1088#1091#1074#1072#1114#1077
      ImageIndex = 19
      OnExecute = aPResenieProdolzuvanjeMiruvanjeExecute
    end
    object aDResenieProdolzuvanjeMiruvanje: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1077' '#1079#1072' '#1087#1088#1086#1076#1086#1083#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1084#1080#1088#1091#1074#1072#1114#1077
      OnExecute = aDResenieProdolzuvanjeMiruvanjeExecute
    end
    object aAdvokatskaKancelarija: TAction
      Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      ImageIndex = 75
      OnExecute = aAdvokatskaKancelarijaExecute
    end
    object aAdvokatskoDruzstvo: TAction
      Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      ImageIndex = 44
      OnExecute = aAdvokatskoDruzstvoExecute
    end
    object aZpisiReport: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZpisiReportExecute
    end
    object aOtkaziReport: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziReportExecute
    end
    object aCopyAK: TAction
      ImageIndex = 8
      OnExecute = aCopyAKExecute
    end
    object aCopyAKVOLK: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1040#1050
      ImageIndex = 8
      OnExecute = aCopyAKVOLKExecute
    end
    object actPorodino: TAction
      Caption = #1055#1086#1088#1086#1076#1080#1083#1085#1086' '#1073#1086#1083#1077#1076#1091#1074#1072#1114#1077
      ImageIndex = 8
      OnExecute = actPorodinoExecute
    end
    object actKreirajAKancelarija: TAction
      ImageIndex = 76
      OnExecute = actKreirajAKancelarijaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 264
    Top = 488
    object dxComponentPrinter1Link1: TdxGridReportLink
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 680
    Top = 520
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 360
    Top = 638
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmMat.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 44
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 49
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        SortIndex = 0
        SortOrder = soAscending
        Width = 345
      end
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 136
    Top = 529
    object N4: TMenuItem
      Action = aDResZapisVoRegistar
    end
    object N3: TMenuItem
      Action = aDSvecenaZakletvaAdvokat
    end
    object N2: TMenuItem
      Action = aDPotvrdaZaZapisVoRegistar
    end
    object N12: TMenuItem
      Action = aDPEObrazec
    end
    object N13: TMenuItem
      Action = aDPropratnoPismoCRM
    end
    object N14: TMenuItem
      Action = aDResenieMiruvanje_Bolest
    end
    object N15: TMenuItem
      Action = aDResenieMiruvanje_RabOdnosNeOpredeleno
    end
    object N16: TMenuItem
      Action = aDResenieMiruvanje_RabOdnosOpredeleno
    end
    object N17: TMenuItem
      Action = aDResenieMiruvanje_RabOdnosProbnaRabota
    end
    object N18: TMenuItem
      Action = aDResenieMiruvanje_StrucnoUsovrsuvanje
    end
    object N19: TMenuItem
      Action = aDResenieMiruvanje_IzborFunkcija
    end
    object N20: TMenuItem
      Action = aDResenieProdolzuvanjeMiruvanje
    end
    object N5: TMenuItem
      Action = aDResOdSamostoenVoVrabotenVoAK
    end
    object N6: TMenuItem
      Action = aDOdVrabotenKajAdvokatVoSamostoen
    end
    object N7: TMenuItem
      Action = aDResBrisiAdvokatImenikOtkazuvanje
    end
    object N21: TMenuItem
      Action = aDResBrisiAdvokatImenikSmrt
    end
    object N8: TMenuItem
      Action = aDResMiruvanje
    end
    object N9: TMenuItem
      Action = aDResMiruvanjeProdolzeno
    end
    object N11: TMenuItem
      Action = aDResPromenaPrezime
    end
    object N10: TMenuItem
      Action = aDResOdSamostoenVoVrabotenVoAK
    end
  end
end
