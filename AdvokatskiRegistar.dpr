program AdvokatskiRegistar;

uses
  Vcl.Forms,
  dmUnit in 'dmUnit.pas' {dm: TDataModule},
  Master in '..\Share2010\Master.pas' {frmMaster},
  Login in '..\Share2010\Login.pas' {frmLogin},
  dmSystem in '..\Share2010\dmSystem.pas' {dmSys: TDataModule},
  dmResources in '..\Share2010\dmResources.pas' {dmRes: TDataModule},
  dmMaticni in '..\Share2010\dmMaticni.pas' {dmMat: TDataModule},
  dmKonekcija in '..\Share2010\dmKonekcija.pas' {dmKon: TDataModule},
  Main in 'Main.pas' {frmMain},
  DaNe in '..\Share2010\DaNe.pas' {frmDaNe},
  cxConstantsMak in '..\Share2010\cxConstantsMak.pas',
  MK in '..\Share2010\MK.pas' {frmMK},
  Mesto in '..\Share2010\Mesto.pas' {frmMesto},
  ResenijaAdvokati in 'ResenijaAdvokati.pas' {frmResenijaAdvokati},
  RegAdvokatskiKancelarii in 'RegAdvokatskiKancelarii.pas' {frmAdvokatskiKancelarii},
  RegAdvokatskiDrustva in 'RegAdvokatskiDrustva.pas' {frmAdvokatskiDrustva},
  RegAdvokati in 'RegAdvokati.pas' {frmRegAdvokati},
  TipResenie in 'TipResenie.pas' {frmTipResenie},
  RegPripravnici in 'RegPripravnici.pas' {frmRegPripravnici},
  AdvokatskiZaednici in 'AdvokatskiZaednici.pas' {frmAdvokatskaZaednica},
  Utils in '..\Share2010\Utils.pas',
  ClenarinaIznos in 'ClenarinaIznos.pas' {frmClenarinaIznos},
  Zadolzuvanje in 'Zadolzuvanje.pas' {frmZadolzuvanje},
  UplataStavki in 'UplataStavki.pas' {frmUplataStavki},
  ImportExcel in 'ImportExcel.pas' {frmImportExcel},
  UplataExcel in 'UplataExcel.pas' {frmUplatiExcel},
  UplataFromExcel in 'UplataFromExcel.pas' {frmUplatiFromExcel},
  Kazni in 'Kazni.pas' {frmKazni},
  TipDisciplinskiMerki in 'TipDisciplinskiMerki.pas' {frmTipDisciplinskiMerki},
  DisciplinskiMerki in 'DisciplinskiMerki.pas' {frmDisciplinskiMerki},
  Opstina in '..\Share2010\Opstina.pas' {frmOpstina},
  Setup in 'Setup.pas' {frmSetup},
  UplataPoedinecno in 'UplataPoedinecno.pas' {frmUplataPoedinecna},
  PocetnaSostojba in 'PocetnaSostojba.pas' {frmPocetnaSostojba},
  PretsedatelKomisija in 'PretsedatelKomisija.pas' {frmPretsedatelKomisija},
  RegStrucniSorabotnici in 'RegStrucniSorabotnici.pas' {frmRegStrucniSorabotnici},
  ResenijaAD in 'ResenijaAD.pas' {frmResenijaAD},
  AdvokatiOsiguruvanje in 'AdvokatiOsiguruvanje.pas' {frmAdvokatiOsiguruvanje},
  OsiguritelniKompanii in 'OsiguritelniKompanii.pas' {frmOsiguritelniKompanii},
  Porodilno in 'Porodilno.pas' {frmPorodilno};

{$R *.res}

begin
  Application.Initialize;

  Application.Title := '���������� ��������';
  Application.CreateForm(TdmMat, dmMat);
  Application.CreateForm(TdmRes, dmRes);
  Application.CreateForm(TdmKon, dmKon);
  Application.CreateForm(TdmSys, dmSys);
  dmKon.aplikacija:='ADV';


  if (dmKon.odbrana_baza and TfrmLogin.Execute) then
  begin
    Application.CreateForm(Tdm, dm);
    Application.CreateForm(TfrmMain, frmMain);
    Application.Run;
  end

  else
  begin
    dmMat.Free;
    dmRes.Free;
    dmKon.Free;
    dmSys.Free;
  end;
end.
