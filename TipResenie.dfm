inherited frmTipResenie: TfrmTipResenie
  Caption = #1058#1080#1087' '#1085#1072' '#1088#1077#1096#1077#1085#1080#1077
  ClientHeight = 635
  ClientWidth = 722
  ExplicitWidth = 738
  ExplicitHeight = 674
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 722
    Height = 236
    ExplicitWidth = 722
    ExplicitHeight = 236
    inherited cxGrid1: TcxGrid
      Width = 718
      Height = 232
      ExplicitWidth = 718
      ExplicitHeight = 232
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsTipResenija
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 47
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 400
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 100
        end
        object cxGrid1DBTableView1PROMENA_PODATOCI: TcxGridDBColumn
          DataBinding.FieldName = 'PROMENA_PODATOCI'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1044#1072
              ImageIndex = 12
              Value = 1
            end
            item
              Description = #1053#1077
              ImageIndex = 8
              Value = 0
            end>
          Width = 120
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1040#1082#1090#1080#1074#1077#1085
              ImageIndex = 2
              Value = 1
            end
            item
              Description = #1042#1086' '#1084#1080#1088#1091#1074#1072#1114#1077
              ImageIndex = 4
              Value = 2
            end
            item
              Description = #1048#1079#1073#1088#1080#1096#1072#1085
              ImageIndex = 3
              Value = 3
            end
            item
              Description = #1053#1077#1084#1072' '#1087#1088#1086#1084#1077#1085#1072
              Value = 0
            end>
          Width = 112
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 362
    Width = 722
    Height = 250
    ExplicitTop = 362
    ExplicitWidth = 722
    ExplicitHeight = 250
    inherited Label1: TLabel
      Left = 533
      Top = 9
      Visible = False
      ExplicitLeft = 533
      ExplicitTop = 9
    end
    object Label2: TLabel [1]
      Left = 37
      Top = 24
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 44
      Top = 50
      Width = 33
      Height = 13
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 585
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsTipResenija
      TabOrder = 1
      Visible = False
      ExplicitLeft = 585
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 590
      Top = 210
      TabOrder = 6
      ExplicitLeft = 590
      ExplicitTop = 210
    end
    inherited ZapisiButton: TcxButton
      Left = 509
      Top = 210
      TabOrder = 5
      ExplicitLeft = 509
      ExplicitTop = 210
    end
    object NAZIV: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 21
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsTipResenija
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 582
    end
    object OPIS: TcxDBMemo
      Left = 83
      Top = 48
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsTipResenija
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 41
      Width = 582
    end
    object cxDBRadioGroupPROMENA_PODATOCI: TcxDBRadioGroup
      Left = 83
      Top = 95
      TabStop = False
      Caption = #1044#1072#1083#1080' '#1088#1077#1096#1077#1085#1080#1077#1090#1086' '#1074#1088#1096#1080' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
      DataBinding.DataField = 'PROMENA_PODATOCI'
      DataBinding.DataSource = dm.dsTipResenija
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1053#1077
          Value = 0
        end
        item
          Caption = #1044#1072
          Value = 1
        end>
      TabOrder = 3
      Height = 45
      Width = 582
    end
    object cxDBRadioGroupSTATUS: TcxDBRadioGroup
      Left = 83
      Top = 146
      TabStop = False
      Caption = #1056#1077#1096#1077#1085#1080#1077#1090#1086' '#1116#1077' '#1080#1079#1074#1088#1096#1080' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089#1086#1090' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
      DataBinding.DataField = 'STATUS'
      DataBinding.DataSource = dm.dsTipResenija
      Properties.Columns = 4
      Properties.Items = <
        item
          Caption = #1057#1090#1072#1090#1091#1089#1086#1090' '#1085#1077' '#1089#1077' '#1084#1077#1085#1091#1074#1072
          Value = 0
        end
        item
          Caption = #1040#1082#1090#1080#1074#1077#1085
          Value = 1
        end
        item
          Caption = #1042#1086' '#1084#1080#1088#1091#1074#1072#1114#1077
          Value = 2
        end
        item
          Caption = #1048#1079#1073#1088#1080#1096#1072#1085
          Value = 3
        end>
      TabOrder = 4
      Height = 50
      Width = 582
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 722
    ExplicitWidth = 722
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 612
    Width = 722
    ExplicitTop = 612
    ExplicitWidth = 722
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 42100.501253125000000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
