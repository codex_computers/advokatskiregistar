unit RegAdvokati;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions, cxNavigator, cxImageComboBox,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, FIBDataSet,
  pFIBDataSet, OsiguruvanjeAdvokat;
//  dxPScxSSLnk;

type
//  niza = Array[1..5] of Variant;

  TfrmRegAdvokati = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    radioStatus: TdxBar;
    radioGroupStatus: TcxBarEditItem;
    dPanel: TPanel;
    cxBarEditItem3: TcxBarEditItem;
    Label9: TLabel;
    cxGroupBoxPodatociKontakt: TcxGroupBox;
    cxGroupBoxOsnovniPodatoci: TcxGroupBox;
    IME: TcxDBTextEdit;
    Label5: TLabel;
    PREZIME: TcxDBTextEdit;
    Label4: TLabel;
    cxGroupBoxLicenca: TcxGroupBox;
    LICENCA_DATUM: TcxDBDateEdit;
    Label3: TLabel;
    LICENCA: TcxDBTextEdit;
    Label2: TLabel;
    ZapisiButton: TcxButton;
    cxButtonOtkazi: TcxButton;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPripravnici: TAction;
    aStrucniSorabotnici: TAction;
    Label1: TLabel;
    EMBG: TcxDBTextEdit;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_FIRMA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    VrabotenVo: TcxGroupBox;
    ADVOKATSKO_DRUSTVO_NAZIV: TcxDBLookupComboBox;
    Label14: TLabel;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton20: TdxBarLargeButton;
    aResIzbranAdvokat: TAction;
    cxGrid1DBTableView1RESENIE_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    aCopyAD: TAction;
    aNovaAdvokatskaKancelarija: TAction;
    aPotvrdaZaZapisVoRegistar: TAction;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton1: TdxBarButton;
    VrabotenVoAK: TcxGroupBox;
    Label13: TLabel;
    ADVOKATSKA_KANCELARIJA_NAZIV: TcxDBLookupComboBox;
    cxButton2: TcxButton;
    aKreirajAK: TAction;
    cxRadioGroupStatus: TcxDBRadioGroup;
    cxGrid1DBTableView1TIP_RESENIE_ID: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1EMBS: TcxGridDBColumn;
    cxGrid1DBTableView1IMPORT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1FLAG_TOCEN: TcxGridDBColumn;
    cxGrid1DBTableView1BR_DISCIPLINSKI: TcxGridDBColumn;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    aDisciplinskiMerki: TAction;
    MESTO_LK_NAZIV: TcxDBLookupComboBox;
    MESTO_LK: TcxDBTextEdit;
    Label16: TLabel;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    aAdvokatskaKancelarija: TAction;
    aAdvokatskoDrustvo: TAction;
    aCopyAK: TAction;
    cxGroupBoxAK: TcxGroupBox;
    Label18: TLabel;
    MESTO: TcxDBTextEdit;
    MESTO_NAZIV: TcxDBLookupComboBox;
    Label10: TLabel;
    ADRESA: TcxDBTextEdit;
    Label7: TLabel;
    MOBILEN_TELEFON: TcxDBTextEdit;
    Label6: TLabel;
    TELEFON: TcxDBTextEdit;
    Label8: TLabel;
    EMAIL: TcxDBTextEdit;
    WEB_STRANA: TcxDBTextEdit;
    Label12: TLabel;
    MOBILEN_TELEFON_LK: TcxDBTextEdit;
    Label20: TLabel;
    TELEFON_LK: TcxDBTextEdit;
    Label17: TLabel;
    Label21: TLabel;
    EMAIL_LK: TcxDBTextEdit;
    WEB_STRANA_LK: TcxDBTextEdit;
    Label22: TLabel;
    aBrisiAK: TAction;
    cxButton3: TcxButton;
    cxButton1: TcxButton;
    cxButton4: TcxButton;
    dxBarManager1Bar9: TdxBar;
    dxbrlrgbtn1: TdxBarLargeButton;
    actIzvestajZaClenarina: TAction;
    pm1: TPopupMenu;
    actDIzvestajZaClenarina: TAction;
    dxbrlrgbtn2: TdxBarLargeButton;
    actPorodilno: TAction;
    dxbrlrgbtn3: TdxBarLargeButton;
    actOsiguruvanje: TAction;
    lbl1: TLabel;
    ADVOKATSKA_ZAEDNICA: TcxDBTextEdit;
    ADVOKATSKA_ZAEDNICA_NAZIV: TcxDBLookupComboBox;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure radioGroupStatusChange(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure dxBarLargeButton19Click(Sender: TObject);
    procedure aPripravniciExecute(Sender: TObject);
    procedure aStrucniSorabotniciExecute(Sender: TObject);
    procedure WEB_STRANAPropertiesChange(Sender: TObject);
    procedure aResIzbranAdvokatExecute(Sender: TObject);
    procedure EMBGPropertiesValidate(Sender: TObject; var DisplayValue: Variant;
      var ErrorText: TCaption; var Error: Boolean);
    function proverkaModul11(broj: Int64): boolean;
    procedure aCopyADExecute(Sender: TObject);
    procedure aNovaAdvokatskaKancelarijaExecute(Sender: TObject);
    procedure aKreirajAKExecute(Sender: TObject);
    procedure aPotvrdaZaZapisVoRegistarExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FLAG_TOCENPropertiesChange(Sender: TObject);
    procedure aDisciplinskiMerkiExecute(Sender: TObject);
    procedure aAdvokatskaKancelarijaExecute(Sender: TObject);
    procedure aAdvokatskoDrustvoExecute(Sender: TObject);
    procedure aCopyAKExecute(Sender: TObject);
    procedure cxRadioGroupStatusPropertiesEditValueChanged(Sender: TObject);
    procedure aPopolniPodatociAK(Sender: TObject);
    procedure aBrisiAKExecute(Sender: TObject);
    procedure EnableActions;
    procedure DisableActions;
    procedure actIzvestajZaClenarinaExecute(Sender: TObject);
    procedure actDIzvestajZaClenarinaExecute(Sender: TObject);
    procedure actPorodilnoExecute(Sender: TObject);
    procedure actOsiguruvanjeExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmRegAdvokati: TfrmRegAdvokati;
  rData : TRepositoryData;
  insert_ak:Integer;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni,
  Mesto,  RegPripravnici, ResenijaAdvokati, RegStrucniSorabotnici,
  cxConstantsMak, AdvokatskiZaednici, RegAdvokatskiKancelarii,
  DisciplinskiMerki, RegAdvokatskiDrustva, Porodilno, AdvokatiOsiguruvanje;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmRegAdvokati.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmRegAdvokati.aNovaAdvokatskaKancelarijaExecute(Sender: TObject);
begin
//
end;

procedure TfrmRegAdvokati.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    LICENCA.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    DisableActions;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmRegAdvokati.aAdvokatskaKancelarijaExecute(Sender: TObject);
begin
  frmAdvokatskiKancelarii := TfrmAdvokatskiKancelarii.Create(self,false);
  frmAdvokatskiKancelarii.Tag:=3;
  frmAdvokatskiKancelarii.ShowModal;
  frmAdvokatskiKancelarii.Free;
end;

procedure TfrmRegAdvokati.aAdvokatskoDrustvoExecute(Sender: TObject);
begin
  frmAdvokatskiDrustva := TfrmAdvokatskiDrustva.Create(self,false);
  frmAdvokatskiDrustva.Tag:=3;
  frmAdvokatskiDrustva.ShowModal;
  frmAdvokatskiDrustva.Free;
end;

procedure TfrmRegAdvokati.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dm.qCountResenija.Close;
    dm.qCountResenija.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
    dm.qCountResenija.ExecQuery;
    if dm.qCountResenija.FldByName['br'].Value = 0 then
       begin
         dPanel.Enabled:=True;
         lPanel.Enabled:=False;
         LICENCA.SetFocus;
         cxGrid1DBTableView1.DataController.DataSet.Edit;
         DisableActions;
       end
    else  ShowMessage('��� ����� ������ �� �� ������� ���弝� �� �������� ������� !!!');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmRegAdvokati.aBrisiAKExecute(Sender: TObject);
begin
           dm.qDeleteAK_PoID.Close;
           dm.qDeleteAK_PoID.ParamByName('AK_ID').Value:=dm.tblAdvokatiADVOKATSKA_KANCELARIJA.Value;
           dm.qDeleteAK_PoID.ExecQuery;
           DM.tblAdvokatskiKancelarii.FullRefresh;
end;

procedure TfrmRegAdvokati.aBrisiExecute(Sender: TObject);
var k_id:Integer;
begin
    dm.qCountResenija.Close;
    dm.qCountResenija.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
    dm.qCountResenija.ExecQuery;
    if dm.qCountResenija.FldByName['br'].Value = 0 then
       begin
         if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
            (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
           begin
            cxGrid1DBTableView1.DataController.DataSet.Delete();
            DM.tblAdvokatskiKancelarii.FullRefresh;
           end;
       end
      else  ShowMessage('��� ����� ������ �� �� ����� ���弝� �� �������� ������� !!!');

end;

procedure TfrmRegAdvokati.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmRegAdvokati.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmRegAdvokati.aResIzbranAdvokatExecute(Sender: TObject);
begin
  frmResenijaAdvokati := TfrmResenijaAdvokati.Create(self,false);
  frmResenijaAdvokati.Tag:=1;
  frmResenijaAdvokati.ShowModal;
  frmResenijaAdvokati.Free;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmRegAdvokati.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmRegAdvokati.aKreirajAKExecute(Sender: TObject);
begin
  //frmAdvokatskiKancelarii:=TfrmAdvokatskiKancelarii.Create(self,false);
  //frmAdvokatskiKancelarii.tag:=1;
  //frmAdvokatskiKancelarii.ShowModal;
  //frmAdvokatskiKancelarii.Free;
  if ((LICENCA_DATUM.Text <> '')and (LICENCA.Text<> ''){and (IME.Text <> '')and (PREZIME.Text<>'')})then
     begin
        dm.qNajdiAK.Close;
        dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblAdvokatiIME.Value+' '+dm.tblAdvokatiPREZIME.Value;
        dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblAdvokatiLICENCA.Value;
        dm.qNajdiAK.ExecQuery;

        if dm.qNajdiAK.FldByName['br'].IsNull then
           begin
             dm.qInsertAK.Close;
             dm.qInsertAK.ParamByName('NAZIV').Value:=dm.tblAdvokatiIME.Value +' '+ dm.tblAdvokatiPREZIME.Value;
             dm.qInsertAK.ParamByName('BROJ').Value:=dm.tblAdvokatiLICENCA.Value;
             dm.qInsertAK.ParamByName('DATUM').Value:=dm.tblAdvokatiLICENCA_DATUM.Value;

             dm.qInsertAK.ParamByName('MESTO').Value:=dm.tblAdvokatiMESTO.Value;
             dm.qInsertAK.ParamByName('ADRESA').Value:=dm.tblAdvokatiADRESA.Value;
             dm.qInsertAK.ParamByName('MOBILEN_TELEFON').Value:=dm.tblAdvokatiMOBILEN_TELEFON.Value;
             dm.qInsertAK.ParamByName('TELEFON').Value:=dm.tblAdvokatiTELEFON.Value;
             dm.qInsertAK.ParamByName('EMAIL').Value:=dm.tblAdvokatiEMAIL.Value;
             dm.qInsertAK.ParamByName('WEB_STRANA').Value:=dm.tblAdvokatiWEB_STRANA.Value;

             dm.qInsertAK.ParamByName('DATUM_OD').Value:=null;
             dm.qInsertAK.ParamByName('DATUM_DO').Value:=null;
             dm.qInsertAK.ExecQuery;

             insert_ak:=1;

             dm.tblAdvokatskiKancelarii.FullRefresh;

             dm.qNajdiAK.Close;
             dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblAdvokatiIME.Value+' '+dm.tblAdvokatiPREZIME.Value;
             dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblAdvokatiLICENCA.Value;
             dm.qNajdiAK.ExecQuery;
             if dm.qNajdiAK.FldByName['br'].Value >0 then
                dm.tblAdvokatiADVOKATSKA_KANCELARIJA.Value:= dm.qNajdiAK.FldByName['id'].Value
             else
                ADVOKATSKA_KANCELARIJA_NAZIV.Clear;

             MESTO_LK.SetFocus;
           end
        else
           begin
             ShowMessage('������������ ���������� � ��� ������� !!!');
           end;
     end
  else ShowMessage('��������� �� ��������� �������� ���, �������, ������� � ����� �� �������');
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmRegAdvokati.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmRegAdvokati.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

function TfrmRegAdvokati.proverkaModul11(broj: Int64): boolean;
var i,tezina, suma, cifra  :integer;
    dolzina, kb : integer;
begin
  suma:=0;
  dolzina:=length(IntToStr(broj));
  for i := 1 to dolzina - 1 do
  begin
    tezina:= (5+i) mod 6+2;
    cifra := StrToInt(copy(IntToStr(broj),dolzina-i,1));
    suma:=suma+tezina*cifra;
  end;
  kb:=11- suma mod 11;
  if( (kb=11) or (kb=10) ) then kb:=0;

  if( kb = StrToInt(copy(IntToStr(broj),dolzina,1))) then  proverkaModul11:=true
  else proverkaModul11:=false;
end;
//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmRegAdvokati.EMBGPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
 if (EMBG.Text <> '') and (cxGrid1DBTableView1.DataController.DataSource.State in [dsInsert, dsEdit]) then
     if proverkaModul11(StrToInt64(EMBG.Text)) = false then
        begin
           Error:=true;
           ErrorText:='��������� ������� ��� �� � ������� !!!';
           EMBG.SetFocus;
        end
     else  Error:=false;
end;

procedure TfrmRegAdvokati.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin

         if ((Sender = MESTO) or (Sender = MESTO_NAZIV)) then
             begin
               frmMesto:=TfrmMesto.Create(self,false);
               frmMesto.ShowModal;
               if (frmMesto.ModalResult = mrOK) then
                    dm.tblAdvokatiMESTO.Value := StrToInt(frmMesto.GetSifra(0));
                frmMesto.Free;
             end

          else if ((Sender = ADVOKATSKA_ZAEDNICA) or (Sender = ADVOKATSKA_ZAEDNICA_NAZIV)) then
             begin
               frmAdvokatskaZaednica:=TfrmAdvokatskaZaednica.Create(self,false);
               frmAdvokatskaZaednica.ShowModal;
               if (frmAdvokatskaZaednica.ModalResult = mrOK) then
                    dm.tblAdvokatiADVOKATSKA_ZAEDNICA.Value := StrToInt(frmAdvokatskaZaednica.GetSifra(0));
                frmMesto.Free;
             end
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;


//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmRegAdvokati.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if(Sender = EMAIL)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);

  if(Sender = WEB_STRANA)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);

  if(Sender = EMAIL_LK)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);

  if(Sender = WEB_STRANA_LK)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmRegAdvokati.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if(Sender = EMAIL)then
            ActivateKeyboardLayout($042F042F, KLF_REORDER)
         else if(Sender = WEB_STRANA)then
            ActivateKeyboardLayout($042F042F, KLF_REORDER)
         else if (Sender = LICENCA) and (LICENCA.Text <> '') then
            begin
                   dm.qProveriLicenca.Close;
                   dm.qProveriLicenca.ParamByName('licenca').Value:=dm.tblAdvokatiLICENCA.Value;
                   dm.qProveriLicenca.ParamByName('adv_id').Value:=dm.tblAdvokatiID.Value;
                   dm.qProveriLicenca.ExecQuery;
                   if (((dm.qProveriLicenca.FldByName['br'].Value >0)and (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert))
                     or ((dm.qProveriLicenca.FldByName['br_edit'].Value >0)and (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit)))then
                      begin
                        ShowMessage
                        ('����� ������������ '+intToStr(dm.qProveriLicenca.FldByName['br'].Value)+' �������� �� ������ �������.'+sLineBreak+'������� '+dm.qProveriLicenca.FldByName['advokat_naziv'].AsString);
                        LICENCA.SetFocus;
                      end
//                   else
//                      begin
//                        if(cxRadioGroupStatus.EditValue = 1) then
//                          begin
//                            dm.qNajdiAK.Close;
//                            dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblAdvokatiIME.Value+' '+dm.tblAdvokatiPREZIME.Value;
//                            dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblAdvokatiLICENCA.Value;
//                            dm.qNajdiAK.ExecQuery;
//                            if dm.qNajdiAK.FldByName['br'].Value >0 then
//                               dm.tblAdvokatiADVOKATSKA_KANCELARIJA.Value:= dm.qNajdiAK.FldByName['id'].Value
//                            else
//                               ADVOKATSKA_KANCELARIJA_NAZIV.Clear;
//                          end;
//                      end;
            end ;
       end;
end;

procedure TfrmRegAdvokati.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.EditValue <>null) and (sifra.EditValue<>null)  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

procedure TfrmRegAdvokati.cxGrid1DBTableView1FLAG_TOCENPropertiesChange(
  Sender: TObject);
begin
     dm.tblAdvokati.Edit;
     dm.tblAdvokati.Post;
end;

procedure TfrmRegAdvokati.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin

end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmRegAdvokati.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmRegAdvokati.WEB_STRANAPropertiesChange(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmRegAdvokati.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmRegAdvokati.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmRegAdvokati.prefrli;
begin
end;

procedure TfrmRegAdvokati.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmRegAdvokati.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmRegAdvokati.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
   procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
   procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
   sobrano := true;

   radioGroupStatus.EditValue:=1;
   insert_ak:=0;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmRegAdvokati.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmRegAdvokati.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmRegAdvokati.cxRadioGroupStatusPropertiesEditValueChanged(
  Sender: TObject);
begin
     if cxRadioGroupStatus.EditValue = 1 then
        begin
          VrabotenVo.Enabled:=False;
          VrabotenVoAK.Enabled:=True;
          cxButton2.Enabled:=True;
          cxButton4.Enabled:=True;
          ADVOKATSKA_KANCELARIJA_NAZIV.Enabled:=False;
          cxGroupBoxAK.Enabled:=True;
          cxGroupBoxAK.Caption := '�������� �� ���������� ����������';
        end
     else if cxRadioGroupStatus.EditValue = 2 then
        begin
          VrabotenVo.Enabled:=True;
          VrabotenVoAK.Enabled:=False;
          cxButton2.Enabled:=True;
          cxButton4.Enabled:=True;
          ADVOKATSKA_KANCELARIJA_NAZIV.Enabled:=True;
          cxGroupBoxAK.Enabled:=False;
          cxGroupBoxAK.Caption := '�������� �� ���������� �������';
        end
     else if cxRadioGroupStatus.EditValue = 3 then
        begin
          VrabotenVo.Enabled:=True;
          VrabotenVoAK.Enabled:=False;
          cxButton2.Enabled:=True;
          cxButton4.Enabled:=True;
          ADVOKATSKA_KANCELARIJA_NAZIV.Enabled:=True;
          cxGroupBoxAK.Enabled:=False;
          cxGroupBoxAK.Caption := '�������� �� ���������� �������';
        end
     else if cxRadioGroupStatus.EditValue = 4 then
        begin
          VrabotenVo.Enabled:=False;
          VrabotenVoAK.Enabled:=True;
          cxButton2.Enabled:=False;
          cxButton4.Enabled:=False ;
          ADVOKATSKA_KANCELARIJA_NAZIV.Enabled:=True;
          cxGroupBoxAK.Enabled:=False;
          cxGroupBoxAK.Caption := '�������� �� ���������� ����������';
        end;

 if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
   begin
   if ((cxRadioGroupStatus.EditValue = 2)or (cxRadioGroupStatus.EditValue = 3)) then
     begin
       ADVOKATSKA_KANCELARIJA_NAZIV.Clear;
     end
   else
     begin
       ADVOKATSKO_DRUSTVO_NAZIV.Clear;
     end;

   if((IME.Text <> '')and(PREZIME.Text <> '')and(cxRadioGroupStatus.EditValue = 1)) then
      begin
        dm.qNajdiAK.Close;
        //dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblAdvokatiIME.Value+' '+dm.tblAdvokatiPREZIME.Value;
        dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblAdvokatiLICENCA.Value;
        dm.qNajdiAK.ExecQuery;
        if dm.qNajdiAK.FldByName['br'].Value >0 then
           dm.tblAdvokatiADVOKATSKA_KANCELARIJA.Value:= dm.qNajdiAK.FldByName['id'].Value
        else
           ADVOKATSKA_KANCELARIJA_NAZIV.Clear;
      end;
   end;
end;

procedure TfrmRegAdvokati.dxBarLargeButton19Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmRegAdvokati.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dm.tblAdvokatskiKancelarii.FullRefresh;
        cxGrid1DBTableView1.DataController.DataSet.Refresh;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
        EnableActions;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmRegAdvokati.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin

      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;

      EnableActions;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmRegAdvokati.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmRegAdvokati.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmRegAdvokati.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmRegAdvokati.aPotvrdaZaZapisVoRegistarExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',1);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmRegAdvokati.aPripravniciExecute(Sender: TObject);
begin
     frmRegPripravnici := TfrmRegPripravnici.Create(self,true);
     frmRegPripravnici.Tag:=1;
     frmRegPripravnici.ShowModal;
     frmRegPripravnici.Free;
end;

procedure TfrmRegAdvokati.aSnimiPecatenjeExecute(Sender: TObject);
begin
 zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmRegAdvokati.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmRegAdvokati.aStrucniSorabotniciExecute(Sender: TObject);
begin
     frmRegStrucniSorabotnici := TfrmRegStrucniSorabotnici.Create(self,true);
     frmRegStrucniSorabotnici.tag:=1;
     frmRegStrucniSorabotnici.ShowModal;
     frmRegStrucniSorabotnici.Free;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmRegAdvokati.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmRegAdvokati.aCopyADExecute(Sender: TObject);
begin
        if (ADVOKATSKO_DRUSTVO_NAZIV.Text <> '') then
       begin
         dm.qADpodatoci.Close;
         dm.qADpodatoci.ParamByName('id').Value:=dm.tblAdvokatiADVOKATSKO_DRUSTVO.Value;
         dm.qADpodatoci.ExecQuery;

         if not dm.qADpodatoci.FldByName['mesto'].IsNull then
            dm.tblAdvokatiMESTO.Value:=dm.qADpodatoci.FldByName['mesto'].Value
         else
            dm.tblAdvokatiMESTO.Clear;
         if not dm.qADpodatoci.FldByName['adresa'].IsNull then
            dm.tblAdvokatiADRESA.Value:=dm.qADpodatoci.FldByName['adresa'].Value
         else
            dm.tblAdvokatiADRESA.Clear;
         if not dm.qADpodatoci.FldByName['telefon'].IsNull then
            dm.tblAdvokatiTELEFON.Value:=dm.qADpodatoci.FldByName['telefon'].Value
         else
            dm.tblAdvokatiTELEFON.Clear;
         if not dm.qADpodatoci.FldByName['email'].IsNull then
            dm.tblAdvokatiEMAIL.Value:=dm.qADpodatoci.FldByName['email'].Value
         else
            dm.tblAdvokatiEMAIL.Clear;
       end;
end;

procedure TfrmRegAdvokati.aCopyAKExecute(Sender: TObject);
begin
 if ((ADVOKATSKA_KANCELARIJA_NAZIV.Text <> '') or (ADVOKATSKO_DRUSTVO_NAZIV.Text <> '')) then
       begin

//  Na nivno baranje e iskluceno (mada mislam deka ke se pismanat) 15.11.2016
//         if not dm.tblAdvokatiMESTO.IsNull then
//            dm.tblAdvokatiMESTO_LK.Value:=dm.tblAdvokatiMESTO.Value
//         else
//            dm.tblAdvokatiMESTO_LK.Clear;
         if not dm.tblAdvokatiMOBILEN_TELEFON.IsNull then
            dm.tblAdvokatiMOBILEN_TELEFON_LK.Value:=dm.tblAdvokatiMOBILEN_TELEFON.Value
         else
            dm.tblAdvokatiMOBILEN_TELEFON_LK.Clear;
         if not dm.tblAdvokatiTELEFON.IsNull then
            dm.tblAdvokatiTELEFON_LK.Value:=dm.tblAdvokatiTELEFON.Value
         else
            dm.tblAdvokatiTELEFON_LK.Clear;
         if not dm.tblAdvokatiEMAIL.IsNull then
            dm.tblAdvokatiEMAIL_LK.Value:=dm.tblAdvokatiEMAIL.Value
         else
            dm.tblAdvokatiEMAIL_LK.Clear;
         if not dm.tblAdvokatiWEB_STRANA.IsNull then
            dm.tblAdvokatiWEB_STRANA_LK.Value:=dm.tblAdvokatiWEB_STRANA.Value
         else
            dm.tblAdvokatiWEB_STRANA_LK.Clear;
       end
 else ShowMessage('�������� ���������� ���������� ��� ���������� ������� ���� �� ��������!!!');
end;

procedure TfrmRegAdvokati.actDIzvestajZaClenarinaExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',23);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmRegAdvokati.actIzvestajZaClenarinaExecute(Sender: TObject);
begin
 try
        dmRes.Spremi('ADV',23);
        dmKon.tblSqlReport.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmRegAdvokati.actOsiguruvanjeExecute(Sender: TObject);
begin
   frmAdvokatiOsiguruvanje := TfrmAdvokatiOsiguruvanje.Create(self,false);
   frmAdvokatiOsiguruvanje.Tag:=1;
   frmAdvokatiOsiguruvanje.ShowModal;
   frmAdvokatiOsiguruvanje.Free;
end;

procedure TfrmRegAdvokati.actPorodilnoExecute(Sender: TObject);
begin
   frmPorodilno := TfrmPorodilno.Create(self,false);
   frmPorodilno.Tag:=1;
   frmPorodilno.ShowModal;
   frmPorodilno.Free;
end;

procedure TfrmRegAdvokati.aDisciplinskiMerkiExecute(Sender: TObject);
begin
   frmDisciplinskiMerki := TfrmDisciplinskiMerki.Create(self,false);
   frmDisciplinskiMerki.Tag:=1;
   frmDisciplinskiMerki.ShowModal;
   frmDisciplinskiMerki.Free;
end;

procedure TfrmRegAdvokati.aPopolniPodatociAK(Sender: TObject);
begin
if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if((cxRadioGroupStatus.EditValue = 1) or (cxRadioGroupStatus.EditValue = 4)) then
            begin
               if ADVOKATSKA_KANCELARIJA_NAZIV.Text <> '' then
                  begin
                          dm.qNajdiAK_poID.Close;
                          dm.qNajdiAK_poID.ParamByName('id').Value:=dm.tblAdvokatiADVOKATSKA_KANCELARIJA.Value;
                          dm.qNajdiAK_poID.ExecQuery;
                          if not dm.qNajdiAK_poID.FldByName['mesto'].IsNull then
                             dm.tblAdvokatiMESTO.Value:=dm.qNajdiAK_poID.FldByName['mesto'].Value
                          else
                             dm.tblAdvokatiMESTO.Clear;
                          if not dm.qNajdiAK_poID.FldByName['adresa'].IsNull  then
                             dm.tblAdvokatiADRESA.Value:=dm.qNajdiAK_poID.FldByName['adresa'].Value
                          else
                             dm.tblAdvokatiADRESA.Clear;
                          if not dm.qNajdiAK_poID.FldByName['telefon'].IsNull  then
                             dm.tblAdvokatiTELEFON.Value:=dm.qNajdiAK_poID.FldByName['telefon'].Value
                          else
                             dm.tblAdvokatiTELEFON.Clear;
                          if not dm.qNajdiAK_poID.FldByName['email'].IsNull then
                             dm.tblAdvokatiEMAIL.Value:=dm.qNajdiAK_poID.FldByName['email'].Value
                          else
                             dm.tblAdvokatiEMAIL.Clear;
                          if not dm.qNajdiAK_poID.FldByName['advokatska_zaednica'].IsNull then
                             dm.tblAdvokatiADVOKATSKA_ZAEDNICA.Value:=dm.qNajdiAK_poID.FldByName['advokatska_zaednica'].Value
                          else
                             dm.tblAdvokatiADVOKATSKA_ZAEDNICA.Clear;
                          if not dm.qNajdiAK_poID.FldByName['mobilen_telefon'].IsNull then
                             dm.tblAdvokatiMOBILEN_TELEFON.Value:=dm.qNajdiAK_poID.FldByName['mobilen_telefon'].Value
                          else
                             dm.tblAdvokatiMOBILEN_TELEFON.Clear;
                          if not dm.qNajdiAK_poID.FldByName['web_strana'].IsNull  then
                             dm.tblAdvokatiWEB_STRANA.Value:=dm.qNajdiAK_poID.FldByName['web_strana'].Value
                          else
                             dm.tblAdvokatiWEB_STRANA.Clear;
                  end;
            end;
       end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmRegAdvokati.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmRegAdvokati.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmRegAdvokati.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


procedure TfrmRegAdvokati.radioGroupStatusChange(Sender: TObject);
begin

       dm.tblAdvokati.Close;
   //    dm.tblAdvokati.ParamByName('advokat').Value:='-1';
//
//       if radioGroupStatus.EditValue = 2 then
//         dm.tblAdvokati.ParamByName('aktiven').Value:=-1;
//       if radioGroupStatus.EditValue = 1 then
//         dm.tblAdvokati.ParamByName('aktiven').Value:=1;
//       if radioGroupStatus.EditValue = 0 then
     //    dm.tblAdvokati.ParamByName('aktiven').Value:='-1';//radioGroupStatus.EditValue;

       dm.tblAdvokati.Open;

end;

procedure TfrmRegAdvokati.EnableActions();
begin
  aNov.Enabled := True;
  aAzuriraj.Enabled := True;
  aBrisi.Enabled := True;
  aRefresh.Enabled := True;
  aPripravnici.Enabled := True;
  aStrucniSorabotnici.Enabled := True;
  aResIzbranAdvokat.Enabled := True;
  aDisciplinskiMerki.Enabled := True;
end;

procedure TfrmRegAdvokati.DisableActions();
begin
  aNov.Enabled := false;
  aAzuriraj.Enabled := false;
  aBrisi.Enabled := false;
  aRefresh.Enabled := false;
  aPripravnici.Enabled := false;
  aStrucniSorabotnici.Enabled := false;
  aResIzbranAdvokat.Enabled := false;
  aDisciplinskiMerki.Enabled := false;
end;

end.
