inherited frmUplataPoedinecna: TfrmUplataPoedinecna
  Caption = #1059#1087#1083#1072#1090#1080
  ClientHeight = 711
  ClientWidth = 828
  ExplicitTop = -69
  ExplicitWidth = 844
  ExplicitHeight = 750
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 828
    Height = 408
    ExplicitWidth = 828
    inherited cxGrid1: TcxGrid
      Width = 824
      Height = 404
      ExplicitWidth = 824
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        DataController.DataSource = dm.dsUplata
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 63
        end
        object cxGrid1DBTableView1ZADOLZUVANJE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ZADOLZUVANJE_ID'
          Visible = False
          Width = 139
        end
        object cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_ID'
          Visible = False
          Width = 180
        end
        object cxGrid1DBTableView1TIP_RESENIE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_RESENIE_ID'
          Width = 127
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Width = 92
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 95
        end
        object cxGrid1DBTableView1IZNOS: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKAT_EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_EMBG'
          Width = 104
        end
        object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA'
          Width = 116
        end
        object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_DATUM'
          Width = 150
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Width = 150
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Width = 150
        end
        object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_naziv'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Width = 150
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 150
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Width = 150
        end
        object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN_TELEFON'
          Width = 150
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 150
        end
        object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
          DataBinding.FieldName = 'WEB_STRANA'
          Width = 150
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 150
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 534
    Width = 828
    ExplicitWidth = 828
    inherited Label1: TLabel
      Left = 623
      Visible = False
      ExplicitLeft = 623
    end
    object Label4: TLabel [1]
      Left = 37
      Top = 24
      Width = 58
      Height = 13
      Caption = #1040#1076#1074#1086#1082#1072#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [2]
      Left = 55
      Top = 105
      Width = 40
      Height = 13
      Caption = #1048#1079#1085#1086#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 50
      Top = 51
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = #1044#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object lbl1: TLabel [4]
      Left = 47
      Top = 78
      Width = 48
      Height = 13
      Caption = #1043#1086#1076#1080#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 675
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsUplata
      TabOrder = 2
      Visible = False
      ExplicitLeft = 675
    end
    inherited OtkaziButton: TcxButton
      Left = 737
      TabOrder = 7
      ExplicitLeft = 737
    end
    inherited ZapisiButton: TcxButton
      Left = 656
      TabOrder = 6
      ExplicitLeft = 656
    end
    object ADVOKAT_ID: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 21
      BeepOnEnter = False
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsUplata
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object ADVOKAT_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 145
      Top = 21
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsUplata
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ADVOKAT_NAZIV'
        end>
      Properties.ListSource = dm.dsAdvokati
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 441
    end
    object IZNOS: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 102
      BeepOnEnter = False
      DataBinding.DataField = 'IZNOS'
      DataBinding.DataSource = dm.dsUplata
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object DATUM: TcxDBDateEdit
      Tag = 1
      Left = 101
      Top = 48
      Hint = #1044#1072#1090#1091#1084
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsUplata
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object GODINA: TcxDBComboBox
      Left = 101
      Top = 75
      DataBinding.DataField = 'GODINA'
      DataBinding.DataSource = dm.dsUplata
      Properties.Items.Strings = (
        '1981'
        '1982'
        '1983'
        '1984'
        '1985'
        '1986'
        '1987'
        '1989'
        '1990'
        '1991'
        '1992'
        '1993'
        '1994'
        '1995'
        '1996'
        '1997'
        '1998'
        '1999'
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 828
    ExplicitWidth = 828
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 688
    Width = 828
    ExplicitWidth = 828
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 232
    Top = 248
  end
  inherited PopupMenu1: TPopupMenu
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    Left = 432
    Top = 248
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 224
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 42426.519447083330000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
