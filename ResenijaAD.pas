﻿unit ResenijaAD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions, cxNavigator, cxImageComboBox,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmResenijaAD = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dPanel: TPanel;
    Label9: TLabel;
    ZapisiButton: TcxButton;
    cxButtonOtkazi: TcxButton;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    PopupMenu2: TPopupMenu;
    cxGroupBoxPodatociResenie: TcxGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    RESENIE_TIP: TcxDBTextEdit;
    RESENIE_TIP_NAZIV: TcxDBLookupComboBox;
    BROJ_RESENIE: TcxDBTextEdit;
    KOMISIJA_PRETSEDATEL: TcxDBTextEdit;
    DATUM_SEDNICA: TcxDBDateEdit;
    cxGroupBoxOsnovniPodatoci: TcxGroupBox;
    Label2: TLabel;
    BROJ: TcxDBTextEdit;
    Label11: TLabel;
    DATUM: TcxDBDateEdit;
    Label1: TLabel;
    NAZIV: TcxDBTextEdit;
    Label3: TLabel;
    MESTO: TcxDBTextEdit;
    MESTO_NAZIV: TcxDBLookupComboBox;
    Label10: TLabel;
    ADRESA: TcxDBTextEdit;
    Label6: TLabel;
    TELEFON: TcxDBTextEdit;
    Label8: TLabel;
    EMAIL: TcxDBTextEdit;
    cxGrid1DBTableView1RESENIE_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1RESENIE_TIP_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1AD_ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_RESENIE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_SEDNICA: TcxGridDBColumn;
    cxGrid1DBTableView1KOMISIJA_PRETSEDATEL: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1REG_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    dxBarSubItem2: TdxBarSubItem;
    cxGroupBoxPeriodVaznost: TcxGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    DATUM_OD: TcxDBDateEdit;
    DATUM_DO: TcxDBDateEdit;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // приватна променлива за чување на шифрата на селектираниот ред

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //пристап до приватната променливата _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmResenijaAD: TfrmResenijaAD;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni,
  Mesto, MK;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmResenijaAD.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	Означува дали формата е повикана за повеќекратно внесување
//	insert = true -> После секој запис автоматски се влегува во Insert Mode
//  insert = false -> После секој запис фокусот се враќа на гридот. За нареден запис треба да се притесне F5
  inserting := insert;
end;

//	Акција за инсертирање. Се исклучува панелот на гридот, се вклучува панелот со едит објектите,
//	се префрла фокусот на првата колона и се влегува во Insert Mode
procedure TfrmResenijaAD.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    RESENIE_TIP.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    if tag = 1 then
       dm.tblResenijaADAD_ID.Value:=dm.tblAdvokatskiDrustvaID.Value;
       dm.tblResenijaADKOMISIJA_PRETSEDATEL.Value := petsedatel_komisija;
       dm.tblResenijaADREG_BROJ.Value := dm.tblAdvokatskiDrustvaBROJ.Value;
       dm.tblResenijaADDATUM.Value := dm.tblAdvokatskiDrustvaDATUM.Value;
       dm.tblResenijaADNAZIV.Value := dm.tblAdvokatskiDrustvaNAZIV.Value;
       dm.tblResenijaADMESTO.Value := dm.tblAdvokatskiDrustvaMESTO.Value;
       dm.tblResenijaADADRESA.Value := dm.tblAdvokatskiDrustvaADRESA.Value;
       dm.tblResenijaADTELEFON.Value := dm.tblAdvokatskiDrustvaTELEFON.Value;
       dm.tblResenijaADMOBILEN_TELEFON.Value := dm.tblAdvokatskiDrustvaMOBILEN_TELEFON.Value;
       dm.tblResenijaADEMAIL.Value := dm.tblAdvokatskiDrustvaEMAIL.Value;
       dm.tblResenijaADWEB_STRANA.Value := dm.tblAdvokatskiDrustvaWEB_STRANA.Value;
  end
  else ShowMessage('Најпрво запишете ги или откажете се од тековните промени,' + sLineBreak + 'а потоа пробајте да внесете нов запис!');
end;

//	Акција за ажурирање. Се исклучува панелот на гридот, се вклучува панелот со едит објектите,
//	се префрла фокусот на првата колона и се влегува во Edit Mode
procedure TfrmResenijaAD.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    RESENIE_TIP.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('Најпрво запишете ги или откажете се од тековните промени,' + sLineBreak + 'а потоа пробајте да внесете нов запис!');
end;

//	Акција за бришење на селектираниот запис
procedure TfrmResenijaAD.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmResenijaAD.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); за pivot ако има
  BrisiFormaIzgled(self);
end;

//	Акција за освежување на податоците
procedure TfrmResenijaAD.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //ресетирање на филтерот
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;


//	Акција за излез од формата со кликање на копчето Излез
procedure TfrmResenijaAD.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	Акција за снимање на изгледот на гридот во база (Utils.pas)
procedure TfrmResenijaAD.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	Акција за експорт на гридот во Excel формат (Utils.pas)
procedure TfrmResenijaAD.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  Процедура за движење на фокусот во контролите со помош на Enter
procedure TfrmResenijaAD.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin

         if ((Sender = MESTO) or (Sender = MESTO_NAZIV)) then
             begin
               frmMesto:=TfrmMesto.Create(self,false);
               frmMesto.ShowModal;
               if (frmMesto.ModalResult = mrOK) then
                    dm.tblResenijaADMESTO.Value := StrToInt(frmMesto.GetSifra(0));
                frmMesto.Free;
             end
         end
          //  за нурко контрола
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;

end;


//	Промена на боја на едитот при добивање на фокус
procedure TfrmResenijaAD.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if(Sender = EMAIL)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

//	Промена на боја на едитот при губење на фокус
procedure TfrmResenijaAD.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if(Sender = EMAIL)then
            ActivateKeyboardLayout($042F042F, KLF_REORDER)
       end;
end;

procedure TfrmResenijaAD.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.EditValue <>null) and (sifra.EditValue<>null)  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

procedure TfrmResenijaAD.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin

end;

//  Процедура за чување на вредноста на примарниот клуч од табелата
//  Параметри се: br - реден број на полето (од 0 до 9) во зависност од бројот на полиња во примарниот клуч
//  s - вредноста на полето од клучот (претворена во string во случај на integer со IntToStr функцијата)
procedure TfrmResenijaAD.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  Функција за читање на вредноста на примарниот клуч од табелата
//  Параметри се: br - реден број на полето (од 0 до 9) во зависност од бројот на полиња во примарниот клуч
function TfrmResenijaAD.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmResenijaAD.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  Се користи во формите кои наследуваат од Master-от за сетирање на вредноста на клучот преку SetSifra
procedure TfrmResenijaAD.prefrli;
begin
end;

procedure TfrmResenijaAD.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	Валидација при затварање на формата
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, 'Незапишани податоци', 'Податоците не се запишани. Дали сакате да се запишат?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
   dmRes.FreeRepository(rData);
end;
procedure TfrmResenijaAD.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmResenijaAD.FormShow(Sender: TObject);
begin
  //  Прочитај ги евентуалните ограничувања/привилегии на корисникот за оваа форма
    SpremiForma(self);
  //	На појавување на формата одреди која компонента е прва а која последна на панелот
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	Прочитај ги подесувањата за изглед на гридот
   procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
   procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
   sobrano := true;

   if Tag = 1 then
      begin
        dm.tblResenijaAD.Close;
        dm.tblResenijaAD.ParamByName('ad_id').Value:=dm.tblAdvokatskiDrustvaID.Value;
        dm.tblResenijaAD.Open;
      end
   else  if Tag = 0 then
      begin
        dm.tblResenijaAD.Close;
        dm.tblResenijaAD.ParamByName('ad_id').Value:=-1;
        dm.tblResenijaAD.Open;
      end;

//  Користење на нурко од базата во cxExtLookupComboBox или cxDBExtLookupComboBox контрола
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmResenijaAD.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmResenijaAD.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  Акција за запис
procedure TfrmResenijaAD.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
  end;
end;

//	Акција за откажување на записот
procedure TfrmResenijaAD.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// Процедури за сетирање, зачувување на својствата и изгледот на Print системот
//----------------------------------------------------------------------------------

//	Акција за сетирање на страната за печатење
procedure TfrmResenijaAD.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	Акција за печатење на гридот
procedure TfrmResenijaAD.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('Партнер : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	Акција за повикување на дизајнерот на печатење
procedure TfrmResenijaAD.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmResenijaAD.aSnimiPecatenjeExecute(Sender: TObject);
begin
 zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	Акција за спуштање или собирање на редовите во гридот кога има групирање по некоја колона
procedure TfrmResenijaAD.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	Акција за бришење на запомнатите сетирања за печатење (Utils.pas)
procedure TfrmResenijaAD.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;


//  Акција за повик на форма за конфигурирање на изгледот и функционалноста на
//  формите(според логираниот корисник)
procedure TfrmResenijaAD.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// КРАЈ -> Процедури за сетирање, зачувување на својствата и изгледот на Print системот
//----------------------------------------------------------------------------------

// Процедура за зачувување на својствата и изгледот на Print системот во .ini датотека
procedure TfrmResenijaAD.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// Процедура за читање на својствата и изгледот на Print системот зачувани во .ini датотека
procedure TfrmResenijaAD.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

end.
