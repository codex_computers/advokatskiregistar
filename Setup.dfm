inherited frmSetup: TfrmSetup
  Caption = 'SETUP'
  ClientHeight = 575
  ExplicitHeight = 606
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 282
    ExplicitHeight = 234
    inherited cxGrid1: TcxGrid
      Height = 278
      ExplicitHeight = 230
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsSetUp
        object cxGrid1DBTableView1PARAMETAR: TcxGridDBColumn
          DataBinding.FieldName = 'PARAMETAR'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1PARAM_VREDNOST: TcxGridDBColumn
          DataBinding.FieldName = 'PARAM_VREDNOST'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1VREDNOST: TcxGridDBColumn
          DataBinding.FieldName = 'VREDNOST'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1P1: TcxGridDBColumn
          DataBinding.FieldName = 'P1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1P2: TcxGridDBColumn
          DataBinding.FieldName = 'P2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1V1: TcxGridDBColumn
          DataBinding.FieldName = 'V1'
          Width = 264
        end
        object cxGrid1DBTableView1V2: TcxGridDBColumn
          DataBinding.FieldName = 'V2'
          Width = 437
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 408
    Height = 144
    ExplicitTop = 408
    ExplicitHeight = 144
    inherited Label1: TLabel
      Left = 41
      Width = 71
      Caption = #1055#1072#1088#1072#1084#1077#1090#1072#1088' :'
      ExplicitLeft = 41
      ExplicitWidth = 71
    end
    object Label3: TLabel [1]
      Left = 52
      Top = 48
      Width = 60
      Height = 13
      Caption = #1042#1088#1077#1076#1085#1086#1089#1090':'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 118
      DataBinding.DataField = 'V1'
      DataBinding.DataSource = dm.dsSetUp
      Enabled = False
      StyleDisabled.TextColor = clBtnText
      StyleDisabled.TextStyle = [fsBold]
      TabOrder = 1
      ExplicitLeft = 118
      ExplicitWidth = 443
      Width = 443
    end
    inherited OtkaziButton: TcxButton
      Top = 104
      TabOrder = 3
      ExplicitTop = 152
    end
    inherited ZapisiButton: TcxButton
      Top = 104
      TabOrder = 2
      ExplicitTop = 152
    end
    object V2: TcxDBTextEdit
      Left = 118
      Top = 45
      BeepOnEnter = False
      DataBinding.DataField = 'V2'
      DataBinding.DataSource = dm.dsSetUp
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 443
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 552
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    ExplicitTop = 552
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 216
  end
  inherited PopupMenu1: TPopupMenu
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    Left = 456
    Top = 232
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 248
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41634.420474884260000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
