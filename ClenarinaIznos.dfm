inherited frmClenarinaIznos: TfrmClenarinaIznos
  Caption = #1043#1086#1076#1080#1096#1077#1085' '#1080#1079#1085#1086#1089' '#1085#1072' '#1095#1083#1077#1085#1072#1088#1080#1085#1072
  ClientHeight = 538
  ClientWidth = 602
  ExplicitWidth = 618
  ExplicitHeight = 577
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 602
    Height = 266
    ExplicitWidth = 602
    ExplicitHeight = 266
    inherited cxGrid1: TcxGrid
      Width = 598
      Height = 262
      ExplicitWidth = 598
      ExplicitHeight = 262
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsClenarinaIznos
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 51
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Width = 85
        end
        object cxGrid1DBTableView1IZNOS: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS'
          Width = 165
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 150
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 392
    Width = 602
    Height = 123
    ExplicitTop = 392
    ExplicitWidth = 602
    ExplicitHeight = 123
    inherited Label1: TLabel
      Left = 431
      Visible = False
      ExplicitLeft = 431
    end
    object Label2: TLabel [1]
      Left = 31
      Top = 64
      Width = 40
      Height = 13
      Caption = #1048#1079#1085#1086#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 23
      Top = 37
      Width = 48
      Height = 13
      Caption = #1043#1086#1076#1080#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 483
      Top = 21
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsClenarinaIznos
      TabOrder = 1
      Visible = False
      ExplicitLeft = 483
      ExplicitTop = 21
    end
    inherited OtkaziButton: TcxButton
      Left = 511
      Top = 83
      TabOrder = 4
      ExplicitLeft = 511
      ExplicitTop = 83
    end
    inherited ZapisiButton: TcxButton
      Left = 430
      Top = 83
      TabOrder = 3
      ExplicitLeft = 430
      ExplicitTop = 83
    end
    object IZNOS: TcxDBTextEdit
      Tag = 1
      Left = 77
      Top = 61
      BeepOnEnter = False
      DataBinding.DataField = 'IZNOS'
      DataBinding.DataSource = dm.dsClenarinaIznos
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 174
    end
    object GODINA: TcxDBComboBox
      Left = 77
      Top = 34
      DataBinding.DataField = 'GODINA'
      DataBinding.DataSource = dm.dsClenarinaIznos
      Properties.Items.Strings = (
        '1981'
        '1982'
        '1983'
        '1984'
        '1985'
        '1986'
        '1987'
        '1989'
        '1990'
        '1991'
        '1992'
        '1993'
        '1994'
        '1995'
        '1996'
        '1997'
        '1998'
        '1999'
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 174
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 602
    ExplicitWidth = 602
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 515
    Width = 602
    ExplicitTop = 515
    ExplicitWidth = 602
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 264
    Top = 240
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 152
    Top = 256
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 42262.556983310180000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 384
    Top = 128
    PixelsPerInch = 96
  end
end
