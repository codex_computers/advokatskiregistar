unit Zadolzuvanje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxPCdxBarPopupMenu, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxBarBuiltInMenu, cxImageComboBox, cxLabel, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmZadolzuvanje = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aBrisiGrupno: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxPageControl1: TcxPageControl;
    cxTabSheetNezadolzeni: TcxTabSheet;
    cxTabSheetZadolzeni: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    dxBarManager1Bar5: TdxBar;
    cbGodina: TcxBarEditItem;
    cbBarGodina: TdxBarCombo;
    dxBarManager1BarNefakturirani: TdxBar;
    aZadolzi: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    Selekcija: TcxGridDBColumn;
    dxBarLargeButton19: TdxBarLargeButton;
    aBrisiZadolzuvanje: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aPecatiFaktura: TAction;
    aDizajnFaktura: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    aGrupnoPecatenje: TAction;
    dxBarManager1BarFakturirani: TdxBar;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxComponentPrinter1Link2: TdxGridReportLink;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid2DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid2DBTableView1EMBG: TcxGridDBColumn;
    cxGrid2DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid2DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid2DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MESTO: TcxGridDBColumn;
    cxGrid2DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid2DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid2DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid2DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid2DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxTabSheetUplati: TcxTabSheet;
    cxGrid2DBTableView1DOLG: TcxGridDBColumn;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1ZADOLZUVANJE_ID: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKAT_EMBG: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid3DBTableView1GODINA: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM: TcxGridDBColumn;
    cxGrid3DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid3DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1STATUS: TcxGridDBColumn;
    cxGrid3DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid3DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1MESTO: TcxGridDBColumn;
    cxGrid3DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid3DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid3DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid3DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid3DBTableView1WEB_STRANA: TcxGridDBColumn;
    aUplati: TAction;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarManager1BarUplati: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    aBrisiUplata: TAction;
    dxBarDateComboDatumZadolzi: TdxBarDateCombo;
    dxBarLargeButton27: TdxBarLargeButton;
    aGrupnoBrisenjeZadolzuvanje: TAction;
    PanelGrupnoBrisenje: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxLabel2: TcxLabel;
    cxLabel1: TcxLabel;
    txtOd: TcxTextEdit;
    txtDo: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGrid3DBTableView1TIP_RESENIE_ID: TcxGridDBColumn;
    cxGrid2DBTableView1TIP_RESENIE_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_RESENIE_ID: TcxGridDBColumn;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton28: TdxBarLargeButton;
    aResenijaAdvokati: TAction;
    cxGrid1DBTableView1RESENIE_ID: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_LK: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV_LK: TcxGridDBColumn;
    dxbrlrgbtn1: TdxBarLargeButton;
    actIzvestajZaClenarina: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aBrisiGrupnoExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure cxGrid2DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cbGodinaChange(Sender: TObject);
    procedure cbBarGodinaChange(Sender: TObject);
    procedure aZadolziExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure SelekcijaPropertiesEditValueChanged(Sender: TObject);
    procedure aBrisiZadolzuvanjeExecute(Sender: TObject);
    procedure aDizajnFakturaExecute(Sender: TObject);
    procedure aGrupnoPecatenjeExecute(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure aUplatiExecute(Sender: TObject);
    procedure aBrisiUplataExecute(Sender: TObject);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aGrupnoBrisenjeZadolzuvanjeExecute(Sender: TObject);
    procedure aResenijaAdvokatiExecute(Sender: TObject);
    procedure actIzvestajZaClenarinaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano1, sobrano2 : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmZadolzuvanje: TfrmZadolzuvanje;
  rData : TRepositoryData;
  pom_godina, pom_check,vk_selekcija, fakturiranje_vlez: Integer;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit,
  UplataStavki, ResenijaAdvokati, UplataPoedinecno;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmZadolzuvanje.CheckBox1Click(Sender: TObject);
var i:Integer;
    status: TStatusWindowHandle;
begin
  {if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
   begin
   status := cxCreateStatusWindow();
   try
    	if CheckBox1.Checked then
       begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
              Values[FilteredRecordIndex[i] , Selekcija.Index]:=  true;
              dm.qUpdateAdvokatFLAG.Close;
              dm.qUpdateAdvokatFLAG.ParamByName('id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
              dm.qUpdateAdvokatFLAG.ParamByName('flag').Value:=100;
              dm.qUpdateAdvokatFLAG.ExecQuery;
              pom_check:=0;
             end;
        end
      else
        begin
          with cxGrid1DBTableView1.DataController do
          for I := 0 to FilteredRecordCount - 1 do
            begin
               Values[FilteredRecordIndex[i] , Selekcija.Index]:= false;
               //if pom_check = 0 then
               //   begin
                    dm.qUpdateAdvokatFLAG.Close;
                    dm.qUpdateAdvokatFLAG.ParamByName('id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                    dm.qUpdateAdvokatFLAG.ParamByName('flag').Value:=null;
                    dm.qUpdateAdvokatFLAG.ExecQuery;
                //  end;
            end;
     end
      finally
	       cxRemoveStatusWindow(status);
      end;
   end;  }
end;

constructor TfrmZadolzuvanje.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmZadolzuvanje.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmZadolzuvanje.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmZadolzuvanje.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmZadolzuvanje.aBrisiZadolzuvanjeExecute(Sender: TObject);
begin
  dm.qCountUplatiPoZadolzuvanje.Close;
  dm.qCountUplatiPoZadolzuvanje.ParamByName('zadolzuvanje_id').Value:=dm.tblZadolzeniAdvokatiID.Value;
  dm.qCountUplatiPoZadolzuvanje.ExecQuery;
if dm.qCountUplatiPoZadolzuvanje.FldByName['br'].Value =0 then
 begin
  if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
      begin
        cxGrid2DBTableView1.DataController.DataSet.Delete();
        dm.tblZadolzeniAdvokati.Close;
        dm.tblNezadolzeniAdvokati.Close;

        if cbBarGodina.Text <> '' then
          begin
             dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
             dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
          end
        else
          begin
            dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=godina;
            dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=godina;
          end;
        dm.tblNezadolzeniAdvokati.Open;
        dm.tblZadolzeniAdvokati.Open;
      end;
 end
else ShowMessage('�� � ��������� ������ �� �������������. �������� � ������ !!!');

end;

procedure TfrmZadolzuvanje.actIzvestajZaClenarinaExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',23);
        if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
           dmKon.tblSqlReport.ParamByName('advokat_id').Value:=dm.tblNezadolzeniAdvokatiID.Value
        else if cxPageControl1.ActivePage = cxTabSheetUplati then
           dmKon.tblSqlReport.ParamByName('advokat_id').Value:=dm.tblUplataADVOKAT_ID.Value
        else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
           dmKon.tblSqlReport.ParamByName('advokat_id').Value:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmZadolzuvanje.aBrisiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
       brisiGridVoBaza(Name,cxGrid1DBTableView1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
       brisiGridVoBaza(Name,cxGrid2DBTableView1);
     end;
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmZadolzuvanje.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmZadolzuvanje.aResenijaAdvokatiExecute(Sender: TObject);
begin
  frmResenijaAdvokati := TfrmResenijaAdvokati.Create(self,false);
  frmResenijaAdvokati.Tag:=2;
  dm.tblResenijaAdvokati.Close;
  if (cxPageControl1.ActivePage = cxTabSheetNezadolzeni) then
     dm.tblResenijaAdvokati.ParamByName('advokat_id').Value:=dm.tblNezadolzeniAdvokatiID.Value
  else if (cxPageControl1.ActivePage = cxTabSheetZadolzeni) then
     dm.tblResenijaAdvokati.ParamByName('advokat_id').Value:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value
  else if (cxPageControl1.ActivePage = cxTabSheetUplati) then
     dm.tblResenijaAdvokati.ParamByName('advokat_id').Value:=dm.tblUplataADVOKAT_ID.Value;
  dm.tblResenijaAdvokati.Open;
  frmResenijaAdvokati.ShowModal;
  frmResenijaAdvokati.Free;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmZadolzuvanje.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmZadolzuvanje.aSnimiIzgledExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
         zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
         zacuvajGridVoBaza(Name,cxGrid2DBTableView1);
     end;
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmZadolzuvanje.aZacuvajExcelExecute(Sender: TObject);
begin
     if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
       zacuvajVoExcel(cxGrid1, Caption);
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
       zacuvajVoExcel(cxGrid2, Caption);
     end
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmZadolzuvanje.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmZadolzuvanje.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmZadolzuvanje.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmZadolzuvanje.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmZadolzuvanje.SelekcijaPropertiesEditValueChanged(Sender: TObject);
var i, momentalen:Integer;
begin
     if Selekcija.EditValue = true then
        begin
          dm.qUpdateAdvokatFLAG.Close;
          dm.qUpdateAdvokatFLAG.ParamByName('id').Value:=dm.tblNezadolzeniAdvokatiID.Value;
          dm.qUpdateAdvokatFLAG.ParamByName('flag').Value:=100;
          dm.qUpdateAdvokatFLAG.ExecQuery;
         end
     else if (selekcija.EditValue  = false) then
         begin
           dm.qUpdateAdvokatFLAG.Close;
           dm.qUpdateAdvokatFLAG.ParamByName('id').Value:=dm.tblNezadolzeniAdvokatiID.Value;
           dm.qUpdateAdvokatFLAG.ParamByName('flag').Value:=Null;
           dm.qUpdateAdvokatFLAG.ExecQuery;
         end;
end;

procedure TfrmZadolzuvanje.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmZadolzuvanje.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmZadolzuvanje.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmZadolzuvanje.prefrli;
begin
end;

procedure TfrmZadolzuvanje.FormClose(Sender: TObject; var Action: TCloseAction);
begin
// if fakturiranje_vlez = 1 then
//   begin
//    dm.qUpdateAdvokatFLAG.Close;
//    dm.qUpdateAdvokatFLAG.ParamByName('id').Value:='%';
//    dm.qUpdateAdvokatFLAG.ParamByName('flag').Value:=null;
//    dm.qUpdateAdvokatFLAG.ExecQuery;
//   end;
  Action := caFree;

  dmRes.FreeRepository(rData);
end;
procedure TfrmZadolzuvanje.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmZadolzuvanje.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid2DBTableView1,false,false);

  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    procitajPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
    sobrano1 := true;
    sobrano2 := true;

    dm.tblNezadolzeniAdvokati.Close;
    dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=godina;
    dm.tblNezadolzeniAdvokati.Open;


    dm.tblZadolzeniAdvokati.Close;
    dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=godina;
    dm.tblZadolzeniAdvokati.Open;

    dm.tblUplata.Close;
    dm.tblUplata.ParamByName('godina').Value:=godina;
    dm.tblUplata.ParamByName('advokat_id').Value:=-100;
    dm.tblUplata.Open;

    pom_godina:=0;
    cbBarGodina.Text:=IntToStr(godina);

    cxPageControl1.ActivePage:=cxTabSheetNezadolzeni;

    dxBarManager1BarFakturirani.Visible:=False;
    dxBarManager1BarUplati.Visible:=False;
    dxBarManager1BarNefakturirani.Visible:=True;
    fakturiranje_vlez:=0;
    pom_check:=0;
end;
//------------------------------------------------------------------------------

procedure TfrmZadolzuvanje.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmZadolzuvanje.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmZadolzuvanje.cxGrid2DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid2DBTableView1);
end;

procedure TfrmZadolzuvanje.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

procedure TfrmZadolzuvanje.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
   if NewPage =  cxTabSheetNezadolzeni then
   begin
       dxBarManager1BarFakturirani.Visible:=False;
       dxBarManager1BarNefakturirani.Visible:=true;
       dxBarManager1BarUplati.Visible:=False;
   end
 else if NewPage = cxTabSheetZadolzeni then
   begin
       dxBarManager1BarFakturirani.Visible:=True;
       dxBarManager1BarNefakturirani.Visible:=False;
       dxBarManager1BarUplati.Visible:=False;
   end
else if NewPage = cxTabSheetUplati then
   begin
       dxBarManager1BarFakturirani.Visible:=False;
       dxBarManager1BarNefakturirani.Visible:=False;
       dxBarManager1BarUplati.Visible:=True;
   end
end;

//  ����� �� �����
procedure TfrmZadolzuvanje.aBrisiGrupnoExecute(Sender: TObject);
var
  st: TDataSetState;
  br_zadolzuvanja, br_brisenja, br_uplati:Integer;
begin
      if ((txtOd.Text <> '')and (txtDo.Text<>'')) then
         begin
        //    br_zadolzuvanja:=dm.insert_return5(dm.pPROC_ADV_GRUPNO_BRISENJE,'SIFRA_OD','SIFRA_DO',null, null,Null, txtOd.Text, txtDo.Text,null, null,Null,'BR_ZADOLZUVANJA');
            br_brisenja:=dm.insert_return5(dm.pPROC_ADV_GRUPNO_BRISENJE,'SIFRA_OD','SIFRA_DO','UPLATA_ZADOLZ','DATUM', 'TIP_BRISENJE', txtOd.Text, txtDo.Text,1,null,null,'BR_BRISENJA');

           // br_uplati:=br_zadolzuvanja - br_brisenja;
            if br_brisenja >0 then
               begin
                // if (br_zadolzuvanja = br_brisenja) then
                   ShowMessage('������� ������ �� ����������� !!!');
              //   else if (br_brisenja < br_zadolzuvanja) then ShowMessage(' ������� ������ �� '+intToStr(br_brisenja)+' ����������� ���������� '+intToStr(br_uplati)+' ����� ������!!!');
                 dm.tblZadolzeniAdvokati.FullRefresh;
                 dm.tblNezadolzeniAdvokati.FullRefresh;
               end
            else ShowMessage('�� � ��������� ���� ���� ����������� !!!');
            PanelGrupnoBrisenje.Visible:=False;
         end
      else ShowMessage('��������� �� ����� ����� �� � ��!!!');
end;

procedure TfrmZadolzuvanje.cbBarGodinaChange(Sender: TObject);
begin
if pom_godina = 1 then
   begin
    dm.tblZadolzeniAdvokati.Close;
    dm.tblNezadolzeniAdvokati.Close;
    dm.tblUplata.Close;
    if cbBarGodina.Text <> '' then
      begin
       dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
       dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
       dm.tblUplata.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
      end
    else
      begin
       dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=godina;
       dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=godina;
       dm.tblUplata.ParamByName('godina').Value:=godina;
      end;
    dm.tblUplata.ParamByName('advokat_id').Value:=-100;
    dm.tblNezadolzeniAdvokati.Open;
    dm.tblZadolzeniAdvokati.Open;
    dm.tblUplata.Open;
   end
else
   begin
      pom_godina:= 1
   end;
end;

procedure TfrmZadolzuvanje.cbGodinaChange(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmZadolzuvanje.aOtkaziExecute(Sender: TObject);
begin
   if PanelGrupnoBrisenje.Visible = true then
      begin
         PanelGrupnoBrisenje.Visible:=False;
         txtOd.Clear;
         txtDo.Clear;
      end
   else
      begin
         ModalResult := mrCancel;
         Close();
      end;

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmZadolzuvanje.aPageSetupExecute(Sender: TObject);
begin
  if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
       dxComponentPrinter1Link1.PageSetup;
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
       dxComponentPrinter1Link2.PageSetup;
     end
end;

//	����� �� ������� �� ������
procedure TfrmZadolzuvanje.aPecatiTabelaExecute(Sender: TObject);
begin
   if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
       dxComponentPrinter1Link1.ReportTitle.Text := '�����������';
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
       dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
       dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
     //  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����� : ' + DatumOd.Text + '-' + DatumDo.Text);

       dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni  then
     begin
       dxComponentPrinter1Link2.ReportTitle.Text := '����� �� �������� ��� �� �� ���������';
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
       dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));
       dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
     //  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������ : ' + godina.Text + ', ��� :  ' + BrojOd.Text + '-' + BrojDo.Text);

       dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
     end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmZadolzuvanje.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
   if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
       dxComponentPrinter1Link1.DesignReport();
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
       dxComponentPrinter1Link2.DesignReport();
     end
end;

procedure TfrmZadolzuvanje.aSnimiPecatenjeExecute(Sender: TObject);
begin
 if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
       zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
       zacuvajPrintVoBaza(Name,cxGrid2DBTableView1.Name,dxComponentPrinter1Link2);
     end
 end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmZadolzuvanje.aSpustiSoberiExecute(Sender: TObject);
begin
 if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
        if (sobrano1 = true) then
          begin
            cxGrid1DBTableView1.ViewData.Expand(false);
            sobrano1 := false;
          end
        else
          begin
            cxGrid1DBTableView1.ViewData.Collapse(false);
            sobrano1 := true;
          end
     end
 else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
        if (sobrano2 = true) then
          begin
            cxGrid2DBTableView1.ViewData.Expand(false);
            sobrano2 := false;
          end
        else
          begin
            cxGrid2DBTableView1.ViewData.Collapse(false);
            sobrano2 := true;
          end
     end
end;

procedure TfrmZadolzuvanje.aUplatiExecute(Sender: TObject);
begin
   frmUplataPoedinecna:= TfrmUplataPoedinecna.Create(self,false);
   dm.tblUplata.Close;
   dm.tblUplata.ParamByName('godina').Value:=-100;
   if (cxPageControl1.ActivePage = cxTabSheetNezadolzeni) then
     begin
       dm.tblUplata.ParamByName('advokat_id').Value:=dm.tblNezadolzeniAdvokatiID.Value;
       frmUplataPoedinecna.Tag:=dm.tblNezadolzeniAdvokatiID.Value;
     end
   else if (cxPageControl1.ActivePage = cxTabSheetZadolzeni) then
     begin
       dm.tblUplata.ParamByName('advokat_id').Value:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
       frmUplataPoedinecna.Tag:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
     end
   else if (cxPageControl1.ActivePage = cxTabSheetUplati) then
     begin
        dm.tblUplata.ParamByName('advokat_id').Value:=dm.tblUplataADVOKAT_ID.Value;
        frmUplataPoedinecna.Tag:=dm.tblUplataADVOKAT_ID.Value;
     end;
   dm.tblUplata.Open;
   frmUplataPoedinecna.ShowModal;
   frmUplataPoedinecna.Free;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmZadolzuvanje.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
    if cxPageControl1.ActivePage = cxTabSheetNezadolzeni then
     begin
        brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     end
  else if cxPageControl1.ActivePage = cxTabSheetZadolzeni then
     begin
       brisiPrintOdBaza(Name,cxGrid2DBTableView1.Name, dxComponentPrinter1Link2);
     end
  end;

procedure TfrmZadolzuvanje.aBrisiUplataExecute(Sender: TObject);
begin
  if ((cxGrid3DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid3DBTableView1.DataController.RecordCount <> 0)) then
      begin
        cxGrid3DBTableView1.DataController.DataSet.Delete();
        dm.tblZadolzeniAdvokati.Close;
        if cbBarGodina.Text <> '' then
          begin
            dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
          end
        else
          begin
            dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=dmKon.godina;
          end;
        dm.tblZadolzeniAdvokati.Open;
      end;
end;

procedure TfrmZadolzuvanje.aDizajnFakturaExecute(Sender: TObject);
begin
     dmRes.Spremi('SAN',2);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmZadolzuvanje.aZadolziExecute(Sender: TObject);
var
  id_mnozestvo : AnsiString;
  oznaceno, kraj : bool;
  momentalen : integer;
  datum:TDateTime;
begin
     if dxBarDateComboDatumZadolzi.Date = null then
        datum:=Now
     else
        datum:=dxBarDateComboDatumZadolzi.Date;

     If cbBarGodina.Text <> '' then
        dm.insert6(dm.pZadolzuvanje, 'GODINA', 'DATUM', 'ADVOKATI_ID_IN', null,null, null,StrToInt(cbBarGodina.Text), datum, null, null,null, null)
     else
        dm.insert6(dm.pZadolzuvanje, 'GODINA', 'DATUM', 'ADVOKATI_ID_IN', null,null, null, godina, datum, null, null,null, null);

     pom_check:=1;

     dm.tblNezadolzeniAdvokati.Close;
     dm.tblZadolzeniAdvokati.Close;
     if cbBarGodina.Text <> '' then
       begin
        dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text);
        dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=StrToInt(cbBarGodina.Text)
       end
      else
       begin
          dm.tblNezadolzeniAdvokati.ParamByName('godina').Value:=godina;
          dm.tblZadolzeniAdvokati.ParamByName('godina').Value:=godina;
       end;
     dm.tblNezadolzeniAdvokati.Open;
     dm.tblZadolzeniAdvokati.Open;

     cxPageControl1.ActivePage:=cxTabSheetZadolzeni;
     fakturiranje_vlez:=1;
end;

procedure TfrmZadolzuvanje.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmZadolzuvanje.aGrupnoBrisenjeZadolzuvanjeExecute(Sender: TObject);
begin
  PanelGrupnoBrisenje.Visible:=True;
  txtOd.SetFocus;
end;

procedure TfrmZadolzuvanje.aGrupnoPecatenjeExecute(Sender: TObject);
begin

end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmZadolzuvanje.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmZadolzuvanje.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
