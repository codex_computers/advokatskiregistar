object frmAdvokatskiDrustva: TfrmAdvokatskiDrustva
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1080' '#1076#1088#1091#1096#1090#1074#1072
  ClientHeight = 729
  ClientWidth = 1115
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1115
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 706
    Width = 1115
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1115
    Height = 259
    Align = alTop
    TabOrder = 2
    object PanelDrustva: TPanel
      Left = 656
      Top = 1
      Width = 458
      Height = 257
      Align = alRight
      Enabled = False
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object Label1: TLabel
        Left = 55
        Top = 59
        Width = 46
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1053#1072#1079#1080#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 30
        Top = 34
        Width = 71
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 15
        Top = 8
        Width = 86
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 21
        Top = 184
        Width = 82
        Height = 13
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 46
        Top = 159
        Width = 57
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 53
        Top = 109
        Width = 50
        Height = 13
        Caption = #1040#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 60
        Top = 84
        Width = 43
        Height = 13
        Caption = #1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 16
        Top = 134
        Width = 87
        Height = 13
        Caption = #1052#1086#1073'. '#1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 26
        Top = 209
        Width = 77
        Height = 13
        Caption = 'Web '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cxButton8: TcxButton
        Left = 259
        Top = 228
        Width = 75
        Height = 25
        Action = aZapisi
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 8
      end
      object cxButton9: TcxButton
        Left = 340
        Top = 228
        Width = 75
        Height = 25
        Action = aOtkazi
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 9
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DATUM: TcxDBDateEdit
        Tag = 1
        Left = 107
        Top = 30
        DataBinding.DataField = 'DATUM'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 126
      end
      object NAZIV: TcxDBTextEdit
        Tag = 1
        Left = 107
        Top = 55
        DataBinding.DataField = 'NAZIV'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 310
      end
      object BROJ: TcxDBTextEdit
        Tag = 1
        Left = 107
        Top = 5
        DataBinding.DataField = 'BROJ'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 126
      end
      object EMAIL: TcxDBTextEdit
        Left = 107
        Top = 180
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 310
      end
      object TELEFON: TcxDBTextEdit
        Left = 107
        Top = 155
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'TELEFON'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 310
      end
      object MESTO_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 150
        Top = 80
        Hint = #1052#1077#1089#1090#1086
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 267
      end
      object MESTO: TcxDBTextEdit
        Tag = 1
        Left = 107
        Top = 80
        Hint = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object ADRESA: TcxDBTextEdit
        Left = 107
        Top = 105
        Hint = #1040#1076#1088#1077#1089#1072
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 310
      end
      object MOBILEN_TELEFON: TcxDBTextEdit
        Left = 107
        Top = 130
        Hint = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN_TELEFON'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 10
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 310
      end
      object WEB_STRANA: TcxDBTextEdit
        Left = 107
        Top = 205
        Hint = 'WWW '#1072#1076#1088#1077#1089#1072' '#1085#1072' '#1040#1044
        BeepOnEnter = False
        DataBinding.DataField = 'WEB_STRANA'
        DataBinding.DataSource = dm.dsAdvokatskiDrustva
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 11
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 310
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 655
      Height = 257
      Align = alClient
      Caption = 'Panel3'
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 653
        Height = 255
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmAlways
          FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
          OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
          DataController.DataSource = dm.dsAdvokatskiDrustva
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 52
          end
          object cxGrid1DBTableView1BROJ: TcxGridDBColumn
            DataBinding.FieldName = 'BROJ'
            Width = 112
          end
          object cxGrid1DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Width = 83
          end
          object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'NAZIV'
            Width = 337
          end
          object cxGrid1DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Visible = False
          end
          object cxGrid1DBTableView1MESTONAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MESTONAZIV'
            Width = 163
          end
          object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Width = 200
          end
          object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'TELEFON'
            Width = 187
          end
          object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN_TELEFON'
            Width = 100
          end
          object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'EMAIL'
            Width = 211
          end
          object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
            DataBinding.FieldName = 'WEB_STRANA'
            Width = 100
          end
          object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 200
          end
          object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 200
          end
          object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 200
          end
          object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 200
          end
          object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 200
          end
          object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 200
          end
          object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 200
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object cxButton1: TcxButton
    Left = 184
    Top = 628
    Width = 89
    Height = 25
    Action = aDodadiDogovorenOrgan
    TabOrder = 3
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 385
    Width = 1115
    Height = 321
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Properties.ActivePage = cxTabSheetAdvokati
    Properties.CustomButtons.Buttons = <>
    Properties.Images = dmRes.cxLargeImages
    Properties.TabHeight = 40
    ClientRectBottom = 321
    ClientRectRight = 1115
    ClientRectTop = 42
    object cxTabSheetAdvokati: TcxTabSheet
      Caption = #1040#1076#1074#1086#1082#1072#1090#1080
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 55
      ParentFont = False
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1115
        Height = 279
        Align = alClient
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid2DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmAlways
          FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
          DataController.DataSource = dm.dsAdvokati_Drustva
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView1EMBG: TcxGridDBColumn
            DataBinding.FieldName = 'EMBG'
            Visible = False
          end
          object cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN'
          end
          object cxGrid2DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_NAZIV'
            Width = 319
          end
          object cxGrid2DBTableView1LICENCA: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA'
            Width = 119
          end
          object cxGrid2DBTableView1LICENCA_DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA_DATUM'
            Width = 122
          end
          object cxGrid2DBTableView1status_naziv: TcxGridDBColumn
            DataBinding.FieldName = 'status_naziv'
            Width = 150
          end
          object cxGrid2DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Visible = False
            Width = 84
          end
          object cxGrid2DBTableView1MESTO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_NAZIV'
            Width = 150
          end
          object cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
            Visible = False
            Width = 176
          end
          object cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
            Width = 150
          end
          object cxGrid2DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Width = 150
          end
          object cxGrid2DBTableView1TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'TELEFON'
            Width = 150
          end
          object cxGrid2DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN_TELEFON'
            Width = 150
          end
          object cxGrid2DBTableView1WEB_STRANA: TcxGridDBColumn
            DataBinding.FieldName = 'WEB_STRANA'
            Width = 150
          end
          object cxGrid2DBTableView1EMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'EMAIL'
            Width = 150
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 432
    Top = 520
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 520
    Top = 472
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 323
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 741
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1072#1094#1080#1112#1072
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 1141
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcelAD
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aZacuvajExcelA
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPPotvrdaUpisAdvokatskoDrustvoBezKazniPovredi
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1087#1077#1095#1072#1090
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aResenijaAD
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcelAD: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel - '#1040#1076#1074#1086#1082#1072#1090#1089#1082#1080' '#1076#1088#1091#1096#1090#1074#1072
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelADExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aDodadiDogovorenOrgan: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiDogovorenOrganExecute
    end
    object aIzvadiDogovorenOrgan: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
      OnExecute = aIzvadiDogovorenOrganExecute
    end
    object aDodadiPaket: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiPaketExecute
    end
    object aIzvadiPAket: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
      OnExecute = aIzvadiPAketExecute
    end
    object aDodadiMedUslugi: TAction
      Caption = #1044#1086#1076#1072#1076#1080
      ImageIndex = 26
      OnExecute = aDodadiMedUslugiExecute
    end
    object aIzvadiMedUslugi: TAction
      Caption = #1048#1079#1074#1072#1076#1080
      ImageIndex = 46
    end
    object aAzurirajDog: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
    end
    object aZapisisDog: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
    end
    object aOtkaziDog: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
    end
    object aZacuvajExcelA: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel - '#1040#1076#1074#1086#1082#1072#1090#1080
      ImageIndex = 9
      OnExecute = aZacuvajExcelAExecute
    end
    object aPPotvrdaUpisAdvokatskoDrustvoBezKazniPovredi: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1091#1087#1080#1089' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' '#1074#1086' '#1080#1084#1077#1085#1080#1082
      Hint = 
        #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1091#1087#1080#1089' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' '#1074#1086' '#1080#1084#1077#1085#1080#1082' ('#1053#1077' '#1077' '#1080#1079#1088#1077#1095#1077#1085#1072' '#1085 +
        #1080#1082#1072#1082#1074#1072' '#1076#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1072' '#1084#1077#1088#1082#1072' '#1085#1080#1090#1091' '#1087#1072#1082' '#1089#1077' '#1082#1072#1079#1085#1091#1074#1072#1085#1080' '#1079#1072' '#1073#1080#1083#1086' '#1082#1072#1082#1074#1072' '#1087#1086 +
        #1074#1088#1077#1076#1072' '#1074#1086' '#1074#1088#1096#1077#1114#1077#1090#1086' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072#1090#1072' '#1087#1088#1086#1092#1077#1089#1080#1112#1072')'
      ImageIndex = 19
      ShortCut = 121
      OnExecute = aPPotvrdaUpisAdvokatskoDrustvoBezKazniPovrediExecute
    end
    object aDPotvrdaUpisAdvokatskoDrustvoBezKazniPovredi: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1091#1087#1080#1089' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' '#1074#1086' '#1080#1084#1077#1085#1080#1082
      SecondaryShortCuts.Strings = (
        'Ctrl+Shift+F10')
      OnExecute = aDPotvrdaUpisAdvokatskoDrustvoBezKazniPovrediExecute
    end
    object aResenijaAD: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1112#1072' '#1079#1072' '#1040#1044
      ImageIndex = 79
      OnExecute = aResenijaADExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 976
    Top = 552
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 1096
    Top = 424
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    PopupMenus = <>
    Left = 912
    Top = 504
  end
end
