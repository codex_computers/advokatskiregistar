unit ResenijaAdvokati;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions, cxNavigator, cxImageComboBox,
  cxBlobEdit, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, cxLabel, cxDBLabel;

type
//  niza = Array[1..5] of Variant;

  TfrmResenijaAdvokati = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dPanel: TPanel;
    cxBarEditItem3: TcxBarEditItem;
    Label9: TLabel;
    cxGroupBoxPodatociKontakt: TcxGroupBox;
    TELEFON: TcxDBTextEdit;
    Label6: TLabel;
    Label11: TLabel;
    MESTO: TcxDBTextEdit;
    ADRESA: TcxDBTextEdit;
    Label10: TLabel;
    MESTO_NAZIV: TcxDBLookupComboBox;
    MOBILEN: TcxDBTextEdit;
    Label7: TLabel;
    EMAIL: TcxDBTextEdit;
    Label8: TLabel;
    cxGroupBoxOsnovniPodatoci: TcxGroupBox;
    IME: TcxDBTextEdit;
    Label5: TLabel;
    PREZIME: TcxDBTextEdit;
    Label4: TLabel;
    cxGroupBoxLicenca: TcxGroupBox;
    LICENCA_DATUM: TcxDBDateEdit;
    Label3: TLabel;
    LICENCA_BROJ: TcxDBTextEdit;
    Label2: TLabel;
    ZapisiButton: TcxButton;
    cxButtonOtkazi: TcxButton;
    lPanel: TPanel;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPripravnici: TAction;
    aStrucniSorabotnici: TAction;
    Label1: TLabel;
    EMBG: TcxDBTextEdit;
    Label12: TLabel;
    WEB_STRANA: TcxDBTextEdit;
    VrabotenVo: TcxGroupBox;
    ADVOKATSKO_DRUSTVO_NAZIV: TcxDBLookupComboBox;
    Label14: TLabel;
    cxGroupBoxPodatociResenie: TcxGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    RESENIE_TIP: TcxDBTextEdit;
    RESENIE_TIP_NAZIV: TcxDBLookupComboBox;
    BROJ_RESENIE: TcxDBTextEdit;
    KOMISIJA_PRETSEDATEL: TcxDBTextEdit;
    DATUM_SEDNICA: TcxDBDateEdit;
    cxGroupBoxPeriodVaznost: TcxGroupBox;
    Label19: TLabel;
    DATUM_OD: TcxDBDateEdit;
    Label20: TLabel;
    DATUM_DO: TcxDBDateEdit;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_RESENIE: TcxGridDBColumn;
    cxGrid1DBTableView1RESENIE_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_RESENIE_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_RESENIE_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_SEDNICA: TcxGridDBColumn;
    cxGrid1DBTableView1KOMISIJA_PRETSEDATEL: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1ADV_DRUSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1ADV_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADV_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADV_KANCELARIJA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1status_naziv: TcxGridDBColumn;
    cxButton1: TcxButton;
    aCopyAD: TAction;
    dxBarSubItem2: TdxBarSubItem;
    aPotvrdaZaZapisVoRegistar: TAction;
    dxBarButton1: TdxBarButton;
    aDPotvrdaZaZapisVoRegistar: TAction;
    PopupMenu2: TPopupMenu;
    N2: TMenuItem;
    aDizajnReport: TAction;
    aPSvecenaZakletvaAdvokat: TAction;
    aDSvecenaZakletvaAdvokat: TAction;
    dxBarButton2: TdxBarButton;
    N3: TMenuItem;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    aPResZapisVoRegistar: TAction;
    aDResZapisVoRegistar: TAction;
    N4: TMenuItem;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarSubItem4: TdxBarSubItem;
    bbPResZapisVoRegistar: TdxBarButton;
    bbPotvrdaZaZapisVoRegistar: TdxBarButton;
    bbPResOdSamostoenVoVrabotenVoAK: TdxBarButton;
    bbPOdVrabotenKajAdvokatVoSamostoen: TdxBarButton;
    dxBarSubItem5: TdxBarSubItem;
    bbPSvecenaZakletvaAdvokat: TdxBarButton;
    aPResOdSamostoenVoVrabotenVoAK: TAction;
    aDResOdSamostoenVoVrabotenVoAK: TAction;
    aPOdVrabotenKajAdvokatVoSamostoen: TAction;
    aDOdVrabotenKajAdvokatVoSamostoen: TAction;
    N5: TMenuItem;
    N6: TMenuItem;
    aPResBrisiAdvokatImenikOtkazuvanje: TAction;
    aDResBrisiAdvokatImenikOtkazuvanje: TAction;
    dxBarSubItem6: TdxBarSubItem;
    bbPResBrisiAdvokatImenikOtkazuvanje: TdxBarButton;
    N7: TMenuItem;
    bbPResMiruvanje: TdxBarButton;
    bbPResMiruvanjeProdolzeno: TdxBarButton;
    bbPResPromenaPrezime: TdxBarButton;
    bbPResPromenaSedisteAK: TdxBarButton;
    aPResMiruvanje: TAction;
    aDResMiruvanje: TAction;
    aPResMiruvanjeProdolzeno: TAction;
    aDResMiruvanjeProdolzeno: TAction;
    aPResPromenaPrezime: TAction;
    aDResPromenaPrezime: TAction;
    aPResPromenaSedisteAK: TAction;
    aDResPromenaSedisteAK: TAction;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    aKreirajAK: TAction;
    aUpdateResenieZapis: TAction;
    bbPEObrazec: TdxBarButton;
    aPEObrazec: TAction;
    aDPEObrazec: TAction;
    N12: TMenuItem;
    Label13: TLabel;
    ADVOKATSKA_KANCELARIJA_NAZIV: TcxDBLookupComboBox;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1BR_DISCIPLINSKI: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton24: TdxBarLargeButton;
    aDisciplinskiMerki: TAction;
    bbPPropratnoPismoCRM: TdxBarLargeButton;
    aPPropratnoPismoCRM: TAction;
    aDPropratnoPismoCRM: TAction;
    N13: TMenuItem;
    dxBarSubItem7: TdxBarSubItem;
    dxBarSubItem8: TdxBarSubItem;
    dxBarSubItem9: TdxBarSubItem;
    dxBarSubItem10: TdxBarSubItem;
    dxBarSubItem11: TdxBarSubItem;
    dxBarSubItem12: TdxBarSubItem;
    dxBarSubItem13: TdxBarSubItem;
    aPResenieMiruvanje_Bolest: TAction;
    aDResenieMiruvanje_Bolest: TAction;
    aPResenieMiruvanje_RabOdnosOpredeleno: TAction;
    aDResenieMiruvanje_RabOdnosOpredeleno: TAction;
    aPResenieMiruvanje_RabOdnosNeOpredeleno: TAction;
    aDResenieMiruvanje_RabOdnosNeOpredeleno: TAction;
    aPResenieMiruvanje_RabOdnosProbnaRabota: TAction;
    aDResenieMiruvanje_RabOdnosProbnaRabota: TAction;
    aPResenieMiruvanje_StrucnoUsovrsuvanje: TAction;
    aDResenieMiruvanje_StrucnoUsovrsuvanje: TAction;
    aPResenieMiruvanje_IzborFunkcija: TAction;
    aDResenieMiruvanje_IzborFunkcija: TAction;
    aPResenieProdolzuvanjeMiruvanje: TAction;
    aDResenieProdolzuvanjeMiruvanje: TAction;
    bbPResenieMiruvanje_Bolest: TdxBarButton;
    bbPResenieMiruvanje_RabOdnosNeOpredeleno: TdxBarButton;
    bbPResenieMiruvanje_RabOdnosOpredeleno: TdxBarButton;
    bbPResenieMiruvanje_StrucnoUsovrsuvanje: TdxBarButton;
    bbPResenieMiruvanje_RabOdnosProbnaRabota: TdxBarButton;
    bbPResenieMiruvanje_IzborFunkcija: TdxBarButton;
    bbPResenieProdolzuvanjeMiruvanje: TdxBarButton;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aAdvokatskaKancelarija: TAction;
    aAdvokatskoDruzstvo: TAction;
    PanelREport: TPanel;
    cxGroupBox1: TcxGroupBox;
    LabelReport: TLabel;
    MemoReport: TcxDBMemo;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    aZpisiReport: TAction;
    aOtkaziReport: TAction;
    cxButton5: TcxButton;
    aCopyAK: TAction;
    cxButton2: TcxButton;
    cxDBRadioGroupStatus: TcxDBRadioGroup;
    cxGroupBox2: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    MESTO_LK_NAZIV: TcxDBLookupComboBox;
    MESTO_LK: TcxDBTextEdit;
    MOBILEN_TELEFON_LK: TcxDBTextEdit;
    TELEFON_LK: TcxDBTextEdit;
    EMAIL_LK: TcxDBTextEdit;
    WEB_STRANA_LK: TcxDBTextEdit;
    cxButton6: TcxButton;
    OPIS: TcxDBMemo;
    Label21: TLabel;
    ADVOKATSKA_ZAEDNICA_NAZIV: TcxDBLookupComboBox;
    ADVOKATSKA_ZAEDNICA: TcxDBTextEdit;
    Label22: TLabel;
    aCopyAKVOLK: TAction;
    KANCELARIJA_NAZIV: TcxDBTextEdit;
    aPResBrisiAdvokatImenikSmrt: TAction;
    aDResBrisiAdvokatImenikSmrt: TAction;
    bbPResBrisiAdvokatImenikSmrt: TdxBarButton;
    N21: TMenuItem;
    cxGroupBoxBaratel: TcxGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    dxbrlrgbtn1: TdxBarLargeButton;
    actPorodino: TAction;
    btnaKreirajAK: TcxButton;
    actKreirajAKancelarija: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aPripravniciExecute(Sender: TObject);
    procedure aStrucniSorabotniciExecute(Sender: TObject);
    procedure WEB_STRANAPropertiesChange(Sender: TObject);
    procedure cxDBRadioGroupStatusClick(Sender: TObject);
    procedure aCopyADExecute(Sender: TObject);
    procedure aPotvrdaZaZapisVoRegistarExecute(Sender: TObject);
    procedure aDPotvrdaZaZapisVoRegistarExecute(Sender: TObject);
    procedure aDizajnReportExecute(Sender: TObject);
    procedure aPSvecenaZakletvaAdvokatExecute(Sender: TObject);
    procedure aDSvecenaZakletvaAdvokatExecute(Sender: TObject);
    procedure aPResZapisVoRegistarExecute(Sender: TObject);
    procedure aDResZapisVoRegistarExecute(Sender: TObject);
    procedure aPResOdSamostoenVoVrabotenVoAKExecute(Sender: TObject);
    procedure aPOdVrabotenKajAdvokatVoSamostoenExecute(Sender: TObject);
    procedure aDResOdSamostoenVoVrabotenVoAKExecute(Sender: TObject);
    procedure aDOdVrabotenKajAdvokatVoSamostoenExecute(Sender: TObject);
    procedure aPResBrisiAdvokatImenikOtkazuvanjeExecute(Sender: TObject);
    procedure aDResBrisiAdvokatImenikOtkazuvanjeExecute(Sender: TObject);
    procedure aPResMiruvanjeExecute(Sender: TObject);
    procedure aPResMiruvanjeProdolzenoExecute(Sender: TObject);
    procedure aPResPromenaPrezimeExecute(Sender: TObject);
    procedure aPResPromenaSedisteAKExecute(Sender: TObject);
    procedure aDResPromenaSedisteAKExecute(Sender: TObject);
    procedure aDResPromenaPrezimeExecute(Sender: TObject);
    procedure aDResMiruvanjeProdolzenoExecute(Sender: TObject);
    procedure aDResMiruvanjeExecute(Sender: TObject);
    procedure aKreirajAKExecute(Sender: TObject);
    procedure aUpdateResenieZapisExecute(tip_resenie_id: Integer);
    procedure aPEObrazecExecute(Sender: TObject);
    procedure aDPEObrazecExecute(Sender: TObject);
    procedure aDisciplinskiMerkiExecute(Sender: TObject);
    procedure aPPropratnoPismoCRMExecute(Sender: TObject);
    procedure aDPropratnoPismoCRMExecute(Sender: TObject);
    procedure aPResenieMiruvanje_BolestExecute(Sender: TObject);
    procedure aDResenieMiruvanje_BolestExecute(Sender: TObject);
    procedure aPResenieMiruvanje_RabOdnosOpredelenoExecute(Sender: TObject);
    procedure aDResenieMiruvanje_RabOdnosOpredelenoExecute(Sender: TObject);
    procedure aPResenieMiruvanje_RabOdnosNeOpredelenoExecute(Sender: TObject);
    procedure aDResenieMiruvanje_RabOdnosNeOpredelenoExecute(Sender: TObject);
    procedure aPResenieMiruvanje_RabOdnosProbnaRabotaExecute(Sender: TObject);
    procedure aDResenieMiruvanje_RabOdnosProbnaRabotaExecute(Sender: TObject);
    procedure aPResenieMiruvanje_IzborFunkcijaExecute(Sender: TObject);
    procedure aDResenieMiruvanje_IzborFunkcijaExecute(Sender: TObject);
    procedure aPResenieMiruvanje_StrucnoUsovrsuvanjeExecute(Sender: TObject);
    procedure aDResenieMiruvanje_StrucnoUsovrsuvanjeExecute(Sender: TObject);
    procedure aPResenieProdolzuvanjeMiruvanjeExecute(Sender: TObject);
    procedure aDResenieProdolzuvanjeMiruvanjeExecute(Sender: TObject);
    procedure aAdvokatskaKancelarijaExecute(Sender: TObject);
    procedure aAdvokatskoDruzstvoExecute(Sender: TObject);
    procedure aZpisiReportExecute(Sender: TObject);
    procedure aOtkaziReportExecute(Sender: TObject);
    procedure DATUM_SEDNICAPropertiesChange(Sender: TObject);
    procedure DATUM_SEDNICAPropertiesCloseUp(Sender: TObject);
    procedure DATUM_SEDNICAPropertiesEditValueChanged(Sender: TObject);
    procedure aCopyAKExecute(Sender: TObject);
    procedure dxBarSubItem3Popup(Sender: TObject);
    procedure cxDBRadioGroupStatusPropertiesEditValueChanged(Sender: TObject);
    procedure aCopyAKVOLKExecute(Sender: TObject);
    procedure ADVOKATSKA_KANCELARIJA_NAZIVPropertiesEditValueChanged(
      Sender: TObject);
    procedure aPResBrisiAdvokatImenikSmrtExecute(Sender: TObject);
    procedure aDResBrisiAdvokatImenikSmrtExecute(Sender: TObject);
    procedure RESENIE_TIPPropertiesEditValueChanged(Sender: TObject);
    procedure actPorodinoExecute(Sender: TObject);
    procedure actKreirajAKancelarijaExecute(Sender: TObject);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmResenijaAdvokati: TfrmResenijaAdvokati;
  rData : TRepositoryData;
  report_br:Integer;
  insert_ak:Integer;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, dmMaticni,
  Mesto,  RegStrucniSorabotnici, RegPripravnici, RegAdvokatskiKancelarii,
  DisciplinskiMerki, RegAdvokatskiDrustva, Porodilno;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmResenijaAdvokati.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmResenijaAdvokati.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    RESENIE_TIP.Enabled:=True;
    RESENIE_TIP_NAZIV.Enabled:=True;
    RESENIE_TIP.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value:=petsedatel_komisija;
    if Tag = 1 then
       begin
         dm.tblResenijaAdvokatiADVOKAT_ID.Value:=dm.tblAdvokatiID.Value;
         dm.tblResenijaAdvokatiEMBG.Value:=dm.tblAdvokatiEMBG.Value;
         dm.tblResenijaAdvokatiIME.Value:=dm.tblAdvokatiIME.Value;
         dm.tblResenijaAdvokatiPREZIME.Value:=dm.tblAdvokatiPREZIME.Value;
         dm.tblResenijaAdvokatiLICENCA_BROJ.Value:=dm.tblAdvokatiLICENCA.Value;
         if not dm.tblAdvokatiLICENCA_DATUM.IsNull then
           begin
            dm.tblResenijaAdvokatiLICENCA_DATUM.Value:=dm.tblAdvokatiLICENCA_DATUM.Value;
            dm.tblResenijaAdvokatiDATUM_OD.Value:=dm.tblAdvokatiLICENCA_DATUM.Value;
           end;
         dm.tblResenijaAdvokatiSTATUS.Value:=dm.tblAdvokatiSTATUS.Value;
         if not dm.tblAdvokatiADVOKATSKO_DRUSTVO.IsNull then
            dm.tblResenijaAdvokatiADV_DRUSTVO.Value:=dm.tblAdvokatiADVOKATSKO_DRUSTVO.Value;
         if not dm.tblAdvokatiADVOKATSKA_KANCELARIJA.IsNull then
            dm.tblResenijaAdvokatiADV_KANCELARIJA.Value:=dm.tblAdvokatiADVOKATSKA_KANCELARIJA.Value;
         dm.tblResenijaAdvokatiMESTO.Value:=dm.tblAdvokatiMESTO.Value;
         dm.tblResenijaAdvokatiMESTO_LK.Value:=dm.tblAdvokatiMESTO_LK.Value;
         dm.tblResenijaAdvokatiADRESA.Value:=dm.tblAdvokatiADRESA.Value;
         dm.tblResenijaAdvokatiTELEFON.Value:=dm.tblAdvokatiTELEFON.Value;
         dm.tblResenijaAdvokatiMOBILEN.Value:=dm.tblAdvokatiMOBILEN_TELEFON.Value;
         dm.tblResenijaAdvokatiEMAIL.Value:=dm.tblAdvokatiEMAIL.Value;
         dm.tblResenijaAdvokatiWEB_STRANA.Value:=dm.tblAdvokatiWEB_STRANA.Value;
         dm.tblResenijaAdvokatiTELEFON_LK.Value:=dm.tblAdvokatiTELEFON_LK.Value;
         dm.tblResenijaAdvokatiMOBILEN_TELEFON_LK.Value:=dm.tblAdvokatiMOBILEN_TELEFON_LK.Value;
         dm.tblResenijaAdvokatiEMAIL_LK.Value:=dm.tblAdvokatiEMAIL_LK.Value;
         dm.tblResenijaAdvokatiWEB_STRANA_LK.Value:=dm.tblAdvokatiWEB_STRANA_LK.Value;
         if (dm.tblAdvokatiADVOKATSKA_ZAEDNICA.Value <> 0) then
            dm.tblResenijaAdvokatiADV_ZAEDNICA.Value:=dm.tblAdvokatiADVOKATSKA_ZAEDNICA.Value;
       end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmResenijaAdvokati.aAdvokatskaKancelarijaExecute(Sender: TObject);
begin
  frmAdvokatskiKancelarii := TfrmAdvokatskiKancelarii.Create(self,false);
  frmAdvokatskiKancelarii.Tag:=3;
  frmAdvokatskiKancelarii.ShowModal;
  frmAdvokatskiKancelarii.Free;
end;

procedure TfrmResenijaAdvokati.aAdvokatskoDruzstvoExecute(Sender: TObject);
begin
  frmAdvokatskiDrustva := TfrmAdvokatskiDrustva.Create(self,false);
  frmAdvokatskiDrustva.ShowModal;
  frmAdvokatskiDrustva.Free;
end;

procedure TfrmResenijaAdvokati.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    DATUM_OD.SetFocus;

    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmResenijaAdvokati.aBrisiExecute(Sender: TObject);
 var iznos_zadolzuvanje_od,iznos_zadolzuvanje_do, tip_resenie_id,advokat_id:Integer;
     datum_od, datum_do:TDateTime;
     k_id:Integer;
begin

  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
     begin
       cxGrid1DBTableView1.DataController.DataSet.Delete();
       dm.tblAdvokati.FullRefresh;
     end;
end;

procedure TfrmResenijaAdvokati.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmResenijaAdvokati.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmResenijaAdvokati.aPResBrisiAdvokatImenikOtkazuvanjeExecute(Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',10);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));

        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResBrisiAdvokatImenikSmrtExecute(
  Sender: TObject);
  var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',11);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResenieMiruvanje_BolestExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
     try
        dmRes.Spremi('ADV',16);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResenieMiruvanje_IzborFunkcijaExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
  dm.tblDokumentiPolinja.Close;
  dm.tblDokumentiPolinja.ParamByName('report_br').Value:=21;
  dm.tblDokumentiPolinja.Open;

  if not dm.tblDokumentiPolinja.IsEmpty then
     begin
       dm.tblDokumentiPolinjaVrednost.Close;
       dm.tblDokumentiPolinjaVrednost.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('pole_id').Value:=dm.tblDokumentiPolinjaPOLE_ID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('resenie_id').Value:=dm.tblResenijaAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.Open;
       if dm.tblDokumentiPolinjaVrednost.IsEmpty then
          dm.tblDokumentiPolinjaVrednost.Insert
       else
          dm.tblDokumentiPolinjaVrednost.Edit;
       PanelReport.Visible:=true;
       labelReport.Caption:=dm.tblDokumentiPolinjaPOLE_NAZIV.Value;
       report_br:=21;
     end
  else ShowMessage('������� �� ��������� �� ������ !!!');
end;

procedure TfrmResenijaAdvokati.aPResenieMiruvanje_RabOdnosNeOpredelenoExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',17);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResenieMiruvanje_RabOdnosOpredelenoExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
  dm.tblDokumentiPolinja.Close;
  dm.tblDokumentiPolinja.ParamByName('report_br').Value:=18;
  dm.tblDokumentiPolinja.Open;

  if not dm.tblDokumentiPolinja.IsEmpty then
     begin
       dm.tblDokumentiPolinjaVrednost.Close;
       dm.tblDokumentiPolinjaVrednost.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('pole_id').Value:=dm.tblDokumentiPolinjaPOLE_ID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('resenie_id').Value:=dm.tblResenijaAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.Open;
       if dm.tblDokumentiPolinjaVrednost.IsEmpty then
          dm.tblDokumentiPolinjaVrednost.Insert
       else
          dm.tblDokumentiPolinjaVrednost.Edit;
       PanelReport.Visible:=true;
       labelReport.Caption:=dm.tblDokumentiPolinjaPOLE_NAZIV.Value;
       report_br:=18;
     end
  else ShowMessage('������� �� ��������� �� ������ !!!');
end;

procedure TfrmResenijaAdvokati.aPResenieMiruvanje_RabOdnosProbnaRabotaExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
  dm.tblDokumentiPolinja.Close;
  dm.tblDokumentiPolinja.ParamByName('report_br').Value:=19;
  dm.tblDokumentiPolinja.Open;

  if not dm.tblDokumentiPolinja.IsEmpty then
     begin
       dm.tblDokumentiPolinjaVrednost.Close;
       dm.tblDokumentiPolinjaVrednost.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('pole_id').Value:=dm.tblDokumentiPolinjaPOLE_ID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('resenie_id').Value:=dm.tblResenijaAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.Open;
       if dm.tblDokumentiPolinjaVrednost.IsEmpty then
          dm.tblDokumentiPolinjaVrednost.Insert
       else
          dm.tblDokumentiPolinjaVrednost.Edit;
       PanelReport.Visible:=true;
       labelReport.Caption:=dm.tblDokumentiPolinjaPOLE_NAZIV.Value;
       report_br:=19;
     end
  else ShowMessage('������� �� ��������� �� ������ !!!');

end;

procedure TfrmResenijaAdvokati.aPResenieMiruvanje_StrucnoUsovrsuvanjeExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
  dm.tblDokumentiPolinja.Close;
  dm.tblDokumentiPolinja.ParamByName('report_br').Value:=20;
  dm.tblDokumentiPolinja.Open;

  if not dm.tblDokumentiPolinja.IsEmpty then
     begin
       dm.tblDokumentiPolinjaVrednost.Close;
       dm.tblDokumentiPolinjaVrednost.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('pole_id').Value:=dm.tblDokumentiPolinjaPOLE_ID.Value;
       dm.tblDokumentiPolinjaVrednost.ParamByName('resenie_id').Value:=dm.tblResenijaAdvokatiID.Value;
       dm.tblDokumentiPolinjaVrednost.Open;
       if dm.tblDokumentiPolinjaVrednost.IsEmpty then
          dm.tblDokumentiPolinjaVrednost.Insert
       else
          dm.tblDokumentiPolinjaVrednost.Edit;
       PanelReport.Visible:=true;
       labelReport.Caption:=dm.tblDokumentiPolinjaPOLE_NAZIV.Value;
       report_br:=20;
     end
  else ShowMessage('������� �� ��������� �� ������ !!!');
end;

procedure TfrmResenijaAdvokati.aPResenieProdolzuvanjeMiruvanjeExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',22);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResMiruvanjeExecute(Sender: TObject);
begin
{
      try
        dmRes.Spremi('ADV',11);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
}
end;

procedure TfrmResenijaAdvokati.aPResMiruvanjeProdolzenoExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',12);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmResenijaAdvokati.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmResenijaAdvokati.aKreirajAKExecute(Sender: TObject);
begin
  frmAdvokatskiKancelarii:=TfrmAdvokatskiKancelarii.Create(self,false);
  frmAdvokatskiKancelarii.tag:=2;
  frmAdvokatskiKancelarii.ShowModal;
  frmAdvokatskiKancelarii.Free;
  MESTO.SetFocus;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmResenijaAdvokati.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmResenijaAdvokati.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmResenijaAdvokati.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin

         if ((Sender = MESTO) or (Sender = MESTO_NAZIV)) then
             begin
               frmMesto:=TfrmMesto.Create(self,false);
               frmMesto.ShowModal;
               if (frmMesto.ModalResult = mrOK) then
                    dm.tblResenijaAdvokatiMESTO.Value := StrToInt(frmMesto.GetSifra(0));
                frmMesto.Free;
             end
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;


//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmResenijaAdvokati.cxDBRadioGroupStatusClick(Sender: TObject);
begin
   if ((cxDBRadioGroupStatus.EditValue = 2)or (cxDBRadioGroupStatus.EditValue = 3)) then
     begin
       ADVOKATSKO_DRUSTVO_NAZIV.SetFocus;
       ADVOKATSKA_KANCELARIJA_NAZIV.Clear;
     end
   else
     begin
       ADVOKATSKA_KANCELARIJA_NAZIV.SetFocus;
       ADVOKATSKO_DRUSTVO_NAZIV.Clear;
     end;
end;

procedure TfrmResenijaAdvokati.cxDBRadioGroupStatusPropertiesEditValueChanged(
  Sender: TObject);
begin
    if((IME.Text <> '')and(PREZIME.Text <> '')and(cxDBRadioGroupStatus.EditValue = 1)and(LICENCA_BROJ.Text <> '')) then
       begin
         dm.qNajdiAK.Close;
         dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblResenijaAdvokatiIME.Value+' '+dm.tblResenijaAdvokatiPREZIME.Value;
         dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblResenijaAdvokatiLICENCA_BROJ.Value;
         dm.qNajdiAK.ExecQuery;
         if dm.qNajdiAK.FldByName['br'].Value >0 then
            dm.tblResenijaAdvokatiADV_KANCELARIJA.Value:= dm.qNajdiAK.FldByName['id'].Value
         else
             ADVOKATSKA_KANCELARIJA_NAZIV.Clear;
       end;

end;

procedure TfrmResenijaAdvokati.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
  if(Sender = EMAIL)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);

  if(Sender = WEB_STRANA)then
       ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmResenijaAdvokati.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
         if(Sender = EMAIL)then
            ActivateKeyboardLayout($042F042F, KLF_REORDER);
         if(Sender = WEB_STRANA)then
            ActivateKeyboardLayout($042F042F, KLF_REORDER)
       end;
end;

procedure TfrmResenijaAdvokati.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.EditValue <>null) and (sifra.EditValue<>null)  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

procedure TfrmResenijaAdvokati.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin


end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmResenijaAdvokati.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmResenijaAdvokati.WEB_STRANAPropertiesChange(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmResenijaAdvokati.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmResenijaAdvokati.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmResenijaAdvokati.prefrli;
begin
end;

procedure TfrmResenijaAdvokati.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmResenijaAdvokati.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmResenijaAdvokati.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
   procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
   procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
   sobrano := true;


   if Tag = 1 then
      begin
        dm.tblResenijaAdvokati.Close;
        dm.tblResenijaAdvokati.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value;
        dm.tblResenijaAdvokati.Open;
      end
   else  if Tag = 0 then
      begin
        dm.tblResenijaAdvokati.Close;
        dm.tblResenijaAdvokati.ParamByName('advokat_id').Value:=-1;
        dm.tblResenijaAdvokati.Open;
      end;


   if Tag = 1 then
      begin
        cxGroupBoxOsnovniPodatoci.TabStop:=False;
        cxGroupBoxLicenca.TabStop:=False;
        VrabotenVo.TabStop:=False;
        cxGroupBoxPodatociKontakt.TabStop:=False;
      end

   else if Tag = 0 then
      begin
        dPanel.Visible:=False;
        aAzuriraj.Enabled:=False;
        aNov.Enabled:=False;
        aBrisi.Enabled:=False;
        dxBarManager1Bar1.Visible:=False;
      end

   else if Tag = 2 then
      begin
        aAzuriraj.Enabled:=False;
        aNov.Enabled:=False;
        aBrisi.Enabled:=False;
        dxBarManager1Bar1.Visible:=False;
      end;
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmResenijaAdvokati.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmResenijaAdvokati.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmResenijaAdvokati.DATUM_SEDNICAPropertiesChange(Sender: TObject);
begin
      // ShowMessage('dtum '+ DateToStr(DATUM_SEDNICA.Date));
      if dm.tblResenijaAdvokati.State in [dsInsert, dsEdit] then
        begin
          if not dm.tblResenijaAdvokatiDATUM_SEDNICA.IsNull then
             begin
              if (Length(DATUM_SEDNICA.Text)=10) then
                 begin
                  dm.qPretsedatel.Close;
                  dm.qPretsedatel.ParamByName('datum_sednica').Value:=dm.tblResenijaAdvokatiDATUM_SEDNICA.Value;
                  dm.qPretsedatel.ExecQuery;
                    if  not dm.qPretsedatel.FldByName['naziv'].IsNull then
                  dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value:=dm.qPretsedatel.FldByName['naziv'].Value
                 end;
             end;
        end;
end;

procedure TfrmResenijaAdvokati.DATUM_SEDNICAPropertiesCloseUp(Sender: TObject);
begin
     if dm.tblResenijaAdvokati.State in [dsInsert, dsEdit] then
        begin
          if not dm.tblResenijaAdvokatiDATUM_SEDNICA.IsNull then
             begin
              if (Length(DATUM_SEDNICA.Text)=10) then
                 begin
                  dm.qPretsedatel.Close;
                  dm.qPretsedatel.ParamByName('datum_sednica').Value:=dm.tblResenijaAdvokatiDATUM_SEDNICA.Value;
                  dm.qPretsedatel.ExecQuery;
                    if  not dm.qPretsedatel.FldByName['naziv'].IsNull then
                  dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value:=dm.qPretsedatel.FldByName['naziv'].Value
                 end;
             end;
        end;
end;

procedure TfrmResenijaAdvokati.DATUM_SEDNICAPropertiesEditValueChanged(
  Sender: TObject);
begin
if dm.tblResenijaAdvokati.State in [dsInsert, dsEdit] then
        begin
          if not dm.tblResenijaAdvokatiDATUM_SEDNICA.IsNull then
             begin
              if (Length(DATUM_SEDNICA.Text)=10) then
                 begin
                  dm.qPretsedatel.Close;
                  dm.qPretsedatel.ParamByName('datum_sednica').Value:=dm.tblResenijaAdvokatiDATUM_SEDNICA.Value;
                  dm.qPretsedatel.ExecQuery;
                    if  not dm.qPretsedatel.FldByName['naziv'].IsNull then
                  dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value:=dm.qPretsedatel.FldByName['naziv'].Value
                 end;
             end;
        end;
end;

procedure TfrmResenijaAdvokati.dxBarSubItem3Popup(Sender: TObject);
begin
//  Setiraj gi site da ne se gledaat
          bbPResZapisVoRegistar.Visible:=ivNever;
          bbPSvecenaZakletvaAdvokat.Visible:=ivNever;
          bbPotvrdaZaZapisVoRegistar.Visible:=ivNever;
          bbPEObrazec.Visible:=ivNever;
          bbPPropratnoPismoCRM.Visible:=ivNever;

          bbPResenieMiruvanje_Bolest.Visible:=ivNever;
          bbPResenieMiruvanje_RabOdnosNeOpredeleno.Visible:=ivNever;
          bbPResenieMiruvanje_RabOdnosOpredeleno.Visible:=ivNever;
          bbPResenieMiruvanje_StrucnoUsovrsuvanje.Visible:=ivNever;
          bbPResenieMiruvanje_RabOdnosProbnaRabota.Visible:=ivNever;
          bbPResenieMiruvanje_IzborFunkcija.Visible:=ivNever;
          bbPResenieProdolzuvanjeMiruvanje.Visible:=ivNever;

          bbPResBrisiAdvokatImenikOtkazuvanje.Visible:=ivNever;
          bbPResBrisiAdvokatImenikSmrt.Visible:=ivNever;

          bbPResPromenaPrezime.Visible:=ivNever;

          bbPResOdSamostoenVoVrabotenVoAK.Visible:=ivNever;
          bbPOdVrabotenKajAdvokatVoSamostoen.Visible:=ivNever;
          bbPResMiruvanje.Visible:=ivNever;
          bbPResMiruvanjeProdolzeno.Visible:=ivNever;
          bbPResPromenaSedisteAK.Visible:=ivNever;

//  Pa vklucuvaj da se gledaat samo onie koi trebaat
     if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 1 then
        begin
          bbPResZapisVoRegistar.Visible:=ivAlways;
          bbPotvrdaZaZapisVoRegistar.Visible:=ivAlways;
          bbPSvecenaZakletvaAdvokat.Visible:=ivAlways;
          bbPEObrazec.Visible:=ivAlways;
          bbPPropratnoPismoCRM.Visible:=ivAlways;
        end
//  Resenija za brisenje na advokati poradi otkazuvanje
     else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 3 then
        begin
          bbPResBrisiAdvokatImenikOtkazuvanje.Visible:=ivAlways;
        end
//  Resenija za brisenje na advokati poradi smrt
     else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 4 then
        begin
          bbPResBrisiAdvokatImenikSmrt.Visible:=ivAlways;
        end
//  Resenija za promena na prezime
     else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 7 then
        begin
          bbPResPromenaPrezime.Visible:=ivAlways;
        end
//  Resenija za miruvanje - bolest
     else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 11 then
        begin
          bbPResenieMiruvanje_Bolest.Visible:=ivAlways;
        end
//  Resenija za miruvanje - raboten odnos na neopredeleno
     else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 13 then
        begin
          bbPResenieMiruvanje_RabOdnosNeOpredeleno.Visible:=ivAlways;
        end
//  Resenija za miruvanje - raboten odnos na opredeleno
      else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 14 then
        begin
          bbPResenieMiruvanje_RabOdnosOpredeleno.Visible:=ivAlways;
        end
//  Resenija za miruvanje - probna rabota
      else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 15 then
        begin
          bbPResenieMiruvanje_RabOdnosProbnaRabota.Visible:=ivAlways;
        end
//  Resenija za miruvanje - strucno usovrsuvanje
      else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 16 then
        begin
          bbPResenieMiruvanje_StrucnoUsovrsuvanje.Visible:=ivAlways;
        end
//  Resenija za miruvanje - izbor na funkcioner
      else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 17 then
        begin
          bbPResenieMiruvanje_IzborFunkcija.Visible:=ivAlways;
        end

     else if dm.tblResenijaAdvokatiRESENIE_TIP.Value = 18 then
        begin
          bbPResMiruvanjeProdolzeno.Visible:=ivAlways;
        end
      else
        begin
//
        end



end;


procedure TfrmResenijaAdvokati.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  tag:Integer;
  iznos_zadolzuvanje_od,iznos_zadolzuvanje_do:Integer ;
begin
  ZapisiButton.SetFocus;
//
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if((DATUM_DO.Text <> '')and (StrToDate(DATUM_DO.Text)<=StrToDate(DATUM_OD.Text))) then
          begin ShowMessage('��������� ������ �� � ������� !!!'); DATUM_OD.SetFocus; end
      else
       begin
         cxGrid1DBTableView1.DataController.DataSet.Post;
         dm.tblAdvokati.FullRefresh;
         dm.tblAdvokati.Locate('ID',dm.tblResenijaAdvokatiADVOKAT_ID.Value, []);
         dPanel.Enabled:=false;
         lPanel.Enabled:=true;
         cxGrid1.SetFocus;
       end;
      end;
  end;
end;

procedure TfrmResenijaAdvokati.aZpisiReportExecute(Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
     if dm.tblDokumentiPolinjaVrednost.State = dsEdit then
        dm.tblDokumentiPolinjaVrednost.Post
     else if dm.tblDokumentiPolinjaVrednost.State = dsInsert then
        begin
          dm.tblDokumentiPolinjaVrednostADVOKAT_ID.Value:=dm.tblAdvokatiID.Value;
          dm.tblDokumentiPolinjaVrednostRESENIJA_ID.Value:=dm.tblResenijaAdvokatiID.Value;
          dm.tblDokumentiPolinjaVrednostPOLE_ID.Value:=dm.tblDokumentiPolinjaPOLE_ID.Value;
          dm.tblDokumentiPolinjaVrednost.Post;
        end;
     Panelreport.visible:=false;
     if report_br = 18 then
      begin
       try
        dmRes.Spremi('ADV',18);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
      end
     else if report_br = 19 then
      begin
       try
        dmRes.Spremi('ADV',19);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
      end
     else if report_br = 20 then
      begin
       try
        dmRes.Spremi('ADV',20);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
      end
     else if report_br = 21 then
      begin
       try
        dmRes.Spremi('ADV',21);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
       except
        ShowMessage('�� ���� �� �� ������� ���������!');
       end
      end;


     PanelREport.Visible:=False;
end;

//	����� �� ���������� �� �������
procedure TfrmResenijaAdvokati.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

procedure TfrmResenijaAdvokati.aOtkaziReportExecute(Sender: TObject);
begin
     dm.tblDokumentiPolinjaVrednost.Cancel;
     Panelreport.visible:=false;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmResenijaAdvokati.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmResenijaAdvokati.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmResenijaAdvokati.aPEObrazecExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',15);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmResenijaAdvokati.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aPotvrdaZaZapisVoRegistarExecute(
  Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',1);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPPropratnoPismoCRMExecute(Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',8);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(dmKon.imeprezime));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResZapisVoRegistarExecute(Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',7);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPripravniciExecute(Sender: TObject);
begin
     frmRegPripravnici := TfrmRegPripravnici.Create(self,true);
     frmRegPripravnici.Tag:=1;
     frmRegPripravnici.ShowModal;
     frmRegPripravnici.Free;
end;

procedure TfrmResenijaAdvokati.aSnimiPecatenjeExecute(Sender: TObject);
begin
 zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmResenijaAdvokati.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmResenijaAdvokati.aStrucniSorabotniciExecute(Sender: TObject);
begin
     frmRegStrucniSorabotnici := TfrmRegStrucniSorabotnici.Create(self,true);
     frmRegStrucniSorabotnici.tag:=1;
     frmRegStrucniSorabotnici.ShowModal;
     frmRegStrucniSorabotnici.Free;
end;

procedure TfrmResenijaAdvokati.aUpdateResenieZapisExecute(tip_resenie_id: Integer);
begin

end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmResenijaAdvokati.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmResenijaAdvokati.aCopyADExecute(Sender: TObject);
begin
        if (ADVOKATSKO_DRUSTVO_NAZIV.Text <> '') then
       begin
         dm.qADpodatoci.Close;
         dm.qADpodatoci.ParamByName('id').Value:=dm.tblResenijaAdvokatiADV_DRUSTVO.Value;
         dm.qADpodatoci.ExecQuery;

         if not dm.qADpodatoci.FldByName['mesto'].IsNull then
            dm.tblResenijaAdvokatiMESTO.Value:=dm.qADpodatoci.FldByName['mesto'].Value;
         if not dm.qADpodatoci.FldByName['adresa'].IsNull then
            dm.tblResenijaAdvokatiADRESA.Value:=dm.qADpodatoci.FldByName['adresa'].Value;
         if not dm.qADpodatoci.FldByName['telefon'].IsNull then
            dm.tblResenijaAdvokatiTELEFON.Value:=dm.qADpodatoci.FldByName['telefon'].Value;
         if not dm.qADpodatoci.FldByName['email'].IsNull then
            dm.tblResenijaAdvokatiEMAIL.Value:=dm.qADpodatoci.FldByName['email'].Value;
       end;
end;

procedure TfrmResenijaAdvokati.aCopyAKExecute(Sender: TObject);
begin
 if (ADVOKATSKA_KANCELARIJA_NAZIV.Text <> '') then
       begin
         dm.qAKpodatoci.Close;
         dm.qAKpodatoci.ParamByName('id').Value:=dm.tblResenijaAdvokatiADV_KANCELARIJA.Value;
         dm.qAKpodatoci.ExecQuery;

         if not dm.qAKpodatoci.FldByName['mesto'].IsNull then
            dm.tblResenijaAdvokatiMESTO.Value:=dm.qAKpodatoci.FldByName['mesto'].Value
         else
            dm.tblResenijaAdvokatiMESTO.Clear;
         if not dm.qAKpodatoci.FldByName['advokatska_zaednica'].IsNull then
            dm.tblResenijaAdvokatiADV_ZAEDNICA.Value:=dm.qAKpodatoci.FldByName['advokatska_zaednica'].Value
         else
            dm.tblResenijaAdvokatiADV_ZAEDNICA.Clear;
         if not dm.qAKpodatoci.FldByName['adresa'].IsNull then
            dm.tblResenijaAdvokatiADRESA.Value:=dm.qAKpodatoci.FldByName['adresa'].Value
         else
            dm.tblResenijaAdvokatiADRESA.Clear;;
        if not dm.qAKpodatoci.FldByName['mobilen_telefon'].IsNull then
            dm.tblResenijaAdvokatiMOBILEN.Value:=dm.qAKpodatoci.FldByName['mobilen_telefon'].Value
         else
            dm.tblResenijaAdvokatiMOBILEN.Clear;;
         if not dm.qAKpodatoci.FldByName['telefon'].IsNull then
            dm.tblResenijaAdvokatiTELEFON.Value:=dm.qAKpodatoci.FldByName['telefon'].Value
         else
            dm.tblResenijaAdvokatiTELEFON.Clear;
         if not dm.qAKpodatoci.FldByName['email'].IsNull then
            dm.tblResenijaAdvokatiEMAIL.Value:=dm.qAKpodatoci.FldByName['email'].Value
         else
            dm.tblResenijaAdvokatiEMAIL.Clear;
         if not dm.qAKpodatoci.FldByName['web_strana'].IsNull then
            dm.tblResenijaAdvokatiWEB_STRANA.Value:=dm.qAKpodatoci.FldByName['web_strana'].Value
         else
            dm.tblResenijaAdvokatiWEB_STRANA.Clear;
       end;
end;

procedure TfrmResenijaAdvokati.aCopyAKVOLKExecute(Sender: TObject);
begin
 if ((ADVOKATSKA_KANCELARIJA_NAZIV.Text <> '') or (ADVOKATSKO_DRUSTVO_NAZIV.Text <> '')) then
       begin
         if not dm.tblResenijaAdvokatiMESTO.IsNull then
            dm.tblResenijaAdvokatiMESTO_LK.Value:=dm.tblResenijaAdvokatiMESTO.Value
         else
            dm.tblResenijaAdvokatiMESTO_LK.Clear;
         if not dm.tblResenijaAdvokatiMOBILEN.IsNull then
            dm.tblResenijaAdvokatiMOBILEN_TELEFON_LK.Value:=dm.tblResenijaAdvokatiMOBILEN.Value
         else
            dm.tblResenijaAdvokatiMOBILEN_TELEFON_LK.Clear;;
         if not dm.tblResenijaAdvokatiTELEFON.IsNull then
            dm.tblResenijaAdvokatiTELEFON_LK.Value:=dm.tblResenijaAdvokatiTELEFON.Value
         else
            dm.tblResenijaAdvokatiTELEFON_LK.Clear;
         if not dm.tblResenijaAdvokatiEMAIL.IsNull then
            dm.tblResenijaAdvokatiEMAIL_LK.Value:=dm.tblResenijaAdvokatiEMAIL.Value
         else
            dm.tblResenijaAdvokatiEMAIL_LK.Clear;
         if not dm.tblResenijaAdvokatiWEB_STRANA.IsNull then
            dm.tblResenijaAdvokatiWEB_STRANA_LK.Value:=dm.tblResenijaAdvokatiWEB_STRANA.Value
         else
            dm.tblResenijaAdvokatiWEB_STRANA_LK.Clear;
       end
end;

procedure TfrmResenijaAdvokati.actKreirajAKancelarijaExecute(Sender: TObject);
begin
if ((LICENCA_DATUM.Text <> '')and (LICENCA_BROJ.Text<> ''){and (IME.Text <> '')and (PREZIME.Text<>'')})then
     begin
        dm.qNajdiAK.Close;
        dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblResenijaAdvokatiIME.Value+' '+dm.tblResenijaAdvokatiPREZIME.Value;
        dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblResenijaAdvokatiLICENCA_BROJ.Value;
        dm.qNajdiAK.ExecQuery;

        if dm.qNajdiAK.FldByName['br'].IsNull then
           begin
             dm.qInsertAK.Close;
             dm.qInsertAK.ParamByName('NAZIV').Value:=dm.tblResenijaAdvokatiIME.Value +' '+ dm.tblResenijaAdvokatiPREZIME.Value;
             dm.qInsertAK.ParamByName('BROJ').Value:=dm.tblResenijaAdvokatiLICENCA_BROJ.Value;
             dm.qInsertAK.ParamByName('DATUM').Value:=dm.tblResenijaAdvokatiLICENCA_DATUM.Value;
             dm.qInsertAK.ParamByName('MESTO').Value:=dm.tblResenijaAdvokatiMESTO.Value;
             dm.qInsertAK.ParamByName('ADRESA').Value:=dm.tblResenijaAdvokatiADRESA.Value;
             dm.qInsertAK.ParamByName('MOBILEN_TELEFON').Value:=dm.tblResenijaAdvokatiMOBILEN.Value;
             dm.qInsertAK.ParamByName('TELEFON').Value:=dm.tblResenijaAdvokatiTELEFON.Value;
             dm.qInsertAK.ParamByName('EMAIL').Value:=dm.tblResenijaAdvokatiEMAIL.Value;
             dm.qInsertAK.ParamByName('WEB_STRANA').Value:=dm.tblResenijaAdvokatiWEB_STRANA.Value;
             dm.qInsertAK.ParamByName('DATUM_OD').Value:=dm.tblResenijaAdvokatiDATUM_OD.Value;
             if not dm.tblResenijaAdvokatiDATUM_DO.IsNull then
                dm.qInsertAK.ParamByName('DATUM_DO').Value:=dm.tblResenijaAdvokatiDATUM_DO.Value
             else
                dm.qInsertAK.ParamByName('DATUM_DO').Value:=null;
             dm.qInsertAK.ExecQuery;

             insert_ak:=1;

             dm.tblAdvokatskiKancelarii.FullRefresh;

             dm.qNajdiAK.Close;
             dm.qNajdiAK.ParamByName('ime_prezime').Value:=dm.tblResenijaAdvokatiIME.Value+' '+dm.tblResenijaAdvokatiPREZIME.Value;
             dm.qNajdiAK.ParamByName('licenca').Value:=dm.tblResenijaAdvokatiLICENCA_BROJ.Value;
             dm.qNajdiAK.ExecQuery;
             if dm.qNajdiAK.FldByName['br'].Value >0 then
                dm.tblResenijaAdvokatiADV_KANCELARIJA.Value:= dm.qNajdiAK.FldByName['id'].Value
             else
                ADVOKATSKA_KANCELARIJA_NAZIV.Clear;

             MESTO.SetFocus;
           end
        else
           begin
             ShowMessage('������������ ���������� � ��� ������� !!!');
           end;
     end
  else ShowMessage('��������� �� ��������� �������� ���, �������, ������� � ����� �� �������');
end;

procedure TfrmResenijaAdvokati.actPorodinoExecute(Sender: TObject);
begin
   frmPorodilno := TfrmPorodilno.Create(self,false);
   frmPorodilno.Tag:=2;
   frmPorodilno.ShowModal;
   frmPorodilno.Free;
end;

procedure TfrmResenijaAdvokati.aPResOdSamostoenVoVrabotenVoAKExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',8);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResPromenaPrezimeExecute(Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime, pretsedatel_inicijali:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',13);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        if (not dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.IsNull) then
            pretsedatel_inicijali:=dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value
        else
            pretsedatel_inicijali:='����������� �������';
        pos:=AnsiPos(' ', pretsedatel_inicijali);
        korisnik_ime:=pretsedatel_inicijali[1];
        korisnik_prezime:=pretsedatel_inicijali[pos+1];
        pretsedatel_inicijali:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel_inicijali', QuotedStr(pretsedatel_inicijali));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aPResPromenaSedisteAKExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',14);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aDResBrisiAdvokatImenikOtkazuvanjeExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',10);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResBrisiAdvokatImenikSmrtExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',11);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieMiruvanje_BolestExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',16);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieMiruvanje_IzborFunkcijaExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',21);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieMiruvanje_RabOdnosNeOpredelenoExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',17);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieMiruvanje_RabOdnosOpredelenoExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',18);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieMiruvanje_RabOdnosProbnaRabotaExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',19);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieMiruvanje_StrucnoUsovrsuvanjeExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',20);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResenieProdolzuvanjeMiruvanjeExecute(
  Sender: TObject);
begin
      dmRes.Spremi('ADV',22);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResMiruvanjeExecute(Sender: TObject);
begin
{
      dmRes.Spremi('ADV',11);
      dmRes.frxReport1.DesignReport();
}
end;

procedure TfrmResenijaAdvokati.aDResMiruvanjeProdolzenoExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',12);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResOdSamostoenVoVrabotenVoAKExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',8);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResPromenaPrezimeExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',13);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDResPromenaSedisteAKExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',14);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aPOdVrabotenKajAdvokatVoSamostoenExecute(Sender: TObject);
begin
      try
        dmRes.Spremi('ADV',9);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aDOdVrabotenKajAdvokatVoSamostoenExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',9);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aPSvecenaZakletvaAdvokatExecute(Sender: TObject);
var korisnik, korisnik_ime, korisnik_prezime:String;
    pos:Integer;
begin
      try
        dmRes.Spremi('ADV',6);
        dmKon.tblSqlReport.ParamByName('resenie_advokat').Value:=dm.tblResenijaAdvokatiID.Value;
        dmKon.tblSqlReport.Open;

        korisnik:=dmKon.imeprezime;
        pos:=AnsiPos(' ', korisnik);
        korisnik_ime:=korisnik[1];
        korisnik_prezime:=korisnik[pos+1];
        korisnik:=korisnik_ime+korisnik_prezime;

        dmRes.frxReport1.Variables.AddVariable('VAR', 'korisnik', QuotedStr(korisnik));
        dmRes.frxReport1.Variables.AddVariable('VAR', 'pretsedatel', QuotedStr(dm.tblResenijaAdvokatiKOMISIJA_PRETSEDATEL.Value));
        dmRes.frxReport1.ShowReport();
      except
        ShowMessage('�� ���� �� �� ������� ���������!');
      end
end;

procedure TfrmResenijaAdvokati.aDPEObrazecExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',15);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDisciplinskiMerkiExecute(Sender: TObject);
begin
   frmDisciplinskiMerki := TfrmDisciplinskiMerki.Create(self,false);
   frmDisciplinskiMerki.Tag:=2;
   frmDisciplinskiMerki.ShowModal;
   frmDisciplinskiMerki.Free;
end;

procedure TfrmResenijaAdvokati.aDizajnReportExecute(Sender: TObject);
begin
      PopupMenu2.Popup(500,500 );
end;

procedure TfrmResenijaAdvokati.aDPotvrdaZaZapisVoRegistarExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',1);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDPropratnoPismoCRMExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',8);
      dmRes.frxReport1.DesignReport();//
end;

procedure TfrmResenijaAdvokati.aDResZapisVoRegistarExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',7);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.aDSvecenaZakletvaAdvokatExecute(Sender: TObject);
begin
      dmRes.Spremi('ADV',6);
      dmRes.frxReport1.DesignReport();
end;

procedure TfrmResenijaAdvokati.ADVOKATSKA_KANCELARIJA_NAZIVPropertiesEditValueChanged(
  Sender: TObject);
begin
if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
       begin
        if ADVOKATSKA_KANCELARIJA_NAZIV.Text <> '' then
           begin
               dm.tblResenijaAdvokatiKANCELARIJA_NAZIV.Value:=dm.tblAdvokatskiKancelariiNAZIV.Value;
           end;
       end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmResenijaAdvokati.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmResenijaAdvokati.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmResenijaAdvokati.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmResenijaAdvokati.RESENIE_TIPPropertiesEditValueChanged(
  Sender: TObject);
begin
  if(dm.tblResenijaAdvokatiRESENIE_TIP.Value = 4) then
    cxGroupBoxBaratel.Visible := True
  else
    cxGroupBoxBaratel.Visible := False;
end;

end.
