unit ImportExcel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Phys,
  FireDAC.Phys.ODBCBase, FireDAC.Phys.ODBC, Data.Win.ADODB, Data.DB, Vcl.Grids,
  Vcl.DBGrids,  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
  TfrmImportExcel = class(TForm)
    DataSource1: TDataSource;
    cxButton1: TcxButton;
    Memo: TMemo;
    DBGrid: TDBGrid;
    ADOQuery1: TADOQuery;
    ADOConnection1: TADOConnection;
    ADOQuery1naziv: TStringField;
    ADOQuery1iznos: TFloatField;
    ADOStoredProc1: TADOStoredProc;
    cxButton2: TcxButton;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportExcel: TfrmImportExcel;

implementation

{$R *.dfm}

uses dmUnit, dmKonekcija;

procedure TfrmImportExcel.cxButton1Click(Sender: TObject);
var
        ADOConn : TADOConnection;
        ADOQuery : TADOQuery;
        DataSrc : TDataSource;
        ConnectionString: WideString;
        SQLStr : String;
begin


        ConnectionString:= 'DSN=uplati;USERID=SYSDBA;PASSWORD=root;';

        SQLStr:= 'SELECT * FROM [uplati$]';
        { Create an ADO connection }
        ADOConn:= TADOConnection.Create(Self);


        { Setup the connection string }
        ADOConn.ConnectionString:= ConnectionString;


        { Disable login prompt }
        ADOConn.LoginPrompt:= False;


        try
                ADOConn.Connected:= True;
        except
                on e: EADOError do
                begin
                     MessageDlg('Error while connecting', mtError, [mbOK], 0);
                     Exit;
                end;
        end;


        { Create the query }
        ADOQuery:= TADOQuery.Create(Self);
        ADOQuery.Connection:= ADOConn;
        ADOQuery.SQL.Clear;
        ADOQuery.SQL.Add(SQLStr);


        { Set the query to Prepared - will improve performance }
        //ADOQuery.Prepared:= true;


        try
               Memo.Text:= Memo.Text + 'This line appears' + CHR(13) + CHR(10);
               ADOQuery.Active:= True;
               Memo.Text:= Memo.Text + 'This line does not appear' + CHR(13) + CHR(10);
        except
                on e: EADOError do
                begin
                   MessageDlg('Error while doing query', mtError, [mbOK], 0);
                   Exit;
                end;
        end;


        DataSrc:= TDataSource.Create(Self);
        DataSrc.DataSet:= ADOQuery;
        DataSrc.Enabled:= true;

        DBGrid.DataSource:= DataSrc;
end;

procedure TfrmImportExcel.cxButton2Click(Sender: TObject);
var i:Integer;
begin
     {if DBGrid.DataSource.DataSet.RecordCount >0 then
   	 begin
          //with DBGrid.DataSource.DataSet.Da do
          with DBGrid do
            begin
              for I := 0 to  DBGrid.DataSource.DataSet.RecordCount - 1 do
               begin
               //  Values[FilteredRecordIndex[i] , Selekcija.Index]:=  true;
                 ShowMessage(Fields[i].AsString);
                 {dm.qUpdateAdvokatFLAG.Close;
                 dm.qUpdateAdvokatFLAG.ParamByName('id').Value:=Values[FilteredRecordIndex[i] , cxGrid1DBTableView1ID.Index];
                 dm.qUpdateAdvokatFLAG.ParamByName('flag').Value:=100;
                 dm.qUpdateAdvokatFLAG.ExecQuery;
               end;

            end

     end;     }
     with ADOQuery1 do
       begin
          Active := True;
          DisableControls;
          try
           First;
             { Initialize each parameter with  excel data, execute the query and repeat }
           while not Eof do
             begin
               dm.insert6(dm.pPROC_ADV_INSERTUPLATA_FROMEXCEL,'GODINA', 'DATUM', 'LICENCA','IZNOS', null,null, Fields.Fields[0].AsString, Fields.Fields[1].AsString, Fields.Fields[2].AsString, Fields.Fields[3].AsString, null,null);
               Next;
             end;
           showmessage('Uspje?no');
          finally
            EnableControls;
          end;

       end;
end;
end.
