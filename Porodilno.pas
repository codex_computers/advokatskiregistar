unit Porodilno;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbonCustomizationForm,
  dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore, dxPScxCommon,
  System.Actions, Vcl.ActnList, cxBarEditItem, cxClasses, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, dxRibbon, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxCalendar, cxGroupBox, cxMemo, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox;

type
  TfrmPorodilno = class(TfrmMaster)
    lbl1: TLabel;
    ADVOKAT_ID: TcxDBTextEdit;
    ADVOKAT_NAZIV: TcxDBLookupComboBox;
    lbl2: TLabel;
    OPIS: TcxDBMemo;
    cxGroupBoxPeriod: TcxGroupBox;
    lbl3: TLabel;
    lbl4: TLabel;
    DATUM_DO: TcxDBDateEdit;
    DATUM: TcxDBDateEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1IME: TcxGridDBColumn;
    cxGrid1DBTableView1PREZIME: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid1DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPorodilno: TfrmPorodilno;

implementation

uses
  dmUnit;

{$R *.dfm}

procedure TfrmPorodilno.aBrisiExecute(Sender: TObject);
 var iznos_zadolzuvanje_od,iznos_zadolzuvanje_do, tip_resenie_id,advokat_id:Integer;
     datum_od, datum_do:TDateTime;
begin
//  inherited;

  advokat_id:=dm.tblPorodilnoADVOKAT_ID.Value;
  datum_od:=dm.tblPorodilnoDATUM_OD.Value;
  datum_do:=dm.tblPorodilnoDATUM_DO.Value;
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
     begin

       cxGrid1DBTableView1.DataController.DataSet.Delete();
       dm.tblAdvokati.FullRefresh;

       iznos_zadolzuvanje_od:=(dm.insert_return5(dm.PROC_ADV_OSLOBODUVANJE, 'DATUM_OD', 'DATUM_DO', 'ADVOKAT_ID', 'DATUM', Null, datum_od, datum_do, advokat_id, Now, Null,'IZNOS_ZADOLZUVANJEOD'));
       iznos_zadolzuvanje_do:=(dm.insert_return5(dm.PROC_ADV_OSLOBODUVANJE, 'DATUM_OD', 'DATUM_DO', 'ADVOKAT_ID', 'DATUM', Null, datum_od, datum_do, advokat_id, Now, Null,'IZNOS_ZADOLZUVANJEDO'));
       if iznos_zadolzuvanje_od >0 then
          ShowMessage('�������� � ������������ �� ����������� '+IntToStr(iznos_zadolzuvanje_od));
       if iznos_zadolzuvanje_do >0 then
          ShowMessage('�������� � ������������ �� ����������� '+IntToStr(iznos_zadolzuvanje_do))
     end;
end;

procedure TfrmPorodilno.aNovExecute(Sender: TObject);
begin
  inherited;

  if tag = 1 then
     dm.tblPorodilnoADVOKAT_ID.Value:=dm.tblAdvokatiID.Value
  else if tag = 2 then
     dm.tblPorodilnoADVOKAT_ID.Value:=dm.tblResenijaAdvokatiADVOKAT_ID.Value
end;

procedure TfrmPorodilno.aZapisiExecute(Sender: TObject);
var iznos_zadolzuvanje_od,iznos_zadolzuvanje_do:Integer ;
begin
  inherited;
       iznos_zadolzuvanje_od:=(dm.insert_return5(dm.PROC_ADV_OSLOBODUVANJE, 'DATUM_OD', 'DATUM_DO', 'ADVOKAT_ID', 'DATUM', Null, dm.tblPorodilnoDATUM_OD.Value, dm.tblPorodilnoDATUM_DO.Value, dm.tblPorodilnoADVOKAT_ID.Value, Now, Null,'IZNOS_ZADOLZUVANJEOD'));
       iznos_zadolzuvanje_do:=(dm.insert_return5(dm.PROC_ADV_OSLOBODUVANJE, 'DATUM_OD', 'DATUM_DO', 'ADVOKAT_ID', 'DATUM', Null, dm.tblPorodilnoDATUM_OD.Value, dm.tblPorodilnoDATUM_DO.Value, dm.tblPorodilnoADVOKAT_ID.Value, Now, Null,'IZNOS_ZADOLZUVANJEDO'));
       if iznos_zadolzuvanje_od >0 then
          ShowMessage('�������� � ������������ �� ����������� '+IntToStr(iznos_zadolzuvanje_od));
       if iznos_zadolzuvanje_do >0 then
          ShowMessage('�������� � ������������ �� ����������� '+IntToStr(iznos_zadolzuvanje_do))
end;

procedure TfrmPorodilno.FormShow(Sender: TObject);
begin
  inherited;
  dm.tblPorodilno.Close;
  if tag = 1 then
       dm.tblPorodilno.ParamByName('advokat_id').Value:=dm.tblAdvokatiID.Value
  else if tag = 2 then
       dm.tblPorodilno.ParamByName('advokat_id').Value:=dm.tblResenijaAdvokatiADVOKAT_ID.Value
  else
       dm.tblPorodilno.ParamByName('advokat_id').Value:=-100;
  dm.tblPorodilno.Open;
end;

end.
