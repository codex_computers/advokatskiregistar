object frmImportExcel: TfrmImportExcel
  Left = 0
  Top = 0
  Caption = 'frmImportExcel'
  ClientHeight = 476
  ClientWidth = 643
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object cxButton1: TcxButton
    Left = 216
    Top = 8
    Width = 75
    Height = 25
    Caption = 'cxButton1'
    TabOrder = 0
    OnClick = cxButton1Click
  end
  object Memo: TMemo
    Left = 16
    Top = 8
    Width = 185
    Height = 65
    Lines.Strings = (
      'Memo')
    TabOrder = 1
  end
  object DBGrid: TDBGrid
    Left = 8
    Top = 347
    Width = 481
    Height = 121
    DataSource = DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object cxButton2: TcxButton
    Left = 496
    Top = 80
    Width = 75
    Height = 25
    Caption = 'cxButton2'
    TabOrder = 3
    OnClick = cxButton2Click
  end
  object DataSource1: TDataSource
    Left = 200
    Top = 168
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from'
      '[uplati$]')
    Left = 408
    Top = 168
    object ADOQuery1naziv: TStringField
      FieldName = 'naziv'
      Size = 255
    end
    object ADOQuery1iznos: TFloatField
      FieldName = 'iznos'
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Mode=ReadWrite;Ex' +
      'tended Properties="DSN=uplati;DBQ=D:\Projects\dstojkova\Uplati.x' +
      'ls;DefaultDir=D:\Projects\dstojkova;DriverId=790;FIL=excel 8.0;M' +
      'axBufferSize=2048;PageTimeout=5;";Initial Catalog=D:\Projects\ds' +
      'tojkova\Uplati'
    LoginPrompt = False
    Provider = 'MSDASQL.1'
    Left = 312
    Top = 168
  end
  object ADOStoredProc1: TADOStoredProc
    Connection = ADOConnection1
    Parameters = <>
    Left = 544
    Top = 184
  end
end
