object frmZadolzuvanje: TfrmZadolzuvanje
  Left = 137
  Top = 231
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1047#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
  ClientHeight = 741
  ClientWidth = 1205
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1205
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1BarNefakturirani'
        end
        item
          ToolbarName = 'dxBarManager1BarFakturirani'
        end
        item
          ToolbarName = 'dxBarManager1BarUplati'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 718
    Width = 1205
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1205
    Height = 592
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Properties.ActivePage = cxTabSheetNezadolzeni
    Properties.CustomButtons.Buttons = <>
    OnPageChanging = cxPageControl1PageChanging
    ClientRectBottom = 592
    ClientRectRight = 1205
    ClientRectTop = 24
    object cxTabSheetNezadolzeni: TcxTabSheet
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080' '#1082#1086#1080' '#1053#1045' '#1089#1077' '#1047#1040#1044#1054#1051#1046#1045#1053#1048
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1205
        Height = 568
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid1DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmAlways
          FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
          DataController.DataSource = dm.dsNezadolzeniAdvokati
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = cxGrid1DBTableView1TIP_RESENIE_ID
            end
            item
              Format = '0.00,.'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.Footer = True
          object Selekcija: TcxGridDBColumn
            Caption = #1057#1077#1083#1077#1082#1090#1080#1088#1072#1112
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.OnEditValueChanged = SelekcijaPropertiesEditValueChanged
            Visible = False
            VisibleForCustomization = False
          end
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
            Width = 63
          end
          object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1040#1082#1090#1080#1074#1077#1085
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1052#1080#1088#1091#1074#1072#1114#1077
                ImageIndex = 4
                Value = 2
              end
              item
                Description = #1048#1079#1073#1088#1080#1096#1072#1085
                ImageIndex = 3
                Value = 3
              end>
            Visible = False
            Options.Editing = False
            Width = 96
          end
          object cxGrid1DBTableView1TIP_RESENIE_ID: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_RESENIE_ID'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1040#1082#1090#1080#1074#1077#1085
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1052#1080#1088#1091#1074#1072#1114#1077
                ImageIndex = 4
                Value = 2
              end
              item
                Description = #1048#1079#1073#1088#1080#1096#1072#1085
                ImageIndex = 3
                Value = 3
              end
              item
                Description = #1053#1077#1084#1072' '#1072#1082#1090#1080#1074#1085#1086' '#1088#1077#1096#1077#1085#1080#1077
                ImageIndex = 14
                Value = 100
              end>
            Options.Editing = False
            Width = 144
          end
          object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1EMBG: TcxGridDBColumn
            DataBinding.FieldName = 'EMBG'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1IME: TcxGridDBColumn
            DataBinding.FieldName = 'IME'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
            DataBinding.FieldName = 'PREZIME'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA_DATUM'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
            DataBinding.FieldName = 'status_naziv'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1STATUS: TcxGridDBColumn
            DataBinding.FieldName = 'STATUS'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1RESENIE_ID: TcxGridDBColumn
            DataBinding.FieldName = 'RESENIE_ID'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1MESTO_LK: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_LK'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1MESTO_NAZIV_LK: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_NAZIV_LK'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
            Visible = False
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'TELEFON'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN_TELEFON'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'EMAIL'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
            DataBinding.FieldName = 'WEB_STRANA'
            Options.Editing = False
            Width = 150
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheetZadolzeni: TcxTabSheet
      Caption = #1047#1040#1044#1054#1051#1046#1045#1053#1048
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1205
        Height = 568
        Align = alClient
        TabOrder = 0
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid2DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmAlways
          FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
          DataController.DataSource = dm.dsZadolzeniAdvokati
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = cxGrid2DBTableView1TIP_RESENIE_ID
            end
            item
              Format = '0.00,.'
              Kind = skSum
              Column = cxGrid2DBTableView1IZNOS
            end
            item
              Format = '0.00,.'
              Kind = skSum
              Column = cxGrid2DBTableView1DOLG
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
          OptionsView.Footer = True
          object cxGrid2DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 78
          end
          object cxGrid2DBTableView1ADVOKAT_ID: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_ID'
            Visible = False
            Width = 176
          end
          object cxGrid2DBTableView1GODINA: TcxGridDBColumn
            DataBinding.FieldName = 'GODINA'
            Width = 94
          end
          object cxGrid2DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Width = 108
          end
          object cxGrid2DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_NAZIV'
            Width = 164
          end
          object cxGrid2DBTableView1EMBG: TcxGridDBColumn
            DataBinding.FieldName = 'EMBG'
            Width = 104
          end
          object cxGrid2DBTableView1IZNOS: TcxGridDBColumn
            DataBinding.FieldName = 'ZADOLZEN_IZNOS'
            Width = 90
          end
          object cxGrid2DBTableView1DOLG: TcxGridDBColumn
            DataBinding.FieldName = 'DOLG'
            Width = 99
          end
          object cxGrid2DBTableView1LICENCA: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA'
            Width = 150
          end
          object cxGrid2DBTableView1LICENCA_DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA_DATUM'
            Width = 150
          end
          object cxGrid2DBTableView1TIP_RESENIE_ID: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_RESENIE_ID'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1053#1077#1084#1072' '#1072#1082#1090#1080#1074#1085#1086' '#1088#1077#1096#1077#1085#1080#1077
                ImageIndex = 14
                Value = 100
              end
              item
                Description = #1040#1082#1090#1080#1074#1077#1085
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1052#1080#1088#1091#1074#1072#1114#1077
                ImageIndex = 4
                Value = 2
              end
              item
                Description = #1048#1079#1073#1088#1080#1096#1072#1085
                ImageIndex = 3
                Value = 3
              end>
            Width = 149
          end
          object cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1040#1082#1090#1080#1074#1077#1085
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1042#1086' '#1084#1080#1088#1091#1074#1072#1114#1077
                ImageIndex = 4
                Value = 2
              end
              item
                Description = #1048#1079#1073#1088#1080#1096#1072#1085
                ImageIndex = 3
                Value = 3
              end>
            Visible = False
            Width = 110
          end
          object cxGrid2DBTableView1status_naziv: TcxGridDBColumn
            DataBinding.FieldName = 'status_naziv'
            Width = 150
          end
          object cxGrid2DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
            Visible = False
            Width = 194
          end
          object cxGrid2DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
            Width = 132
          end
          object cxGrid2DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
            Visible = False
            Width = 150
          end
          object cxGrid2DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
            Width = 150
          end
          object cxGrid2DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Visible = False
            Width = 150
          end
          object cxGrid2DBTableView1MESTO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_NAZIV'
            Width = 150
          end
          object cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
            Visible = False
            Width = 150
          end
          object cxGrid2DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
            Width = 150
          end
          object cxGrid2DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Width = 150
          end
          object cxGrid2DBTableView1TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'TELEFON'
            Width = 150
          end
          object cxGrid2DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN_TELEFON'
            Width = 150
          end
          object cxGrid2DBTableView1EMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'EMAIL'
            Width = 150
          end
          object cxGrid2DBTableView1WEB_STRANA: TcxGridDBColumn
            DataBinding.FieldName = 'WEB_STRANA'
            Width = 150
          end
        end
        object cxGrid2Level1: TcxGridLevel
          GridView = cxGrid2DBTableView1
        end
      end
    end
    object cxTabSheetUplati: TcxTabSheet
      Caption = #1059#1055#1051#1040#1058#1048
      ImageIndex = 3
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1205
        Height = 568
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid3DBTableView1: TcxGridDBTableView
          OnKeyPress = cxGrid3DBTableView1KeyPress
          Navigator.Buttons.CustomButtons = <>
          FindPanel.DisplayMode = fpdmAlways
          FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
          DataController.DataSource = dm.dsUplata
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGrid3DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 62
          end
          object cxGrid3DBTableView1ZADOLZUVANJE_ID: TcxGridDBColumn
            DataBinding.FieldName = 'ZADOLZUVANJE_ID'
            Visible = False
            Width = 141
          end
          object cxGrid3DBTableView1GODINA: TcxGridDBColumn
            DataBinding.FieldName = 'GODINA'
            Width = 75
          end
          object cxGrid3DBTableView1DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'DATUM'
            Width = 67
          end
          object cxGrid3DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_NAZIV'
            Width = 195
          end
          object cxGrid3DBTableView1ADVOKAT_EMBG: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_EMBG'
            Width = 98
          end
          object cxGrid3DBTableView1ADVOKAT_ID: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKAT_ID'
            Visible = False
            Width = 185
          end
          object cxGrid3DBTableView1LICENCA: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA'
            Width = 116
          end
          object cxGrid3DBTableView1LICENCA_DATUM: TcxGridDBColumn
            DataBinding.FieldName = 'LICENCA_DATUM'
            Width = 103
          end
          object cxGrid3DBTableView1IZNOS: TcxGridDBColumn
            DataBinding.FieldName = 'IZNOS'
            Width = 90
          end
          object cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM1'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM2'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOM3'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1TS_INS: TcxGridDBColumn
            DataBinding.FieldName = 'TS_INS'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1TS_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'TS_UPD'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1USR_INS: TcxGridDBColumn
            DataBinding.FieldName = 'USR_INS'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1USR_UPD: TcxGridDBColumn
            DataBinding.FieldName = 'USR_UPD'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1AKTIVEN: TcxGridDBColumn
            DataBinding.FieldName = 'AKTIVEN'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1040#1082#1090#1080#1074#1077#1085
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1042#1086' '#1084#1080#1088#1091#1074#1072#1114#1077
                ImageIndex = 4
                Value = 2
              end
              item
                Description = #1048#1079#1073#1088#1080#1096#1072#1085
                ImageIndex = 3
                Value = 3
              end>
            Visible = False
            Width = 111
          end
          object cxGrid3DBTableView1TIP_RESENIE_ID: TcxGridDBColumn
            DataBinding.FieldName = 'TIP_RESENIE_ID'
            PropertiesClassName = 'TcxImageComboBoxProperties'
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1040#1082#1090#1080#1074#1077#1085
                ImageIndex = 2
                Value = 1
              end
              item
                Description = #1048#1079#1073#1088#1080#1096#1072#1085
                ImageIndex = 3
                Value = 3
              end
              item
                Description = #1052#1080#1088#1091#1074#1072#1114#1077
                ImageIndex = 4
                Value = 2
              end
              item
                Description = #1053#1077#1084#1072' '#1072#1082#1090#1080#1074#1085#1086' '#1088#1077#1096#1077#1085#1080#1077
                ImageIndex = 14
                Value = 100
              end>
            Width = 130
          end
          object cxGrid3DBTableView1STATUS: TcxGridDBColumn
            DataBinding.FieldName = 'STATUS'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1status_naziv: TcxGridDBColumn
            DataBinding.FieldName = 'status_naziv'
            Width = 150
          end
          object cxGrid3DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
            Visible = False
            Width = 184
          end
          object cxGrid3DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
            Width = 150
          end
          object cxGrid3DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
            Width = 150
          end
          object cxGrid3DBTableView1MESTO: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1MESTO_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'MESTO_NAZIV'
            Width = 150
          end
          object cxGrid3DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
            Visible = False
            Width = 150
          end
          object cxGrid3DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
            DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
            Width = 150
          end
          object cxGrid3DBTableView1ADRESA: TcxGridDBColumn
            DataBinding.FieldName = 'ADRESA'
            Width = 150
          end
          object cxGrid3DBTableView1TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'TELEFON'
            Width = 150
          end
          object cxGrid3DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
            DataBinding.FieldName = 'MOBILEN_TELEFON'
            Width = 150
          end
          object cxGrid3DBTableView1EMAIL: TcxGridDBColumn
            DataBinding.FieldName = 'EMAIL'
            Width = 150
          end
          object cxGrid3DBTableView1WEB_STRANA: TcxGridDBColumn
            DataBinding.FieldName = 'WEB_STRANA'
            Width = 150
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
    end
  end
  object PanelGrupnoBrisenje: TPanel
    Left = 376
    Top = 280
    Width = 321
    Height = 145
    TabOrder = 3
    Visible = False
    DesignSize = (
      321
      145)
    object cxGroupBox1: TcxGroupBox
      Left = 8
      Top = 8
      Anchors = [akLeft, akTop, akRight, akBottom]
      Caption = #1048#1079#1073#1077#1088#1080' '#1088#1072#1085#1075' '#1085#1072' '#1096#1080#1092#1088#1080' '#1085#1072' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1072' '#1079#1072' '#1073#1088#1080#1096#1077#1114#1077' '
      TabOrder = 0
      Height = 129
      Width = 305
      object cxLabel2: TcxLabel
        Left = 31
        Top = 35
        Caption = #1054#1044' :'
      end
      object cxLabel1: TcxLabel
        Left = 31
        Top = 62
        Caption = #1044#1054' :'
      end
      object txtOd: TcxTextEdit
        Left = 64
        Top = 32
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 209
      end
      object txtDo: TcxTextEdit
        Left = 64
        Top = 59
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 209
      end
      object cxButton1: TcxButton
        Left = 179
        Top = 86
        Width = 93
        Height = 25
        Action = aOtkazi
        TabOrder = 5
      end
      object cxButton2: TcxButton
        Left = 64
        Top = 86
        Width = 93
        Height = 25
        Action = aBrisiGrupno
        TabOrder = 4
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 913
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1082
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1060#1080#1083#1090#1077#1088
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1177
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 73
          Visible = True
          ItemName = 'cbBarGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarNefakturirani: TdxBar
      Caption = #1047#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080' '#1072#1076#1074#1086#1082#1072#1090#1080
      CaptionButtons = <>
      DockedLeft = 130
      DockedTop = 0
      FloatLeft = 1177
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarDateComboDatumZadolzi'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarFakturirani: TdxBar
      Caption = '  '
      CaptionButtons = <>
      DockedLeft = 366
      DockedTop = 0
      FloatLeft = 1239
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarUplati: TdxBar
      Caption = '         '
      CaptionButtons = <>
      DockedLeft = 679
      DockedTop = 0
      FloatLeft = 1231
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar1: TdxBar
      Caption = '           '
      CaptionButtons = <>
      DockedLeft = 743
      DockedTop = 0
      FloatLeft = 1231
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = True
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        '2011'
        '2012'
        '2013'
        '2014'
        '2015')
    end
    object cbBarGodina: TdxBarCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbBarGodinaChange
      Items.Strings = (
        '1981'
        '1982'
        '1983'
        '1984'
        '1985'
        '1986'
        '1987'
        '1989'
        '1990'
        '1991'
        '1992'
        '1993'
        '1994'
        '1995'
        '1996'
        '1997'
        '1998'
        '1999'
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
      ItemIndex = -1
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aZadolzi
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiZadolzuvanje
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPecatiFaktura
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aDizajnFaktura
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPecatiFaktura
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aDizajnFaktura
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aGrupnoPecatenje
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aUplati
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aBrisiUplata
      Category = 0
    end
    object dxBarDateComboDatumZadolzi: TdxBarDateCombo
      Caption = #1044#1072#1090#1091#1084
      Category = 0
      Hint = #1044#1072#1090#1091#1084
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FFFFFFFFFF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF0000
        00FF000000FF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000080FF000080FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFF000080FF000080FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000080FF0000
        80FFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        80FF000080FFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFF000080FF000080FF000080FF0000
        80FF000080FFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFF000080FF000000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF800000FF800000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFF000000FF800000FF800000FF800000FF800000FF800000FF8000
        00FF800000FF800000FF800000FF000000FFFF00FF00000000FF800000FF8000
        00FF800000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FF800000FF8000
        00FF800000FF800000FF800000FF800000FF800000FF800000FF800000FF0000
        00FFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FFFF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aGrupnoBrisenjeZadolzuvanje
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aResenijaAdvokati
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = actIzvestajZaClenarina
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aBrisiGrupno: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 46
      OnExecute = aBrisiGrupnoExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aZadolzi: TAction
      Caption = #1047#1072#1076#1086#1083#1078#1080
      ImageIndex = 92
      OnExecute = aZadolziExecute
    end
    object aBrisiZadolzuvanje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1086' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
      ImageIndex = 91
      OnExecute = aBrisiZadolzuvanjeExecute
    end
    object aPecatiFaktura: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076#1112'/'#1055#1077#1095#1072#1090#1080' '#1060#1072#1082#1090#1091#1088#1072
      ImageIndex = 19
      ShortCut = 121
    end
    object aDizajnFaktura: TAction
      Caption = #1060#1072#1082#1090#1091#1088#1072
      ShortCut = 24697
      OnExecute = aDizajnFakturaExecute
    end
    object aGrupnoPecatenje: TAction
      Caption = #1043#1088#1091#1087#1085#1086' '#1087#1077#1095#1072#1090#1077#1114#1077' '#1085#1072' '#1060#1072#1082#1090#1091#1088#1080
      ImageIndex = 15
      OnExecute = aGrupnoPecatenjeExecute
    end
    object aUplati: TAction
      Caption = #1059#1087#1083#1072#1090#1080
      ImageIndex = 90
      OnExecute = aUplatiExecute
    end
    object aBrisiUplata: TAction
      Caption = #1041#1088#1080#1096#1080' '#1091#1087#1083#1072#1090#1072
      ImageIndex = 91
      OnExecute = aBrisiUplataExecute
    end
    object aGrupnoBrisenjeZadolzuvanje: TAction
      Caption = #1043#1088#1091#1087#1085#1086' '#1073#1088#1080#1096#1077#1114#1077' '#1079#1072#1076#1086#1083#1078#1091#1074#1072#1114#1072
      ImageIndex = 46
      OnExecute = aGrupnoBrisenjeZadolzuvanjeExecute
    end
    object aResenijaAdvokati: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1112#1072' '#1079#1072' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 79
      OnExecute = aResenijaAdvokatiExecute
    end
    object actIzvestajZaClenarina: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1095#1083#1077#1085#1072#1088#1080#1085#1072
      ImageIndex = 83
      OnExecute = actIzvestajZaClenarinaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 248
    Top = 344
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 464
    Top = 416
  end
end
