inherited frmDisciplinskiMerki: TfrmDisciplinskiMerki
  Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1080' '#1084#1077#1088#1082#1080
  ClientHeight = 741
  ClientWidth = 939
  ExplicitWidth = 955
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 939
    Height = 276
    ExplicitWidth = 939
    ExplicitHeight = 276
    inherited cxGrid1: TcxGrid
      Width = 935
      Height = 272
      ExplicitWidth = 935
      ExplicitHeight = 272
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        DataController.DataSource = dm.dsDisciplinskiMerki
        DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 72
        end
        object cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_ID'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Visible = False
          Width = 62
        end
        object cxGrid1DBTableView1TIP: TcxGridDBColumn
          DataBinding.FieldName = 'TIP'
          Width = 25
        end
        object cxGrid1DBTableView1TIP_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_NAZIV'
          Width = 308
        end
        object cxGrid1DBTableView1DO_BR: TcxGridDBColumn
          DataBinding.FieldName = 'DO_BR'
          Width = 100
        end
        object cxGrid1DBTableView1DS_BR: TcxGridDBColumn
          DataBinding.FieldName = 'DS_BR'
          Width = 100
        end
        object cxGrid1DBTableView1EV_BR: TcxGridDBColumn
          DataBinding.FieldName = 'EV_BR'
          Width = 100
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          PropertiesClassName = 'TcxBlobEditProperties'
          Properties.BlobEditKind = bekMemo
          Properties.BlobPaintStyle = bpsText
          Width = 291
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 84
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 75
        end
        object cxGrid1DBTableView1IZNOS: TcxGridDBColumn
          DataBinding.FieldName = 'IZNOS'
          Width = 79
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
          Width = 111
        end
        object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA'
          Width = 115
        end
        object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_DATUM'
          Width = 102
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Width = 86
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Width = 118
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
          Width = 131
        end
        object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_naziv'
          Width = 150
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
          Visible = False
          Width = 165
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
          Width = 205
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
          Width = 199
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 150
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Width = 150
        end
        object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN_TELEFON'
          Width = 150
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 150
        end
        object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
          DataBinding.FieldName = 'WEB_STRANA'
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 150
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 402
    Width = 939
    Height = 316
    ParentFont = False
    ExplicitTop = 368
    ExplicitWidth = 923
    ExplicitHeight = 316
    inherited Label1: TLabel
      Left = 751
      Top = 45
      Visible = False
      ExplicitLeft = 751
      ExplicitTop = 45
    end
    object Label2: TLabel [1]
      Left = 62
      Top = 226
      Width = 33
      Height = 13
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [2]
      Left = 69
      Top = 51
      Width = 26
      Height = 13
      Caption = #1058#1080#1087' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 37
      Top = 24
      Width = 58
      Height = 13
      Caption = #1040#1076#1074#1086#1082#1072#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel [4]
      Left = 55
      Top = 274
      Width = 40
      Height = 13
      Caption = #1048#1079#1085#1086#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 803
      Top = 42
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      TabOrder = 4
      Visible = False
      ExplicitLeft = 803
      ExplicitTop = 42
    end
    inherited OtkaziButton: TcxButton
      Left = 840
      Top = 244
      TabOrder = 10
      ExplicitLeft = 824
      ExplicitTop = 260
    end
    inherited ZapisiButton: TcxButton
      Left = 759
      Top = 244
      TabOrder = 9
      ExplicitLeft = 743
      ExplicitTop = 260
    end
    object OPIS: TcxDBMemo
      Left = 101
      Top = 223
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      Properties.WantReturns = False
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Height = 42
      Width = 604
    end
    object TIP_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 144
      Top = 48
      Hint = #1058#1080#1087' '#1085#1072' '#1076#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1072' '#1084#1077#1088#1082#1072
      DataBinding.DataField = 'TIP'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListSource = dm.dsTipDisciplinskiMerki
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 561
    end
    object TIP: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 48
      Hint = #1058#1080#1087' '#1085#1072' '#1076#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1072' '#1084#1077#1088#1082#1072' - '#1096#1080#1092#1088#1072
      BeepOnEnter = False
      DataBinding.DataField = 'TIP'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object ADVOKAT_ID: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 21
      BeepOnEnter = False
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object ADVOKAT_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 151
      Top = 21
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ADVOKAT_NAZIV'
        end>
      Properties.ListSource = dm.dsAdvokati
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 554
    end
    object cxGroupBoxPeriod: TcxGroupBox
      Left = 101
      Top = 151
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090
      TabOrder = 6
      Height = 66
      Width = 604
      object Label6: TLabel
        Left = 206
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label3: TLabel
        Left = 14
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM_DO: TcxDBDateEdit
        Left = 277
        Top = 28
        Hint = #1044#1072#1090#1091#1084
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsDisciplinskiMerki
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 115
      end
      object DATUM: TcxDBDateEdit
        Tag = 1
        Left = 85
        Top = 28
        Hint = #1044#1072#1090#1091#1084
        DataBinding.DataField = 'DATUM'
        DataBinding.DataSource = dm.dsDisciplinskiMerki
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 115
      end
    end
    object IZNOS: TcxDBTextEdit
      Left = 101
      Top = 271
      BeepOnEnter = False
      DataBinding.DataField = 'IZNOS'
      DataBinding.DataSource = dm.dsDisciplinskiMerki
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 150
    end
    object cxGroupBoxBr: TcxGroupBox
      Left = 101
      Top = 75
      Caption = '  '
      TabOrder = 5
      Height = 66
      Width = 604
      object lbl1: TLabel
        Left = 225
        Top = 32
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1057'. '#1073#1088'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object lbl2: TLabel
        Left = 37
        Top = 32
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = #1045#1042'. '#1073#1088'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object lbl3: TLabel
        Left = 428
        Top = 32
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1054'. :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object EV_BR: TcxDBTextEdit
        Left = 86
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'EV_BR'
        DataBinding.DataSource = dm.dsDisciplinskiMerki
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 115
      end
      object DS_BR: TcxDBTextEdit
        Left = 277
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'DS_BR'
        DataBinding.DataSource = dm.dsDisciplinskiMerki
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 115
      end
      object DO_BR: TcxDBTextEdit
        Left = 461
        Top = 29
        BeepOnEnter = False
        DataBinding.DataField = 'DO_BR'
        DataBinding.DataSource = dm.dsDisciplinskiMerki
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 115
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 939
    ExplicitWidth = 939
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 939
    ExplicitTop = 684
    ExplicitWidth = 923
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Top = 232
  end
  inherited PopupMenu1: TPopupMenu
    Left = 656
    Top = 128
  end
  inherited dxBarManager1: TdxBarManager
    Top = 240
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 368
    Top = 136
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 42409.598189699070000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
