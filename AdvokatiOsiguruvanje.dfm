inherited frmAdvokatiOsiguruvanje: TfrmAdvokatiOsiguruvanje
  Caption = #1054#1089#1080#1075#1091#1088#1091#1074#1072#1114#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
  ClientHeight = 741
  ClientWidth = 928
  ExplicitWidth = 944
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 928
    Height = 275
    ExplicitWidth = 928
    ExplicitHeight = 275
    inherited cxGrid1: TcxGrid
      Width = 924
      Height = 271
      ExplicitWidth = 924
      ExplicitHeight = 271
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        DataController.DataSource = dm.dsOsiguruvanje
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 78
        end
        object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA'
          Width = 93
        end
        object cxGrid1DBTableView1OSIGURITELNA_KOMP_ID: TcxGridDBColumn
          DataBinding.FieldName = 'OSIGURITELNA_KOMP_ID'
          Visible = False
          Width = 188
        end
        object cxGrid1DBTableView1OSIGURITELNA_KOMP_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OSIGURITELNA_KOMP_NAZIV'
          Width = 198
        end
        object cxGrid1DBTableView1POLISA: TcxGridDBColumn
          DataBinding.FieldName = 'POLISA'
          Width = 126
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 77
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 80
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
          Width = 131
        end
        object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_DATUM'
          Width = 250
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Width = 250
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Width = 82
        end
        object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_naziv'
          Width = 72
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
          Visible = False
          Width = 181
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
          Width = 198
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 133
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
          Width = 194
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 123
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Width = 139
        end
        object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN_TELEFON'
          Width = 143
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 124
        end
        object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
          DataBinding.FieldName = 'WEB_STRANA'
          Width = 107
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 174
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 177
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 212
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 243
        end
        object cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_ID'
          Visible = False
          Width = 182
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 250
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 401
    Width = 928
    Height = 317
    ExplicitTop = 488
    ExplicitWidth = 928
    ExplicitHeight = 317
    inherited Label1: TLabel
      Left = 662
      Top = 9
      Visible = False
      ExplicitLeft = 662
      ExplicitTop = 9
    end
    object lbl1: TLabel [1]
      Left = 61
      Top = 40
      Width = 58
      Height = 13
      Caption = #1040#1076#1074#1086#1082#1072#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 714
      Top = 6
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsOsiguruvanje
      TabOrder = 2
      Visible = False
      ExplicitLeft = 714
      ExplicitTop = 6
    end
    inherited OtkaziButton: TcxButton
      Left = 837
      Top = 277
      TabOrder = 6
      ExplicitLeft = 837
      ExplicitTop = 277
    end
    inherited ZapisiButton: TcxButton
      Left = 756
      Top = 277
      TabOrder = 5
      ExplicitLeft = 756
      ExplicitTop = 277
    end
    object ADVOKAT_ID: TcxDBTextEdit
      Tag = 1
      Left = 125
      Top = 37
      BeepOnEnter = False
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsOsiguruvanje
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object ADVOKAT_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 168
      Top = 37
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsOsiguruvanje
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ADVOKAT_NAZIV'
        end>
      Properties.ListSource = dm.dsAdvokati
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 428
    end
    object cxGroupBoxPeriod: TcxGroupBox
      Left = 24
      Top = 191
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090
      TabOrder = 4
      Height = 66
      Width = 572
      object lbl3: TLabel
        Left = 342
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object lbl4: TLabel
        Left = 30
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 413
        Top = 28
        Hint = #1044#1072#1090#1091#1084
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsOsiguruvanje
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 126
      end
      object DATUM_OD: TcxDBDateEdit
        Tag = 1
        Left = 101
        Top = 28
        Hint = #1044#1072#1090#1091#1084
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsOsiguruvanje
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 124
      end
    end
    object cxGroupBoxOsiguritelnaKompanija: TcxGroupBox
      Left = 24
      Top = 75
      Caption = #1054#1089#1080#1075#1091#1088#1080#1090#1077#1083#1085#1072' '#1082#1086#1084#1087#1072#1085#1080#1112#1072
      TabOrder = 3
      Height = 94
      Width = 572
      object lbl5: TLabel
        Left = 55
        Top = 31
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = #1053#1072#1079#1080#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object lbl6: TLabel
        Left = 49
        Top = 58
        Width = 47
        Height = 13
        Caption = #1055#1086#1083#1080#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object OSIGURITELNA_KOMP_ID: TcxDBTextEdit
        Tag = 1
        Left = 101
        Top = 28
        BeepOnEnter = False
        DataBinding.DataField = 'OSIGURITELNA_KOMP_ID'
        DataBinding.DataSource = dm.dsOsiguruvanje
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object OSIGURITELNA_KOMP_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 143
        Top = 28
        DataBinding.DataField = 'OSIGURITELNA_KOMP_ID'
        DataBinding.DataSource = dm.dsOsiguruvanje
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsOsiguritelniKompanii
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 396
      end
      object POLISA: TcxDBTextEdit
        Tag = 1
        Left = 101
        Top = 55
        BeepOnEnter = False
        DataBinding.DataField = 'POLISA'
        DataBinding.DataSource = dm.dsOsiguruvanje
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnKeyDown = EnterKakoTab
        Width = 80
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 928
    ExplicitWidth = 928
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 928
    ExplicitTop = 805
    ExplicitWidth = 928
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 120
  end
  inherited PopupMenu1: TPopupMenu
    Left = 352
    Top = 136
  end
  inherited dxBarManager1: TdxBarManager
    Left = 648
    Top = 136
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 211
      FloatClientHeight = 135
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 61
          Visible = True
          ItemName = 'cxGodina'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButtonSoberi'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 562
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = actPregledNaOsiguruvanje
      Category = 0
    end
    object cxGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        '1981'
        '1982'
        '1983'
        '1984'
        '1985'
        '1986'
        '1987'
        '1989'
        '1990'
        '1991'
        '1992'
        '1993'
        '1994'
        '1995'
        '1996'
        '1997'
        '1998'
        '1999'
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016'
        '2017'
        '2018'
        '2019'
        '2020'
        '2021'
        '2022'
        '2023'
        '2024'
        '2025'
        '2026'
        '2027'
        '2028'
        '2029'
        '2030'
        '2031'
        '2032'
        '2033'
        '2034'
        '2035'
        '2036'
        '2037'
        '2038'
        '2039'
        '2040'
        '2041'
        '2042'
        '2043'
        '2044'
        '2045'
        '2046'
        '2047'
        '2048'
        '2049'
        '2050')
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 144
    object actPregledNaOsiguruvanje: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1086#1089#1080#1075#1091#1088#1091#1074#1072#1114#1077' '#1087#1086' '#1075#1086#1076#1080#1085#1072
      ImageIndex = 19
      OnExecute = actPregledNaOsiguruvanjeExecute
    end
    object actDPregledNaOsiguruvanje: TAction
      Caption = 'actDPregledNaOsiguruvanje'
      ImageIndex = 19
      ShortCut = 24697
      OnExecute = actDPregledNaOsiguruvanjeExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 43696.455671886570000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
