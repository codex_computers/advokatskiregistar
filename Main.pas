unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinOffice2013White, System.Actions, cxCheckBox, cxRadioGroup, cxCalendar,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, System.ImageList,
  dxSkinscxPCPainter, cxImageList;

type
  TfrmMain = class(TForm)
    dxRibbon1TabMeni: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    Image2: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    lblDatumVreme: TcxLabel;
    PanelMain: TPanel;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    aLogout: TAction;
    dxBarLargeButton8: TdxBarLargeButton;
    aRegAdvokati: TAction;
    dxBarManager1Bar4: TdxBar;
    aSifFirmi: TAction;
    aSifMesto: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    aOsveziPodatociSifrarnici: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aRegStrucniSorabotnici: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    aRegPripravnici: TAction;
    dxBarLargeButton14: TdxBarLargeButton;
    aAdvokatskiDrustva: TAction;
    dxBarLargeButton9: TdxBarLargeButton;
    aAdvokatskiKancelarii: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    aResenijaAdvokati: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    aSifTipResenie: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    aAdvokatskiZaednici: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aGodisenIznosClenarina: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aZadolzuvanje: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aUplatiDolgovi: TAction;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    aImportExcel: TAction;
    dxBarLargeButton23: TdxBarLargeButton;
    aUplatExcel: TAction;
    dxBarButton2: TdxBarButton;
    aImportFromExcel: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    aDisciplinskiMerki: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    aTipDisciplinskiMerki: TAction;
    aSifOpstina: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    aSifsysSetup: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    aPocetnaSostojba: TAction;
    dxBarLargeButton32: TdxBarLargeButton;
    aSifPretsedatelKomisija: TAction;
    dxRibbon1TabClenarina: TdxRibbonTab;
    dxRibbon1TabResenija: TdxRibbonTab;
    dxBarManager1Bar8: TdxBar;
    aResenijaAD: TAction;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarManager1Bar9: TdxBar;
    dxbrlrgbtn1: TdxBarLargeButton;
    actporodilno: TAction;
    dxbrlrgbtn2: TdxBarLargeButton;
    actOsiguritelniKompanii: TAction;
    dxbrlrgbtn3: TdxBarLargeButton;
    actOsiguruvanje: TAction;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aLogoutExecute(Sender: TObject);
    procedure aRegAdvokatiExecute(Sender: TObject);
    procedure aSifMestoExecute(Sender: TObject);
    procedure aOsveziPodatociSifrarniciExecute(Sender: TObject);
    procedure aRegStrucniSorabotniciExecute(Sender: TObject);
    procedure aRegPripravniciExecute(Sender: TObject);
    procedure aAdvokatskiDrustvaExecute(Sender: TObject);
    procedure aAdvokatskiKancelariiExecute(Sender: TObject);
    procedure aResenijaAdvokatiExecute(Sender: TObject);
    procedure aSifTipResenieExecute(Sender: TObject);
    procedure aAdvokatskiZaedniciExecute(Sender: TObject);
    procedure aGodisenIznosClenarinaExecute(Sender: TObject);
    procedure aZadolzuvanjeExecute(Sender: TObject);
    procedure aUplatiDolgoviExecute(Sender: TObject);
    procedure aImportExcelExecute(Sender: TObject);
    procedure aUplatExcelExecute(Sender: TObject);
    procedure aImportFromExcelExecute(Sender: TObject);
    procedure aDisciplinskiMerkiExecute(Sender: TObject);
    procedure aTipDisciplinskiMerkiExecute(Sender: TObject);
    procedure aSifOpstinaExecute(Sender: TObject);
    procedure aSifsysSetupExecute(Sender: TObject);
    procedure aPocetnaSostojbaExecute(Sender: TObject);
    procedure aSifPretsedatelKomisijaExecute(Sender: TObject);
    procedure aResenijaADExecute(Sender: TObject);
    procedure actporodilnoExecute(Sender: TObject);
    procedure actOsiguritelniKompaniiExecute(Sender: TObject);
    procedure actOsiguruvanjeExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, RegAdvokati, MK,Mesto,
  dmUnit, RegPripravnici, RegAdvokatskiDrustva,
  RegAdvokatskiKancelarii, ResenijaAdvokati, cxConstantsMak, TipResenie,
  RegStrucniSorabotnici, AdvokatskiZaednici, ClenarinaIznos, Zadolzuvanje,
  Uplati, ImportExcel, UplataStavki, UplataExcel, UplataFromExcel,
  TipDisciplinskiMerki, DisciplinskiMerki, Opstina, Setup, PocetnaSostojba, PretsedatelKomisija, ResenijaAD, Porodilno, OsiguritelniKompanii, AdvokatiOsiguruvanje;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aAdvokatskiDrustvaExecute(Sender: TObject);
begin
  frmAdvokatskiDrustva := TfrmAdvokatskiDrustva.Create(self,false);
  frmAdvokatskiDrustva.ShowModal;
  frmAdvokatskiDrustva.Free;
end;

procedure TfrmMain.aAdvokatskiKancelariiExecute(Sender: TObject);
begin
  frmAdvokatskiKancelarii := TfrmAdvokatskiKancelarii.Create(self,false);
  frmAdvokatskiKancelarii.ShowModal;
  frmAdvokatskiKancelarii.Free;
end;

procedure TfrmMain.aAdvokatskiZaedniciExecute(Sender: TObject);
begin
  frmAdvokatskaZaednica := TfrmAdvokatskaZaednica.Create(self,True);
  frmAdvokatskaZaednica.ShowModal;
  frmAdvokatskaZaednica.Free;
end;

procedure TfrmMain.actOsiguritelniKompaniiExecute(Sender: TObject);
begin
   frmOsiguritelniKompanii := TfrmOsiguritelniKompanii.Create(self,true);
   frmOsiguritelniKompanii.ShowModal;
   frmOsiguritelniKompanii.Free;
end;

procedure TfrmMain.actOsiguruvanjeExecute(Sender: TObject);
begin
   frmAdvokatiOsiguruvanje := TfrmAdvokatiOsiguruvanje.Create(self,false);
   frmAdvokatiOsiguruvanje.ShowModal;
   frmAdvokatiOsiguruvanje.Free;
end;

procedure TfrmMain.actporodilnoExecute(Sender: TObject);
begin
   frmPorodilno := TfrmPorodilno.Create(self,false);
   frmPorodilno.ShowModal;
   frmPorodilno.Free;
end;

procedure TfrmMain.aDisciplinskiMerkiExecute(Sender: TObject);
begin
   frmDisciplinskiMerki := TfrmDisciplinskiMerki.Create(self,false);
   frmDisciplinskiMerki.ShowModal;
   frmDisciplinskiMerki.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aGodisenIznosClenarinaExecute(Sender: TObject);
begin
  frmClenarinaIznos := TfrmClenarinaIznos.Create(self,True);
  frmClenarinaIznos.ShowModal;
  frmClenarinaIznos.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aImportExcelExecute(Sender: TObject);
begin
  frmImportExcel:= TfrmImportExcel.Create(Application);
  frmImportExcel.ShowModal;
  frmImportExcel.Free;
end;

procedure TfrmMain.aImportFromExcelExecute(Sender: TObject);
begin
  frmUplatiFromExcel:= TfrmUplatiFromExcel.Create(Application);
  frmUplatiFromExcel.ShowModal;
  frmUplatiFromExcel.Free;
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aLogoutExecute(Sender: TObject);
begin
  if (dmKon.Logout = false) then Close
  else
  begin
    SpremiForma(self);

    if dmKon.user = 'SYSDBA' then
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : �������������'
    else
      dxRibbonStatusBar1.Panels[0].Text := ' �������� : ' + dmKon.imeprezime; // ������� �� ���������� ��������

    // Ovde otvori posebni datasetovi koi ne se otvaraat vo OtvoriTabeli()
    OtvoriTabeli();
  end;
end;

procedure TfrmMain.aOsveziPodatociSifrarniciExecute(Sender: TObject);
begin
  dmMat.tblMesto.Close;
  dmMat.tblMesto.Open;
  dm.tblTipResenija.Close;
  dm.tblTipResenija.Open;
  dm.tblTipResenijaAD.Close;
  dm.tblTipResenijaAD.Open;
  dm.tblZaednici.Close;
  dm.tblZaednici.Open;
  dm.tblClenarinaIznos.Close;
  dm.tblClenarinaIznos.Open;
  dm.tblUplatiOdExcel.Close;
  dm.tblUplatiOdExcel.Open;
  dm.tblTipDisciplinskiMerki.Close;
  dm.tblTipDisciplinskiMerki.Open;
  dm.tblDisciplinskiMerki.Close;
  dm.tblDisciplinskiMerki.Open;
  dm.tblOsiguritelniKompanii.Close;
  dm.tblOsiguritelniKompanii.Open;
end;

procedure TfrmMain.aRegPripravniciExecute(Sender: TObject);
begin
  frmRegPripravnici := TfrmRegPripravnici.Create(self,true);
  frmRegPripravnici.ShowModal;
  frmRegPripravnici.Free;
end;

procedure TfrmMain.aPocetnaSostojbaExecute(Sender: TObject);
begin
  frmPocetnaSostojba := TfrmPocetnaSostojba.Create(self,true);
  frmPocetnaSostojba.ShowModal;
  frmPocetnaSostojba.Free;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aRegAdvokatiExecute(Sender: TObject);
begin
  frmRegAdvokati := TfrmRegAdvokati.Create(self,false);
  frmRegAdvokati.ShowModal;
  frmRegAdvokati.Free;
end;

procedure TfrmMain.aRegStrucniSorabotniciExecute(Sender: TObject);
begin
  frmRegStrucniSorabotnici := TfrmRegStrucniSorabotnici.Create(self,true);
  frmRegStrucniSorabotnici.ShowModal;
  frmRegStrucniSorabotnici.Free;
end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aSifMestoExecute(Sender: TObject);
begin
  frmMesto := TfrmMesto.Create(self,true);
  frmMesto.ShowModal;
  frmMesto.Free;
end;

procedure TfrmMain.aSifOpstinaExecute(Sender: TObject);
begin
  frmOpstina := TfrmOpstina.Create(self,true);
  frmOpstina.ShowModal;
  frmOpstina.Free;
end;

procedure TfrmMain.aSifPretsedatelKomisijaExecute(Sender: TObject);
begin
   frmPretsedatelKomisija := TfrmPretsedatelKomisija.Create(self,False);
   frmPretsedatelKomisija.ShowModal;
   frmPretsedatelKomisija.Free;
end;

procedure TfrmMain.aSifsysSetupExecute(Sender: TObject);
begin
   frmSetup := TfrmSetup.Create(self,False);
   frmSetup.ShowModal;
   frmSetup.Free;
end;

procedure TfrmMain.aSifTipResenieExecute(Sender: TObject);
begin
   frmTipResenie := TfrmTipResenie.Create(self,true);
   frmTipResenie.ShowModal;
   frmTipResenie.Free;
end;

procedure TfrmMain.aTipDisciplinskiMerkiExecute(Sender: TObject);
begin
   frmTipDisciplinskiMerki := TfrmTipDisciplinskiMerki.Create(self,false);
   frmTipDisciplinskiMerki.ShowModal;
   frmTipDisciplinskiMerki.Free;
end;

procedure TfrmMain.aUplatExcelExecute(Sender: TObject);
begin
   frmUplatiExcel := TfrmUplatiExcel.Create(self,false);
   frmUplatiExcel.ShowModal;
   frmUplatiExcel.Free;
end;

procedure TfrmMain.aUplatiDolgoviExecute(Sender: TObject);
begin
   frmUplati := TfrmUplati.Create(self,false);
   frmUplati.ShowModal;
   frmUplati.Free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex ����� ��������
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.aZadolzuvanjeExecute(Sender: TObject);
begin
   frmZadolzuvanje := TfrmZadolzuvanje.Create(self,false);
   frmZadolzuvanje.ShowModal;
   frmZadolzuvanje.Free;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);

  //cxSetEureka('danica.stojkova@codex.mk');

  frmMK:=TfrmMK.Create(nil);
  frmMK.ShowModal;
  frmMK.free;
end;

procedure TfrmMain.aResenijaADExecute(Sender: TObject);
begin
  frmResenijaAD := TfrmResenijaAD.Create(self,true);
  frmResenijaAD.ShowModal;
  frmResenijaAD.Free;
end;

procedure TfrmMain.aResenijaAdvokatiExecute(Sender: TObject);
begin
  frmResenijaAdvokati := TfrmResenijaAdvokati.Create(self,true);
  frmResenijaAdvokati.ShowModal;
  frmResenijaAdvokati.Free;
end;

procedure TfrmMain.OtvoriTabeli;
var tip_banka:Integer;
    datum:String;
begin
  //SYS_SETUP
    dm.qSysSetup.ParamByName('p2').Value:='tip_banka';
    dm.qSysSetup.ExecQuery;
    tip_banka:=dm.qSysSetup.FldByName['v1'].Value;

    dm.qSysSetup.Close;
    dm.qSysSetup.ParamByName('p2').Value:='petsedatel_komisija';
    dm.qSysSetup.ExecQuery;
    if not dm.qSysSetup.FldByName['v2'].IsNull then
       petsedatel_komisija:=dm.qSysSetup.FldByName['v2'].Value
    else
       petsedatel_komisija:='';


    datum:=DateToStr(Now);
    godina:=StrToInt(Copy(datum,7,4));
  // ������ �� ���������� dataset-���

  dmMat.tblMesto.Open;

  dm.tblAdvokatskiDrustva.Open;
  dm.tblAdvokatskiKancelarii.Open;
  dm.tblAdvokati_Drustva.Open;
  dm.tblAdvokati_Kancelarii.Open;
  dm.tblTipResenija.Open;
  dm.tblTipResenijaAD.Open;
  dm.tblZaednici.Open;
  dm.tblClenarinaIznos.Open;
  dm.tblTipDisciplinskiMerki.Open;
  dm.tblAdvokati.Open;
  dm.tblPocetnaSostojba.Open;
  dm.tblOsiguritelniKompanii.Open;
  dmMat.tblDrzava.Open;
  dmMat.tblOpstina.Open;

  //dm.tblUplatiOdExcel.Open;
end;

//TODO

end.
