unit UplataStavki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxLabel, cxImageComboBox, dxSkinOffice2013White, cxNavigator, dxCore,
  cxDateUtils, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmUplataStavki = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ADRESA: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MESTONAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MB: TcxGridDBColumn;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPregledFinansiska: TAction;
    aPregledNeplateniSmetki: TAction;
    aPrikaziDolgovi: TAction;
    aUplatiDolg: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aFinansiskaSiteGodini: TAction;
    Panel3: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Panel1: TPanel;
    Panel2: TPanel;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    aBankovIzvod: TAction;
    aDizajnBankovIzvod: TAction;
    cxButton1: TcxButton;
    Label8: TLabel;
    Datum: TcxDateEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_EMBG: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DOLG: TcxGridDBColumn;
    Selekcija: TcxGridDBColumn;
    Label1: TLabel;
    ADV_SIFRA: TcxTextEdit;
    ADV_NAZIV: TcxLookupComboBox;
    Redosled: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure aPregledNeplateniSmetkiExecute(Sender: TObject);
    procedure aPrikaziDolgoviExecute(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure aUplatiDolgExecute(Sender: TObject);
    procedure SelekcijaPropertiesEditValueChanged(Sender: TObject);
    procedure UpdateRedosled(broj : integer);
    procedure aDizajnBankovIzvodExecute(Sender: TObject);
    procedure cxCheckBox1Click(Sender: TObject);
    procedure dxBarLargeButton20Click(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmUplataStavki: TfrmUplataStavki;
  rData : TRepositoryData;
  vk_selekcija:Integer;
implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit, Partner;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmUplataStavki.CheckBox1Click(Sender: TObject);
var i:Integer;
    status: TStatusWindowHandle;
    iznos, OstanatIznos_var, Avans_iznos_var, IznosUplata_var, OstanatNovAvans_var, IZNOS_AVANS_var:double;
begin
//  if (cxGrid1DBTableView1.DataController.RecordCount <> 0) then
//   begin
//   status := cxCreateStatusWindow();
//   iznos:=0;
//   vk_selekcija:=0;
//   try
//    	if CheckBox1.Checked then
//       begin
//          with cxGrid1DBTableView1.DataController do
//          for I := 0 to FilteredRecordCount - 1 do
//            begin
//               Values[FilteredRecordIndex[i] , Selekcija.Index]:=  true;
//               iznos:=iznos + GetValue(FilteredRecordIndex[i],cxGrid1DBTableView1DOLG.Index);
//               txtSelektiranIznos.Text:=FloatToStr(iznos);
//               txtSelektiranIznos.EditValue:=FormatFloat('0.00,.',iznos);
//               vk_selekcija := vk_selekcija + 1;
//               Values[FilteredRecordIndex[i] , Redosled.Index]:=  vk_selekcija;
//            end;
//        end
//      else
//        begin
//          with cxGrid1DBTableView1.DataController do
//          for I := 0 to FilteredRecordCount - 1 do
//            begin
//               Values[FilteredRecordIndex[i] , Selekcija.Index]:= false;
//               Values[FilteredRecordIndex[i] , Redosled.Index] := Variants.Null;
//               iznos:=0;
//               txtSelektiranIznos.EditValue:=FormatFloat('0.00,.',iznos);
//            end;
//     end
//      finally
//	       cxRemoveStatusWindow(status);
//      end;
//   end;
end;

constructor TfrmUplataStavki.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmUplataStavki.aNovExecute(Sender: TObject);
begin
dm.tblUplata.Cancel;
if(dm.tblUplata.State = dsBrowse) then
  begin
//     Panel1.Enabled:=true;
//     Panel2.Enabled:=true;
//     cxGrid1.Enabled:=True;
//     aUplatiDolg.Enabled:=true;
//     dm.tblUplata.Insert;
//     CheckBox1.Checked:=False;
//     dm.tblStavkiZaUplati.Close;
//     dm.tblStavkiZaUplati.ParamByName('advokat_id').Value:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
//     dm.tblStavkiZaUplati.Open;
//     txtSelektiranIznos.EditValue:=FormatFloat('0.00,.',0);
//     vk_selekcija:=0;
  end
else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmUplataStavki.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmUplataStavki.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmUplataStavki.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmUplataStavki.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmUplataStavki.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmUplataStavki.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmUplataStavki.aZacuvajExcelExecute(Sender: TObject);
begin
//  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmUplataStavki.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
//          if kom = IznosUplata then
//             begin
//               if IznosUplata.Text <> '' then
//                  begin
//                    if IznosUplata.EditValue > 0 then
//                       begin
//                         aUplatiDolg.Execute();
//                       end;
//                  end;
//             end;
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
//        VK_SPACE:
//        begin
//          if kom = cxCheckBox1 then
//            if cxCheckBox1.Checked = true then
//               cxCheckBox1.Checked:=false
//            else if cxCheckBox1.Checked = False then
//               cxCheckBox1.Checked:=true
//        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmUplataStavki.cxCheckBox1Click(Sender: TObject);
begin

       {  if cxCheckBox1.Checked = true then
           begin
            CheckBox1.Checked:=False;
            cxGrid1.SetFocus;
           end
         else
            CheckBox1.Checked:=True;        }

end;

procedure TfrmUplataStavki.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmUplataStavki.cxDBTextEditAllExit(Sender: TObject);
var
  kom : TWinControl;
  Avans_iznos_var,IznosUplata_var, SelektiranIznos_var, OstanatIznos_var:Double;
begin
    TEdit(Sender).Color:=clWhite;
    kom := Sender as TWinControl;

  {  if (kom = BankaID) then
       begin
         if BankaID.Text <> '' then BankaNaziv.EditValue:=strToInt(BankaID.Text)
         else BankaNaziv.Text:='';
       end
    else if (kom = BankaNaziv) then
      begin
         if (BankaNaziv.Text <> '') then  BankaID.Text:= bankaNaziv.EditValue
         else  BankaID.Text:=''
      end
    else if ((kom = TIP_PARTNER) or (kom = PARTNER)) then
      begin
         if ((TIP_PARTNER.Text <> '')and (PARTNER.Text<> '')) then
            begin
             ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
             dm.qEMBG_Partner.Close;
             dm.qEMBG_Partner.ParamByName('tip_partner').Value:=TIP_PARTNER.EditValue;
             dm.qEMBG_Partner.ParamByName('partner').Value:=PARTNER.EditValue;
             dm.qEMBG_Partner.ExecQuery;
             if dm.qEMBG_Partner.FldByName['danocen'].Value <> Null then
                EMBG.Text:= dm.qEMBG_Partner.FldByName['danocen'].Value
             else
                EMBG.Text:='';
            end
         else
            begin
              EMBG.Text:='';
              PARTNERNAZIV.Text:='';
            end;
         aPrikaziDolgovi.Execute();
         if not dm.tblUplataStavki.IsEmpty then
            CheckBox1.Checked:=true;
      end
    else if (kom = PARTNERNAZIV) then
      begin
         if PARTNERNAZIV.Text <> '' then
            begin
              dm.tblUplataP.Value:=PARTNERNAZIV.EditValue[1];
              dm.tblUplataTP.Value:=PARTNERNAZIV.EditValue[0];
              if ((TIP_PARTNER.Text <> '')and (PARTNER.Text<> '')) then
                 begin
                   dm.qEMBG_Partner.Close;
                   dm.qEMBG_Partner.ParamByName('tip_partner').Value:=TIP_PARTNER.EditValue;
                   dm.qEMBG_Partner.ParamByName('partner').Value:=PARTNER.EditValue;
                   dm.qEMBG_Partner.ExecQuery;
                   if dm.qEMBG_Partner.FldByName['danocen'].Value <> Null then
                      EMBG.Text:= dm.qEMBG_Partner.FldByName['danocen'].Value
                   else
                      EMBG.Text:='';
                 end;
            end
         else
            begin
              TIP_PARTNER.Text:='';
              PARTNER.Text:='';
              EMBG.Text:='';
            end;
         aPrikaziDolgovi.Execute();
         if not dm.tblUplataStavki.IsEmpty then
            CheckBox1.Checked:=true;
      end
    else if (kom = EMBG) then
      begin
        if EMBG.Text <> '' then
           begin
             dm.qPartnerOdEMBG.Close;
             dm.qPartnerOdEMBG.ParamByName('danocen').Value:=EMBG.EditValue;
             if (TIP_PARTNER.Text <> '') and (PARTNER.Text <> '')then
                begin
                   dm.qPartnerOdEMBG.ParamByName('p').Value:=PARTNER.EditValue;
                   dm.qPartnerOdEMBG.ParamByName('tp').Value:=TIP_PARTNER.EditValue;
                   dm.qPartnerOdEMBG.ParamByName('param').Value:=1;
                end
             else dm.qPartnerOdEMBG.ParamByName('param').Value:=0;
             dm.qPartnerOdEMBG.ExecQuery;
             if (dm.qPartnerOdEMBG.FldByName['id'].Value <> Null) and (dm.qPartnerOdEMBG.FldByName['tip_partner'].Value <> Null) then
                begin
                  dm.tblUplataP.Value:=dm.qPartnerOdEMBG.FldByName['id'].Value;
                  dm.tblUplataTP.Value:=dm.qPartnerOdEMBG.FldByName['tip_partner'].Value;
                  ZemiImeDvoenKluc(TIP_PARTNER,PARTNER, PARTNERNAZIV);
                end;
             aPrikaziDolgovi.Execute();
             if not dm.tblUplataStavki.IsEmpty then
               CheckBox1.Checked:=true;
           end
      end
    else if (kom = IznosUplata) and (IznosUplata.Text<> '') then
      begin
         Avans_iznos_var:=Avans_iznos.EditValue;
         IznosUplata_var:=IznosUplata.EditValue;
         SelektiranIznos_var:=SelektiranIznos.EditValue;
         OstanatIznos_var:= Avans_iznos_var+IznosUplata_var-SelektiranIznos_var;

         OstanatIznos.EditValue:=FormatFloat('0.00 , .',(OstanatIznos_var));
         if dm.tblUplataStavkiIZNOS_AVANS.Value <> Null then // ??
            OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',(OstanatIznos_var - dm.tblUplataStavkiIZNOS_AVANS.Value))
         else
            OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',(OstanatIznos_var));
         if OstanatNovAvans.EditValue < 0 then
            OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',0);
      end
    else if ((kom = CheckBox1) and (Panel1.Enabled = true)) then
         begin
           if CheckBox1.Checked = true then
              IznosUplata.SetFocus
           else
              cxGrid1.SetFocus;
         end;   }
end;

procedure TfrmUplataStavki.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmUplataStavki.SelekcijaPropertiesEditValueChanged(
  Sender: TObject);
var i, momentalen:Integer;
    iznos, avans_v, Avans_iznos_var,OstanatIznos_var,IznosUplata_var, IZNOS_AVANS_var, OstanatNovAvans_var :Double;
begin
//     iznos:=0;
//     if Selekcija.EditValue = true then
//        begin
//            vk_selekcija := vk_selekcija + 1;
//            Redosled.EditValue:=vk_selekcija;
//         end
//     else if (selekcija.EditValue  = false) then
//         begin
//            vk_selekcija := vk_selekcija - 1;
//            momentalen := Redosled.EditValue;
//            Redosled.EditValue:= - 1;
//            UpdateRedosled(momentalen);
//         end;
//     with cxGrid1DBTableView1.DataController do
//     for I := 0 to FilteredRecordCount - 1 do
//         begin
//           if (GetValue(FilteredRecordIndex[i],Selekcija.Index) = true) then
//              iznos:=iznos + GetValue(FilteredRecordIndex[i],cxGrid1DBTableView1DOLG.Index);
//              txtSelektiranIznos.EditValue:=FormatFloat('0.00,.',iznos);
//         end;

end;

procedure TfrmUplataStavki.UpdateRedosled(broj : integer);
var  oznaceno, kraj : bool;
begin
 { // namali gi ostanatite brojki pogolemi od brojot
   cxGrid1DBTableView1.Controller.GoToFirst(true);

    if dm.tblStavkiZaUplati.IsEmpty then kraj := true
    else kraj := false;

    while (kraj = false) do
    begin
      if (Selekcija.EditValue <> null) and (Redosled.EditValue > broj) then
      begin
        Redosled.EditValue := Redosled.EditValue - 1;
      end;
      kraj := cxGrid1DBTableView1.Controller.IsFinish;
      cxGrid1DBTableView1.Controller.GoToNext(true,true);
    end;
// za da go vratime fokusot na kliknatoto
    cxGrid1DBTableView1.Controller.GoToFirst(true);

    if dm.tblStavkiZaUplati.IsEmpty then kraj := true
    else kraj := false;

    while (kraj = false) do
    begin
      if (Redosled.EditValue = -1) then
      begin
        Redosled.EditValue := Variants.Null;
        kraj := true;
      end;
      if kraj = false then
      begin
        kraj := cxGrid1DBTableView1.Controller.IsFinish;
        cxGrid1DBTableView1.Controller.GoToNext(true,true);
      end;
    end;     }
end;

procedure TfrmUplataStavki.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmUplataStavki.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmUplataStavki.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmUplataStavki.prefrli;
begin
end;

procedure TfrmUplataStavki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  DM.tblUplata.Cancel;
  dm.tblUplata.FullRefresh;
  //dm.tblUplataStavki.Close;
  dmRes.FreeRepository(rData);
end;
procedure TfrmUplataStavki.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmUplataStavki.FormShow(Sender: TObject);
begin
 { //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
      sobrano := true;
      dm.tblStavkiZaUplati.Close;
      dm.tblStavkiZaUplati.ParamByName('advokat_id').Value:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
      dm.tblStavkiZaUplati.Open;

      dm.tblUplata.Insert;

      ADV_NAZIV.EditValue:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
      ADV_SIFRA.EditValue:=dm.tblZadolzeniAdvokatiADVOKAT_ID.Value;
      Datum.EditValue:=Now;
      Datum.SetFocus;
      vk_selekcija:=0;
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
   }
end;
//------------------------------------------------------------------------------

procedure TfrmUplataStavki.ZemiImeDvoenKluc(tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin

  if (tip.Text <>'') and (sifra.Text<>'')  then
  begin
      lukap.EditValue := VarArrayOf([ StrToInt(tip.Text), StrToInt(sifra.Text)]);
  end
  else
  begin
      lukap.Clear;
  end;
end;

procedure TfrmUplataStavki.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmUplataStavki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmUplataStavki.dxBarLargeButton20Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmUplataStavki.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmUplataStavki.aOtkaziExecute(Sender: TObject);
begin

    Close();

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmUplataStavki.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmUplataStavki.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
 // dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������������ : ' +PARTNER.Text + ' - ' + PARTNERNAZIV.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmUplataStavki.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmUplataStavki.aPregledNeplateniSmetkiExecute(Sender: TObject);
begin
//
end;

procedure TfrmUplataStavki.aPrikaziDolgoviExecute(Sender: TObject);
var ostanatIznos_var,IznosUplata_var,Avans_iznos_var:Double;
begin
 {   if ((TIP_PARTNER.Text <> '')and (PARTNER.Text<> '') and (PARTNERNAZIV.Text <> '')) then
       begin
        dm.tblUplataStavki.Close;
        dm.tblUplataStavki.ParamByName('partner').Value:=PARTNER.EditValue;
        dm.tblUplataStavki.ParamByName('tip_partner').Value:=TIP_PARTNER.EditValue;
        dm.tblUplataStavki.ParamByName('re').Value:=dmKon.firma_id;
        dm.tblUplataStavki.ParamByName('godina').Value:=dmKon.godina;
        dm.tblUplataStavki.Open;

        CheckBox1.Checked:=false;

        if dm.tblUplataStavki.IsEmpty then
           begin
             Avans_iznos.Visible:=false;
             Label6.Visible:=False;
             SelektiranIznos.EditValue:=FormatFloat('0.00 , .',0);
           end
        else
           begin
             Avans_iznos.Visible:=True;
             Label6.Visible:=True;
           end;

       IznosUplata_var:=IznosUplata.EditValue;

       if dm.tblUplataStavkiIZNOS_AVANS.IsNull then
          begin
            Avans_iznos.EditValue:=FormatFloat('0.00 , .',0);
            OstanatIznos.EditValue:=FormatFloat('0.00 , .',(0+IznosUplata_var));
            ostanatIznos_var:= OstanatIznos.EditValue;
            OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',(ostanatIznos_var - 0));
          end
       else
          begin
             Avans_iznos_var:=Avans_iznos.EditValue;
             IznosUplata_var:=IznosUplata.EditValue;
             ostanatIznos_var:=Avans_iznos_var +IznosUplata_var;
             OstanatIznos.EditValue:=FormatFloat('0.00 , .',(ostanatIznos_var));
             OstanatNovAvans.EditValue:=FormatFloat('0.00 , .',(ostanatIznos_var - dm.tblUplataStavkiIZNOS_AVANS.Value));
          end;
       if OstanatNovAvans.EditValue < 0 then
          OstanatNovAvans.EditValue:= FormatFloat('0.00 , .',0);
       vk_selekcija:=0;
    end;  }
end;

procedure TfrmUplataStavki.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmUplataStavki.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmUplataStavki.aUplatiDolgExecute(Sender: TObject);
var
  id_mnozestvo : AnsiString;
  oznaceno, kraj : bool;
  momentalen : integer;
  OstanatIznos_var,IZNOS_AVANS_var, IznosUplata_var:Double;
begin
//  if (Validacija(Panel2) = False) and (Validacija(Panel1) = False) then // 1
//     begin
//       id_mnozestvo := '';
//       momentalen := 1;
//       while (momentalen <= vk_selekcija) and (vk_selekcija > 0) do
//          begin
//            cxGrid1DBTableView1.Controller.GoToFirst(true);
//
//            if dm.tblStavkiZaUplati.IsEmpty then kraj := true
//            else kraj := false;
//
//            while (kraj = false) do
//              begin
//                if (Redosled.EditValue = momentalen) then
//                   begin
//                     if id_mnozestvo <> '' then
//                        id_mnozestvo := id_mnozestvo + 'x' + dm.tblStavkiZaUplatiID.AsString
//                     else
//                        id_mnozestvo := dm.tblStavkiZaUplatiID.AsString;
//                      momentalen := momentalen + 1;
//                   end;
//                   kraj := cxGrid1DBTableView1.Controller.IsFinish;
//                   cxGrid1DBTableView1.Controller.GoToNext(true,true);
//              end;
//          end;
//
//       IznosUplata_var:=IznosUplata.EditValue;
//        if id_mnozestvo <> '' then //3
//           begin
//             dm.insert6(dm.pPROC_ADV_INSERT_UPLATA,'ID_MNOZESTVO', 'IZNOS_ZA_UPLATA', 'DATUM', 'ADVOKAT_ID', 'GODINA', 'TIP',
//                                                    id_mnozestvo, IznosUplata_var,Datum.EditValue,dm.tblZadolzeniAdvokatiADVOKAT_ID.Value, -1, 0);
//             ShowMessage('�������� � ������� ������������ !!!');
//             Panel1.Enabled:=false;
//             aUplatiDolg.Enabled:=false;
//             Panel2.Enabled:=False;
//             cxGrid1.Enabled:=False;
//             dm.tblZadolzeniAdvokati.FullRefresh;
//             dm.tblUplata.FullRefresh;
//           end
//     end
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmUplataStavki.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmUplataStavki.aDizajnBankovIzvodExecute(Sender: TObject);
begin
     dmRes.Spremi('VZ',30086);
     dmRes.frxReport1.DesignReport();
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmUplataStavki.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmUplataStavki.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmUplataStavki.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
