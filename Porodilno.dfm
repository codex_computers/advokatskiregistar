inherited frmPorodilno: TfrmPorodilno
  Caption = #1055#1086#1088#1086#1076#1080#1083#1085#1086' '#1073#1086#1083#1077#1076#1091#1074#1072#1114#1077
  ClientHeight = 741
  ClientWidth = 928
  ExplicitWidth = 944
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 928
    Height = 352
    ExplicitWidth = 928
    ExplicitHeight = 352
    inherited cxGrid1: TcxGrid
      Width = 924
      Height = 348
      ExplicitWidth = 924
      ExplicitHeight = 348
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        DataController.DataSource = dm.dsPorodilno
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 78
        end
        object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_NAZIV'
          Width = 200
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA'
          Width = 93
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Width = 77
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Width = 80
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Width = 228
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
          Width = 131
        end
        object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_DATUM'
          Width = 250
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Width = 250
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Width = 82
        end
        object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_naziv'
          Width = 72
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
          Visible = False
          Width = 181
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
          Width = 198
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 133
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
          Width = 194
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Width = 123
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Width = 139
        end
        object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN_TELEFON'
          Width = 143
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Width = 124
        end
        object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
          DataBinding.FieldName = 'WEB_STRANA'
          Width = 107
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 174
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 177
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 212
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 243
        end
        object cxGrid1DBTableView1ADVOKAT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_ID'
          Visible = False
          Width = 182
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 250
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 478
    Width = 928
    Height = 240
    ExplicitTop = 495
    ExplicitWidth = 928
    ExplicitHeight = 240
    inherited Label1: TLabel
      Left = 590
      Top = 61
      Visible = False
      ExplicitLeft = 590
      ExplicitTop = 61
    end
    object lbl1: TLabel [1]
      Left = 37
      Top = 24
      Width = 58
      Height = 13
      Caption = #1040#1076#1074#1086#1082#1072#1090' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl2: TLabel [2]
      Left = 62
      Top = 139
      Width = 33
      Height = 13
      Caption = #1054#1087#1080#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 642
      Top = 58
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsPorodilno
      TabOrder = 2
      Visible = False
      ExplicitLeft = 642
      ExplicitTop = 58
    end
    inherited OtkaziButton: TcxButton
      Left = 837
      Top = 200
      TabOrder = 6
      ExplicitLeft = 837
      ExplicitTop = 200
    end
    inherited ZapisiButton: TcxButton
      Left = 756
      Top = 200
      TabOrder = 5
      ExplicitLeft = 756
      ExplicitTop = 200
    end
    object ADVOKAT_ID: TcxDBTextEdit
      Tag = 1
      Left = 101
      Top = 21
      BeepOnEnter = False
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsPorodilno
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 44
    end
    object ADVOKAT_NAZIV: TcxDBLookupComboBox
      Tag = 1
      Left = 144
      Top = 21
      DataBinding.DataField = 'ADVOKAT_ID'
      DataBinding.DataSource = dm.dsPorodilno
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ADVOKAT_NAZIV'
        end>
      Properties.ListSource = dm.dsAdvokati
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 441
    end
    object OPIS: TcxDBMemo
      Left = 101
      Top = 136
      DataBinding.DataField = 'OPIS'
      DataBinding.DataSource = dm.dsPorodilno
      Properties.WantReturns = False
      TabOrder = 4
      OnKeyDown = EnterKakoTab
      Height = 58
      Width = 484
    end
    object cxGroupBoxPeriod: TcxGroupBox
      Left = 13
      Top = 55
      Caption = #1055#1077#1088#1080#1086#1076' '#1085#1072' '#1074#1072#1078#1085#1086#1089#1090
      TabOrder = 3
      Height = 66
      Width = 572
      object lbl3: TLabel
        Left = 342
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1044#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object lbl4: TLabel
        Left = 17
        Top = 31
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' '#1054#1076' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object DATUM_DO: TcxDBDateEdit
        Tag = 1
        Left = 413
        Top = 28
        Hint = #1044#1072#1090#1091#1084
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsPorodilno
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 126
      end
      object DATUM: TcxDBDateEdit
        Tag = 1
        Left = 88
        Top = 28
        Hint = #1044#1072#1090#1091#1084
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsPorodilno
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 124
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 928
    ExplicitWidth = 928
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 928
    ExplicitTop = 735
    ExplicitWidth = 928
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 256
    Top = 120
  end
  inherited PopupMenu1: TPopupMenu
    Top = 104
  end
  inherited dxBarManager1: TdxBarManager
    Left = 648
    Top = 136
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 160
    Top = 144
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 43696.455671886570000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
