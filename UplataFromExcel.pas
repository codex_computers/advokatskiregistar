unit UplataFromExcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinOffice2013White, System.Actions,// dxPScxSSLnk, cxSSheet,

  dxBarBuiltInMenu, cxNavigator, cxImageComboBox, cxLabel, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxRibbonCustomizationForm, dxCore, dxCoreClasses, dxHashUtils,
  dxSpreadSheetCore, dxSpreadSheetCoreHistory,
  dxSpreadSheetConditionalFormatting, dxSpreadSheetConditionalFormattingRules,
  dxSpreadSheetClasses, dxSpreadSheetContainers, dxSpreadSheetFormulas,
  dxSpreadSheetHyperlinks, dxSpreadSheetFunctions, dxSpreadSheetGraphics,
  dxSpreadSheetPrinting, dxSpreadSheetTypes, dxSpreadSheetUtils, dxSpreadSheet,
  dxSpreadSheetFormattedTextUtils;

type
//  niza = Array[1..5] of Variant;

  TfrmUplatiFromExcel = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1BarExcel: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aVcitajUplata: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    OpenFileDialog: TOpenDialog;
    dxBarLargeButton19: TdxBarLargeButton;
    aZacuvajVoBaza: TAction;
    dxRibbon1Tab3: TdxRibbonTab;
    dxBarManager1Bar6: TdxBar;
    pbProgress: TdxBarProgressItem;
    cxPageControl1: TcxPageControl;
    cxTabSheetExcel: TcxTabSheet;
  //  Book: TcxSpreadSheetBook;
    cxTabSheetUplati: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1ZADOLZUVANJE_ID: TcxGridDBColumn;
    cxGrid3DBTableView1GODINA: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKAT_EMBG: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKAT_ID: TcxGridDBColumn;
    cxGrid3DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid3DBTableView1LICENCA_DATUM: TcxGridDBColumn;
    cxGrid3DBTableView1IZNOS: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid3DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid3DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid3DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid3DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid3DBTableView1TIP_RESENIE_ID: TcxGridDBColumn;
    cxGrid3DBTableView1STATUS: TcxGridDBColumn;
    cxGrid3DBTableView1status_naziv: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKO_DRUSTVO_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1MESTO: TcxGridDBColumn;
    cxGrid3DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn;
    cxGrid3DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn;
    cxGrid3DBTableView1ADRESA: TcxGridDBColumn;
    cxGrid3DBTableView1TELEFON: TcxGridDBColumn;
    cxGrid3DBTableView1MOBILEN_TELEFON: TcxGridDBColumn;
    cxGrid3DBTableView1EMAIL: TcxGridDBColumn;
    cxGrid3DBTableView1WEB_STRANA: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    dxBarManager1BarBrisenje: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    aGrupnoBrisenje: TAction;
    PanelGrupnoBrisenje: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxLabel2: TcxLabel;
    cxLabel1: TcxLabel;
    txtOd: TcxTextEdit;
    txtDo: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    aBrisiGrupno: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    aGrupnoBrisenjePoDatum: TAction;
    dxBarDateCombo: TdxBarDateCombo;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarLargeButton21: TdxBarLargeButton;
    cxBarDatum: TcxBarEditItem;
    cxTabSheetNeuspesniUplati: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1LICENCA: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS: TcxGridDBColumn;
    Book: TdxSpreadSheet;
    cxGridPopupMenu2: TcxGridPopupMenu;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aVcitajUplataExecute(Sender: TObject);
    procedure aZacuvajVoBazaExecute(Sender: TObject);
    procedure cxPageControl1PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);
    procedure aGrupnoBrisenjeExecute(Sender: TObject);
    procedure aBrisiGrupnoExecute(Sender: TObject);
    procedure aGrupnoBrisenjePoDatumExecute(Sender: TObject);
    procedure cxGrid3DBTableView1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmUplatiFromExcel: TfrmUplatiFromExcel;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmUplatiFromExcel.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmUplatiFromExcel.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmUplatiFromExcel.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmUplatiFromExcel.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid3DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid3DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid3DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmUplatiFromExcel.aBrisiGrupnoExecute(Sender: TObject);
var
  st: TDataSetState;
  br_zadolzuvanja, br_brisenja, br_uplati:Integer;
begin
      if ((txtOd.Text <> '')and (txtDo.Text<>'')) then
         begin
        //    br_zadolzuvanja:=dm.insert_return5(dm.pPROC_ADV_GRUPNO_BRISENJE,'SIFRA_OD','SIFRA_DO',null, null,Null, txtOd.Text, txtDo.Text,null, null,Null,'BR_ZADOLZUVANJA');
            br_brisenja:=dm.insert_return5(dm.pPROC_ADV_GRUPNO_BRISENJE,'SIFRA_OD','SIFRA_DO','UPLATA_ZADOLZ', 'DATUM','TIP_BRISENJE', txtOd.Text, txtDo.Text,2, null,1,'BR_BRISENJA');
           // br_uplati:=br_zadolzuvanja - br_brisenja;
            if br_brisenja >0 then
               begin
                // if (br_zadolzuvanja = br_brisenja) then
                   ShowMessage('������� ������ �� ������ !!!');
              //   else if (br_brisenja < br_zadolzuvanja) then ShowMessage(' ������� ������ �� '+intToStr(br_brisenja)+' ����������� ���������� '+intToStr(br_uplati)+' ����� ������!!!');
                 dm.tblUplata.FullRefresh;
               end
            else ShowMessage('�� � ��������� ���� ���� ������ !!!');
            PanelGrupnoBrisenje.Visible:=False;
         end
      else ShowMessage('��������� �� ����� ����� �� � ��!!!');
end;

procedure TfrmUplatiFromExcel.aBrisiIzgledExecute(Sender: TObject);
begin
  //brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmUplatiFromExcel.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmUplatiFromExcel.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmUplatiFromExcel.aSnimiIzgledExecute(Sender: TObject);
begin
//  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmUplatiFromExcel.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmUplatiFromExcel.aZacuvajVoBazaExecute(Sender: TObject);
var
    Y: integer;
 //   strani: integer;
 //   procent : integer;
 //   prodolzi : boolean;
begin
   { DM.qryCheckPlan.ParamByName('RE').Value := dmKon.re;
    DM.qryCheckPlan.ParamByName('GODINA').Value := lcbGodina.EditValue;
    DM.qryCheckPlan.Open();

    if(DM.qryCheckPlanCOUNT.Value > 0) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '������ ���� �� ' + string(lcbGodina.EditValue) + ' ������.' , '�� �� �������� ����������� �� �� �� ������ ������ ����?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
            Abort
        else
        begin
            DM.qryDeletePlan.ParamByName('RE').Value := dmKon.re;
            DM.qryDeletePlan.ParamByName('GODINA').Value := lcbGodina.EditValue;
            DM.qryDeletePlan.ExecQuery();
        end;
    end;}



//    dxRibbon1Tab3.Visible := true;
//    pbProgress.Position := 0;
//    dxRibbon1Tab3.Active := true;
//    Application.ProcessMessages;
//    strani := Book.SheetCount;

    if (not dm.tblNeuspesniUplati.IsEmpty) then
        dm.tblNeuspesniUplati.Delete;
   // ShowMessage('1');
   // with Book do
   // begin
  //   ShowMessage('2');
       // Book.ActiveSheet.Index := 0;  // ��� �� ����� ������

        //pbProgress.Position := 10;
        //Application.ProcessMessages;

       // if (Book.ActiveSheet.Index < strani) then prodolzi := true
       //  else prodolzi := false;

       // while (prodolzi = true) do
       // begin
          //procent := 90 div strani;
        //  with ActiveSheet do
       //   begin
            Y := 1;
          //  ShowMessage('3');
           //   ShowMessage('���� '+ TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,0].AsString);

            //while (GetCellObject(0,Y).Text <> '') do
           while (not TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,0].IsEmpty)do
            begin

         //   ShowMessage('4');
              //dm.insert6(dm.pPROC_ADV_INSERTUPLATA_FROMEXCEL,'IZNOS','GODINA', 'LICENCA', 'DATUM', null,null, ActiveSheet.GetCellObject(0,Y).Text, StrToInt(ActiveSheet.GetCellObject(1,Y).Text), ActiveSheet.GetCellObject(2,Y).Text,StrToDate(ActiveSheet.GetCellObject(3,Y).Text), null,null);
              // dm.insert6(dm.pPROC_ADV_INSERT_UPLATA,'LICENCA', 'IZNOS_ZA_UPLATA', 'DATUM', 'GODINA', Null, null,
                                  //                  ActiveSheet.GetCellObject(2,Y).Text, ActiveSheet.GetCellObject(0,Y).Text,StrToDate(ActiveSheet.GetCellObject(3,Y).Text), StrToInt(ActiveSheet.GetCellObject(1,Y).Text), Null, Null);
             //  ShowMessage('Licenca ' +TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,2].AsString);
              // ShowMessage('Iznos ' +TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,0].AsString);
              // ShowMessage('Datum ' + TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,3].AsString);
             //  ShowMessage('Godina ' +TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,1].AsString);
               dm.insert6(dm.pPROC_ADV_INSERT_UPLATA,'LICENCA',                                                    'IZNOS_ZA_UPLATA',                                          'DATUM',                                                                'GODINA',                                                              Null, null,
                                                     TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,2].AsString, TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,0].AsString,StrToDate(TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,3].AsString), StrToInt(TdxSpreadSheetTableView(Book.Sheets[0]).Cells[Y,1].AsString), Null, Null);

              Y := Y + 1;
              //pbProgress.Position := pbProgress.Position + procent;
              //Application.ProcessMessages;
            end;
       //   end;

          //if (ActivePage < strani - 1) then ActivePage := ActivePage + 1 // ��� �� � �������� ������� �� excel-�� ����� ��� �� ��������
        //  else
       //   prodolzi := false;


       // end;
   // end;

   // pbProgress.Position := 100;
    //Application.ProcessMessages;
    ShowMessage('�������� � ������� �������� �� ������ �� ��������.');
    dm.tblUplata.Close;
    dm.tblUplata.ParamByName('advokat_id').Value:=-100;
    dm.tblUplata.ParamByName('godina').Value:=-100;
    dm.tblUplata.Open;
    dm.tblNeuspesniUplati.Close;
    dm.tblNeuspesniUplati.Open;
    cxPageControl1.ActivePage:=cxTabSheetUplati;
    dxRibbon1Tab3.Visible := false;
    dxRibbon1Tab1.Active := true;

    //Book.ActiveSheet := 0;

end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmUplatiFromExcel.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmUplatiFromExcel.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmUplatiFromExcel.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmUplatiFromExcel.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmUplatiFromExcel.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmUplatiFromExcel.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmUplatiFromExcel.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmUplatiFromExcel.prefrli;
begin
end;

procedure TfrmUplatiFromExcel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmUplatiFromExcel.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmUplatiFromExcel.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1BarExcel.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid3DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
   procitajPrintOdBaza(Name,cxGrid3DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;
    dxRibbon1Tab3.Active := False;
    cxPageControl1.ActivePage:=cxTabSheetExcel;

    dm.tblUplata.Close;
    dm.tblUplata.ParamByName('godina').Value:=-100;
    dm.tblUplata.ParamByName('advokat_id').Value:=-100;
    dm.tblUplata.Open;

    dm.tblNeuspesniUplati.Open;
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmUplatiFromExcel.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmUplatiFromExcel.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmUplatiFromExcel.cxGrid3DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid3DBTableView1);
end;

procedure TfrmUplatiFromExcel.cxPageControl1PageChanging(Sender: TObject;
  NewPage: TcxTabSheet; var AllowChange: Boolean);
begin
 if NewPage =  cxTabSheetExcel then
   begin
       dxBarManager1BarBrisenje.Visible:=False;
       dxBarManager1BarExcel.Visible:=true;
   end
 else if NewPage = cxTabSheetUplati then
   begin
       dxBarManager1BarBrisenje.Visible:=True;
       dxBarManager1BarExcel.Visible:=False;

   end
end;

//  ����� �� �����
procedure TfrmUplatiFromExcel.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmUplatiFromExcel.aOtkaziExecute(Sender: TObject);
begin
   if PanelGrupnoBrisenje.Visible = true then
      begin
         PanelGrupnoBrisenje.Visible:=False;
         txtOd.Clear;
         txtDo.Clear;
      end
   else
      begin
         ModalResult := mrCancel;
         Close();
      end;

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmUplatiFromExcel.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmUplatiFromExcel.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmUplatiFromExcel.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmUplatiFromExcel.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmUplatiFromExcel.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmUplatiFromExcel.aVcitajUplataExecute(Sender: TObject);
var
  status: TStatusWindowHandle;
begin
   OpenFileDialog.Title := '����� �� ������ �� ...';
    if(OpenFileDialog.Execute)then
    begin
      status := cxCreateStatusWindow();
      try
        Book.LoadFromFile(OpenFileDialog.FileName);
      finally
        cxRemoveStatusWindow(status);
      end;
    end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmUplatiFromExcel.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmUplatiFromExcel.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmUplatiFromExcel.aGrupnoBrisenjeExecute(Sender: TObject);
begin
  PanelGrupnoBrisenje.Visible:=True;
  txtOd.SetFocus;
end;

procedure TfrmUplatiFromExcel.aGrupnoBrisenjePoDatumExecute(Sender: TObject);
var
  st: TDataSetState;
  br_zadolzuvanja, br_brisenja, br_uplati:Integer;
begin
      if (cxBarDatum.EditValue <> null) then
         begin
            br_brisenja:=dm.insert_return5(dm.pPROC_ADV_GRUPNO_BRISENJE,'SIFRA_OD','SIFRA_DO','UPLATA_ZADOLZ', 'DATUM','TIP_BRISENJE', -1, -1,2, cxBarDatum.EditValue,2,'BR_BRISENJA');
            if br_brisenja >0 then
               begin
                  ShowMessage('������� ������ �� ������ !!!');
                  dm.tblUplata.FullRefresh;
               end
            else ShowMessage('�� � ��������� ���� ���� ������ !!!');
            PanelGrupnoBrisenje.Visible:=False;
         end
      else ShowMessage('��������� �� ������ �� ����� !!!');
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmUplatiFromExcel.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmUplatiFromExcel.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
