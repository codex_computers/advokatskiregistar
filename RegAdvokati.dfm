object frmRegAdvokati: TfrmRegAdvokati
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
  ClientHeight = 741
  ClientWidth = 1189
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel
    Left = 39
    Top = 29
    Width = 46
    Height = 13
    Caption = #1064#1080#1092#1088#1072' :'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1189
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'radioStatus'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          Caption = '        '
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          Caption = #1055#1088#1077#1075#1083#1077#1076#1080
          ToolbarName = 'dxBarManager1Bar9'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 718
    Width = 1189
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 1189
    Height = 245
    Align = alTop
    Enabled = False
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGroupBoxPodatociKontakt: TcxGroupBox
      Left = 877
      Top = 6
      Caption = #1051#1080#1095#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      TabOrder = 6
      DesignSize = (
        309
        187)
      Height = 187
      Width = 309
      object Label16: TLabel
        Left = 43
        Top = 16
        Width = 59
        Height = 26
        Alignment = taRightJustify
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077':'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label20: TLabel
        Left = 16
        Top = 52
        Width = 87
        Height = 13
        Caption = #1052#1086#1073'. '#1090#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 46
        Top = 75
        Width = 57
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 21
        Top = 98
        Width = 82
        Height = 13
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 28
        Top = 121
        Width = 75
        Height = 13
        Caption = 'Web '#1089#1090#1088#1072#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MESTO_LK_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 151
        Top = 20
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'MESTO_LK'
        DataBinding.DataSource = dm.dsAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'OpstinaNaziv'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 147
      end
      object MESTO_LK: TcxDBTextEdit
        Tag = 1
        Left = 108
        Top = 20
        Hint = #1052#1077#1089#1090#1086' '#1085#1072' '#1078#1080#1074#1077#1077#1114#1077' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO_LK'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 44
      end
      object MOBILEN_TELEFON_LK: TcxDBTextEdit
        Left = 109
        Top = 49
        Hint = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN_TELEFON_LK'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object TELEFON_LK: TcxDBTextEdit
        Left = 109
        Top = 72
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'TELEFON_LK'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object EMAIL_LK: TcxDBTextEdit
        Left = 109
        Top = 95
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL_LK'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object WEB_STRANA_LK: TcxDBTextEdit
        Left = 109
        Top = 118
        Hint = #1042#1077#1073' '#1089#1090#1088#1072#1085#1072' '
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'WEB_STRANA_LK'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.OnChange = WEB_STRANAPropertiesChange
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cxButton3: TcxButton
        Left = 119
        Top = 152
        Width = 162
        Height = 25
        Action = aCopyAK
        Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 6
        TabStop = False
      end
    end
    object cxGroupBoxOsnovniPodatoci: TcxGroupBox
      Left = 1
      Top = 107
      Caption = #1054#1089#1085#1086#1074#1085#1080' '#1087#1086#1076#1072#1090#1086#1094#1080
      TabOrder = 1
      DesignSize = (
        227
        132)
      Height = 132
      Width = 227
      object Label5: TLabel
        Left = 12
        Top = 84
        Width = 55
        Height = 13
        Caption = #1055#1088#1077#1079#1080#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 38
        Top = 57
        Width = 29
        Height = 13
        Caption = #1048#1084#1077' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 32
        Top = 30
        Width = 35
        Height = 13
        Caption = #1045#1052#1041#1043' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object IME: TcxDBTextEdit
        Tag = 1
        Left = 73
        Top = 54
        Hint = #1048#1084#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'IME'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 144
      end
      object PREZIME: TcxDBTextEdit
        Tag = 1
        Left = 73
        Top = 81
        Hint = #1055#1088#1077#1079#1080#1084#1077' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'PREZIME'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.CharCase = ecUpperCase
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 144
      end
      object EMBG: TcxDBTextEdit
        Left = 73
        Top = 27
        Hint = #1045#1052#1041#1043' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1086#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'EMBG'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.OnValidate = EMBGPropertiesValidate
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 144
      end
    end
    object cxGroupBoxLicenca: TcxGroupBox
      Left = 2
      Top = 6
      Caption = #1051#1080#1094#1077#1085#1094#1072
      TabOrder = 0
      DesignSize = (
        226
        93)
      Height = 93
      Width = 226
      object Label3: TLabel
        Left = 21
        Top = 57
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label2: TLabel
        Left = 35
        Top = 30
        Width = 31
        Height = 13
        Caption = #1041#1088#1086#1112' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LICENCA_DATUM: TcxDBDateEdit
        Tag = 1
        Left = 72
        Top = 54
        Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1080#1079#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'LICENCA_DATUM'
        DataBinding.DataSource = dm.dsAdvokati
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 144
      end
      object LICENCA: TcxDBTextEdit
        Tag = 1
        Left = 72
        Top = 27
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1094#1077#1085#1094#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'LICENCA'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 144
      end
    end
    object ZapisiButton: TcxButton
      Left = 1020
      Top = 212
      Width = 75
      Height = 25
      Action = aZapisi
      TabOrder = 7
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButtonOtkazi: TcxButton
      Left = 1101
      Top = 212
      Width = 75
      Height = 25
      Action = aOtkazi
      TabOrder = 8
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object VrabotenVo: TcxGroupBox
      Left = 512
      Top = 173
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086
      StyleDisabled.BorderStyle = ebsUltraFlat
      TabOrder = 5
      DesignSize = (
        359
        64)
      Height = 64
      Width = 359
      object Label14: TLabel
        Left = 8
        Top = 22
        Width = 80
        Height = 31
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object ADVOKATSKO_DRUSTVO_NAZIV: TcxDBLookupComboBox
        Left = 94
        Top = 27
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ADVOKATSKO_DRUSTVO'
        DataBinding.DataSource = dm.dsAdvokati
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsAdvokatskiDrustva
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 229
      end
      object cxButton1: TcxButton
        Left = 327
        Top = 25
        Width = 25
        Height = 25
        Action = aCopyAD
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 1
        TabStop = False
      end
    end
    object VrabotenVoAK: TcxGroupBox
      Left = 512
      Top = 103
      Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086
      TabOrder = 4
      DesignSize = (
        359
        64)
      Height = 64
      Width = 359
      object Label13: TLabel
        Left = 8
        Top = 22
        Width = 80
        Height = 31
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object ADVOKATSKA_KANCELARIJA_NAZIV: TcxDBLookupComboBox
        Left = 94
        Top = 27
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' - '#1085#1072#1079#1080#1074
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'ADVOKATSKA_KANCELARIJA'
        DataBinding.DataSource = dm.dsAdvokati
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsAdvokatskiKancelarii
        Properties.OnEditValueChanged = aPopolniPodatociAK
        StyleDisabled.TextColor = clBackground
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 229
      end
      object cxButton2: TcxButton
        Left = 325
        Top = 25
        Width = 25
        Height = 25
        Action = aKreirajAK
        OptionsImage.Images = dmRes.cxSmallImages
        TabOrder = 1
        TabStop = False
        Visible = False
      end
    end
    object cxRadioGroupStatus: TcxDBRadioGroup
      Left = 512
      Top = 6
      TabStop = False
      Caption = #1057#1090#1072#1090#1091#1089
      DataBinding.DataField = 'STATUS'
      DataBinding.DataSource = dm.dsAdvokati
      ParentColor = False
      Properties.Columns = 2
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1057#1072#1084#1086#1089#1090#1086#1077#1085' '#1072#1076#1074#1086#1082#1072#1090
          Value = 1
        end
        item
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1040'. '#1050#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
          Value = 4
        end
        item
          Caption = #1054#1089#1085#1086#1074#1072#1095' '#1085#1072' '#1040'. '#1044#1088#1091#1096#1090#1074#1086
          Value = 2
        end
        item
          Caption = #1042#1088#1072#1073#1086#1090#1077#1085' '#1074#1086' '#1040'. '#1044#1088#1091#1096#1090#1074#1086
          Value = 3
        end>
      Properties.OnEditValueChanged = cxRadioGroupStatusPropertiesEditValueChanged
      Style.Color = clBtnFace
      Style.LookAndFeel.Kind = lfFlat
      Style.LookAndFeel.NativeStyle = True
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleDisabled.LookAndFeel.SkinName = ''
      TabOrder = 3
      Transparent = True
      Height = 91
      Width = 359
    end
    object cxGroupBoxAK: TcxGroupBox
      Left = 234
      Top = 6
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1072#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      TabOrder = 2
      DesignSize = (
        272
        232)
      Height = 232
      Width = 272
      object Label18: TLabel
        Left = 23
        Top = 23
        Width = 80
        Height = 26
        Alignment = taRightJustify
        Caption = #1052#1077#1089#1090#1086' '#1085#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Label10: TLabel
        Left = 53
        Top = 90
        Width = 50
        Height = 13
        Caption = #1040#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 16
        Top = 119
        Width = 87
        Height = 13
        Caption = #1052#1086#1073'. '#1090#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 46
        Top = 146
        Width = 57
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 21
        Top = 173
        Width = 82
        Height = 13
        Caption = 'eMail '#1072#1076#1088#1077#1089#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 28
        Top = 200
        Width = 75
        Height = 13
        Caption = 'Web '#1089#1090#1088#1072#1085#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbl1: TLabel
        Left = 30
        Top = 49
        Width = 75
        Height = 26
        Alignment = taRightJustify
        Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object MESTO: TcxDBTextEdit
        Tag = 1
        Left = 109
        Top = 27
        Hint = #1052#1077#1089#1090#1086' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 28
      end
      object MESTO_NAZIV: TcxDBLookupComboBox
        Tag = 1
        Left = 136
        Top = 27
        Hint = #1052#1077#1089#1090#1086
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataBinding.DataField = 'MESTO'
        DataBinding.DataSource = dm.dsAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end
          item
            FieldName = 'OpstinaNaziv'
          end>
        Properties.ListSource = dmMat.dsMesto
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object ADRESA: TcxDBTextEdit
        Left = 109
        Top = 87
        Hint = #1040#1076#1088#1077#1089#1072
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'ADRESA'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 148
      end
      object MOBILEN_TELEFON: TcxDBTextEdit
        Left = 109
        Top = 116
        Hint = #1052#1086#1073#1080#1083#1077#1085' '#1090#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'MOBILEN_TELEFON'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 148
      end
      object TELEFON: TcxDBTextEdit
        Left = 109
        Top = 143
        Hint = #1058#1077#1083#1077#1092#1086#1085' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'TELEFON'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 148
      end
      object EMAIL: TcxDBTextEdit
        Left = 109
        Top = 170
        Hint = 'eMail '#1072#1076#1088#1077#1089#1072' '#1079#1072' '#1082#1086#1085#1090#1072#1082#1090
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'EMAIL'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 148
      end
      object WEB_STRANA: TcxDBTextEdit
        Left = 109
        Top = 197
        Hint = #1042#1077#1073' '#1089#1090#1088#1072#1085#1072' '
        Anchors = [akLeft, akTop, akRight, akBottom]
        BeepOnEnter = False
        DataBinding.DataField = 'WEB_STRANA'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Properties.OnChange = WEB_STRANAPropertiesChange
        Style.Shadow = False
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 148
      end
      object ADVOKATSKA_ZAEDNICA: TcxDBTextEdit
        Left = 111
        Top = 54
        Hint = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1079#1072#1077#1076#1085#1080#1094#1072' - '#1096#1080#1092#1088#1072
        BeepOnEnter = False
        DataBinding.DataField = 'ADVOKATSKA_ZAEDNICA'
        DataBinding.DataSource = dm.dsAdvokati
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 28
      end
      object ADVOKATSKA_ZAEDNICA_NAZIV: TcxDBLookupComboBox
        Left = 136
        Top = 54
        DataBinding.DataField = 'ADVOKATSKA_ZAEDNICA'
        DataBinding.DataSource = dm.dsAdvokati
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsZaednici
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
    end
    object cxButton4: TcxButton
      Left = 923
      Top = 214
      Width = 25
      Height = 25
      Action = aBrisiAK
      OptionsImage.Images = dmRes.cxSmallImages
      TabOrder = 9
      TabStop = False
      Visible = False
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 371
    Width = 1189
    Height = 347
    Align = alClient
    Caption = 'lPanel'
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1187
      Height = 345
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.InfoText = #1042#1085#1077#1089#1077#1090#1077' '#1090#1077#1082#1089#1090' '#1079#1072' '#1087#1088#1077#1073#1072#1088#1091#1074#1072#1114#1077
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsAdvokati
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            Column = cxGrid1DBTableView1TIP_RESENIE_ID
          end>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.NoDataToDisplayInfoText = #1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1087#1088#1080#1082#1072#1078#1091#1074#1072#1114#1077
        OptionsView.Footer = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1RESENIE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'RESENIE_ID'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1TIP_RESENIE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_RESENIE_ID'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1040#1082#1090#1080#1074#1077#1085
              ImageIndex = 2
              Value = 1
            end
            item
              Description = #1052#1080#1088#1091#1074#1072#1114#1077
              ImageIndex = 4
              Value = 2
            end
            item
              Description = #1048#1079#1073#1088#1080#1096#1072#1085
              ImageIndex = 3
              Value = 3
            end
            item
              Description = #1053#1077#1084#1072' '#1072#1082#1090#1080#1074#1085#1086' '#1088#1077#1096#1077#1085#1080#1077
              ImageIndex = 14
              Value = 100
            end>
          Options.Editing = False
          Width = 105
        end
        object cxGrid1DBTableView1EMBG: TcxGridDBColumn
          DataBinding.FieldName = 'EMBG'
          Options.Editing = False
          Width = 96
        end
        object cxGrid1DBTableView1EMBS: TcxGridDBColumn
          DataBinding.FieldName = 'EMBS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1IME: TcxGridDBColumn
          DataBinding.FieldName = 'IME'
          Visible = False
          Options.Editing = False
          Width = 119
        end
        object cxGrid1DBTableView1PREZIME: TcxGridDBColumn
          DataBinding.FieldName = 'PREZIME'
          Visible = False
          Options.Editing = False
          Width = 90
        end
        object cxGrid1DBTableView1ADVOKAT_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKAT_NAZIV'
          Options.Editing = False
          Width = 201
        end
        object cxGrid1DBTableView1LICENCA: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA'
          Options.Editing = False
          Width = 68
        end
        object cxGrid1DBTableView1LICENCA_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'LICENCA_DATUM'
          Options.Editing = False
          Width = 106
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
          Options.Editing = False
          Width = 91
        end
        object cxGrid1DBTableView1status_naziv: TcxGridDBColumn
          DataBinding.FieldName = 'status_naziv'
          Options.Editing = False
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO'
          Visible = False
          Options.Editing = False
          Width = 162
        end
        object cxGrid1DBTableView1ADVOKATSKO_DRUSTVO_FIRMA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKO_DRUSTVO_NAZIV'
          Options.Editing = False
          Width = 250
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA'
          Visible = False
          Options.Editing = False
          Width = 182
        end
        object cxGrid1DBTableView1ADVOKATSKA_KANCELARIJA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_KANCELARIJA_NAZIV'
          Options.Editing = False
          Width = 222
        end
        object cxGrid1DBTableView1MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO'
          Visible = False
          Options.Editing = False
          Width = 147
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Options.Editing = False
          Width = 115
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA'
          Visible = False
          Options.Editing = False
          Width = 170
        end
        object cxGrid1DBTableView1ADVOKATSKA_ZAEDNICA_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ADVOKATSKA_ZAEDNICA_NAZIV'
          Options.Editing = False
          Width = 185
        end
        object cxGrid1DBTableView1ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'ADRESA'
          Options.Editing = False
          Width = 110
        end
        object cxGrid1DBTableView1TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'TELEFON'
          Options.Editing = False
          Width = 113
        end
        object cxGrid1DBTableView1MOBILEN_TELEFON: TcxGridDBColumn
          DataBinding.FieldName = 'MOBILEN_TELEFON'
          Options.Editing = False
          Width = 136
        end
        object cxGrid1DBTableView1EMAIL: TcxGridDBColumn
          DataBinding.FieldName = 'EMAIL'
          Options.Editing = False
          Width = 97
        end
        object cxGrid1DBTableView1WEB_STRANA: TcxGridDBColumn
          DataBinding.FieldName = 'WEB_STRANA'
          Options.Editing = False
          Width = 201
        end
        object cxGrid1DBTableView1BR_DISCIPLINSKI: TcxGridDBColumn
          DataBinding.FieldName = 'BR_DISCIPLINSKI'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Images = dmRes.cxImageGrid
          Properties.Items = <
            item
              Description = #1053#1077#1084#1072
              ImageIndex = 15
              Value = 0
            end
            item
              Description = #1048#1084#1072' '
              ImageIndex = 7
              Value = 1
            end>
          Width = 117
        end
        object cxGrid1DBTableView1FLAG_TOCEN: TcxGridDBColumn
          DataBinding.FieldName = 'FLAG_TOCEN'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Properties.OnChange = cxGrid1DBTableView1FLAG_TOCENPropertiesChange
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Options.Editing = False
          Width = 64
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Options.Editing = False
          Width = 64
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Options.Editing = False
          Width = 64
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Options.Editing = False
          Width = 169
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Options.Editing = False
          Width = 176
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Options.Editing = False
          Width = 207
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Options.Editing = False
          Width = 230
        end
        object cxGrid1DBTableView1IMPORT_ID: TcxGridDBColumn
          DataBinding.FieldName = 'IMPORT_ID'
          Visible = False
          Options.Editing = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 848
    Top = 528
  end
  object PopupMenu1: TPopupMenu
    Left = 824
    Top = 648
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 736
    Top = 504
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 975
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 1021
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object radioStatus: TdxBar
      Caption = #1055#1077#1073#1072#1088#1072#1112
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 800
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'radioGroupStatus'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      CaptionButtons = <>
      DockedLeft = 261
      DockedTop = 0
      FloatLeft = 1153
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = '    '
      CaptionButtons = <>
      DockedLeft = 152
      DockedTop = 0
      FloatLeft = 1100
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = '      '
      CaptionButtons = <>
      DockedLeft = 589
      DockedTop = 0
      FloatLeft = 1274
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 420
      DockedTop = 0
      FloatLeft = 1274
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar9: TdxBar
      Caption = 'Custom 3'
      CaptionButtons = <>
      DockedLeft = 885
      DockedTop = 0
      FloatLeft = 1223
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object radioGroupStatus: TcxBarEditItem
      Caption = #1040#1076#1074#1086#1082#1090#1080
      Category = 0
      Hint = #1040#1076#1074#1086#1082#1090#1080
      Visible = ivAlways
      OnChange = radioGroupStatusChange
      ImageIndex = 32
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.ImmediatePost = True
      Properties.Items = <
        item
          Caption = #1040#1082#1090#1080#1074#1085#1080
          Value = 1
        end
        item
          Caption = #1042#1086' '#1084#1080#1088#1091#1074#1072#1114#1077
          Value = 2
        end
        item
          Caption = #1048#1079#1073#1088#1080#1096#1072#1085#1080
          Value = 3
        end
        item
          Caption = #1057#1080#1090#1077
          Value = -1
        end>
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxRadioGroupProperties'
      Properties.Items = <>
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPripravnici
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aStrucniSorabotnici
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aResIzbranAdvokat
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1080' '#1087#1077#1095#1072#1090#1077#1114#1077' '#1085#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 19
      Images = dmRes.cxLargeImages
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end>
    end
    object dxBarButton1: TdxBarButton
      Action = aPotvrdaZaZapisVoRegistar
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aDisciplinskiMerki
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aAdvokatskaKancelarija
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aAdvokatskoDrustvo
      Category = 0
      Visible = ivNever
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = actIzvestajZaClenarina
      Category = 0
    end
    object dxbrlrgbtn2: TdxBarLargeButton
      Action = actPorodilno
      Category = 0
    end
    object dxbrlrgbtn3: TdxBarLargeButton
      Action = actOsiguruvanje
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 616
    Top = 520
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPripravnici: TAction
      Caption = #1055#1088#1080#1087#1088#1072#1074#1085#1080#1094#1080
      ImageIndex = 44
      OnExecute = aPripravniciExecute
    end
    object aStrucniSorabotnici: TAction
      Caption = #1057#1090#1088#1091#1095#1085#1080' '#1089#1086#1088#1072#1073#1086#1090#1085#1080#1094#1080
      ImageIndex = 44
      OnExecute = aStrucniSorabotniciExecute
    end
    object aResIzbranAdvokat: TAction
      Caption = #1056#1077#1096#1077#1085#1080#1112#1072' '#1079#1072' '#1080#1079#1073#1088#1072#1085' '#1072#1076#1074#1086#1082#1072#1090
      ImageIndex = 79
      OnExecute = aResIzbranAdvokatExecute
    end
    object aCopyAD: TAction
      Hint = 
        #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1079#1072' '#1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086' ('#1052#1077#1089#1090#1086', '#1040#1076#1088#1077#1089#1072', '#1058#1077#1083#1077#1092#1086#1085', ' +
        'Email)'
      ImageIndex = 8
      OnExecute = aCopyADExecute
    end
    object aNovaAdvokatskaKancelarija: TAction
      Hint = #1053#1086#1074#1072' '#1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      ImageIndex = 104
      OnExecute = aNovaAdvokatskaKancelarijaExecute
    end
    object aPotvrdaZaZapisVoRegistar: TAction
      Caption = #1055#1086#1090#1074#1088#1076#1072' '#1079#1072' '#1079#1072#1087#1080#1089' '#1074#1086' '#1088#1077#1075#1080#1089#1090#1072#1088' '#1085#1072' '#1072#1076#1074#1086#1082#1072#1090#1080
      OnExecute = aPotvrdaZaZapisVoRegistarExecute
    end
    object aKreirajAK: TAction
      Hint = #1050#1088#1077#1080#1088#1072#1112' '#1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1050#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      ImageIndex = 76
      OnExecute = aKreirajAKExecute
    end
    object aDisciplinskiMerki: TAction
      Caption = #1044#1080#1089#1094#1080#1087#1083#1080#1085#1089#1082#1080'  '#1052#1077#1088#1082#1080
      ImageIndex = 77
      OnExecute = aDisciplinskiMerkiExecute
    end
    object aAdvokatskaKancelarija: TAction
      Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      ImageIndex = 75
      OnExecute = aAdvokatskaKancelarijaExecute
    end
    object aAdvokatskoDrustvo: TAction
      Caption = #1040#1076#1074#1086#1082#1072#1090#1089#1082#1086' '#1076#1088#1091#1096#1090#1074#1086
      ImageIndex = 81
      OnExecute = aAdvokatskoDrustvoExecute
    end
    object aCopyAK: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1086#1076' '#1040#1050'/'#1040#1044'('#1052#1077#1089#1090#1086', '#1040#1076#1088#1077#1089#1072', '#1058#1077#1083#1077#1092#1086#1085', Email)'
      Hint = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1076#1072#1090#1086#1094#1080' '#1086#1076' '#1040#1050' '#1080#1083#1080' '#1040#1044' ('#1052#1077#1089#1090#1086', '#1040#1076#1088#1077#1089#1072', '#1058#1077#1083#1077#1092#1086#1085', Email)'
      ImageIndex = 8
      OnExecute = aCopyAKExecute
    end
    object aBrisiAK: TAction
      Hint = #1041#1088#1080#1096#1080' '#1040#1076#1074#1086#1082#1072#1090#1089#1082#1072' '#1082#1072#1085#1094#1077#1083#1072#1088#1080#1112#1072
      ImageIndex = 77
      OnExecute = aBrisiAKExecute
    end
    object actIzvestajZaClenarina: TAction
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112' '#1079#1072' '#1095#1083#1077#1085#1072#1088#1080#1085#1072
      ImageIndex = 83
      OnExecute = actIzvestajZaClenarinaExecute
    end
    object actDIzvestajZaClenarina: TAction
      Caption = 'actDIzvestajZaClenarina'
      ShortCut = 24697
      OnExecute = actDIzvestajZaClenarinaExecute
    end
    object actPorodilno: TAction
      Caption = #1055#1086#1088#1086#1076#1080#1083#1085#1086' '#1073#1086#1083#1077#1076#1091#1074#1072#1114#1077
      ImageIndex = 8
      OnExecute = actPorodilnoExecute
    end
    object actOsiguruvanje: TAction
      Caption = #1054#1089#1080#1075#1091#1088#1091#1074#1072#1114#1077
      ImageIndex = 3
      OnExecute = actOsiguruvanjeExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 336
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 336
    Top = 496
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 656
    Top = 582
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dmMat.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Width = 44
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Width = 49
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        SortIndex = 0
        SortOrder = soAscending
        Width = 345
      end
    end
  end
  object pm1: TPopupMenu
    Left = 448
    Top = 555
  end
end
